package mfs.robot.shared.erepublik;

import java.io.Serializable;
import java.util.Collection;

import mfs.robot.shared.erepublik.data.CountryPresident;

import com.google.gwt.user.client.rpc.IsSerializable;

public class ExpensesInitialInfo implements IsSerializable, Serializable {

	private static final long			serialVersionUID	= 1L;

	public Integer						eRepDay;

	public Collection<CountryPresident>	presidents;

}
