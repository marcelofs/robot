package mfs.robot.shared.erepublik;

import mfs.robot.shared.erepublik.data.ExpenseType;

public interface ITransactionWithCategory extends ITransaction {

	public ExpenseType getCategory();

	public void setCategory(ExpenseType category);

}
