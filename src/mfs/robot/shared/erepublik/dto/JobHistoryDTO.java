package mfs.robot.shared.erepublik.dto;

import java.util.Collection;

import mfs.robot.shared.erepublik.data.AgregatedJobMarketOffer;
import mfs.robot.shared.erepublik.data.CountryTaxes;

import com.google.gwt.user.client.rpc.IsSerializable;

public class JobHistoryDTO implements IsSerializable {

	public Collection<AgregatedJobMarketOffer>	jobs;

	public Collection<CountryTaxes>				taxes;
}
