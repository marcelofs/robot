package mfs.robot.shared.erepublik.dto;

import java.util.Collection;

import com.google.common.collect.Multimap;
import com.google.gwt.user.client.rpc.IsSerializable;

import mfs.robot.shared.erepublik.data.AgregatedMarketplaceOffer;
import mfs.robot.shared.erepublik.data.AgregatedMonetaryOffer;
import mfs.robot.shared.erepublik.data.ProductQuality;

public class RocketHistoryPriceDTO implements IsSerializable {

	public Multimap<ProductQuality, AgregatedMarketplaceOffer>	prices;

	public Collection<AgregatedMonetaryOffer>					goldValue;

}
