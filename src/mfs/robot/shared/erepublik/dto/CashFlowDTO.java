package mfs.robot.shared.erepublik.dto;

import java.util.Collection;

import mfs.robot.shared.erepublik.data.CitizenAccount;
import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.CountryIncome;
import mfs.robot.shared.erepublik.data.CountryMoneyTransfer;
import mfs.robot.shared.erepublik.data.CountryTreasury;

import com.google.gwt.user.client.rpc.IsSerializable;

public class CashFlowDTO implements IsSerializable {

	public Integer							eRepDay;

	public Collection<CountryIncome>		incomes;

	public Collection<CountryMoneyTransfer>	transfers;

	public Collection<CountryExpense>		expenses;

	public Collection<CountryTreasury>		treasuries;

	public Collection<CitizenAccount>		countryOrg;

}
