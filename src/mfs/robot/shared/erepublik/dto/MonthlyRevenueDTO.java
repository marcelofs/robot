package mfs.robot.shared.erepublik.dto;

import java.util.Collection;

import mfs.robot.shared.erepublik.data.CountryPresident;
import mfs.robot.shared.erepublik.data.CountryTreasury;

import com.google.gwt.user.client.rpc.IsSerializable;

public class MonthlyRevenueDTO implements IsSerializable {

	public Collection<CountryPresident>	presidents;

	public Collection<CountryTreasury>	taxRevenues;

}
