package mfs.robot.shared.erepublik.dto;

import java.io.Serializable;

import mfs.robot.shared.erepublik.data.ExpenseType;

import com.google.gwt.user.client.rpc.IsSerializable;

public class CountryExpenseSum implements IsSerializable, Serializable {

	public ExpenseType	category;

	public Double		value;

}
