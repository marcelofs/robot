package mfs.robot.shared.erepublik.dto;

import java.util.Collection;

import mfs.robot.shared.erepublik.data.MarketplaceOffer;
import mfs.robot.shared.erepublik.data.MonetaryOffer;
import mfs.robot.shared.erepublik.data.ProductQuality;

import com.google.common.collect.Multimap;
import com.google.gwt.user.client.rpc.IsSerializable;

public class RocketDailyPriceDTO implements IsSerializable {

	public Integer										day;

	public Multimap<ProductQuality, MarketplaceOffer>	prices;

	public Collection<MonetaryOffer>					goldValue;

}
