package mfs.robot.shared.erepublik.dto;

import java.util.Collection;

import mfs.robot.shared.erepublik.data.CountryIncome;
import mfs.robot.shared.erepublik.data.CountryTreasury;

import com.google.common.collect.Multimap;
import com.google.gwt.user.client.rpc.IsSerializable;

public class BalanceDTO implements IsSerializable {

	public Multimap<Integer, CountryExpenseSum>	expenses;

	public Collection<CountryTreasury>			taxRevenues;

	public Collection<CountryIncome>	incomes;

}
