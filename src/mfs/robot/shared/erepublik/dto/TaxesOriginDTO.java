package mfs.robot.shared.erepublik.dto;

import java.util.Collection;

import mfs.robot.shared.erepublik.data.AgregatedJobMarketOffer;
import mfs.robot.shared.erepublik.data.CountryStats;
import mfs.robot.shared.erepublik.data.CountryTaxes;
import mfs.robot.shared.erepublik.data.CountryTreasury;

import com.google.gwt.user.client.rpc.IsSerializable;

public class TaxesOriginDTO implements IsSerializable {

	public Integer								eRepDay;

	public Collection<CountryTreasury>			treasuries;

	public Collection<AgregatedJobMarketOffer>	jobMarkets;

	public Collection<CountryStats>				citizens;

	public Collection<CountryTaxes>				taxes;

}
