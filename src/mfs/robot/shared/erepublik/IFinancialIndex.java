package mfs.robot.shared.erepublik;

import com.google.gwt.user.client.rpc.IsSerializable;

public interface IFinancialIndex extends IsSerializable {

	public Double getIndexValue();

	public Integer geteRepDay();

}
