package mfs.robot.shared.erepublik;

public interface IHistorySeries {

	public Double getMinimum();

	public Double getMaximum();

	public Double getAverage();

	public Integer geteRepDay();
 
}
