package mfs.robot.shared.erepublik;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class InitialInfo implements IsSerializable, Serializable {

	private static final long	serialVersionUID	= 1L;

	public String				eRepName;

	public boolean				isMDF;

	public boolean				isAdmin;

	public Integer				eRepDay;

}
 