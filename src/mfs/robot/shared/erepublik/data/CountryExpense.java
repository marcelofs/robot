package mfs.robot.shared.erepublik.data;

import java.io.Serializable;
import java.util.Date;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import mfs.robot.shared.erepublik.ITransactionWithCategory;

import com.google.gwt.user.client.rpc.IsSerializable;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class CountryExpense implements Serializable, IsSerializable,
		ITransactionWithCategory {

	private static final long	serialVersionUID	= 1L;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long				id;

	@Persistent
	private Integer				eRepDay;

	@Persistent
	private ExpenseType			category;

	@Persistent
	private Double				value;

	@Persistent
	private MonetaryType		type;

	@Persistent
	private String				origin;

	@Persistent
	private String				personResponsible;

	@Persistent
	private String				destination;

	@Persistent
	private String				observations;

	// eRepUser
	@Persistent(defaultFetchGroup = "false")
	private String				recordedById;

	@Persistent
	private Date				recordedOn;

	@Persistent(defaultFetchGroup = "false")
	private Date				deletedOn;

	// eRepUser
	@Persistent(defaultFetchGroup = "false")
	private String				deletedById;

	@Override
	public Integer geteRepDay() {
		return eRepDay;
	}

	@Override
	public void seteRepDay(Integer eRepDay) {
		this.eRepDay = eRepDay;
	}

	@Override
	public ExpenseType getCategory() {
		return category;
	}

	@Override
	public void setCategory(ExpenseType category) {
		this.category = category;
	}

	@Override
	public Double getValue() {
		return value;
	}

	@Override
	public void setValue(Double value) {
		this.value = value;
	}

	@Override
	public MonetaryType getType() {
		return type;
	}

	@Override
	public void setType(MonetaryType type) {
		this.type = type;
	}

	@Override
	public String getPersonResponsible() {
		return personResponsible;
	}

	@Override
	public void setPersonResponsible(String personResponsible) {
		this.personResponsible = personResponsible;
	}

	@Override
	public String getDestination() {
		return destination;
	}

	@Override
	public void setDestination(String destination) {
		this.destination = destination;
	}

	@Override
	public String getObservations() {
		return observations;
	}

	@Override
	public void setObservations(String observations) {
		this.observations = observations;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public String getRecordedById() {
		return recordedById;
	}

	@Override
	public void setRecordedById(String recordedById) {
		this.recordedById = recordedById;
	}

	@Override
	public Date getRecordedOn() {
		return recordedOn;
	}

	@Override
	public void setRecordedOn(Date recordedOn) {
		this.recordedOn = recordedOn;
	}

	@Override
	public Date getDeletedOn() {
		return deletedOn;
	}

	@Override
	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	@Override
	public String getDeletedById() {
		return deletedById;
	}

	@Override
	public void setDeletedById(String deletedById) {
		this.deletedById = deletedById;
	}

	@Override
	public String getOrigin() {
		return origin;
	}

	@Override
	public void setOrigin(String origin) {
		this.origin = origin;
	}

}
