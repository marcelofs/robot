package mfs.robot.shared.erepublik.data;

import java.io.Serializable;

import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.gwt.user.client.rpc.IsSerializable;

@Deprecated
@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class MarketplaceProfit implements Serializable, IsSerializable {

	private static final long	serialVersionUID	= 1L;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long				id;
	@Persistent
	private ProductQuality		quality;

	@Persistent
	private ProductType			type;

	@Persistent
	@Extension(vendorName = "datanucleus", key = "gae.unindexed", value = "true")
	private Double				averageProfit;

	@Persistent
	@Extension(vendorName = "datanucleus", key = "gae.unindexed", value = "true")
	private Double				averageProfitGold;

	@Persistent
	@Extension(vendorName = "datanucleus", key = "gae.unindexed", value = "true")
	private Double				averageRoi;

	@Persistent
	@Extension(vendorName = "datanucleus", key = "gae.unindexed", value = "true")
	private Double				investmentNeeded;

	@Persistent
	private Integer				eRepDay;

	@Persistent
	private Integer				eRepTime;

	public ProductQuality getQuality() {
		return quality;
	}

	public void setQuality(ProductQuality quality) {
		this.quality = quality;
	}

	public ProductType getType() {
		return type;
	}

	public void setType(ProductType type) {
		this.type = type;
	}

	public Double getAverageProfit() {
		return averageProfit;
	}

	public void setAverageProfit(Double averageProfit) {
		this.averageProfit = averageProfit;
	}

	public Integer geteRepDay() {
		return eRepDay;
	}

	public void seteRepDay(Integer eRepDay) {
		this.eRepDay = eRepDay;
	}

	public Integer geteRepTime() {
		return eRepTime;
	}

	public void seteRepTime(Integer eRepTime) {
		this.eRepTime = eRepTime;
	}

	public Long getId() {
		return id;
	}

	public Double getAverageProfitGold() {
		return averageProfitGold;
	}

	public void setAverageProfitGold(Double averageProfitGold) {
		this.averageProfitGold = averageProfitGold;
	}

	public Double getAverageRoi() {
		return averageRoi;
	}

	public void setAverageRoi(Double roi) {
		this.averageRoi = roi;
	}

	public Double getInvestmentNeeded() {
		if (investmentNeeded != null)
			return investmentNeeded;
		return 0d;
	}

	public void setInvestmentNeeded(Double investmentNeeded) {
		this.investmentNeeded = investmentNeeded;
	}
}
