package mfs.robot.shared.erepublik.data;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.gwt.user.client.rpc.IsSerializable;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class CountryStats implements Serializable, IsSerializable {

	private static final long	serialVersionUID	= 1L;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long				id;

	// 1817
	@Deprecated
	@NotPersistent
	private String				eRepCountryID;

	@Persistent
	private Integer				activeCitizens;

	@Persistent
	private Integer				egov4youCitizens;

	@Persistent
	private Integer				egov4youFighters;

	@Persistent
	private Integer				miners;

	@Persistent
	private Integer				newCitizens;

	@Persistent(defaultFetchGroup = "false")
	private Integer				averageLevel;

	@Persistent(defaultFetchGroup = "false")
	private Integer				regionCount;

	@Persistent
	private Integer				eRepDay;

	public Integer getActiveCitizens() {
		return activeCitizens;
	}

	public void setActiveCitizens(Integer activeCitizens) {
		this.activeCitizens = activeCitizens;
	}

	public Integer getNewCitizens() {
		return newCitizens;
	}

	public void setNewCitizens(Integer newCitizens) {
		this.newCitizens = newCitizens;
	}

	public Integer getAverageLevel() {
		return averageLevel;
	}

	public void setAverageLevel(Integer averageLevel) {
		this.averageLevel = averageLevel;
	}

	public Integer getRegionCount() {
		return regionCount;
	}

	public void setRegionCount(Integer regionCount) {
		this.regionCount = regionCount;
	}

	public Integer geteRepDay() {
		return eRepDay;
	}

	public void seteRepDay(Integer eRepDay) {
		this.eRepDay = eRepDay;
	}

	public Long getId() {
		return id;
	}

	public Integer getEgov4youCitizens() {
		return egov4youCitizens;
	}

	public void setEgov4youCitizens(Integer egov4youCitizens) {
		this.egov4youCitizens = egov4youCitizens;
	}

	public Integer getEgov4youFighters() {
		return egov4youFighters;
	}

	public void setEgov4youFighters(Integer egov4youFighters) {
		this.egov4youFighters = egov4youFighters;
	}

	public Integer getMiners() {
		return miners;
	}

	public void setMiners(Integer miners) {
		this.miners = miners;
	}

}
