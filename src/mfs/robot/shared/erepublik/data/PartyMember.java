package mfs.robot.shared.erepublik.data;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class PartyMember implements Serializable, IsSerializable {

	private static final long	serialVersionUID	= 1L;

	private String				eRepId;

	private String				eRepName;

	public String geteRepId() {
		return eRepId;
	}

	public void seteRepId(String eRepId) {
		this.eRepId = eRepId;
	}

	public String geteRepName() {
		return eRepName;
	}

	public void seteRepName(String eRepName) {
		this.eRepName = eRepName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((eRepId == null) ? 0 : eRepId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PartyMember other = (PartyMember) obj;
		if (eRepId == null) {
			if (other.eRepId != null)
				return false;
		} else
			if (!eRepId.equals(other.eRepId))
				return false;
		return true;
	}

}
