package mfs.robot.shared.erepublik.data;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.gwt.user.client.rpc.IsSerializable;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class CountryTaxes implements Serializable, IsSerializable {

	private static final long	serialVersionUID	= 1L;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long				id;

	// @Persistent
	// 1817
	@Deprecated
	@NotPersistent
	private String				eRepCountryID;

	@Persistent
	private Integer				incomeTax;

	@Persistent
	private Integer				foodVat;

	@Persistent
	private Integer				foodImport;

	@Persistent
	private Integer				weaponsVat;

	@Persistent
	private Integer				weaponsImport;

	@Persistent
	private Integer				frmImport;

	@Persistent
	private Integer				wrmImport;

	@Persistent
	private Integer				eRepDay;

	public Integer getIncomeTax() {
		return incomeTax;
	}

	public void setIncomeTax(Integer incomeTax) {
		this.incomeTax = incomeTax;
	}

	public Integer getFoodVat() {
		return foodVat;
	}

	public void setFoodVat(Integer foodVat) {
		this.foodVat = foodVat;
	}

	public Integer getFoodImport() {
		return foodImport;
	}

	public void setFoodImport(Integer foodImport) {
		this.foodImport = foodImport;
	}

	public Integer getWeaponsVat() {
		return weaponsVat;
	}

	public void setWeaponsVat(Integer weaponsVat) {
		this.weaponsVat = weaponsVat;
	}

	public Integer getWeaponsImport() {
		return weaponsImport;
	}

	public void setWeaponsImport(Integer weaponsImport) {
		this.weaponsImport = weaponsImport;
	}

	public Integer getFrmImport() {
		return frmImport;
	}

	public void setFrmImport(Integer frmImport) {
		this.frmImport = frmImport;
	}

	public Integer getWrmImport() {
		return wrmImport;
	}

	public void setWrmImport(Integer wrmImport) {
		this.wrmImport = wrmImport;
	}

	public Integer geteRepDay() {
		return eRepDay;
	}

	public void seteRepDay(Integer eRepDay) {
		this.eRepDay = eRepDay;
	}

	public Long getId() {
		return id;
	}

}
