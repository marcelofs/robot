package mfs.robot.shared.erepublik.data;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import mfs.robot.shared.erepublik.IFinancialIndex;
import mfs.robot.shared.erepublik.IHistorySeries;

import com.google.gwt.user.client.rpc.IsSerializable;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class AgregatedMarketplaceOffer implements Serializable, IsSerializable,
		IHistorySeries, IFinancialIndex {

	private static final long	serialVersionUID	= 1L;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long				id;

	@Persistent(defaultFetchGroup = "false")
	private ProductQuality		quality;

	@Persistent(defaultFetchGroup = "false")
	private ProductType			type;

	@Deprecated
	@NotPersistent
	private Integer				quantity;

	@Persistent
	private Double				averagePrice;

	@Persistent
	private Double				minimumPrice;

	@Persistent
	private Double				maximumPrice;

	@Persistent
	private Integer				eRepDay;

	public ProductQuality getQuality() {
		return quality;
	}

	public void setQuality(ProductQuality quality) {
		this.quality = quality;
	}

	public ProductType getType() {
		return type;
	}

	public void setType(ProductType type) {
		this.type = type;
	}

	@Deprecated
	public Integer getQuantity() {
		return quantity;
	}

	@Deprecated
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Override
	public Double getAverage() {
		return averagePrice;
	}

	public void setAveragePrice(Double averagePrice) {
		this.averagePrice = averagePrice;
	}

	@Override
	public Double getMinimum() {
		return minimumPrice;
	}

	public void setMinimumPrice(Double minimumPrice) {
		this.minimumPrice = minimumPrice;
	}

	@Override
	public Double getMaximum() {
		return maximumPrice;
	}

	public void setMaximumPrice(Double maximumPrice) {
		this.maximumPrice = maximumPrice;
	}

	@Override
	public Integer geteRepDay() {
		return eRepDay;
	}

	public void seteRepDay(Integer eRepDay) {
		this.eRepDay = eRepDay;
	}

	public Long getId() {
		return id;
	}

	@Override
	public Double getIndexValue() {
		return getAverage();
	}

}
