package mfs.robot.shared.erepublik.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Set;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.gwt.user.client.rpc.IsSerializable;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class PoliticalPartyMembers implements Serializable, IsSerializable,
		Iterable<PartyMember> {

	private static final long	serialVersionUID	= 1L;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long				id;

	@Persistent
	private Integer				eRepDay;

	@Persistent
	private PoliticalParty		party;

	@Persistent(serialized = "true")
	private Set<PartyMember>	members;

	public Integer geteRepDay() {
		return eRepDay;
	}

	public void seteRepDay(Integer eRepDay) {
		this.eRepDay = eRepDay;
	}

	public PoliticalParty getParty() {
		return party;
	}

	public void setParty(PoliticalParty party) {
		this.party = party;
	}

	public Set<PartyMember> getMembers() {
		return members;
	}

	public void setMembers(Set<PartyMember> members) {
		this.members = members;
	}

	public Long getId() {
		return id;
	}

	@Override
	public Iterator<PartyMember> iterator() {
		if (members == null)
			throw new IllegalStateException("Members set is null");
		return members.iterator();
	}

	public boolean contains(Object o) {
		if (members == null)
			throw new IllegalStateException("Members set is null");
		return members.contains(o);
	}

}
