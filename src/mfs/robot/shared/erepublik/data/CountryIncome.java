package mfs.robot.shared.erepublik.data;

import java.io.Serializable;
import java.util.Date;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import mfs.robot.shared.erepublik.ITransaction;

import com.google.gwt.user.client.rpc.IsSerializable;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class CountryIncome implements Serializable, IsSerializable,
		ITransaction {

	private static final long	serialVersionUID	= 1L;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long				id;

	@Persistent
	private Integer				eRepDay;

	@Persistent
	private Double				value;

	@Persistent
	private MonetaryType		type;

	@Persistent
	private String				origin;

	@Persistent
	private String				personResponsible;

	@Persistent
	private String				destination;

	@Persistent
	private String				observations;

	// eRepUser
	@Persistent(defaultFetchGroup = "false")
	private String				recordedById;

	@Persistent
	private Date				recordedOn;

	@Persistent(defaultFetchGroup = "false")
	private Date				deletedOn;

	// eRepUser
	@Persistent(defaultFetchGroup = "false")
	private String				deletedById;

	public Integer geteRepDay() {
		return eRepDay;
	}

	public void seteRepDay(Integer eRepDay) {
		this.eRepDay = eRepDay;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public MonetaryType getType() {
		return type;
	}

	public void setType(MonetaryType type) {
		this.type = type;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getObservations() {
		return observations;
	}

	public void setObservations(String observations) {
		this.observations = observations;
	}

	public Long getId() {
		return id;
	}

	public String getRecordedById() {
		return recordedById;
	}

	public void setRecordedById(String recordedById) {
		this.recordedById = recordedById;
	}

	public Date getRecordedOn() {
		return recordedOn;
	}

	public void setRecordedOn(Date recordedOn) {
		this.recordedOn = recordedOn;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public String getDeletedById() {
		return deletedById;
	}

	public void setDeletedById(String deletedById) {
		this.deletedById = deletedById;
	}

	public String getPersonResponsible() {
		return personResponsible;
	}

	public void setPersonResponsible(String personResponsible) {
		this.personResponsible = personResponsible;
	}
}
