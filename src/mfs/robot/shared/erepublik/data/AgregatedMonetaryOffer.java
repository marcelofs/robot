package mfs.robot.shared.erepublik.data;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import mfs.robot.shared.erepublik.IFinancialIndex;
import mfs.robot.shared.erepublik.IHistorySeries;

import com.google.gwt.user.client.rpc.IsSerializable;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class AgregatedMonetaryOffer implements Serializable, IsSerializable,
		IHistorySeries, IFinancialIndex {

	private static final long	serialVersionUID	= 1L;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long				id;

	@Persistent(defaultFetchGroup = "false")
	private Double				quantity;

	@Persistent
	private Double				averagePrice;

	@Persistent
	private Double				minimumPrice;

	@Persistent
	private Double				maximumPrice;

	@Persistent
	private Integer				eRepDay;

	@Persistent(defaultFetchGroup = "false")
	private MonetaryType		type;

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	@Override
	public Double getAverage() {
		return averagePrice;
	}

	public void setAveragePrice(Double averagePrice) {
		this.averagePrice = averagePrice;
	}

	@Override
	public Double getMinimum() {
		return minimumPrice;
	}

	public void setMinimumPrice(Double minimumPrice) {
		this.minimumPrice = minimumPrice;
	}

	@Override
	public Double getMaximum() {
		return maximumPrice;
	}

	public void setMaximumPrice(Double maximumPrice) {
		this.maximumPrice = maximumPrice;
	}

	@Override
	public Integer geteRepDay() {
		return eRepDay;
	}

	public void seteRepDay(Integer eRepDay) {
		this.eRepDay = eRepDay;
	}

	public MonetaryType getType() {
		return type;
	}

	public void setType(MonetaryType type) {
		this.type = type;
	}

	public Long getId() {
		return id;
	}

	@Override
	public Double getIndexValue() {
		return getAverage();
	}
}
