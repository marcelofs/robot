package mfs.robot.shared.erepublik.data;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.gwt.user.client.rpc.IsSerializable;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class CountryPresident implements Serializable, IsSerializable {

	private static final long	serialVersionUID	= 1L;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long				id;

	@Persistent
	private String				name;

	@Persistent
	private Integer				firstErepDay;

	@Persistent
	private Integer				lastErepDay;

	@Persistent
	private Boolean				showFinanceTables;

	@Persistent
	private Boolean				showFinanceGraphs;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getFirstErepDay() {
		return firstErepDay;
	}

	public void setFirstErepDay(Integer firstErepDay) {
		this.firstErepDay = firstErepDay;
	}

	public Integer getLastErepDay() {
		return lastErepDay;
	}

	public void setLastErepDay(Integer lastErepDay) {
		this.lastErepDay = lastErepDay;
	}

	public Long getId() {
		return id;
	}

	public Boolean getShowFinanceTables() {
		return showFinanceTables;
	}

	public void setShowFinanceTables(Boolean showFinanceTables) {
		this.showFinanceTables = showFinanceTables;
	}

	public Boolean getShowFinanceGraphs() {
		return showFinanceGraphs;
	}

	public void setShowFinanceGraphs(Boolean showFinanceGraphs) {
		this.showFinanceGraphs = showFinanceGraphs;
	}

}
