package mfs.robot.shared.erepublik.data;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import mfs.robot.shared.erepublik.IFinancialIndex;
import mfs.robot.shared.erepublik.IHistorySeries;

import com.google.gwt.user.client.rpc.IsSerializable;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class AgregatedJobMarketOffer implements Serializable, IsSerializable,
		IHistorySeries, IFinancialIndex {

	private static final long	serialVersionUID	= 1L;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long				id;

	@Persistent
	private Double				averageSalary;

	@Persistent
	private Double				minimumSalary;

	@Persistent
	private Double				maximumSalary;

	@Persistent
	private Double				oficialAvgSalary;

	@Persistent
	private Integer				eRepDay;

	@Override
	public Double getAverage() {
		return averageSalary;
	}

	public void setAverageSalary(Double averageSalary) {
		this.averageSalary = averageSalary;
	}

	@Override
	public Double getMinimum() {
		return minimumSalary;
	}

	public void setMinimumSalary(Double minimumSalary) {
		this.minimumSalary = minimumSalary;
	}

	@Override
	public Double getMaximum() {
		return maximumSalary;
	}

	public void setMaximum(Double maximumSalary) {
		this.maximumSalary = maximumSalary;
	}

	@Override
	public Integer geteRepDay() {
		return eRepDay;
	}

	public void seteRepDay(Integer eRepDay) {
		this.eRepDay = eRepDay;
	}

	public Long getId() {
		return id;
	}

	@Override
	public Double getIndexValue() {
		return getAverage();
	}

	public Double getOficialAvgSalary() {
		return oficialAvgSalary;
	}

	public void setOficialAvgSalary(Double oficialAvgSalary) {
		this.oficialAvgSalary = oficialAvgSalary;
	}

}
