package mfs.robot.shared.erepublik.data;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.gwt.user.client.rpc.IsSerializable;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class CountryBonuses implements Serializable, IsSerializable {

	private static final long	serialVersionUID	= 1L;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long				id;

	// 1817
	@Deprecated
	@NotPersistent
	private String				eRepCountryID;

	@Persistent
	private Double				foodBonus;

	// 2118
	@Deprecated
	@NotPersistent
	private Double				frmBonus;

	@Persistent
	private Double				weaponsBonus;

	// 2118
	@Deprecated
	@NotPersistent
	private Double				wrmBonus;

	@Persistent
	private Integer				eRepDay;

	public Double getFoodBonus() {
		return foodBonus;
	}

	public void setFoodBonus(Double foodBonus) {
		this.foodBonus = foodBonus;
	}

	@Deprecated
	public Double getFrmBonus() {
		return frmBonus;
	}

	@Deprecated
	public void setFrmBonus(Double frmBonus) {
		this.frmBonus = frmBonus;
	}

	public Double getWeaponsBonus() {
		return weaponsBonus;
	}

	public void setWeaponsBonus(Double weaponsBonus) {
		this.weaponsBonus = weaponsBonus;
	}

	@Deprecated
	public Double getWrmBonus() {
		return wrmBonus;
	}

	@Deprecated
	public void setWrmBonus(Double wrmBonus) {
		this.wrmBonus = wrmBonus;
	}

	public Integer geteRepDay() {
		return eRepDay;
	}

	public void seteRepDay(Integer eRepDay) {
		this.eRepDay = eRepDay;
	}

	public Long getId() {
		return id;
	}

}
