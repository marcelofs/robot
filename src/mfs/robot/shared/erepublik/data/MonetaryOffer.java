package mfs.robot.shared.erepublik.data;

import java.io.Serializable;

import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import mfs.robot.shared.erepublik.IDailySeries;

import com.google.gwt.user.client.rpc.IsSerializable;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class MonetaryOffer implements Serializable, IsSerializable,
		IDailySeries {

	private static final long	serialVersionUID	= 1L;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long				id;

	@Persistent(defaultFetchGroup = "false")
	@Extension(vendorName = "datanucleus", key = "gae.unindexed", value = "true")
	private Double				quantity;

	@Persistent
	@Extension(vendorName = "datanucleus", key = "gae.unindexed", value = "true")
	private Double				averagePrice;

	@Persistent
	@Extension(vendorName = "datanucleus", key = "gae.unindexed", value = "true")
	private Double				minimumPrice;

	@Persistent
	private Integer				eRepDay;

	@Persistent
	private Integer				eRepTime;

	@Persistent(defaultFetchGroup = "false")
	private MonetaryType		type;

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	@Override
	public Integer geteRepDay() {
		return eRepDay;
	}

	public void seteRepDay(Integer eRepDay) {
		this.eRepDay = eRepDay;
	}

	@Override
	public Integer geteRepTime() {
		return eRepTime;
	}

	public void seteRepTime(Integer eRepTime) {
		this.eRepTime = eRepTime;
	}

	public Long getId() {
		return id;
	}

	public MonetaryType getType() {
		return type;
	}

	public void setType(MonetaryType type) {
		this.type = type;
	}

	@Override
	public Double getAverage() {
		// if (averagePrice != null)
		return averagePrice;
		// return getPrice();
	}

	public void setAveragePrice(Double averagePrice) {
		this.averagePrice = averagePrice;
	}

	@Override
	public Double getMinimum() {
		// if (minimumPrice != null)
		return minimumPrice;
		// return getPrice();
	}

	public void setMinimumPrice(Double minimumPrice) {
		this.minimumPrice = minimumPrice;
	}
}
