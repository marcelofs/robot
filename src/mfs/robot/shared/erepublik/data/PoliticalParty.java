package mfs.robot.shared.erepublik.data;

public enum PoliticalParty
{
	_1242("Arena", true),
	_4004("Fenix", true),
	_4485("N.E.R.D.", true),
	_2520("M.P.B.", false),
	_2071("Partido Militar", true),
	_4222("Partido Socialista", false),
	_5003("PPB", true),
	_3064("Sparta", false),
	_61("PDB", false),
	_2708("Anarquia", false),
	
//	_2524("Thunder", false),
//	_4528("Heroes", false),
//	_1112("ODIN", false),
// 	_2275("Orb", false), 
//	_2941("Persona", false), 
//	_3044("PNT", false), 
//	_90("Party in Rio", false),  
//	_4303("União Nacional Libertadora", false), 
//	_101("União Socialista Brasileira", false), 
//	_4175("ROMA (ex-Partido Liberal)", false)
	;

	private String	partyName;
	private boolean	trackMembers;

	private PoliticalParty(String name, boolean trackMembers) { 
		this.partyName = name;
		this.trackMembers = trackMembers;
	}

	public String getPartyName() {
		return partyName;
	}

	public boolean getTrackMembers() {
		return this.trackMembers;
	}

	public Integer getPartyCode() {
		return Integer.valueOf(name().replace("_", ""));
	}

	public static PoliticalParty fromId(String id) {
		return valueOf("_" + id);
	}

}
