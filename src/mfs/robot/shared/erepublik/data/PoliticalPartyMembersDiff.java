package mfs.robot.shared.erepublik.data;

import java.io.Serializable;
import java.util.Set;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.gwt.user.client.rpc.IsSerializable;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class PoliticalPartyMembersDiff implements Serializable, IsSerializable {

	private static final long	serialVersionUID	= 1L;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long				id;

	@Persistent
	private Integer				eRepDay;
	@Persistent
	private PoliticalParty		party;

	@Persistent(serialized = "true")
	private Set<PartyMember>	newMembers;

	@Persistent
	private Integer				newMembersCount;

	@Persistent(serialized = "true")
	private Set<PartyMember>	leftMembers;

	@Persistent
	private Integer				leftMembersCount;

	public Integer geteRepDay() {
		return eRepDay;
	}

	public void seteRepDay(Integer eRepDay) {
		this.eRepDay = eRepDay;
	}

	public PoliticalParty getParty() {
		return party;
	}

	public void setParty(PoliticalParty party) {
		this.party = party;
	}

	public Set<PartyMember> getNewMembers() {
		return newMembers;
	}

	public void setNewMembers(Set<PartyMember> newMembers) {
		this.newMembers = newMembers;
		if (newMembers != null)
			this.newMembersCount = newMembers.size();
	}

	public Set<PartyMember> getLeftMembers() {
		return leftMembers;
	}

	public void setLeftMembers(Set<PartyMember> leftMembers) {
		this.leftMembers = leftMembers;
		if (leftMembers != null)
			this.leftMembersCount = leftMembers.size();
	}

	public Long getId() {
		return id;
	}

	public Integer getNewMembersCount() {
		return newMembersCount;
	}

	public Integer getLeftMembersCount() {
		return leftMembersCount;
	}

}
