package mfs.robot.shared.erepublik.data;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.gwt.user.client.rpc.IsSerializable;

@Deprecated
@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class AgregatedMarketplaceProfit implements Serializable, IsSerializable {

	private static final long	serialVersionUID	= 1L;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long				id;
	@Persistent
	private ProductQuality		quality;

	@Persistent
	private ProductType			type;

	@Persistent
	private Double				averageProfit;

	@Deprecated
	@NotPersistent
	// 1770
	private Double				maximumProfit;

	@Deprecated
	@NotPersistent
	// 1770
	private Double				minimumProfit;

	@Persistent
	private Double				averageProfitGold;

	@Deprecated
	@NotPersistent
	// 1770
	private Double				maximumProfitGold;

	@Deprecated
	@NotPersistent
	// 1770
	private Double				minimumProfitGold;

	@Persistent
	private Double				averageRoi;

	@Deprecated
	@NotPersistent
	// 1770
	private Double				maximumRoi;

	@Deprecated
	@NotPersistent
	// 1770
	private Double				minimumRoi;

	@Persistent
	private Double				averageInvestmentNeeded;

	@Deprecated
	@NotPersistent
	// 1770
	private Double				minimumInvestmentNeeded;

	@Deprecated
	@NotPersistent
	// 1770
	private Double				maximumInvestmentNeeded;

	@Persistent
	private Integer				eRepDay;

	public ProductQuality getQuality() {
		return quality;
	}

	public void setQuality(ProductQuality quality) {
		this.quality = quality;
	}

	public ProductType getType() {
		return type;
	}

	public void setType(ProductType type) {
		this.type = type;
	}

	public Double getAverageProfit() {
		return averageProfit;
	}

	public void setAverageProfit(Double averageProfit) {
		this.averageProfit = averageProfit;
	}

	public Integer geteRepDay() {
		return eRepDay;
	}

	public void seteRepDay(Integer eRepDay) {
		this.eRepDay = eRepDay;
	}

	public Long getId() {
		return id;
	}

	public Double getAverageProfitGold() {
		return averageProfitGold;
	}

	public void setAverageProfitGold(Double averageProfitGold) {
		this.averageProfitGold = averageProfitGold;
	}

	public Double getAverageRoi() {
		return averageRoi;
	}

	public void setAverageRoi(Double averageRoi) {
		this.averageRoi = averageRoi;
	}

	public Double getAverageInvestmentNeeded() {
		if (averageInvestmentNeeded != null)
			return averageInvestmentNeeded;
		return 0d;
	}

	public void setAverageInvestmentNeeded(Double averageInvestmentNeeded) {
		this.averageInvestmentNeeded = averageInvestmentNeeded;
	}

}
