package mfs.robot.shared.erepublik.data;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import mfs.robot.shared.erepublik.IHistorySeries;

import com.google.gwt.user.client.rpc.IsSerializable;

@Deprecated
@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class AgregatedRocketPrice implements Serializable, IsSerializable,
		IHistorySeries {
	private static final long	serialVersionUID	= 1L;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long				id;

	@Persistent
	private RocketQuality		quality;

	@Persistent
	private Double				averagePrice;

	@Persistent
	private Double				averagePriceGold;

	@Persistent
	private Double				minimumPrice;

	@Persistent
	private Double				maximumPrice;

	@Persistent
	private Integer				eRepDay;

	public RocketQuality getQuality() {
		return quality;
	}

	public void setQuality(RocketQuality quality) {
		this.quality = quality;
	}

	@Override
	public Double getAverage() {
		return averagePrice;
	}

	public void setAveragePrice(Double averagePrice) {
		this.averagePrice = averagePrice;
	}

	@Override
	public Integer geteRepDay() {
		return eRepDay;
	}

	public void seteRepDay(Integer eRepDay) {
		this.eRepDay = eRepDay;
	}

	public Long getId() {
		return id;
	}

	@Override
	public Double getMinimum() {
		return minimumPrice;
	}

	public void setMinimumPrice(Double minimumPrice) {
		this.minimumPrice = minimumPrice;
	}

	@Override
	public Double getMaximum() {
		return maximumPrice;
	}

	public void setMaximumPrice(Double maximumPrice) {
		this.maximumPrice = maximumPrice;
	}

	public Double getAveragePriceGold() {
		return averagePriceGold;
	}

	public void setAveragePriceGold(Double averagePriceGold) {
		this.averagePriceGold = averagePriceGold;
	}
}
