package mfs.robot.shared.erepublik.data;

import java.io.Serializable;

import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import mfs.robot.shared.erepublik.IDailySeries;

import com.google.gwt.user.client.rpc.IsSerializable;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class JobMarketOffer implements Serializable, IsSerializable,
		IDailySeries {

	private static final long	serialVersionUID	= 1L;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long				id;

	@Persistent
	@Extension(vendorName = "datanucleus", key = "gae.unindexed", value = "true")
	private Double				averageSalary;

	@Persistent
	@Extension(vendorName = "datanucleus", key = "gae.unindexed", value = "true")
	private Double				maximumSalary;

	@Persistent
	private Integer				eRepDay;

	@Persistent
	private Integer				eRepTime;

	public Integer geteRepDay() {
		return eRepDay;
	}

	public void seteRepDay(Integer eRepDay) {
		this.eRepDay = eRepDay;
	}

	public Integer geteRepTime() {
		return eRepTime;
	}

	public void seteRepTime(Integer eRepTime) {
		this.eRepTime = eRepTime;
	}

	public Long getId() {
		return id;
	}

	public Double getAverage() {
		return averageSalary;
	}

	public void setAverageSalary(Double averageSalary) {
		this.averageSalary = averageSalary;
	}

	public Double getMaximum() {
		return maximumSalary;
	}

	@Override
	/**
	 * Salary is the opposite
	 */
	public Double getMinimum() {
		return maximumSalary;
	}

	public void setMaximumSalary(Double maximumSalary) {
		this.maximumSalary = maximumSalary;
	}
}
