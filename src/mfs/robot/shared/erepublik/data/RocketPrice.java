package mfs.robot.shared.erepublik.data;

import java.io.Serializable;

import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.gwt.user.client.rpc.IsSerializable;

@Deprecated
// 2055
@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class RocketPrice implements Serializable, IsSerializable {
	private static final long	serialVersionUID	= 1L;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long				id;
	@Persistent
	private RocketQuality		quality;

	@Persistent
	@Extension(vendorName = "datanucleus", key = "gae.unindexed", value = "true")
	private Double				averagePrice;

	@Persistent
	@Extension(vendorName = "datanucleus", key = "gae.unindexed", value = "true")
	private Double				averagePriceGold;

	@Persistent
	private Integer				eRepDay;

	@Persistent
	private Integer				eRepTime;

	public RocketQuality getQuality() {
		return quality;
	}

	public void setQuality(RocketQuality quality) {
		this.quality = quality;
	}

	public Double getAveragePrice() {
		return averagePrice;
	}

	public void setAveragePrice(Double averagePrice) {
		this.averagePrice = averagePrice;
	}

	public Integer geteRepDay() {
		return eRepDay;
	}

	public void seteRepDay(Integer eRepDay) {
		this.eRepDay = eRepDay;
	}

	public Integer geteRepTime() {
		return eRepTime;
	}

	public void seteRepTime(Integer eRepTime) {
		this.eRepTime = eRepTime;
	}

	public Long getId() {
		return id;
	}

	public Double getAveragePriceGold() {
		return averagePriceGold;
	}

	public void setAveragePriceGold(Double averagePriceGold) {
		this.averagePriceGold = averagePriceGold;
	}
}
