package mfs.robot.shared.erepublik.data;

public enum ExpenseType
{

	ARMY("Exército (cota)"),
	BRNUKE("BrNuke"),
	SU("Soldado Universal"),
	FARMER("Projeto Farmer"), 
	MPP("MPPs"), 
	RENT("Aluguel de regiões"),
	CO("Combat Orders"),
	DIST("Distribuições (MMV e BI)"), 
	DIST_EXTRA("Distribuições extras"), 
	DANO("Compra de Dano"),
	AIRSTRIKE("Air Strike"), 
	SAM("SAM"),  
	MON_MARKET("Investimento no Mercado Monetário"),
	OTHER("Outros"),   
	NEW_USERS("Novos Usuários", false)

	; 

	private String	name;
	private boolean	isManual;

	private ExpenseType(String name) {
		this(name, true);
	}

	private ExpenseType(String name, boolean isManual) {
		this.name = name;
		this.isManual = isManual;
	}

	public String getName() {
		return name;
	}

	public boolean isManual() {
		return this.isManual;
	}

	public static ExpenseType fromName(String name) {
		for (ExpenseType t : values())
			if (t.getName().equalsIgnoreCase(name))
				return t;
		throw new IllegalArgumentException("Expense type could not be found");
	}
}
