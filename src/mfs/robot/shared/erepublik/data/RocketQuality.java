package mfs.robot.shared.erepublik.data;

public enum RocketQuality
{

	Q1(10, 0), Q2(20, 0), Q3(60, 0), Q4(120, 0), Q5(90, 0);

	private final Integer	requiredWeapons;
	private final Integer	fixedPrice;

	private RocketQuality(Integer requiredWeapons, Integer fixedPrice) {
		this.requiredWeapons = requiredWeapons;
		this.fixedPrice = fixedPrice;
	}

	public Integer getRequiredWeapons() {
		return requiredWeapons;
	}

	public Integer getFixedPrice() {
		return fixedPrice;
	}

}
