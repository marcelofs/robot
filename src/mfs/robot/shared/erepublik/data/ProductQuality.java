package mfs.robot.shared.erepublik.data;

public enum ProductQuality
{

	Q1, Q2, Q3, Q4, Q5, Q6, Q7, RAW;

}
