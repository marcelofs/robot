package mfs.robot.shared.erepublik.data;

public enum MonetaryType
{
	CURRENCY, GOLD, RAW, WQ7
}
