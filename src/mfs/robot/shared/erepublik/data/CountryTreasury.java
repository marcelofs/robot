package mfs.robot.shared.erepublik.data;

import java.io.Serializable;
import java.util.Date;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import mfs.robot.shared.erepublik.IFinancialIndex;

import com.google.gwt.user.client.rpc.IsSerializable;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class CountryTreasury implements Serializable, IsSerializable,
		IFinancialIndex {

	private static final long	serialVersionUID	= 1L;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long				id;

	// 1817
	@Deprecated
	@NotPersistent
	private String				eRepCountryID;

	@Persistent
	private Double				goldTreasury;

	@Persistent
	private Double				currencyTreasury;

	@Persistent(defaultFetchGroup = "false")
	private Date				collectedAt;

	@Persistent
	private Double				calculatedTaxRevenue;

	@Persistent
	private Double				calculatedGoldTaxRevenue;

	@Persistent
	private Double				manualMdFTaxRevenue;

	@Persistent
	private Double				manualMdFGoldTaxRevenue;

	@Persistent
	private Integer				eRepDay;

	@Persistent
	private String				comments;

	public Double getGoldTreasury() {
		return goldTreasury;
	}

	public void setGoldTreasury(Double goldTreasury) {
		this.goldTreasury = goldTreasury;
	}

	public Double getCurrencyTreasury() {
		return currencyTreasury;
	}

	public CountryTreasury setCurrencyTreasury(Double currencyTreasury) {
		this.currencyTreasury = currencyTreasury;
		return this;
	}

	public Integer geteRepDay() {
		return eRepDay;
	}

	public void seteRepDay(Integer eRepDay) {
		this.eRepDay = eRepDay;
	}

	public Long getId() {
		return id;
	}

	public Double getTaxRevenue() {
		if (manualMdFTaxRevenue != null)
			return manualMdFTaxRevenue;
		return calculatedTaxRevenue;
	}

	public Double getGoldTaxRevenue() {
		if (manualMdFGoldTaxRevenue != null)
			return manualMdFGoldTaxRevenue;
		return calculatedGoldTaxRevenue;
	}

	public Double getCalculatedTaxRevenue() {
		return calculatedTaxRevenue;
	}

	public void setCalculatedTaxRevenue(Double calculatedTaxRevenue) {
		this.calculatedTaxRevenue = calculatedTaxRevenue;
	}

	public Double getCalculatedGoldTaxRevenue() {
		return calculatedGoldTaxRevenue;
	}

	public void setCalculatedGoldTaxRevenue(Double calculatedGoldTaxRevenue) {
		this.calculatedGoldTaxRevenue = calculatedGoldTaxRevenue;
	}

	public Double getManualMdFTaxRevenue() {
		return manualMdFTaxRevenue;
	}

	public void setManualMdFTaxRevenue(Double manualMdFTaxRevenue) {
		this.manualMdFTaxRevenue = manualMdFTaxRevenue;
	}

	public Double getManualMdFGoldTaxRevenue() {
		return manualMdFGoldTaxRevenue;
	}

	public void setManualMdFGoldTaxRevenue(Double manualMdFGoldTaxRevenue) {
		this.manualMdFGoldTaxRevenue = manualMdFGoldTaxRevenue;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public Double getIndexValue() {
		return getTaxRevenue();
	}

	public Date getCollectedAt() {
		return collectedAt;
	}

	public void setCollectedAt(Date collectedAt) {
		this.collectedAt = collectedAt;
	}

}
