package mfs.robot.shared.erepublik.data;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.gwt.user.client.rpc.IsSerializable;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class CitizenAccount implements Serializable, IsSerializable {

	private static final long	serialVersionUID	= 1L;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long				id;
	@Persistent
	private Long				eRepId;

	@Persistent(defaultFetchGroup = "false")
	private String				eRepName;

	@Persistent
	private Double				gold;

	@Persistent
	private Double				currency;

	@Persistent
	private int					eRepDay;

	public Long geteRepId() {
		return eRepId;
	}

	public void seteRepId(Long eRepId) {
		this.eRepId = eRepId;
	}

	public String geteRepName() {
		return eRepName;
	}

	public void seteRepName(String eRepName) {
		this.eRepName = eRepName;
	}

	public Double getGold() {
		return gold;
	}

	public void setGold(Double gold) {
		this.gold = gold;
	}

	public Double getCurrency() {
		return currency;
	}

	public void setCurrency(Double currency) {
		this.currency = currency;
	}

	public Long getId() {
		return id;
	}

	public int geteRepDay() {
		return eRepDay;
	}

	public void seteRepDay(int eRepDay) {
		this.eRepDay = eRepDay;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((currency == null) ? 0 : currency.hashCode());
		result = prime * result + eRepDay;
		result = prime * result + ((eRepId == null) ? 0 : eRepId.hashCode());
		result = prime * result
				+ ((eRepName == null) ? 0 : eRepName.hashCode());
		result = prime * result + ((gold == null) ? 0 : gold.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CitizenAccount other = (CitizenAccount) obj;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else
			if (!currency.equals(other.currency))
				return false;
		if (eRepDay != other.eRepDay)
			return false;
		if (eRepId == null) {
			if (other.eRepId != null)
				return false;
		} else
			if (!eRepId.equals(other.eRepId))
				return false;
		if (eRepName == null) {
			if (other.eRepName != null)
				return false;
		} else
			if (!eRepName.equals(other.eRepName))
				return false;
		if (gold == null) {
			if (other.gold != null)
				return false;
		} else
			if (!gold.equals(other.gold))
				return false;
		return true;
	}

}
