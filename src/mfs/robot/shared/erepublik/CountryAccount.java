package mfs.robot.shared.erepublik;

public enum CountryAccount
{

	CN("Controle Nacional", "1208385", "", true, true, true),
	FMN("Fundo Monetario Nacional", "3448075", "",false, true, true),
	MD("Ministerio da Defesa", "1244257", "",true, true, true),	
	
	DIV("---------", "", "",true, true),
	
	ASI("Assistencia Social", "1510350", "",true, true, true),
	AUX("Auxilio ao Iniciante", "2734907", "",true, true, true),
	BRI("Brasil Investimentos", "1982777", "",true, true, true),
	EBB("eBanco do Brasil", "2012545", "",true, true, true),
	IN1("Infantaria 1.CIA", "3338875", "",true, true, true),
	EN2("Empresas Nacionais II", "1209945","", true, true, true),
	EN3("Empresas Nacionais III", "1211496", "",true, true, true),
	EN5("Empresas Nacionais V", "2161715", "",true, true, true),
	EN6("Empresas Nacionais VI", "2230101", "",true, true, true),
	EN7("Empresas Nacionais VII", "2230104", "",true, true, true),
	EN8("Empresas Nacionais VIII", "2328924", "",true, true, true),
	EN9("Empresas Nacionais IX", "2329041", "",true, true, true),
	EN10("Empresas Nacionais X", "2329135", "",true, true, true),
	EN11("Empresas Nacionais XI", "2713853", "",true, true, true),
	EN12("Empresas Nacionais XII", "2734903", "",true, true, true),
	SIS("Empresas SIS", "1271957", "",true, true, true),
	GUA("Guara", "2523731", "",true, true, true),
	MF("Ministerio da Fazenda", "1616081", "",true, true, true),

	DIV2("---------", "", "",false, false),
	
	ANA("Anaconda 1.CIA", "1800970", "",false, false, true),
	ARI("Ariranha", "2523757", "",false, false, true),
	EMB("Embassy of eBrazil", "2055913", "",false, false, true),
	EN4("Empresas Nacionais IV", "1211497", "",false, false, true), //CS USA
	JAR("Jararaca", "2523694", "",false, false, true),
	JIB("Jiboia 1.CIA", "3590661", "",false, false, true),
	LOF("Loteria Federal", "2012551", "",false, false, true),
	MOF("MoFA do Brasil", "1531963", "",false, false, true),
	MDE("Ministerio da Educacao", "4022881", "",false, false, true),
	MDC("Ministerio das Comunicacoes", "1464032", "",false, false, true),
	SAM("SAM eBR", "3469919", "",false, false, true),
	SSP("Congresso eBrasileiro", "1262629", "ex-SISPRO",false, false, true),
	URU("Urutu 1.cia", "1482818", "",false, false, true),
	MDS("Ministerio da Solidariedade", "1488241", "",false, false, true),
	
	DIV3("---------", "", "",false, false),
	
	AGN("Agulhas Negras", "2956933", "(F.A.B.)",false, false, true),
	BCS("BANCO DA CAIPIRINHA", "1599565", "(Caipirinha Squad)",false, false),
	CAS("Cascavel 1.CIA", "1482831", "(BrNuke)",false, false, true),
	CEB("Central do Exercito", "3977944", "(EB)", true, true, true), 
	EN1("Empresas Nacionais I", "1209944", "(F.O.D.A.C.E)",false, false, true),
	IN2("Infantaria 2.CIA", "3338884", "(SU)",true, true, true),
	

//	DIV4("---------", "", "",false, false),
//	ESA("Reserve Bank of South Africa",	"1214517", "(África do Sul)",false, false),
//	ARG("Gobierno de Argentina", "1244160", "(Argentina)",false, false),
//	CHI("Chilean Organization", "1275437", "(Chile)",false, false),
//	COL("Banco de la Republica", "1583388", "(Colômbia)",false, false),
//	USA("Congressional Budget Office", "1384821", "(EUA)",false, false),
//	PER("CONGRESO del ePERU", "2417346", "(Peru)",false, false),
//	TWO("TWO Alliance", "3118612", "",false, false)
	;
	
	
	public String	name;
	public String	id;
	public String prefix;
	
	public boolean trackTransactions;
	public boolean isPublic;
	public boolean notifyGold;
	
	private CountryAccount(String name, String id, String prefix, boolean trackTransactions, boolean isPublic) {
		this(name, id, prefix, trackTransactions, isPublic, false);
	}
	
	private CountryAccount(String name, String id, String prefix, boolean trackTransactions, boolean isPublic, boolean notifyGold) {
		this.name = name;
		this.id = id;
		this.prefix = prefix;
		this.trackTransactions = trackTransactions;
		this.isPublic = isPublic;
		this.notifyGold = notifyGold;
	}
	
	public static CountryAccount fromId(String id){
		for (CountryAccount acc : CountryAccount.values())
			if(acc.id != null && acc.id.equals(id))
				return acc;
		return null;
	}
}
