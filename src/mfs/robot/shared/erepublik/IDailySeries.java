package mfs.robot.shared.erepublik;

public interface IDailySeries {

	public Integer geteRepDay();

	public Integer geteRepTime();

	public Double getAverage();

	public Double getMinimum();

} 
