package mfs.robot.shared.erepublik;

import com.google.gwt.user.client.rpc.IsSerializable;

public enum FinancialIndexType implements IsSerializable
{
	GOLD("Valor Médio do Ouro", true, true),
	REVENUE("Arrecadação (BRL)", true, false),
	JOBS("Salário (Bruto)", true, true),
	WQ7("Armas Q7 (Preço médio)", true, true)
	;

	private final String	title;
	private final boolean	canBeDivided;
	private final boolean	canDivide;

	private FinancialIndexType(String title, boolean canBeDivided,
			boolean canDivide) {
		this.title = title;
		this.canBeDivided = canBeDivided;
		this.canDivide = canDivide;
	}

	public String getTitle() {
		return title;
	}

	public boolean isCanBeDivided() {
		return canBeDivided;
	}

	public boolean isCanDivide() {
		return canDivide;
	}

}
