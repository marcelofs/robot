package mfs.robot.shared.erepublik;

import java.util.Date;

import mfs.robot.shared.erepublik.data.MonetaryType;

public interface ITransaction {

	public abstract Integer geteRepDay();

	public abstract void seteRepDay(Integer eRepDay);

	public abstract Double getValue();

	public abstract void setValue(Double value);

	public abstract MonetaryType getType();

	public abstract void setType(MonetaryType type);

	public abstract String getPersonResponsible();

	public abstract void setPersonResponsible(String personResponsible);

	public abstract String getDestination();

	public abstract void setDestination(String destination);

	public abstract String getObservations();

	public abstract void setObservations(String observations);

	public abstract Long getId();

	public abstract String getRecordedById();

	public abstract void setRecordedById(String recordedById);

	public abstract Date getRecordedOn();

	public abstract void setRecordedOn(Date recordedOn);

	public abstract Date getDeletedOn();

	public abstract void setDeletedOn(Date deletedOn);

	public abstract String getDeletedById();

	public abstract void setDeletedById(String deletedById);

	public abstract String getOrigin();

	public abstract void setOrigin(String origin);

}
