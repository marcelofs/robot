package mfs.robot.shared.admin;

import java.io.Serializable;
import java.util.Date;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.gwt.user.client.rpc.IsSerializable;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class eRepUser implements Serializable, IsSerializable {

	private static final long	serialVersionUID	= 1L;

	@PrimaryKey
	@Persistent
	private String				gId;

	@Persistent
	private String				gEmail;

	@Persistent
	private String				eRepId;

	@Persistent
	private String				eRepName;

	@Persistent
	private Date				registered;

	// se o usuário já selecionou as permissões?
	@Persistent
	private Boolean				isAuthorized;

	// access to /MdF/
	@Persistent
	private Boolean				isMdF;

	@SuppressWarnings("unused")
	private eRepUser() {}

	public eRepUser(String gId) {
		this.gId = gId;
	}

	public String getgId() {
		return gId;
	}

	public String getgEmail() {
		return gEmail;
	}

	public void setgEmail(String gEmail) {
		this.gEmail = gEmail;
	}

	public String geteRepId() {
		return eRepId;
	}

	public void seteRepId(String eRepId) {
		if (eRepId != null && eRepId.trim().isEmpty())
			this.eRepId = null;
		else
			this.eRepId = eRepId;
	}

	public String geteRepName() {
		return eRepName;
	}

	public void seteRepName(String eRepName) {
		if (eRepName != null && eRepName.trim().isEmpty())
			this.eRepName = null;
		else
			this.eRepName = eRepName;
	}

	public Date getRegistered() {
		return registered;
	}

	public void setRegistered(Date registered) {
		this.registered = registered;
	}

	public Boolean isAuthorized() {
		return isAuthorized;
	}

	public void setIsAuthorized(Boolean isAuthorized) {
		this.isAuthorized = isAuthorized;
	}

	public Boolean getIsMdF() {
		if (isMdF == null)
			return false;
		return isMdF;
	}

	public void setIsMdF(Boolean isMdF) {
		this.isMdF = isMdF;
	}

}
