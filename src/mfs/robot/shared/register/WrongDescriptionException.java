package mfs.robot.shared.register;

public class WrongDescriptionException extends Exception {

	private static final long	serialVersionUID	= 1L;

	public WrongDescriptionException() {
		super(
				"Você deve adicionar o código acima à descrição no seu perfil do jogo!"); 
	}

}
