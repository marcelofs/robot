package mfs.robot.shared.mdf;

import java.io.Serializable;
import java.util.Collection;

import mfs.robot.shared.erepublik.data.CountryPresident;
import mfs.robot.shared.erepublik.data.CountryTreasury;

import com.google.gwt.user.client.rpc.IsSerializable;

public class MdFInitialInfo implements Serializable, IsSerializable {

	private static final long	serialVersionUID	= 1L;

	public Boolean				alreadyExists;

	public Integer				eRepDay;

	public CountryTreasury		treasuryInTheDay;

	public CountryTreasury		treasuryTheDayBefore;

	public CountryPresident		currentPresident;

	public Collection<String>	mdfUsers;
}
