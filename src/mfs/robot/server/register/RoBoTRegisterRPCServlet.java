package mfs.robot.server.register;

import static com.google.appengine.api.urlfetch.FetchOptions.Builder.withDeadline;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import mfs.robot.client.register.RoBoTRegisterRPC;
import mfs.robot.server.Updater;
import mfs.robot.server.erepublik.data.UserDAO;
import mfs.robot.server.erepublik.scrapper.ScrapperURL;
import mfs.robot.shared.admin.eRepUser;
import mfs.robot.shared.register.WrongDescriptionException;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class RoBoTRegisterRPCServlet extends RemoteServiceServlet implements
		RoBoTRegisterRPC {

	private static final long	serialVersionUID	= 1L;

	private URLFetchService		fetchService		= URLFetchServiceFactory
															.getURLFetchService();
	private Updater				updater				= new Updater();

	private static final Logger	log					= Logger.getLogger(RoBoTRegisterRPCServlet.class
															.getName());

	@Override
	public String getIdentString() throws Exception {

		eRepUser eUser = getCurrentUser();
		if (eUser == null)
			throw new IllegalArgumentException("Usuário inválido");
		if (eUser.geteRepId() != null)
			throw new IllegalArgumentException("Usuário já registrado?");

		return buildVerifId(eUser);
	}

	@Override
	public String verifUser(String eId) throws Exception {

		if (eId == null || eId.trim().isEmpty()
				|| !StringUtils.containsOnly(eId, "0123456789"))
			throw new IllegalArgumentException("Sua ID é inválida");

		eRepUser eUser = getCurrentUser();
		JSONParser parser = new JSONParser();
		URL url = new URL(ScrapperURL.CITIZEN.getUrl(eId));

		HTTPResponse response = fetchService.fetch(new HTTPRequest(url,
				HTTPMethod.GET, withDeadline(60)));

		String html = new String(response.getContent(),
				Charset.defaultCharset());
		try {
			JSONObject object = (JSONObject) parser.parse(html);

			if (!object.get("description").toString()
					.contains(buildVerifId(eUser)))
				throw new WrongDescriptionException();

			eUser.seteRepId(eId);
			eUser.seteRepName(object.get("name").toString());
			eUser.setRegistered(new Date());

			updater.save(eUser);

		} catch (ParseException e) {
			log.log(Level.WARNING, "Could not parse json: '" + html + "'", e);
			throw new IOException("error parsing json", e);
		}

		return eUser.geteRepName();
	}

	private eRepUser getCurrentUser() {
		User gUser = UserServiceFactory.getUserService().getCurrentUser();
		return new UserDAO().geteRepUserUncached(gUser);
	}

	private String buildVerifId(eRepUser eUser) {
		return "[gId#" + eUser.getgId() + "]";
	}

}
