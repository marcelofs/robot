package mfs.robot.server.mdf;

import java.util.Collection;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import mfs.robot.client.erepublik.rpc.RoBoTMdFRPC;
import mfs.robot.server.Updater;
import mfs.robot.server.admin.AdminDAO;
import mfs.robot.server.cache.CacheFacade;
import mfs.robot.server.erepublik.eRepTimeCalculator;
import mfs.robot.server.erepublik.data.CountryDAO;
import mfs.robot.server.erepublik.data.ExpensesDAO;
import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.CountryIncome;
import mfs.robot.shared.erepublik.data.CountryMoneyTransfer;
import mfs.robot.shared.erepublik.data.CountryPresident;
import mfs.robot.shared.erepublik.data.CountryTreasury;
import mfs.robot.shared.mdf.MdFInitialInfo;
import mfs.robot.shared.mdf.MdFLog;

import com.google.appengine.api.users.UserServiceFactory;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class RoBoTMdFRPCServlet extends RemoteServiceServlet implements
		RoBoTMdFRPC {

	private static final long	serialVersionUID	= 1L;

	private static Logger		log					= Logger.getLogger(RoBoTMdFRPCServlet.class
															.getName());

	private static MdFLogger	mdfLogger			= new MdFLogger();

	private CountryDAO			country				= new CountryDAO();

	private ExpensesDAO			expenses			= new ExpensesDAO();

	private AdminDAO			users				= new AdminDAO();

	@Override
	public void saveManualRevenue(String day, String revenue, String comments)
			throws Exception {

		try {

			Integer parsedDay = Integer.valueOf(day.trim());
			Double parsedRevenue = Double.valueOf(revenue.trim());

			CountryTreasury treasury = country.getCountryTreasury(parsedDay);

			if (treasury == null && parsedDay < eRepTimeCalculator.geteRepDay()) {
				treasury = new CountryTreasury();
				treasury.setCurrencyTreasury(0d);
				treasury.seteRepDay(parsedDay);
				treasury.setGoldTreasury(0d);
			}

			treasury.setManualMdFTaxRevenue(parsedRevenue);
			treasury.setComments(comments.trim().isEmpty() ? null : comments
					.trim());

			new Updater().save(treasury);
			mdfLogger.log(treasury);

			CacheFacade
					.remove("mfs.robot.server.erepublik.data.CountryDAO.getCountryTreasuryHistory()");
			CacheFacade
					.remove("mfs.robot.server.erepublik.data.CountryDAO.getCountryTreasury(Integer="
							+ parsedDay + ";)");
		} catch (Exception e) {
			log.log(Level.WARNING, e.getMessage(), e);
			throw e;
		}

	}

	@Override
	public MdFInitialInfo getRevenueInitialInfo() {
		return getRevenueInfo(eRepTimeCalculator.geteRepDay() - 1);
	}

	@Override
	public MdFInitialInfo getRevenueInfo(Integer day) {
		MdFInitialInfo info = new MdFInitialInfo();

		info.eRepDay = day;

		CountryTreasury t = country.getCountryTreasury(day);

		info.alreadyExists = t != null && t.getManualMdFTaxRevenue() != null;
		info.treasuryInTheDay = t;
		info.treasuryTheDayBefore = country.getCountryTreasury(day - 1);

		info.currentPresident = country.getPresidents().iterator().next();
		info.mdfUsers = users.getMdFUsers();

		return info;
	}

	@Override
	public Collection<MdFLog> getTopLogs() {
		return new LogDAO().getTopLogs();
	}

	@Override
	public Collection<CountryExpense> getExpenses(Integer day) {
		return new ExpensesDAO().getExpenses(day);
	}

	@Override
	public Collection<CountryIncome> getIncomes(Integer day) {
		return new ExpensesDAO().getIncomes(day);
	}

	@Override
	public Collection<CountryMoneyTransfer> getTransfers(Integer day) {
		return new ExpensesDAO().getTransfers(day);
	}

	@Override
	public boolean addExpense(CountryExpense e) {
		e.setRecordedOn(new Date());
		e.setRecordedById(UserServiceFactory.getUserService().getCurrentUser()
				.getUserId());
		new Updater().save(e);
		mdfLogger.log(e);

		CacheFacade
				.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getExpenses(Integer="
						+ e.geteRepDay() + ";)");
		CacheFacade
				.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getExpensesSum()");
		CacheFacade
				.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getExpensesHistory(String="
						+ e.getOrigin() + ";)");

		CountryPresident cp = getLastPresident();
		CacheFacade
				.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getExpensesHistory(Integer="
						+ (cp.getFirstErepDay() - 1) + ";)");

		return true;
	}

	@Override
	public boolean deleteExpense(CountryExpense e) {
		new ExpensesDAO().delete(e);
		mdfLogger.logDeletion(e);

		CacheFacade
				.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getExpenses(Integer="
						+ e.geteRepDay() + ";)");
		CountryPresident cp = getLastPresident();
		CacheFacade
				.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getExpensesHistory(Integer="
						+ (cp.getFirstErepDay() - 1) + ";)");
		CacheFacade
				.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getExpensesHistory(String="
						+ e.getOrigin() + ";)");
		CacheFacade
				.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getExpensesSum()");
		return true;
	}

	@Override
	public boolean addIncome(CountryIncome e) {
		e.setRecordedOn(new Date());
		e.setRecordedById(UserServiceFactory.getUserService().getCurrentUser()
				.getUserId());
		new Updater().save(e);
		mdfLogger.log(e);

		CacheFacade
				.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getIncomes(Integer="
						+ e.geteRepDay() + ";)");
		CacheFacade
				.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getIncomesHistory()");

		CacheFacade
				.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getIncomesHistory(String="
						+ e.getDestination() + ";)");

		CountryPresident cp = getLastPresident();
		CacheFacade
				.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getIncomesHistory(Integer="
						+ (cp.getFirstErepDay() - 1) + ";)");
		return true;
	}

	@Override
	public boolean deleteIncome(CountryIncome e) {
		new ExpensesDAO().delete(e);
		mdfLogger.logDeletion(e);

		CacheFacade
				.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getIncomes(Integer="
						+ e.geteRepDay() + ";)");
		CacheFacade
				.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getIncomesHistory()");

		CacheFacade
				.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getIncomesHistory(String="
						+ e.getDestination() + ";)");
		CountryPresident cp = getLastPresident();
		CacheFacade
				.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getIncomesHistory(Integer="
						+ (cp.getFirstErepDay() - 1) + ";)");
		return true;
	}

	@Override
	public boolean addTransfer(CountryMoneyTransfer e) {
		e.setRecordedOn(new Date());
		e.setRecordedById(UserServiceFactory.getUserService().getCurrentUser()
				.getUserId());
		new Updater().save(e);
		mdfLogger.log(e);

		CacheFacade
				.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getTransfers(Integer="
						+ e.geteRepDay() + ";)");
		CacheFacade
				.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getTransfersHistory()");
		return true;
	}

	@Override
	public boolean deleteTransfer(CountryMoneyTransfer e) {
		new ExpensesDAO().delete(e);
		mdfLogger.logDeletion(e);

		CacheFacade
				.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getTransfers(Integer="
						+ e.geteRepDay() + ";)");
		CacheFacade
				.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getTransfersHistory()");
		return true;
	}

	private CountryPresident getLastPresident() {
		return country.getPresidents().iterator().next();
	}
}
