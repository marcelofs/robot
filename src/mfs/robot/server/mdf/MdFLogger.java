package mfs.robot.server.mdf;

import java.text.NumberFormat;
import java.util.Date;

import mfs.robot.server.Updater;
import mfs.robot.server.cache.CacheFacade;
import mfs.robot.server.erepublik.data.UserDAO;
import mfs.robot.shared.admin.eRepUser;
import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.CountryIncome;
import mfs.robot.shared.erepublik.data.CountryMoneyTransfer;
import mfs.robot.shared.erepublik.data.CountryTreasury;
import mfs.robot.shared.mdf.MdFLog;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class MdFLogger {

	private MdFLog newLog() {
		MdFLog log = new MdFLog();

		log.setRecordedOn(new Date());

		UserService userService = UserServiceFactory.getUserService();
		User gUser = userService.getCurrentUser();

		if (gUser != null) {
			eRepUser eUser = new UserDAO().geteRepUser(gUser);
			log.setUserName(eUser.geteRepName());
		} else {
			log.setUserName("RoBoT");
		}

		return log;
	}

	public void log(CountryTreasury treasury) {
		MdFLog log = newLog();

		log.setMessage("Definiu o valor de arrecadação para o dia "
				+ treasury.geteRepDay() + " como "
				+ format(treasury.getManualMdFTaxRevenue()) + " BRL.");

		new Updater().save(log);

		CacheFacade.remove("mfs.robot.server.mdf.LogDAO.getTopLogs()");
	}

	public void log(CountryExpense expense) {
		MdFLog log = newLog();

		String category = expense.getCategory() != null ? expense.getCategory()
				.getName() : " ?? ";
		String type = expense.getType() != null ? expense.getType().toString()
				.toLowerCase() : " ?? ";

		log.setMessage("Adicionou um gasto (" + category + ") de "
				+ format(expense.getValue()) + " " + type + " no dia "
				+ expense.geteRepDay());

		new Updater().save(log);
		CacheFacade.remove("mfs.robot.server.mdf.LogDAO.getTopLogs()");
	}

	public void log(CountryIncome income) {
		MdFLog log = newLog();

		String type = income.getType() != null ? income.getType().toString()
				.toLowerCase() : " ?? ";

		log.setMessage("Adicionou uma entrada de " + format(income.getValue())
				+ " " + type + " no dia " + income.geteRepDay());

		new Updater().save(log);
		CacheFacade.remove("mfs.robot.server.mdf.LogDAO.getTopLogs()");
	}

	public void log(CountryMoneyTransfer transfer) {
		MdFLog log = newLog();

		String type = transfer.getType() != null ? transfer.getType()
				.toString().toLowerCase() : " ?? ";

		log.setMessage("Adicionou uma transferência de "
				+ format(transfer.getValue()) + " " + type + " no dia "
				+ transfer.geteRepDay());

		new Updater().save(log);
		CacheFacade.remove("mfs.robot.server.mdf.LogDAO.getTopLogs()");
	}

	private String format(Double value) {
		return NumberFormat.getNumberInstance().format(value);
	}

	public void logDeletion(CountryExpense expense) {
		MdFLog log = newLog();

		String category = expense.getCategory() != null ? expense.getCategory()
				.getName() : " ?? ";
		String type = expense.getType() != null ? expense.getType().toString()
				.toLowerCase() : " ?? ";

		log.setMessage("Excluiu um gasto (" + category + ") de "
				+ format(expense.getValue()) + " " + type + " no dia "
				+ expense.geteRepDay() + " (id: " + expense.getId() + ")");

		new Updater().save(log);
		CacheFacade.remove("mfs.robot.server.mdf.LogDAO.getTopLogs()");

	}

	public void logDeletion(CountryIncome income) {
		MdFLog log = newLog();

		String type = income.getType() != null ? income.getType().toString()
				.toLowerCase() : " ?? ";

		log.setMessage("Excluiu uma entrada de " + format(income.getValue())
				+ " " + type + " no dia " + income.geteRepDay() + " (id: "
				+ income.getId() + ")");

		new Updater().save(log);
		CacheFacade.remove("mfs.robot.server.mdf.LogDAO.getTopLogs()");

	}

	public void logDeletion(CountryMoneyTransfer transfer) {
		MdFLog log = newLog();

		String type = transfer.getType() != null ? transfer.getType()
				.toString().toLowerCase() : " ?? ";

		log.setMessage("Excluiu uma transferência de "
				+ format(transfer.getValue()) + " " + type + " no dia "
				+ transfer.geteRepDay() + " (id: " + transfer.getId() + ")");

		new Updater().save(log);
		CacheFacade.remove("mfs.robot.server.mdf.LogDAO.getTopLogs()");

	}
}
