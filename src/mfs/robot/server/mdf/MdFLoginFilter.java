package mfs.robot.server.mdf;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import mfs.robot.server.erepublik.data.UserDAO;
import mfs.robot.shared.admin.eRepUser;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class MdFLoginFilter implements Filter {

	private static Logger	log	= Logger.getLogger(MdFLoginFilter.class
										.getName());

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {}

	@Override
	public void destroy() {}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain filterChain) throws IOException, ServletException {

		UserService userService = UserServiceFactory.getUserService();

		if (!userService.isUserLoggedIn()) {
			((HttpServletResponse) resp).sendError(401);
			log.log(Level.SEVERE, "User is not logged-in in filter");
			return;
		}

		User gUser = userService.getCurrentUser();
		eRepUser eUser = new UserDAO().geteRepUser(gUser);

		if (!eUser.getIsMdF()) {
			((HttpServletResponse) resp).sendError(403);
			return;
		}

		filterChain.doFilter(req, resp);

	}
}
