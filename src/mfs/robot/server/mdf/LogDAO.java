package mfs.robot.server.mdf;

import java.util.Collection;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import mfs.robot.server.PMF;
import mfs.robot.server.cache.Cacheable;
import mfs.robot.shared.mdf.MdFLog;

public class LogDAO {

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<MdFLog> getTopLogs() {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(MdFLog.class);
			q.setOrdering("recordedOn desc");
			q.setRange(0, 20);
			return pm.detachCopyAll((List<MdFLog>) q.execute());
		} finally {
			pm.close();
		}
	}

}
