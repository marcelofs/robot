package mfs.robot.server.bip;

import static mfs.robot.server.erepublik.eRepTimeCalculator.geteRepDay;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mfs.robot.server.erepublik.data.MarketDAO;
import mfs.robot.shared.erepublik.data.MonetaryOffer;
import mfs.robot.shared.erepublik.data.MonetaryType;

import org.apache.commons.io.IOUtils;
import org.apache.http.Consts;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;
import org.esxx.js.protocol.GAEConnectionManager;

public class BIPUpdateGoldTask extends HttpServlet {

	private static final long	serialVersionUID	= 1L;
	private static final Logger	logger				= Logger.getLogger(BIPUpdateGoldTask.class
															.getName());

	private MarketDAO			dao					= new MarketDAO();

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doPost(req, resp);
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		DefaultHttpClient client = createNewHttpClient();

		HttpPost httpPost = new HttpPost("http://r0b0t.site90.net/gold.php");

		List<NameValuePair> nvps = new ArrayList<NameValuePair>(2);
		nvps.add(new BasicNameValuePair("key", "yup61Eplt5HBFfB"));
		nvps.add(new BasicNameValuePair("data", getArrayDT()));

		logger.log(Level.INFO, "POSTing new gold data to BIP...");

		httpPost.setEntity(new UrlEncodedFormEntity(nvps, Consts.UTF_8));
		HttpResponse postResponse = client.execute(httpPost);
		read(postResponse);

		logger.log(Level.INFO, "POST successfull!");

	}

	private String read(HttpResponse response) throws IOException {
		String html = IOUtils.toString(response.getEntity().getContent(),
				"UTF-8");
		EntityUtils.consume(response.getEntity());
		return html;
	}

	private String getArrayDT() {
		Collection<MonetaryOffer> offers = dao.getDailyMonetaryOffers(
				MonetaryType.GOLD, geteRepDay());

		StringBuilder builder = new StringBuilder();
		builder.append("["
				+ "['Horário no Servidor', 'Valor médio', 'Valor Mínimo']");

		int i = 0;
		for (MonetaryOffer offer : offers) {
			builder.append(",\n").append("[\"").append(offer.geteRepTime())
					.append("h00\'\"").append(", ").append(offer.getAverage())
					.append(", ").append(offer.getMinimum()).append("]");
			i++;
		}

		for (; i <= 24; i++)
			appendNew(builder, i);

		builder.append("]");

		return builder.toString();
	}

	private void appendNew(StringBuilder builder, int time) {
		builder.append(",\n").append("[\"").append(time).append("h00\'\"")
				.append(", null, null]");
	}

	private DefaultHttpClient createNewHttpClient() {

		String userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20100101 Firefox/15.0.1";

		DefaultHttpClient client = new DefaultHttpClient(
				new GAEConnectionManager());

		client.getParams().setParameter(ClientPNames.COOKIE_POLICY,
				CookiePolicy.BROWSER_COMPATIBILITY);

		client.getParams().setParameter(CoreProtocolPNames.USER_AGENT,
				userAgent);

		client.getParams().setParameter("http.socket.timeout", 0);

		return client;
	}

}
