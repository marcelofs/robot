package mfs.robot.server;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Mailer {

	/**
	 * @param nameEmails
	 *            [name][email]
	 */
	public void mail(String subject, String message, String[]... nameEmails)
			throws IOException {

		try {
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);

			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress("RoBoT.erep@gmail.com"));

			for (String[] nameEmail : nameEmails)
				msg.addRecipient(Message.RecipientType.TO, new InternetAddress(
						nameEmail[1], nameEmail[0]));

			msg.setSubject(subject);
			msg.setContent(message, "text/html");
			Transport.send(msg);
		} catch (Exception e) {
			throw new IOException(e);
		}
	}

}
