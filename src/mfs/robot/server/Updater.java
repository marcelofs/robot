package mfs.robot.server;

import java.util.Collection;

import javax.jdo.PersistenceManager;


public class Updater {

	public void save(Object obj) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			pm.makePersistent(obj);
		} finally {
			pm.close();
		}
	}

	public void save(Collection<? extends Object> objs) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			pm.makePersistentAll(objs);
		} finally {
			pm.close();
		}
	}
}
