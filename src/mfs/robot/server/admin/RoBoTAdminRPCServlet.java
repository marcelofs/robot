package mfs.robot.server.admin;

import java.util.Collection;
import java.util.Date;

import org.joda.time.LocalDate;

import mfs.robot.client.erepublik.rpc.RoBoTAdminRPC;
import mfs.robot.server.Updater;
import mfs.robot.server.cache.CacheFacade;
import mfs.robot.server.erepublik.eRepTimeCalculator;
import mfs.robot.shared.admin.eRepUser;
import mfs.robot.shared.erepublik.data.CountryPresident;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class RoBoTAdminRPCServlet extends RemoteServiceServlet implements
		RoBoTAdminRPC {

	private static final long	serialVersionUID	= 1L;

	private AdminDAO			adminDAO			= new AdminDAO();
	private Updater				updater				= new Updater();

	@Override
	public Collection<eRepUser> getUsers() {
		return adminDAO.getAlleRepUsers();
	}

	@Override
	public void saveUser(eRepUser user) {
		updater.save(user);
		CacheFacade
				.remove("mfs.robot.server.erepublik.data.UserDAO.geteRepUser(User="
						+ user.getgEmail() + ";)");
	}

	@Override
	public boolean createPresident(String name, Date firstDayDate,
			Boolean showFinanceTables, Boolean showFinanceGraphs) {

		CountryPresident cp = new CountryPresident();
		cp.setName(name);
		cp.setShowFinanceGraphs(showFinanceGraphs);
		cp.setShowFinanceTables(showFinanceTables);

		LocalDate firstDay = new LocalDate(firstDayDate);
		LocalDate lastDay = firstDay.plusMonths(1);

		cp.setFirstErepDay(eRepTimeCalculator.toErepDate(firstDay));
		cp.setLastErepDay(eRepTimeCalculator.toErepDate(lastDay));

		updater.save(cp);

		CacheFacade
				.remove("mfs.robot.server.erepublik.data.CountryDAO.getPresidents()");

		return true;
	}

}
