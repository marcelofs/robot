package mfs.robot.server.admin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import mfs.robot.server.PMF;
import mfs.robot.server.cache.Cacheable;
import mfs.robot.shared.admin.eRepUser;

public class AdminDAO {

	@SuppressWarnings("unchecked")
	public Collection<eRepUser> getAlleRepUsers() {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(eRepUser.class);
			q.setOrdering("registered desc");
			return pm.detachCopyAll((List<eRepUser>) q.execute());
		} finally {
			pm.close();
		}
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<String> getMdFUsers() {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(eRepUser.class);
			q.setFilter("isMdF == true");
			List<eRepUser> users = (List<eRepUser>) q.execute();

			List<String> usernames = new ArrayList<>();
			for (eRepUser u : users)
				usernames.add(u.geteRepName());

			return usernames;
		} finally {
			pm.close();
		}
	}
}
