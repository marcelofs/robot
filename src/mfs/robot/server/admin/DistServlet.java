package mfs.robot.server.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.http.Consts;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;
import org.esxx.js.protocol.GAEConnectionManager;

public class DistServlet extends HttpServlet {

	private static final long	serialVersionUID	= 1L;

	private static final Logger	log					= Logger.getLogger(DistServlet.class
															.getName());

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		log.warning("Inicializando pedido de dist...");

		DefaultHttpClient client = new DefaultHttpClient(
				new GAEConnectionManager());

		client.getParams().setParameter(ClientPNames.COOKIE_POLICY,
				CookiePolicy.BROWSER_COMPATIBILITY);

		client.getParams()
				.setParameter(CoreProtocolPNames.USER_AGENT,
						"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20100101 Firefox/15.0.1");

		// GET http://trolls.webuda.com/form/FAB_OFF
		// GET http://trolls.webuda.com/form/getuser.php?q=nW0lf&m=FAB_OFF
		// POST http://trolls.webuda.com/form/process/test.php
		// GET http://trolls.webuda.com/form/process/acompanha.php
		// GET http://trolls.webuda.com/form/process/envio.php?cap=&resp=

		HttpGet httpGet = new HttpGet("http://trolls.webuda.com/form/FAB_OFF");
		read(client.execute(httpGet));

		httpGet = new HttpGet(
				"http://trolls.webuda.com/form/getuser.php?q=nW0lf&m=FAB_OFF");
		read(client.execute(httpGet));

		HttpPost httpPost = new HttpPost(
				"http://trolls.webuda.com/form/process/test.php");

		List<NameValuePair> nvps = new ArrayList<NameValuePair>(6);
		nvps.add(new BasicNameValuePair("mu", "FAB_OFF"));
		nvps.add(new BasicNameValuePair("numlutas", "10"));
		nvps.add(new BasicNameValuePair("submit.x", "43"));
		nvps.add(new BasicNameValuePair("submit.y", "15"));
		nvps.add(new BasicNameValuePair("tipo", "tudo"));
		nvps.add(new BasicNameValuePair("username", "nW0lf"));

		httpPost.setEntity(new UrlEncodedFormEntity(nvps, Consts.UTF_8));
		read(client.execute(httpPost));

		httpGet = new HttpGet(
				"http://trolls.webuda.com/form/process/acompanha.php");
		read(client.execute(httpGet));

		httpGet = new HttpGet(
				"http://trolls.webuda.com/form/process/envio.php?cap=&resp=");
		String html = read(client.execute(httpGet));

		if (html.contains("Envio completo"))
			log.warning("Pedido de dist efetuado com sucesso");
		else
			log.warning("Erro no pedido de dist, resultado: " + html);

	}

	private String read(HttpResponse response) throws IOException {
		String html = IOUtils.toString(response.getEntity().getContent(),
				"UTF-8");
		EntityUtils.consume(response.getEntity());
		return html;
	}

}
