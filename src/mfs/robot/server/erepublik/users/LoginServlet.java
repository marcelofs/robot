package mfs.robot.server.erepublik.users;

import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.PersistenceManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mfs.robot.server.PMF;
import mfs.robot.server.Updater;
import mfs.robot.shared.admin.eRepUser;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class LoginServlet extends HttpServlet {

	private static final long	serialVersionUID	= 1L;
	private static Logger		log					= Logger.getLogger(LoginServlet.class
															.getName());

	private final Updater		updater				= new Updater();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		UserService userService = UserServiceFactory.getUserService();

		if (!userService.isUserLoggedIn()) {
			log.log(Level.SEVERE, "User is not logged-in in login page");
			resp.sendError(401);
			return;
		}

		User gUser = userService.getCurrentUser();

		eRepUser eUser = geteRepUser(gUser);

		if (eUser == null)
			eUser = newUser(gUser);

		if (!isFullyRegistered(eUser)) {
			resp.sendRedirect("/login/register/");
			return;
		}

		if (eUser.isAuthorized() == null || eUser.isAuthorized() == true) {
			resp.sendRedirect("/");
			return;
		}

		resp.getWriter().println("<h1>403: Forbidden</h1>");
		resp.getWriter()
				.println(
						"Seu usu&aacute;rio ainda n&atilde;o foi autorizado. Favor entrar em contato com o administrador no canal #RoBoT, "
								+ "rede Rizon.");

	}

	private boolean isFullyRegistered(eRepUser eUser) {
		return eUser.geteRepId() != null && eUser.geteRepName() != null;
	}

	private eRepUser newUser(User gUser) {
		eRepUser eUser = new eRepUser(gUser.getUserId());
		eUser.setgEmail(gUser.getEmail());
		// eUser.setIsAuthorized(false);
		eUser.setRegistered(new Date());

		updater.save(eUser);

		log.log(Level.WARNING, "new user: id=" + gUser.getUserId() + ", email="
				+ gUser.getEmail());

		return eUser;
	}

	private eRepUser geteRepUser(User gUser) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			try {
				return pm.getObjectById(eRepUser.class, gUser.getUserId());
			} catch (JDOObjectNotFoundException e) {
				return null;
			}
		} finally {
			pm.close();
		}
	}
}
