package mfs.robot.server.erepublik;

import static com.google.appengine.api.taskqueue.TaskOptions.Builder.withUrl;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mfs.robot.shared.erepublik.data.MonetaryType;
import mfs.robot.shared.erepublik.data.ProductQuality;
import mfs.robot.shared.erepublik.data.ProductType;
import mfs.robot.shared.erepublik.data.RocketQuality;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;

public class AgregatorServlet extends HttpServlet {

	private static final long	serialVersionUID	= 1L;

	private static final Logger	log					= Logger.getLogger(AgregatorServlet.class
															.getName());

	@Override
	public void doGet(final HttpServletRequest req,
			final HttpServletResponse res) throws ServletException {

		String day = req.getParameter("day");
		String eRepDay = Integer.toString(eRepTimeCalculator.geteRepDay());

		if (day != null)
			eRepDay = day;

		log.log(Level.INFO, "Starting agregation for day " + eRepDay);

		String agregate = "/erepublik/agregate/task";

		Queue queue = QueueFactory.getDefaultQueue();

		for (ProductType t : ProductType.values())
			for (ProductQuality q : ProductQuality.values())
				queue.add(withUrl(agregate).param("market", "MARKETPLACE")
						.param("type", t.name()).param("quality", q.name())
						.param("day", eRepDay));

		queue.add(withUrl(agregate).param("market", "JOB")
				.param("day", eRepDay));

		// for (MonetaryType t : MonetaryType.values())
		queue.add(withUrl(agregate).param("market", "MONETARY")
				.param("type", MonetaryType.GOLD.name()).param("day", eRepDay));

		// for (ProductType t : ProductType.values())
		// for (ProductQuality q : ProductQuality.values())
		// if (!q.equals(ProductQuality.RAW))
		// queue.add(withUrl("/erepublik/profit")
		// .param("operation",
		// ProfitServlet.Operation.AGREGATE.name())
		// .param("type", t.name()).param("quality", q.name())
		// .param("day", eRepDay.toString()));
		//
		// for (RocketQuality q : RocketQuality.values())
		// queue.add(withUrl("/erepublik/profit")
		// .param("operation",
		// ProfitServlet.Operation.AGREGATE_ROCKET.name())
		// .param("quality", q.name())
		// .param("day", eRepDay.toString()));

	}
}
