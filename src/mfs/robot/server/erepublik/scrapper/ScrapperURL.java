package mfs.robot.server.erepublik.scrapper;

public enum ScrapperURL
{
	CITIZEN_ACCOUNT("http://dev.erpkapi.appspot.com/citizenAcc?id={id}"), 
	CITIZEN("http://dev.erpkapi.appspot.com/citizen?id={id}"),
	PARTY_STATS("http://dev.erpkapi.appspot.com/party?id={id}"),
	PARTY_MEMBERS("http://dev.erpkapi.appspot.com/partyMembers?id={id}"), 
	EGOV_COUNTRY("http://dev.erpkapi.appspot.com/egov4you/countryHistory?id={id}"), 
	COUNTRY_ECONOMY("http://dev.erpkapi.appspot.com/economy?country={id}"), 
	COUNTRY_SOCIETY("http://dev.erpkapi.appspot.com/society?country={id}"),
	COUNTRY_LAWS("http://dev.erpkapi.appspot.com/laws?country={id}");

	private String	url;

	private ScrapperURL(String url) {
		this.url = url;
	}

	public String getUrl(String id) {
		return this.url.replace("{id}", id);
	}
}
