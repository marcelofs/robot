package mfs.robot.server.erepublik.scrapper;

import static com.google.appengine.api.taskqueue.TaskOptions.Builder.withUrl;
import static com.google.appengine.api.urlfetch.FetchOptions.Builder.withDeadline;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mfs.robot.server.PMF;
import mfs.robot.server.Updater;
import mfs.robot.server.cache.CacheFacade;
import mfs.robot.server.erepublik.EmailTask;
import mfs.robot.server.erepublik.eRepTimeCalculator;
import mfs.robot.server.erepublik.data.CountryDAO;
import mfs.robot.server.mdf.MdFLogger;
import mfs.robot.shared.erepublik.CountryAccount;
import mfs.robot.shared.erepublik.data.AgregatedJobMarketOffer;
import mfs.robot.shared.erepublik.data.CitizenAccount;
import mfs.robot.shared.erepublik.data.CountryBonuses;
import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.CountryIncome;
import mfs.robot.shared.erepublik.data.CountryMoneyTransfer;
import mfs.robot.shared.erepublik.data.CountryStats;
import mfs.robot.shared.erepublik.data.CountryTaxes;
import mfs.robot.shared.erepublik.data.CountryTreasury;
import mfs.robot.shared.erepublik.data.ExpenseType;
import mfs.robot.shared.erepublik.data.MonetaryType;
import mfs.robot.shared.erepublik.data.PartyMember;
import mfs.robot.shared.erepublik.data.PoliticalParty;
import mfs.robot.shared.erepublik.data.PoliticalPartyMembers;
import mfs.robot.shared.erepublik.data.PoliticalPartyMembersDiff;
import mfs.robot.shared.erepublik.data.PoliticalPartyStats;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;

public class ErepScrapperTask extends HttpServlet {

	public enum Operation
	{
		CITIZEN_ACCOUNT, PARTY_INFO, PARTY_MEMBERS, PARTY_MEMBERS_DIFF, COUNTRY_EGOV, COUNTRY_ECONOMY, COUNTRY_SOCIETY, SET_MINERS, GET_LAWS, SET_AVG_WAGE;
	}

	private static final long	serialVersionUID	= 1L;
	private final Updater		updater				= new Updater();

	private URLFetchService		fetchService		= URLFetchServiceFactory
															.getURLFetchService();

	private static Logger		log					= Logger.getLogger(ErepScrapperTask.class
															.getName());

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Operation op = Operation.valueOf(req.getParameter("operation"));
		switch (op) {
			case CITIZEN_ACCOUNT:
				getCitizenAccounts(req.getParameter("day"),
						req.getParameterValues("id"));
				break;
			case PARTY_INFO:
				getPartyInfo(req.getParameter("day"), req.getParameter("id"));
				break;
			case PARTY_MEMBERS:
				getPartyMembers(req.getParameter("day"), req.getParameter("id"));
				break;
			case PARTY_MEMBERS_DIFF:
				createPartyDiff(req.getParameter("day"), req.getParameter("id"));
				break;
			case COUNTRY_EGOV:
				getEgovCountryInfo(req.getParameter("day"),
						req.getParameter("statusId"));
				break;
			case COUNTRY_SOCIETY:
				updateCountrySociety(req.getParameter("day"));
				break;
			case COUNTRY_ECONOMY:
				updateCountryEconomy(req.getParameter("day"));
				break;
			case SET_MINERS:
				setMiners(req.getParameter("day"), req.getParameter("miners"));
				break;
			case GET_LAWS:
				getLaws(req.getParameter("day"));
				break;
			case SET_AVG_WAGE:
				setAvgWage(req.getParameter("day"), req.getParameter("avgWage"));
				break;
			default:
				break;
		}
	}

	private void setAvgWage(String day, String avgWage) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(AgregatedJobMarketOffer.class);
			q.setFilter("eRepDay == " + day);
			q.setUnique(true);
			AgregatedJobMarketOffer offer = (AgregatedJobMarketOffer) q
					.execute();
			offer.setOficialAvgSalary(Double.valueOf(avgWage));
			CacheFacade
					.remove("mfs.robot.server.erepublik.data.MarketDAO.getAgregatedJobMarketOffers()");
		} finally {
			pm.close();
		}
	}

	private void getCitizenAccounts(String eRepDay, String[] ids)
			throws IOException {
		JSONParser parser = new JSONParser();

		for (String id : ids) {
			if (id == null || id.trim().isEmpty())
				continue;

			URL url = new URL(ScrapperURL.CITIZEN_ACCOUNT.getUrl(id));
			HTTPResponse response = fetchService.fetch(new HTTPRequest(url,
					HTTPMethod.GET, withDeadline(60)));
			String html = new String(response.getContent(),
					Charset.defaultCharset());

			CitizenAccount account = new CitizenAccount();
			try {
				JSONObject object = (JSONObject) parser.parse(html);
				account.setCurrency(Double.valueOf(object.get("currency")
						.toString()));
				account.seteRepName(object.get("eRepName").toString());
				account.setGold(Double.valueOf(object.get("gold").toString()));
			} catch (ParseException e) {
				throw new IOException("error parsing json", e);
			}

			account.seteRepDay(Integer.valueOf(eRepDay));
			account.seteRepId(Long.valueOf(id));
			updater.save(account);

			if (CountryAccount.fromId(id).notifyGold)
				createNotificationTask(eRepDay, id);

			CacheFacade
					.remove("mfs.robot.server.erepublik.data.CountryDAO.getCitizenAccountHistory(Long="
							+ id + ";)");

		}

	}

	private void createWageTask(String day, JSONObject object) {
		String avgWage = ((JSONObject) object.get("salary")).get("average")
				.toString();
		Queue scrapperQueue = QueueFactory.getQueue("scrapper");
		scrapperQueue.add(withUrl("/erepublik/update/scrapper")
				.param("operation", Operation.SET_AVG_WAGE.name())
				.param("day", day).param("avgWage", avgWage));

	}

	private void updateCountrySociety(String day) throws IOException {
		log.log(Level.INFO, "Starting update for country stats");

		CountryStats stats = new CountryStats();

		URL url = new URL(ScrapperURL.COUNTRY_SOCIETY.getUrl("Brazil"));
		HTTPResponse response = fetchService.fetch(new HTTPRequest(url,
				HTTPMethod.GET, withDeadline(60)));
		String html = new String(response.getContent(),
				Charset.defaultCharset());

		try {
			JSONParser parser = new JSONParser();
			JSONObject jsonObj = (JSONObject) parser.parse(html);

			JSONObject citizens = (JSONObject) jsonObj.get("citizens");
			stats.setActiveCitizens(Integer.valueOf(citizens.get("active")
					.toString()));
			stats.seteRepDay(Integer.valueOf(day)); 
			stats.setNewCitizens(Integer
					.valueOf(citizens.get("new").toString()));

		} catch (ParseException e) {
			throw new IOException(e);
		}

		updater.save(stats);

		Queue defaultQueue = QueueFactory.getQueue("scrapper");
		defaultQueue.add(withUrl("/erepublik/update/scrapper")
				.param("operation",
						ErepScrapperTask.Operation.COUNTRY_EGOV.name())
				.param("statusId", stats.getId().toString())
				.param("day", day.toString()));

		CacheFacade
				.remove("mfs.robot.server.erepublik.data.CountryDAO.getCountryStatsHistory()");
		CacheFacade
				.remove("mfs.robot.server.erepublik.data.CountryDAO.getCountryStats(Integer="
						+ day + ";)");
	}

	private void updateCountryEconomy(String day) throws IOException {
		CountryTreasury treasury = new CountryTreasury();
		CountryTaxes taxes = new CountryTaxes();
		CountryBonuses bonuses = new CountryBonuses();

		Integer eRepDay = Integer.valueOf(day);
		treasury.seteRepDay(eRepDay);
		taxes.seteRepDay(eRepDay);
		bonuses.seteRepDay(eRepDay);

		URL url = new URL(ScrapperURL.COUNTRY_ECONOMY.getUrl("Brazil"));
		HTTPResponse response = fetchService.fetch(new HTTPRequest(url,
				HTTPMethod.GET, withDeadline(60)));
		String html = new String(response.getContent(),
				Charset.defaultCharset());

		try {
			JSONParser parser = new JSONParser();
			JSONObject jsonObj = (JSONObject) parser.parse(html);

			{
				JSONObject jsTreasury = (JSONObject) jsonObj.get("treasury");

				treasury.setGoldTreasury(Double.valueOf(jsTreasury.get("gold")
						.toString()));
				treasury.setCurrencyTreasury(Double.valueOf(jsTreasury
						.get("cc").toString()));

				treasury.setCollectedAt(new Date());

				updater.save(treasury);
			}
			{
				JSONObject jsBonus = (JSONObject) jsonObj.get("bonus");
				bonuses.setFoodBonus(Double.valueOf(jsBonus.get("food")
						.toString()));
				bonuses.setWeaponsBonus(Double.valueOf(jsBonus.get("weapons")
						.toString()));

				updater.save(bonuses);
			}
			{
				JSONObject jsTaxes = (JSONObject) jsonObj.get("taxes");
				taxes.setFoodImport(Double.valueOf(
						jsTaxes.get("foodImport").toString()).intValue());
				taxes.setFoodVat(Double.valueOf(
						jsTaxes.get("foodVAT").toString()).intValue());
				taxes.setFrmImport(Double.valueOf(
						jsTaxes.get("frmImport").toString()).intValue());
				taxes.setIncomeTax(Double.valueOf(
						jsTaxes.get("workTax").toString()).intValue());
				taxes.setWeaponsImport(Double.valueOf(
						jsTaxes.get("weaponsImport").toString()).intValue());
				taxes.setWeaponsVat(Double.valueOf(
						jsTaxes.get("weaponsVAT").toString()).intValue());
				taxes.setWrmImport(Double.valueOf(
						jsTaxes.get("wrmImport").toString()).intValue());

				updater.save(taxes);
			}

			createTaxRevenueTask(treasury);
			createWageTask(day, jsonObj);

			CacheFacade
					.remove("mfs.robot.server.erepublik.data.CountryDAO.getCountryTreasuryHistory()");
			CacheFacade
					.remove("mfs.robot.server.erepublik.data.CountryDAO.getCountryBonusesHistory()");
			CacheFacade
					.remove("mfs.robot.server.erepublik.data.CountryDAO.getCountryTaxesHistory()");
			CacheFacade
					.remove("mfs.robot.server.erepublik.RoBoTRPCServlet.getDayHour()");

		} catch (ParseException e) {
			throw new IOException(e);
		}
	}

	private void createTaxRevenueTask(CountryTreasury treasury) {
		Queue scrapperQueue = QueueFactory.getQueue("scrapper");
		scrapperQueue.add(withUrl("/erepublik/update/scrapper").param(
				"operation", Operation.GET_LAWS.name()).param("day",
				treasury.geteRepDay().toString()));
	}

	private void calculateTaxRevenue(int day, double tnIncome,
			double donationsCC, double donationsG, int mpps) {

		CountryTreasury todayTreasury = getTreasury(day);
		CountryTreasury yesterdayTreasury = getTreasury(todayTreasury
				.geteRepDay() - 1);

		Double taxRevenue = todayTreasury.getCurrencyTreasury()
				- yesterdayTreasury.getCurrencyTreasury();
		taxRevenue += donationsCC += mpps -= tnIncome;
		todayTreasury.setCalculatedTaxRevenue(taxRevenue < 0 ? null
				: taxRevenue);

		Double goldTaxRevenue = todayTreasury.getGoldTreasury()
				- yesterdayTreasury.getGoldTreasury();
		goldTaxRevenue += donationsG;

		todayTreasury.setCalculatedGoldTaxRevenue(goldTaxRevenue < 0 ? null
				: goldTaxRevenue);

		updater.save(todayTreasury);

		CacheFacade
				.remove("mfs.robot.server.erepublik.data.CountryDAO.getCountryTreasuryHistory()");
		CacheFacade
				.remove("mfs.robot.server.erepublik.data.CountryDAO.getCountryTreasury(Integer="
						+ day + ";)");

//		if (todayTreasury.getGoldTaxRevenue() != null
//				&& todayTreasury.getGoldTaxRevenue() > 0)
//			createMinersTask(day + "", todayTreasury.getGoldTaxRevenue() / 0.1);
	}

	@SuppressWarnings("rawtypes")
	private void getLaws(String sday) throws IOException {

		Integer day = Integer.valueOf(sday);

		if (day.equals(eRepTimeCalculator.geteRepDay()))
			throw new IllegalStateException("Trying to get laws for " + day
					+ " but the day is not over yet");

		URL url = new URL(ScrapperURL.COUNTRY_LAWS.getUrl("Brazil"));
		HTTPResponse response = fetchService.fetch(new HTTPRequest(url,
				HTTPMethod.GET, withDeadline(60)));
		String html = new String(response.getContent(),
				Charset.defaultCharset());

		double donationsCC = 0;
		double donationsG = 0;
		int mpps = 0;

		try {
			JSONParser parser = new JSONParser();
			JSONArray jsonArray = (JSONArray) parser.parse(html);

			Iterator it = jsonArray.iterator();
			while (it.hasNext()) {
				JSONObject jsObj = (JSONObject) it.next();

				String type = jsObj.get("type").toString().trim();
				switch (type) {
					case "Mutual Protection Pact":
						Integer mppValue = createMPP(day, jsObj);
						mpps += mppValue;
						break;
					case "Donate":
						MonetaryType mType = getMonetaryTypeFromDonation(jsObj);
						Double donationValue = createDonation(day, jsObj, mType);
						if (MonetaryType.CURRENCY.equals(mType))
							donationsCC += donationValue;
						else
							donationsG += donationValue;
						break;
					default:
						log.log(Level.WARNING, "Lei tipo: " + type);
						break;
				}
			}
		} catch (ParseException e) {
			throw new IOException(e);
		}

		double tnIncome = getTreasuryIncome(day);

		calculateTaxRevenue(day, tnIncome, donationsCC, donationsG, mpps);
	}

	@SuppressWarnings("unchecked")
	private double getTreasuryIncome(Integer eRepDay) {
		double income = 0;

		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(CountryIncome.class);
			q.setFilter("eRepDay == " + eRepDay);
			List<CountryIncome> incomes = (List<CountryIncome>) q.execute();

			// should really use an index?
			for (CountryIncome c : incomes)
				if ("Tesouro Nacional".equalsIgnoreCase(c.getDestination())
						&& !c.getObservations().contains("MPP Rejeitada"))
					income += c.getValue();

			Query transfers = pm.newQuery(CountryMoneyTransfer.class);
			transfers.setFilter("eRepDay == " + eRepDay);
			List<CountryMoneyTransfer> transfersList = (List<CountryMoneyTransfer>) transfers
					.execute();

			for (CountryMoneyTransfer t : transfersList)
				if ("Tesouro Nacional".equalsIgnoreCase(t.getDestination()))
					income += t.getValue();

		} finally {
			pm.close();
		}

		return income;
	}

	private Double createDonation(Integer day, JSONObject jsObj,
			MonetaryType mType) {

		Integer proposedDate = Integer.valueOf(jsObj.get("date").toString());

		String status = jsObj.get("status").toString();

		JSONObject jsDetails = (JSONObject) jsObj.get("details");

		String id = jsObj.get("id").toString();
		String destination = jsDetails.get("destination").toString();
		String proposedBy = jsDetails.get("proposedBy").toString();

		String value = jsDetails.get("value").toString();

		value = value.replace("BRL", "").replace("GOLD", "").trim();

		Double donateValue = Double.valueOf(value);

		log.log(Level.WARNING,
				"Day {0} | Found donation for day {1} with value {2} and status {3}",
				new Object[] { day, proposedDate, donateValue, status });

		switch (status) {
			case "Pending":
				if (day.equals(proposedDate)) {
					CountryMoneyTransfer e = new CountryMoneyTransfer();
					e.seteRepDay(proposedDate);
					e.setOrigin("Tesouro Nacional");
					e.setPersonResponsible(proposedBy);
					e.setRecordedById("RoBoT");
					e.setRecordedOn(new Date());
					e.setType(mType);
					e.setValue(donateValue);
					e.setDestination(destination);
					e.setObservations("ID: " + id);
					updater.save(e);
					new MdFLogger().log(e);

					CacheFacade
							.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getTransfers(Integer="
									+ e.geteRepDay() + ";)");
					CacheFacade
							.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getTransfersHistory()");

					return donateValue;
				}
				break;

			case "Rejected":

				if (proposedDate.equals(day - 1)) {
					CountryIncome income = new CountryIncome();
					income.setDestination("Tesouro Nacional");
					income.seteRepDay(day);
					income.setObservations("Doação Rejeitada - ID: " + id);
					income.setPersonResponsible(proposedBy);
					income.setRecordedById("RoBoT");
					income.setRecordedOn(new Date());
					income.setType(mType);
					income.setValue(donateValue);
					updater.save(income);
					new MdFLogger().log(income);

					CacheFacade
							.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getIncomes(Integer="
									+ day + ";)");
					CacheFacade
							.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getIncomesHistory()");

					return donateValue * -1;
				}
				break;
		}

		return 0d;
	}

	private MonetaryType getMonetaryTypeFromDonation(JSONObject jsObj) {
		JSONObject jsDetails = (JSONObject) jsObj.get("details");

		String value = jsDetails.get("value").toString();

		if (value.contains("BRL"))
			return MonetaryType.CURRENCY;

		return MonetaryType.GOLD;

	}

	private Integer createMPP(Integer day, JSONObject jsObj) {

		Integer proposedDate = Integer.valueOf(jsObj.get("date").toString());

		String status = jsObj.get("status").toString();

		JSONObject jsDetails = (JSONObject) jsObj.get("details");

		String country1 = jsDetails.get("country1").toString();
		String country2 = jsDetails.get("country2").toString();

		String id = jsObj.get("id").toString();
		String withCountry = "Brazil".equals(country1) ? country2 : country1;
		String proposedBy = jsDetails.get("proposedBy").toString();

		log.log(Level.WARNING,
				"Day {0} | Found MPP for day {1} with status {2}",
				new Object[] { day, proposedDate, status });

		switch (status) {
			case "Pending":
				if (day.equals(proposedDate)) {
					CountryExpense e = new CountryExpense();
					e.setCategory(ExpenseType.MPP);
					e.seteRepDay(proposedDate);
					e.setObservations(withCountry + " - ID: " + id);
					e.setOrigin("Tesouro Nacional");
					e.setPersonResponsible(proposedBy);
					e.setRecordedById("RoBoT");
					e.setRecordedOn(new Date());
					e.setType(MonetaryType.CURRENCY);
					e.setValue(10000d);
					updater.save(e);
					new MdFLogger().log(e);

					CacheFacade
							.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getExpenses(Integer="
									+ e.geteRepDay() + ";)");
					CacheFacade
							.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getExpensesSum()");
					CacheFacade
							.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getExpensesHistory(Integer=0;)");

					return 10000;
				}
				break;

			case "Rejected":

				if (proposedDate.equals(day - 1)) {
					CountryIncome income = new CountryIncome();
					income.setDestination("Tesouro Nacional");
					income.seteRepDay(day);
					income.setObservations("MPP Rejeitada - " + withCountry
							+ " - ID: " + id);
					income.setPersonResponsible(proposedBy);
					income.setRecordedById("RoBoT");
					income.setRecordedOn(new Date());
					income.setType(MonetaryType.CURRENCY);
					income.setValue(10000d);
					updater.save(income);
					new MdFLogger().log(income);
					CacheFacade
							.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getIncomes(Integer="
									+ day + ";)");
					CacheFacade
							.remove("mfs.robot.server.erepublik.data.ExpensesDAO.getIncomesHistory()");

					return -10000;
				}
				break;
		}

		return 0;
	}

	private void createMinersTask(String day, Double miners) {
		Queue scrapperQueue = QueueFactory.getQueue("scrapper");
		scrapperQueue.add(withUrl("/erepublik/update/scrapper")
				.param("operation", Operation.SET_MINERS.name())
				.param("day", day).param("miners", miners.toString()));
	}

	private void createNotificationTask(String day, String orgId) {
		Queue defaultQueue = QueueFactory.getDefaultQueue();
		defaultQueue.add(withUrl("/erepublik/emailParty")
				.param("operation",
						EmailTask.Operation.NOTIFY_GOLD_CHANGE.name())
				.param("id", orgId).param("day", day));
	}

	/**
	 * Defines the number of miners in CountryStats
	 */
	private void setMiners(String day, String miners) {
		Integer iDay = Integer.valueOf(day);
		Integer iMiners = Double.valueOf(miners).intValue();

		CountryStats stats = new CountryDAO().getCountryStats(iDay);
		if (stats == null)
			throw new IllegalStateException("Could not find stats for day "
					+ day);

		stats.setMiners(iMiners);
		updater.save(stats);
	}

	private CountryTreasury getTreasury(int eRepDay) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(CountryTreasury.class);
			q.setFilter("eRepDay == " + eRepDay);
			q.setUnique(true);
			return (CountryTreasury) pm.detachCopy(q.execute());
		} catch (JDOObjectNotFoundException e) {
			return new CountryTreasury().setCurrencyTreasury(0d);
		} finally {
			pm.close();
		}
	}

	private void getPartyInfo(String day, String id) throws IOException {

		log.log(Level.INFO, "Starting update for party " + id);

		PoliticalParty party = PoliticalParty.fromId(id);
		PoliticalPartyStats stats = new PoliticalPartyStats();

		stats.seteRepDay(Integer.valueOf(day));
		stats.setParty(party);

		JSONParser parser = new JSONParser();

		URL url = new URL(ScrapperURL.PARTY_STATS.getUrl(id));
		HTTPResponse response = fetchService.fetch(new HTTPRequest(url,
				HTTPMethod.GET, withDeadline(60)));
		String html = new String(response.getContent(),
				Charset.defaultCharset());

		try {
			JSONObject jsObj = (JSONObject) parser.parse(html);
			stats.setMembersCount(Integer.valueOf(jsObj.get("members")
					.toString()));
		} catch (ParseException e) {
			throw new IOException("error parsing members json", e);
		}

		updater.save(stats);

		CacheFacade
				.remove("mfs.robot.server.erepublik.data.CountryDAO.getPartyHistory(PoliticalParty="
						+ party.name() + ";)");
		CacheFacade
				.remove("mfs.robot.server.erepublik.data.CountryDAO.getPoliticalHistory()");

		log.log(Level.INFO, "Finished update");
	}

	@SuppressWarnings("rawtypes")
	private void getPartyMembers(String day, String id) throws IOException {
		JSONParser parser = new JSONParser();

		URL url = new URL(ScrapperURL.PARTY_MEMBERS.getUrl(id));
		HTTPResponse response = fetchService.fetch(new HTTPRequest(url,
				HTTPMethod.GET, withDeadline(60)));
		String html = new String(response.getContent(),
				Charset.defaultCharset());

		PoliticalPartyMembers memberStats = new PoliticalPartyMembers();

		memberStats.seteRepDay(Integer.valueOf(day));
		memberStats.setParty(PoliticalParty.fromId(id));

		Set<PartyMember> members = new HashSet<PartyMember>();

		try {
			JSONArray array = (JSONArray) parser.parse(html);

			Iterator it = array.iterator();
			while (it.hasNext()) {
				JSONObject obj = (JSONObject) it.next();
				PartyMember member = new PartyMember();
				member.seteRepId(obj.get("id").toString());
				member.seteRepName(obj.get("name").toString());
				members.add(member);
			}

		} catch (ParseException e) {
			throw new IOException("error parsing members json", e);
		}

		memberStats.setMembers(members);
		updater.save(memberStats);

		PoliticalPartyStats stats = new PoliticalPartyStats();
		stats.seteRepDay(Integer.valueOf(day));
		stats.setParty(PoliticalParty.fromId(id));
		stats.setMembersCount(members.size());
		updater.save(stats);

		CacheFacade
				.remove("mfs.robot.server.erepublik.data.CountryDAO.getPartyHistory(PoliticalParty="
						+ PoliticalParty.fromId(id).name() + ";)");
		CacheFacade
				.remove("mfs.robot.server.erepublik.data.CountryDAO.getPoliticalHistory()");

		Queue defaultQueue = QueueFactory.getDefaultQueue();
		defaultQueue.add(withUrl("/erepublik/update/scrapper")
				.param("operation", Operation.PARTY_MEMBERS_DIFF.name())
				.param("id", id).param("day", day));

	}

	private void createPartyDiff(String day, String partyId) {
		CountryDAO dao = new CountryDAO();

		PoliticalParty party = PoliticalParty.fromId(partyId);
		Integer today = Integer.valueOf(day);

		PoliticalPartyMembers todayMembers = dao.getPartyMembers(party, today);
		PoliticalPartyMembers yesterdayMembers = dao.getPartyMembers(party,
				today - 1);

		if (yesterdayMembers == null)
			return;

		Set<PartyMember> newMembers = new HashSet<PartyMember>();
		Set<PartyMember> leftMembers = new HashSet<PartyMember>();

		for (PartyMember m : todayMembers)
			if (!yesterdayMembers.contains(m))
				newMembers.add(m);

		for (PartyMember m : yesterdayMembers)
			if (!todayMembers.contains(m))
				leftMembers.add(m);

		PoliticalPartyMembersDiff diff = new PoliticalPartyMembersDiff();
		diff.seteRepDay(today);
		diff.setParty(party);
		diff.setLeftMembers(leftMembers);
		diff.setNewMembers(newMembers);

		updater.save(diff);

		Queue defaultQueue = QueueFactory.getDefaultQueue();
		defaultQueue.add(withUrl("/erepublik/emailParty")
				.param("operation", EmailTask.Operation.PARTY_EMAIL.name())
				.param("id", partyId).param("day", day));
	}

	@SuppressWarnings("rawtypes")
	private void getEgovCountryInfo(String day, String statusId)
			throws IOException {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			CountryStats status = pm.getObjectById(CountryStats.class,
					Long.valueOf(statusId));

			URL url = new URL(ScrapperURL.EGOV_COUNTRY.getUrl("9"));
			HTTPResponse response = fetchService.fetch(new HTTPRequest(url,
					HTTPMethod.GET, withDeadline(60)));
			String html = new String(response.getContent(),
					Charset.defaultCharset());

			try {
				JSONParser parser = new JSONParser();
				JSONArray array = (JSONArray) parser.parse(html);
				Integer eday = Integer.valueOf(day);
				Iterator it = array.iterator();
				while (it.hasNext()) {
					JSONObject obj = (JSONObject) it.next();
					Integer objDay = Integer.valueOf(obj.get("day").toString());
					if (eday.equals(objDay)) {
						status.setEgov4youCitizens(Integer.valueOf(obj.get(
								"citizens").toString()));
						status.setEgov4youFighters(Integer.valueOf(obj.get(
								"fighters").toString()));
						break;
					} else
						if (objDay > eday + 3) // no cookies for you :(
							return;
				}

				if (status.getEgov4youCitizens() == null
						|| status.getEgov4youFighters() == null)
					throw new IllegalStateException(
							"Invalid egov4you data for day " + day);

			} catch (ParseException e) {
				throw new IOException("error parsing members json", e);
			}

			CacheFacade
					.remove("mfs.robot.server.erepublik.data.CountryDAO.getCountryStatsHistory()");

		} finally {
			pm.close();
		}
	}
}
