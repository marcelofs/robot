package mfs.robot.server.erepublik;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import mfs.robot.client.erepublik.rpc.RoBoTRPC;
import mfs.robot.server.Mailer;
import mfs.robot.server.erepublik.data.CountryDAO;
import mfs.robot.server.erepublik.data.MarketDAO;
import mfs.robot.server.erepublik.data.UserDAO;
import mfs.robot.shared.admin.eRepUser;
import mfs.robot.shared.erepublik.FinancialIndexType;
import mfs.robot.shared.erepublik.IFinancialIndex;
import mfs.robot.shared.erepublik.InitialInfo;
import mfs.robot.shared.erepublik.data.AgregatedJobMarketOffer;
import mfs.robot.shared.erepublik.data.AgregatedMarketplaceOffer;
import mfs.robot.shared.erepublik.data.AgregatedMarketplaceProfit;
import mfs.robot.shared.erepublik.data.AgregatedMonetaryOffer;
import mfs.robot.shared.erepublik.data.AgregatedRocketPrice;
import mfs.robot.shared.erepublik.data.CountryBonuses;
import mfs.robot.shared.erepublik.data.CountryStats;
import mfs.robot.shared.erepublik.data.CountryTaxes;
import mfs.robot.shared.erepublik.data.JobMarketOffer;
import mfs.robot.shared.erepublik.data.MarketplaceOffer;
import mfs.robot.shared.erepublik.data.MarketplaceProfit;
import mfs.robot.shared.erepublik.data.MonetaryOffer;
import mfs.robot.shared.erepublik.data.MonetaryType;
import mfs.robot.shared.erepublik.data.PoliticalParty;
import mfs.robot.shared.erepublik.data.PoliticalPartyMembersDiff;
import mfs.robot.shared.erepublik.data.PoliticalPartyStats;
import mfs.robot.shared.erepublik.data.ProductQuality;
import mfs.robot.shared.erepublik.data.ProductType;
import mfs.robot.shared.erepublik.data.RocketPrice;
import mfs.robot.shared.erepublik.data.RocketQuality;
import mfs.robot.shared.erepublik.dto.JobHistoryDTO;
import mfs.robot.shared.erepublik.dto.RocketDailyPriceDTO;
import mfs.robot.shared.erepublik.dto.RocketHistoryPriceDTO;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class RoBoTRPCServlet extends RemoteServiceServlet implements RoBoTRPC {

	private static final long	serialVersionUID	= 1L;

	private MarketDAO			market				= new MarketDAO();
	private CountryDAO			country				= new CountryDAO();

	@Override
	public InitialInfo getInitialInfo() {
		InitialInfo info = new InitialInfo();

		info.eRepDay = geteRepDay();

		eRepUser user = new UserDAO().geteRepUser(UserServiceFactory
				.getUserService().getCurrentUser());
		info.eRepName = user.geteRepName();
		info.isAdmin = "mr.marcelofs@gmail.com".equals(user.getgEmail());
		info.isMDF = user.getIsMdF();

		return info;
	}

	@Override
	public Integer geteRepDay() {
		return eRepTimeCalculator.geteRepDay();
	}

	@Override
	public Collection<CountryStats> getCountryStatsHistory() {
		return country.getCountryStatsHistory();
	}

	@Override
	public Collection<CountryBonuses> getCountryBonusesHistory() {
		return country.getCountryBonusesHistory();
	}

	@Override
	public Collection<CountryTaxes> getCountryTaxesHistory() {
		return country.getCountryTaxesHistory();
	}

	@Override
	public Collection<JobMarketOffer> getDailyJobMarketOffers() {
		return getDailyJobMarketOffers(eRepTimeCalculator.geteRepDay());
	}

	@Override
	public Collection<JobMarketOffer> getDailyJobMarketOffers(Integer day) {
		return market.getDailyJobMarketOffers(day);
	}

	@Override
	public Collection<AgregatedJobMarketOffer> getAgregatedJobMarketOffers() {
		return market.getAgregatedJobMarketOffers();
	}

	@Override
	public JobHistoryDTO getJobMarketHistory() {
		JobHistoryDTO dto = new JobHistoryDTO();
		dto.jobs = getAgregatedJobMarketOffers();
		dto.taxes = getCountryTaxesHistory();
		return dto;
	}

	@Override
	public Collection<MonetaryOffer> getDailyMonetaryOffers(MonetaryType type) {
		return getDailyMonetaryOffers(type, eRepTimeCalculator.geteRepDay());
	}

	@Override
	public Collection<MonetaryOffer> getDailyMonetaryOffers(MonetaryType type,
			Integer day) {
		return market.getDailyMonetaryOffers(type, day);
	}

	@Override
	public Collection<AgregatedMonetaryOffer> getAgregatedMonetaryOffers(
			MonetaryType type) {
		return market.getAgregatedMonetaryOffers(type);
	}

	@Override
	public Collection<MarketplaceOffer> getDailyMarketplaceOffer(
			ProductType type, ProductQuality quality) {
		// TODO if day is not over, fill with objects with null value
		return getDailyMarketplaceOffer(type, quality,
				eRepTimeCalculator.geteRepDay());
	}

	@Override
	public Collection<MarketplaceOffer> getDailyMarketplaceOffer(
			ProductType type, ProductQuality quality, Integer day) {
		return market.getDailyMarketplaceOffer(type, quality, day);
	}

	@Override
	public Collection<AgregatedMarketplaceOffer> getAgregatedMarketplaceOffer(
			ProductType type, ProductQuality quality) {
		return market.getAgregatedMarketplaceOffer(type, quality);
	}

	@Override
	public Collection<MarketplaceProfit> getDailyMarketplaceProfit(
			ProductType type, ProductQuality quality) {

		return getDailyMarketplaceProfit(type, quality,
				eRepTimeCalculator.geteRepDay());
	}

	@Override
	public Collection<MarketplaceProfit> getDailyMarketplaceProfit(
			ProductType type, ProductQuality quality, Integer day) {
		// FIXME
		if (type.equals(ProductType.FOOD)
				&& !UserServiceFactory.getUserService().getCurrentUser()
						.getEmail().equals("mr.marcelofs@gmail.com"))
			return new ArrayList<MarketplaceProfit>();

		return market.getDailyMarketplaceProfit(type, quality, day);

	}

	@Override
	public Collection<AgregatedMarketplaceProfit> getAgregatedMarketplaceProfit(
			ProductType type, ProductQuality quality) {

		// FIXME
		if (type.equals(ProductType.FOOD)
				&& !UserServiceFactory.getUserService().getCurrentUser()
						.getEmail().equals("mr.marcelofs@gmail.com"))
			return new ArrayList<AgregatedMarketplaceProfit>();

		return market.getAgregatedMarketplaceProfit(type, quality);
	}

	@Override
	public Collection<RocketPrice> getDailyRocketPrice(RocketQuality quality) {
		return getDailyRocketPrice(quality, eRepTimeCalculator.geteRepDay());
	}

	@Override
	public Collection<RocketPrice> getDailyRocketPrice(RocketQuality quality,
			Integer day) {
		return market.getDailyRocketPrice(quality, day);
	}

	@Override
	public Collection<AgregatedRocketPrice> getAgregatedRocketPrice(
			RocketQuality quality) {
		return market.getAgregatedRocketPrice(quality);
	}

	@Override
	public Map<PoliticalParty, Collection<PoliticalPartyStats>> getPoliticalHistory() {
		return country.getPoliticalHistory();
	}

	@Override
	public PoliticalPartyMembersDiff getMembersDiff(PoliticalParty party,
			Integer day) {
		return country.getPartyMembersDiff(party, day);
	}

	@Override
	public Collection<PoliticalPartyMembersDiff> getMembersDiffHistory(
			PoliticalParty party) {
		return country.getPartyMembersDiffHistory(party);
	}

	@Override
	public void sendFeedback(String message, String location) throws Exception {
		UserService userService = UserServiceFactory.getUserService();
		eRepUser eUser = new UserDAO()
				.geteRepUser(userService.getCurrentUser());

		StringBuilder builder = new StringBuilder("Sugestão para o RoBoT: \n\n");
		builder.append("User: ").append(eUser.geteRepName()).append(", ")
				.append(eUser.getgEmail()).append("\n\n");
		builder.append("Location: ").append(location).append("\n\n");
		builder.append("Mensagem: \n").append(message);

		new Mailer().mail("Feedback RoBoT", builder.toString(), new String[] {
				"Admin", "mr.marcelofs@gmail.com" });

	}

	@Override
	public RocketHistoryPriceDTO getRocketPricesHistory() {
		RocketHistoryPriceDTO dto = new RocketHistoryPriceDTO();

		dto.goldValue = market.getAgregatedMonetaryOffers(MonetaryType.GOLD);

		Multimap<ProductQuality, AgregatedMarketplaceOffer> prices = ArrayListMultimap
				.create();
		for (ProductQuality q : ProductQuality.values())
			prices.putAll(q,
					market.getAgregatedMarketplaceOffer(ProductType.WEAPON, q));
		dto.prices = prices;
		return dto;
	}

	@Override
	public RocketDailyPriceDTO getRocketPricesDaily(Integer day) {
		RocketDailyPriceDTO dto = new RocketDailyPriceDTO();

		dto.goldValue = market.getDailyMonetaryOffers(MonetaryType.GOLD, day);

		Multimap<ProductQuality, MarketplaceOffer> prices = ArrayListMultimap
				.create();
		for (ProductQuality q : ProductQuality.values())
			prices.putAll(q,
					market.getDailyMarketplaceOffer(ProductType.WEAPON, q, day));
		dto.prices = prices;
		dto.day = day;

		return dto;
	}

	@Override
	public Multimap<FinancialIndexType, IFinancialIndex> getIndex(
			FinancialIndexType[] types) {

		Multimap<FinancialIndexType, IFinancialIndex> indexes = ArrayListMultimap
				.create();

		for (FinancialIndexType type : types)
			indexes.putAll(type, getIndex(type));

		return indexes;
	}

	private Collection<? extends IFinancialIndex> getIndex(
			FinancialIndexType type) {
		switch (type) {

			case GOLD:
				return market.getAgregatedMonetaryOffers(MonetaryType.GOLD);
			case REVENUE:
				return country.getCountryTreasuryHistory();
			case JOBS:
				return market.getAgregatedJobMarketOffers();
			case WQ7:
				return market.getAgregatedMarketplaceOffer(ProductType.WEAPON,
						ProductQuality.Q7);

			default:
				return null;
		}
	}

}
