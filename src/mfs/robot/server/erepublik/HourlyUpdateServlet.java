package mfs.robot.server.erepublik;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mfs.robot.shared.erepublik.data.Market;
import mfs.robot.shared.erepublik.data.MonetaryType;
import mfs.robot.shared.erepublik.data.ProductQuality;
import mfs.robot.shared.erepublik.data.ProductType;
import mfs.robot.shared.erepublik.data.RocketQuality;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

public class HourlyUpdateServlet extends HttpServlet {

	private static final long	serialVersionUID	= 1L;

	@Override
	public void doGet(final HttpServletRequest req,
			final HttpServletResponse res) throws ServletException {

		Integer eRepDay = eRepTimeCalculator.geteRepDay();
		Integer eRepHour = eRepTimeCalculator.geteRepHour();

		Queue queue = QueueFactory.getQueue("hourly");

		String eRpkHourly = "/erepublik/update/erpk/hourly";

		for (ProductType t : ProductType.values())
			for (ProductQuality q : ProductQuality.values())
				queue.add(withUrl(eRpkHourly)
						.param("market", Market.MARKETPLACE.name())
						.param("type", t.name()).param("quality", q.name())
						.param("day", eRepDay.toString())
						.param("hour", eRepHour.toString()));

		queue.add(withUrl(eRpkHourly).param("market", Market.JOB.name())
				.param("day", eRepDay.toString())
				.param("hour", eRepHour.toString()));

		// for (MonetaryType t : MonetaryType.values())
		queue.add(withUrl(eRpkHourly).param("market", Market.MONETARY.name())
				.param("type", MonetaryType.GOLD.name())
				.param("day", eRepDay.toString())
				.param("hour", eRepHour.toString()));


	}

}
