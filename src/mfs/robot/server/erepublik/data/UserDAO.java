package mfs.robot.server.erepublik.data;

import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.PersistenceManager;

import mfs.robot.server.PMF;
import mfs.robot.server.cache.Cacheable;
import mfs.robot.shared.admin.eRepUser;

import com.google.appengine.api.users.User;

public class UserDAO {

	@Cacheable
	public eRepUser geteRepUser(User gUser) {
		return geteRepUserUncached(gUser);
	}

	@Cacheable
	public eRepUser geteRepUser(String userId) {
		return geteRepUserUncached(userId);
	}

	public eRepUser geteRepUserUncached(User gUser) {
		return geteRepUserUncached(gUser.getUserId());
	}

	public eRepUser geteRepUserUncached(String userId) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			try {
				return pm.getObjectById(eRepUser.class, userId);
			} catch (JDOObjectNotFoundException e) {
				return null;
			}
		} finally {
			pm.close();
		}
	}

}
