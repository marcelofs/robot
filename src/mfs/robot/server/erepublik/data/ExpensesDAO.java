package mfs.robot.server.erepublik.data;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import mfs.robot.server.PMF;
import mfs.robot.server.cache.Cacheable;
import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.CountryIncome;
import mfs.robot.shared.erepublik.data.CountryMoneyTransfer;
import mfs.robot.shared.erepublik.data.MonetaryType;
import mfs.robot.shared.erepublik.dto.CountryExpenseSum;

import com.google.appengine.api.users.UserServiceFactory;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

public class ExpensesDAO {

	public void delete(CountryExpense e) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			CountryExpense exp = pm.getObjectById(CountryExpense.class,
					e.getId());

			exp.setDeletedOn(new Date());
			exp.setDeletedById(UserServiceFactory.getUserService()
					.getCurrentUser().getUserId());

		} finally {
			pm.close();
		}
	}

	public void delete(CountryIncome e) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			CountryIncome exp = pm
					.getObjectById(CountryIncome.class, e.getId());

			exp.setDeletedOn(new Date());
			exp.setDeletedById(UserServiceFactory.getUserService()
					.getCurrentUser().getUserId());

		} finally {
			pm.close();
		}
	}

	public void delete(CountryMoneyTransfer e) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			CountryMoneyTransfer exp = pm.getObjectById(
					CountryMoneyTransfer.class, e.getId());

			exp.setDeletedOn(new Date());
			exp.setDeletedById(UserServiceFactory.getUserService()
					.getCurrentUser().getUserId());

		} finally {
			pm.close();
		}
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<CountryExpense> getExpenses(Integer day) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(CountryExpense.class);
			q.setFilter("eRepDay == " + day + " && deletedOn == null");
			q.setOrdering("recordedOn desc");
			return pm.detachCopyAll((List<CountryExpense>) q.execute());
		} finally {
			pm.close();
		}
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<CountryIncome> getIncomes(Integer day) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(CountryIncome.class);
			q.setFilter("eRepDay == " + day + " && deletedOn == null");
			q.setOrdering("recordedOn desc");
			return pm.detachCopyAll((List<CountryIncome>) q.execute());
		} finally {
			pm.close();
		}
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<CountryIncome> getIncomesHistory(String orgName) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(CountryIncome.class);
			q.setFilter("deletedOn == null && destination == '" + orgName + "'");
			q.setOrdering("eRepDay asc");

			Collection<CountryIncome> incomes = pm
					.detachCopyAll((List<CountryIncome>) q.execute());

			return incomes;

		} finally {
			pm.close();
		}
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<CountryIncome> getIncomesHistory(Integer firstDay) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(CountryIncome.class);
			q.setFilter("deletedOn == null && eRepDay >= " + firstDay);
			q.setOrdering("eRepDay asc");

			Collection<CountryIncome> incomes = pm
					.detachCopyAll((List<CountryIncome>) q.execute());

			return incomes;

		} finally {
			pm.close();
		}
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<CountryMoneyTransfer> getTransfers(Integer day) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(CountryMoneyTransfer.class);
			q.setFilter("eRepDay == " + day + " && deletedOn == null");
			q.setOrdering("recordedOn desc");
			return pm.detachCopyAll((List<CountryMoneyTransfer>) q.execute());
		} finally {
			pm.close();
		}
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<CountryMoneyTransfer> getTransfersHistory(Integer firstDay) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(CountryMoneyTransfer.class);
			q.setFilter("deletedOn == null && eRepDay >= " + firstDay);
			q.setOrdering("eRepDay asc");

			Collection<CountryMoneyTransfer> transfers = pm
					.detachCopyAll((List<CountryMoneyTransfer>) q.execute());

			return transfers;

		} finally {
			pm.close();
		}
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	public Multimap<Integer, CountryExpenseSum> getExpensesSum() {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(CountryExpense.class);
			q.setFilter("deletedOn == null");
			q.setOrdering("recordedOn asc");

			Collection<CountryExpense> expenses = pm
					.detachCopyAll((List<CountryExpense>) q.execute());

			return createSum(expenses);

		} finally {
			pm.close();
		}
	}

	public Collection<CountryExpense> getExpensesHistory() {
		return getExpensesHistory(0);
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<CountryExpense> getExpensesHistory(String orgName) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(CountryExpense.class);
			q.setFilter("deletedOn == null && origin == '" + orgName + "'");
			q.setOrdering("eRepDay asc");

			Collection<CountryExpense> expenses = pm
					.detachCopyAll((List<CountryExpense>) q.execute());

			return expenses;

		} finally {
			pm.close();
		}
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<CountryExpense> getExpensesHistory(Integer firstDay) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(CountryExpense.class);
			q.setFilter("deletedOn == null && eRepDay >= " + firstDay);
			q.setOrdering("eRepDay asc");

			Collection<CountryExpense> expenses = pm
					.detachCopyAll((List<CountryExpense>) q.execute());

			return expenses;

		} finally {
			pm.close();
		}
	}

	private Multimap<Integer, CountryExpenseSum> createSum(
			Collection<CountryExpense> expenses) {
		Multimap<Integer, CountryExpenseSum> map = HashMultimap.create();

		for (CountryExpense e : expenses) {
			CountryExpenseSum sum = findDaily(e, map);
			if (MonetaryType.CURRENCY.equals(e.getType()))
				sum.value += e.getValue();
		}

		return map;
	}

	private CountryExpenseSum findDaily(CountryExpense expense,
			Multimap<Integer, CountryExpenseSum> map) {

		Collection<CountryExpenseSum> dailySums = map.get(expense.geteRepDay());

		CountryExpenseSum daily = null;
		for (CountryExpenseSum c : dailySums)
			if (expense.getCategory().equals(c.category))
				daily = c;

		if (daily == null) {
			daily = new CountryExpenseSum();
			daily.category = expense.getCategory();
			daily.value = 0d;
			map.put(expense.geteRepDay(), daily);
		}

		return daily;
	}

}
