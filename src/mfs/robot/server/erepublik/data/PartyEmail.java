package mfs.robot.server.erepublik.data;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class PartyEmail {

	@PrimaryKey
	@Persistent
	private String	id;

	@Persistent
	private String	email;

	@Persistent
	private Boolean	sendBBCode;

	@Persistent
	private String	recipientName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRecipientName() {
		return recipientName;
	}

	public void setRecipientName(String recipientName) {
		this.recipientName = recipientName;
	}

	public Boolean getSendBBCode() {
		return sendBBCode;
	}

	public void setSendBBCode(Boolean sendBBCode) {
		this.sendBBCode = sendBBCode;
	}

}
