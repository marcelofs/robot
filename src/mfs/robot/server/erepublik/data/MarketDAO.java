package mfs.robot.server.erepublik.data;

import java.util.Collection;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import mfs.robot.server.PMF;
import mfs.robot.server.cache.Cacheable;
import mfs.robot.shared.erepublik.data.AgregatedJobMarketOffer;
import mfs.robot.shared.erepublik.data.AgregatedMarketplaceOffer;
import mfs.robot.shared.erepublik.data.AgregatedMarketplaceProfit;
import mfs.robot.shared.erepublik.data.AgregatedMonetaryOffer;
import mfs.robot.shared.erepublik.data.AgregatedRocketPrice;
import mfs.robot.shared.erepublik.data.JobMarketOffer;
import mfs.robot.shared.erepublik.data.MarketplaceOffer;
import mfs.robot.shared.erepublik.data.MarketplaceProfit;
import mfs.robot.shared.erepublik.data.MonetaryOffer;
import mfs.robot.shared.erepublik.data.MonetaryType;
import mfs.robot.shared.erepublik.data.ProductQuality;
import mfs.robot.shared.erepublik.data.ProductType;
import mfs.robot.shared.erepublik.data.RocketPrice;
import mfs.robot.shared.erepublik.data.RocketQuality;

public class MarketDAO {

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<JobMarketOffer> getDailyJobMarketOffers(Integer day) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(JobMarketOffer.class);
			q.setFilter("eRepDay == " + day);
			q.setOrdering("eRepTime asc");
			return pm.detachCopyAll((List<JobMarketOffer>) q.execute());
		} finally {
			pm.close();
		}
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<AgregatedJobMarketOffer> getAgregatedJobMarketOffers() {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(AgregatedJobMarketOffer.class);
			q.setOrdering("eRepDay asc");
			return pm
					.detachCopyAll((List<AgregatedJobMarketOffer>) q.execute());
		} finally {
			pm.close();
		}
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<MonetaryOffer> getDailyMonetaryOffers(MonetaryType type,
			Integer day) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(MonetaryOffer.class);
			q.setFilter("eRepDay == " + day + " && type == '" + type.name()
					+ "'");
			q.setOrdering("eRepTime asc");

			List<MonetaryOffer> result = (List<MonetaryOffer>) q.execute();
			return pm.detachCopyAll(result);

		} finally {
			pm.close();
		}
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<AgregatedMonetaryOffer> getAgregatedMonetaryOffers(
			MonetaryType type) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			String filter = "type == '" + type.name() + "'";
			Query q = pm.newQuery(AgregatedMonetaryOffer.class, filter);
			q.setOrdering("eRepDay asc");
			return pm.detachCopyAll((List<AgregatedMonetaryOffer>) q.execute());
		} finally {
			pm.close();
		}
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<MarketplaceOffer> getDailyMarketplaceOffer(
			ProductType type, ProductQuality quality, Integer day) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(MarketplaceOffer.class);
			q.setFilter("eRepDay == " + day + " && type == '" + type.name()
					+ "' && quality == '" + quality.name() + "'");
			q.setOrdering("eRepTime asc");
			return pm.detachCopyAll((List<MarketplaceOffer>) q.execute());
		} finally {
			pm.close();
		}
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<AgregatedMarketplaceOffer> getAgregatedMarketplaceOffer(
			ProductType type, ProductQuality quality) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(AgregatedMarketplaceOffer.class);
			q.setFilter("type == '" + type.name() + "' && quality == '"
					+ quality.name() + "'");
			q.setOrdering("eRepDay asc");
			return pm.detachCopyAll((List<AgregatedMarketplaceOffer>) q
					.execute());
		} finally {
			pm.close();
		}
	}

	@Deprecated
	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<MarketplaceProfit> getDailyMarketplaceProfit(
			ProductType type, ProductQuality quality, Integer day) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(MarketplaceProfit.class);
			q.setFilter("eRepDay == " + day + " && type == '" + type.name()
					+ "' && quality == '" + quality.name() + "'");
			q.setOrdering("eRepTime asc");
			return pm.detachCopyAll((List<MarketplaceProfit>) q.execute());
		} finally {
			pm.close();
		}
	}

	@Deprecated
	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<AgregatedMarketplaceProfit> getAgregatedMarketplaceProfit(
			ProductType type, ProductQuality quality) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(AgregatedMarketplaceProfit.class);
			q.setFilter("type == '" + type.name() + "' && quality == '"
					+ quality.name() + "'");
			q.setOrdering("eRepDay asc");
			return pm.detachCopyAll((List<AgregatedMarketplaceProfit>) q
					.execute());
		} finally {
			pm.close();
		}
	}

	@Deprecated
	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<RocketPrice> getDailyRocketPrice(RocketQuality quality,
			Integer day) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(RocketPrice.class);
			q.setFilter("eRepDay == " + day + " && quality == '"
					+ quality.name() + "'");
			q.setOrdering("eRepTime asc");
			return pm.detachCopyAll((List<RocketPrice>) q.execute());
		} finally {
			pm.close();
		}
	}

	@Deprecated
	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<AgregatedRocketPrice> getAgregatedRocketPrice(
			RocketQuality quality) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(AgregatedRocketPrice.class);
			q.setFilter("quality == '" + quality.name() + "'");
			q.setOrdering("eRepDay asc");
			return pm.detachCopyAll((List<AgregatedRocketPrice>) q.execute());
		} finally {
			pm.close();
		}
	}

}
