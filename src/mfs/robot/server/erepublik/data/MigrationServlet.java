package mfs.robot.server.erepublik.data;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.LocalDate;

import static mfs.robot.server.erepublik.eRepTimeCalculator.*;
import mfs.robot.server.PMF;
import mfs.robot.server.Updater;
import mfs.robot.server.cache.CacheFacade;
import mfs.robot.server.erepublik.eRepTimeCalculator;
import mfs.robot.shared.erepublik.data.AgregatedJobMarketOffer;
import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.CountryPresident;
import mfs.robot.shared.erepublik.data.CountryStats;
import mfs.robot.shared.erepublik.data.CountryTreasury;
import mfs.robot.shared.erepublik.data.ExpenseType;
import mfs.robot.shared.erepublik.data.PoliticalParty;
import mfs.robot.shared.erepublik.data.PoliticalPartyMembersDiff;
import mfs.robot.shared.erepublik.data.PoliticalPartyStats;

public class MigrationServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write("Starting... \n");

		//addParty(PoliticalParty._5003, eRepTimeCalculator.geteRepDay());
		createPartyEmails(PoliticalParty._5003);
		
		//addParty(PoliticalParty._2708, eRepTimeCalculator.geteRepDay());
		createPartyEmails(PoliticalParty._2708);
		
		resp.getWriter().write("done");

		// MarketDAO dao = new MarketDAO();
		//
		// int today = eRepTimeCalculator.geteRepDay();
		// int min = today - 30;
		// Double maxSum = 0d;
		// Double avgSum = 0d;
		// Double minSum = 0d;
		// int count = 0;
		// for (AgregatedJobMarketOffer offer :
		// dao.getAgregatedJobMarketOffers())
		// if (offer.geteRepDay() >= min) {
		// maxSum += offer.getMaximum();
		// avgSum += offer.getAverage();
		// minSum += offer.getMinimum();
		// count++;
		// }
		//
		// resp.getWriter().write("Número de dias contados: " + count);
		// resp.getWriter().write(
		// "\nMédia dos valores mínimos: " + (minSum / count) + "cc");
		// resp.getWriter()
		// .write("\nMédia das médias: " + (avgSum / count) + "cc");
		// resp.getWriter().write(
		// "\nMédia das máximas: " + (maxSum / count) + "cc");
		
	}

	// private void calculateMiners() {
	// PersistenceManager pm = PMF.getNewPersistenceManager();
	// Query q = pm.newQuery(CountryTreasury.class);
	// CountryDAO dao = new CountryDAO();
	// List<CountryTreasury> treasuries = (List<CountryTreasury>) q.execute();
	// for (CountryTreasury treasury : treasuries) {
	// if (treasury.getCalculatedGoldTaxRevenue() != null
	// && treasury.getCalculatedGoldTaxRevenue() > 0) {
	// CountryStats stats = dao.getCountryStats(treasury.geteRepDay());
	// stats.setMiners((int) (treasury.getGoldTaxRevenue() / 0.1));
	// pm.makePersistent(stats);
	// }
	// }
	// pm.close();
	//
	// }

	// private void zeroGoldRevenue() {
	// PersistenceManager pm = PMF.getNewPersistenceManager();
	// Query q = pm.newQuery(CountryTreasury.class);
	// List<CountryTreasury> treasuries = (List<CountryTreasury>) q.execute();
	// for (CountryTreasury treasury : treasuries) {
	// if (treasury.getCalculatedGoldTaxRevenue() == null)
	// treasury.setCalculatedGoldTaxRevenue(0d);
	// }
	// pm.close();
	// }

	// private void updatePartyMembersDiff() {
	// PersistenceManager pm = PMF.getNewPersistenceManager();
	// Query q = pm.newQuery(PoliticalPartyMembersDiff.class);
	// List<PoliticalPartyMembersDiff> diffs = (List<PoliticalPartyMembersDiff>)
	// q
	// .execute();
	// for (PoliticalPartyMembersDiff diff : diffs) {
	// if (diff.getNewMembersCount() == null)
	// diff.setNewMembers(diff.getNewMembers());
	// if (diff.getLeftMembersCount() == null)
	// diff.setLeftMembers(diff.getLeftMembers());
	// }
	// pm.close();
	// }

	// private void updateExpensesOrigin() {
	// PersistenceManager pm = PMF.getNewPersistenceManager();
	// Query q = pm.newQuery(CountryExpense.class);
	// List<CountryExpense> expenses = (List<CountryExpense>) q.execute();
	// for (CountryExpense e : expenses)
	// if (e.getOrigin() == null)
	// if (e.getCategory() == ExpenseType.MPP)
	// e.setOrigin("Tesouro Nacional");
	// else
	// e.setOrigin("Controle Nacional");
	// }

	// private void deleteNewUsersRevenue() {
	// PersistenceManager pm = PMF.getNewPersistenceManager();
	//
	// Query q = pm.newQuery(CountryExpense.class);
	// q.setFilter("category == '" + ExpenseType.NEW_USERS.name() + "'");
	// List<CountryExpense> newUsers = (List<CountryExpense>) q.execute();
	// for (CountryExpense e : newUsers) {
	// Query revenueQ = pm.newQuery(CountryTreasury.class);
	// revenueQ.setFilter("eRepDay == " + e.geteRepDay());
	// revenueQ.setUnique(true);
	// CountryTreasury t = (CountryTreasury) revenueQ.execute();
	// t.setManualMdFTaxRevenue(t.getManualMdFTaxRevenue() - e.getValue());
	// e.setDeletedOn(new Date());
	// e.setDeletedById("RoBoT");
	// }
	//
	// }

	private void createPartyEmails(PoliticalParty party) {

		Updater updater = new Updater();

		PartyEmail email = new PartyEmail();
		email.setId(party.name());
		email.setEmail("mr.marcelofs@gmail.com");
		email.setRecipientName("nW0lf");
		email.setSendBBCode(false);
		updater.save(email);

	}

	private void addParty(PoliticalParty party, Integer partyFirstDay) {

		int daysToAdd = partyFirstDay - 1798;

		Updater updater = new Updater();
		for (int i = 0; i < daysToAdd; i++) {
			PoliticalPartyStats stats = new PoliticalPartyStats();
			stats.seteRepDay(1798 + i);
			stats.setMembersCount(0);
			stats.setParty(party);
			updater.save(stats);
		}

		CacheFacade
				.remove("mfs.robot.server.erepublik.data.CountryDAO.getPartyHistory(PoliticalParty="
						+ party.name() + ";)");
		CacheFacade
				.remove("mfs.robot.server.erepublik.data.CountryDAO.getPoliticalHistory()");

	}

	// private void calculateExpenses(HttpServletResponse resp, Integer
	// firstDay)
	// throws IOException {
	//
	// resp.getWriter().write(
	// "Calculando desde o dia: " + firstDay + " até o dia "
	// + geteRepDay() + "\n");
	//
	// CountryDAO cdao = new CountryDAO();
	// ExpensesDAO edao = new ExpensesDAO();
	//
	// double totalRevenue = 0;
	// for (CountryTreasury t : cdao.getCountryTreasuryHistory(firstDay))
	// totalRevenue += t.getTaxRevenue();
	//
	// resp.getWriter().write("Total arrecadado: " + totalRevenue + "BRL\n");
	//
	// double totalExpenses = 0;
	// for (CountryExpense e : edao.getExpensesHistory(firstDay))
	// totalExpenses += e.getValue();
	//
	// resp.getWriter().write("Total gasto: " + totalExpenses + "BRL\n");
	//
	// resp.getWriter().write(
	// "Balanço: " + (totalRevenue - totalExpenses) + "BRL\n");
	// }
}
