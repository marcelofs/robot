package mfs.robot.server.erepublik.data;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import mfs.robot.server.PMF;
import mfs.robot.server.cache.Cacheable;
import mfs.robot.server.erepublik.eRepTimeCalculator;
import mfs.robot.shared.erepublik.data.CitizenAccount;
import mfs.robot.shared.erepublik.data.CountryBonuses;
import mfs.robot.shared.erepublik.data.CountryPresident;
import mfs.robot.shared.erepublik.data.CountryStats;
import mfs.robot.shared.erepublik.data.CountryTaxes;
import mfs.robot.shared.erepublik.data.CountryTreasury;
import mfs.robot.shared.erepublik.data.PoliticalParty;
import mfs.robot.shared.erepublik.data.PoliticalPartyMembers;
import mfs.robot.shared.erepublik.data.PoliticalPartyMembersDiff;
import mfs.robot.shared.erepublik.data.PoliticalPartyStats;

public class CountryDAO {

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<CountryStats> getCountryStatsHistory() {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(CountryStats.class);
			q.setOrdering("eRepDay asc");
			return pm.detachCopyAll((List<CountryStats>) q.execute());
		} finally {
			pm.close();
		}
	}

	@Cacheable
	public CountryStats getCountryStats(Integer eRepDay) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(CountryStats.class);
			q.setFilter("eRepDay == " + eRepDay);
			q.setUnique(true);
			return pm.detachCopy((CountryStats) q.execute());
		} finally {
			pm.close();
		}
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<CountryBonuses> getCountryBonusesHistory() {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(CountryBonuses.class);
			q.setOrdering("eRepDay asc");
			return pm.detachCopyAll((List<CountryBonuses>) q.execute());
		} finally {
			pm.close();
		}
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<CitizenAccount> getCitizenAccountHistory(Long eRepId) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(CitizenAccount.class);
			q.setFilter("eRepId == " + eRepId);
			q.setOrdering("eRepDay asc");
			return pm.detachCopyAll((List<CitizenAccount>) q.execute());
		} finally {
			pm.close();
		}
	}

	@Cacheable
	public CitizenAccount getCitizenAccount(Long eRepId, Integer eRepDay) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(CitizenAccount.class);
			q.setFilter("eRepId == " + eRepId + " && eRepDay == " + eRepDay);
			q.setUnique(true);
			return pm.detachCopy((CitizenAccount) q.execute());
		} finally {
			pm.close();
		}
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<CountryTaxes> getCountryTaxesHistory() {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(CountryTaxes.class);
			q.setOrdering("eRepDay asc");
			return pm.detachCopyAll((List<CountryTaxes>) q.execute());
		} finally {
			pm.close();
		}
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<CountryTreasury> getCountryTreasuryHistory() {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(CountryTreasury.class);
			q.setOrdering("eRepDay asc");
			return pm.detachCopyAll((List<CountryTreasury>) q.execute());
		} finally {
			pm.close();
		}
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<CountryTreasury> getCountryTreasuryHistory(
			Integer firstDay) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(CountryTreasury.class);
			q.setFilter("eRepDay >= " + firstDay);
			q.setOrdering("eRepDay asc");
			return pm.detachCopyAll((List<CountryTreasury>) q.execute());
		} finally {
			pm.close();
		}
	}

	@Cacheable
	public CountryTreasury getCountryTreasury(Integer day) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(CountryTreasury.class);
			q.setFilter("eRepDay == " + day);
			q.setUnique(true);
			return pm.detachCopy((CountryTreasury) q.execute());
		} finally {
			pm.close();
		}
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<PoliticalPartyStats> getPartyHistory(PoliticalParty party) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			int today = eRepTimeCalculator.geteRepDay();
			Query q = pm.newQuery(PoliticalPartyStats.class);
			q.setFilter("party == '" + party.name() + "' && eRepDay >= " + (today - 90));
			q.setOrdering("eRepDay asc");
			return pm.detachCopyAll((List<PoliticalPartyStats>) q.execute());
		} finally {
			pm.close();
		}
	}

	@Cacheable
	public Map<PoliticalParty, Collection<PoliticalPartyStats>> getPoliticalHistory() {

		Map<PoliticalParty, Collection<PoliticalPartyStats>> parties = new HashMap<PoliticalParty, Collection<PoliticalPartyStats>>();
		for (PoliticalParty p : PoliticalParty.values())
			parties.put(p, getPartyHistory(p));

		return parties;
	}

	public PoliticalPartyMembers getPartyMembers(PoliticalParty party,
			Integer eRepDay) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(PoliticalPartyMembers.class);
			q.setFilter("eRepDay == " + eRepDay + " && party == '"
					+ party.name() + "'");
			q.setUnique(true);
			PoliticalPartyMembers m = (PoliticalPartyMembers) q.execute();
			// lazy loading sucks
			if (m == null)
				return null;
			m.getMembers();
			return pm.detachCopy(m);
		} finally {
			pm.close();
		}
	}

	@Cacheable
	public PoliticalPartyMembersDiff getPartyMembersDiff(PoliticalParty party,
			Integer eRepDay) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(PoliticalPartyMembersDiff.class);
			q.setFilter("eRepDay == " + eRepDay + " && party == '"
					+ party.name() + "'");
			q.setUnique(true);
			PoliticalPartyMembersDiff m = (PoliticalPartyMembersDiff) q
					.execute();
			// lazy loading sucks
			m.getLeftMembers();
			m.getNewMembers();
			return pm.detachCopy(m);
		} finally {
			pm.close();
		}
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<PoliticalPartyMembersDiff> getPartyMembersDiffHistory(
			PoliticalParty party) {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(PoliticalPartyMembersDiff.class);
			q.setFilter("party == '" + party.name() + "'");
			q.setOrdering("eRepDay asc");
			return pm.detachCopyAll((List<PoliticalPartyMembersDiff>) q
					.execute());
		} finally {
			pm.close();
		}
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	public Collection<CountryPresident> getPresidents() {
		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {
			Query q = pm.newQuery(CountryPresident.class);
			q.setOrdering("firstErepDay desc");
			return pm.detachCopyAll((List<CountryPresident>) q.execute());
		} finally {
			pm.close();
		}
	}

}
