package mfs.robot.server.erepublik.erpk;

import static com.google.appengine.api.urlfetch.FetchOptions.Builder.withDeadline;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;

import mfs.robot.shared.erepublik.data.CountryBonuses;
import mfs.robot.shared.erepublik.data.CountryStats;
import mfs.robot.shared.erepublik.data.CountryTaxes;
import mfs.robot.shared.erepublik.data.JobMarketOffer;
import mfs.robot.shared.erepublik.data.MarketplaceOffer;
import mfs.robot.shared.erepublik.data.MonetaryOffer;
import mfs.robot.shared.erepublik.data.MonetaryType;
import mfs.robot.shared.erepublik.data.PoliticalPartyStats;
import mfs.robot.shared.erepublik.data.ProductQuality;
import mfs.robot.shared.erepublik.data.ProductType;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;

public class ErpkConnector {

	private URLFetchService		fetchService	= URLFetchServiceFactory
														.getURLFetchService();
	private JSONParser			parser			= new JSONParser();

	private static final Logger	log				= Logger.getLogger(ErpkConnector.class
														.getName());

	private String fetchUrl(URL url) throws IOException {

		HTTPResponse response = fetchService.fetch(new HTTPRequest(url,
				HTTPMethod.GET, withDeadline(60)));

		if (response.getResponseCode() != 200)
			throw new IllegalStateException("Invalid HTTP Response Code from "
					+ url.toString() + " : " + response.getResponseCode());

		return new String(response.getContent(), Charset.defaultCharset());
	}

	public MarketplaceOffer getMarketplaceOffers(ProductType type,
			ProductQuality quality, Integer eRepDay, Integer eRepHour)
			throws IOException, ParseException {

		String fetchResponse = fetchUrl(ErpkApiURL.getUrl(type, quality));

		try {
			JSONArray json = (JSONArray) parser.parse(fetchResponse);
			MarketplaceOffer offers = new MarketplaceOffer();
			double averagePrices = 0;
			double minimumPrice = Double.MAX_VALUE;
			int sumQuantities = 0;
			for (Object obj : json) {
				JSONObject jsonObj = (JSONObject) obj;
				double offerPrice = Double.valueOf(jsonObj.get("price")
						.toString());
				double offerQuantity = Integer.valueOf(jsonObj.get("amount")
						.toString());
				averagePrices += offerPrice * offerQuantity;
				sumQuantities += offerQuantity;

				if (minimumPrice > offerPrice)
					minimumPrice = offerPrice;
			}

			offers.setType(type);
			offers.setQuality(quality);
			offers.setAveragePrice(averagePrices / sumQuantities);
			offers.setMinimumPrice(minimumPrice);
			offers.setQuantity(sumQuantities);
			offers.seteRepDay(eRepDay);
			offers.seteRepTime(eRepHour);

			return offers;
		} catch (ParseException e) {
			log.log(Level.FINE, fetchResponse);
			throw e;
		}
	}

	public JobMarketOffer getJobMarketOffers(Integer eRepDay, Integer eRepHour)
			throws IOException, ParseException {
		String fetchResponse = fetchUrl(ErpkApiURL
				.getUrl(ErpkApiURL.JOB_MARKET));

		JSONArray json = (JSONArray) parser.parse(fetchResponse);
		JobMarketOffer offer = new JobMarketOffer();
		double averageSalary = 0;
		double maximumSalary = Double.MIN_VALUE;
		for (Object obj : json) {
			JSONObject jsonObj = (JSONObject) obj;
			double salary = Double.valueOf(jsonObj.get("salary").toString());
			averageSalary += salary;
			if (maximumSalary < salary)
				maximumSalary = salary;
		}
		offer.setAverageSalary(averageSalary / json.size());
		offer.setMaximumSalary(maximumSalary);
		offer.seteRepDay(eRepDay);
		offer.seteRepTime(eRepHour);
		return offer;
	}

	public MonetaryOffer getMonetaryMarketOffers(MonetaryType type,
			Integer eRepDay, Integer eRepHour) throws ParseException,
			IOException {
		String fetchResponse = fetchUrl(ErpkApiURL.getUrl(type));

		JSONObject json = (JSONObject) parser.parse(fetchResponse);
		JSONArray jsOffers = (JSONArray) json.get("offers");
		MonetaryOffer offers = new MonetaryOffer();
		double averagePrice = 0;
		double minimumPrice = Double.MAX_VALUE;
		double sumQuantities = 0;
		for (Object obj : jsOffers) {
			JSONObject jsonObj = (JSONObject) obj;
			double offerPrice = Double.valueOf(jsonObj.get("rate").toString());
			double offerQuantity = Double.valueOf(jsonObj.get("amount")
					.toString());
			averagePrice += offerPrice * offerQuantity;
			sumQuantities += offerQuantity;

			if (minimumPrice > offerPrice)
				minimumPrice = offerPrice;
		}

		offers.setType(type);
		offers.setAveragePrice(averagePrice / sumQuantities);
		offers.setMinimumPrice(minimumPrice);
		offers.setQuantity(sumQuantities);
		offers.seteRepDay(eRepDay);
		offers.seteRepTime(eRepHour);
		return offers;
	}

	public void fillCountrySociety(CountryStats stats) throws IOException,
			ParseException {

		String fetchResponse = fetchUrl(ErpkApiURL.COUNTRY_SOCIETY.getUrl());
		JSONObject jsonObj = (JSONObject) parser.parse(fetchResponse);

		// stats.seteRepCountryID(jsonObj.get("id").toString());
		stats.setActiveCitizens(Integer.valueOf(jsonObj.get("active_citizens")
				.toString()));
		stats.setNewCitizens(Integer.valueOf(jsonObj.get("new_citizens_today")
				.toString()));
		stats.setAverageLevel(Integer.valueOf(jsonObj.get(
				"average_citizen_level").toString()));
		stats.setRegionCount(Integer.valueOf(jsonObj.get("region_count")
				.toString()));

	}

	public void fillCountryEconomy(CountryTaxes cTaxes, CountryBonuses cBonuses)
			throws IOException, ParseException {

		String fetchResponse = fetchUrl(ErpkApiURL.COUNTRY_ECONOMY.getUrl());
		JSONObject jsonObj = (JSONObject) parser.parse(fetchResponse);

		// JSONObject treasury = (JSONObject) jsonObj.get("treasury");
		JSONObject bonuses = (JSONObject) jsonObj.get("bonuses");
		JSONObject taxes = (JSONObject) jsonObj.get("taxes");

		JSONObject foodTaxes = (JSONObject) taxes.get("food");
		JSONObject frmTaxes = (JSONObject) taxes.get("frm");
		JSONObject weaponTaxes = (JSONObject) taxes.get("weapons");
		JSONObject wrmTaxes = (JSONObject) taxes.get("wrm");

		// cTreasury.setGoldTreasury(Double.valueOf(treasury.get("gold")
		// .toString()));
		// cTreasury.setCurrencyTreasury(Double.valueOf(treasury.get("cc")
		// .toString()));
		cBonuses.setFoodBonus(Double.valueOf(bonuses.get("food").toString()));
		cBonuses.setFrmBonus(Double.valueOf(bonuses.get("frm").toString()));
		cBonuses.setWeaponsBonus(Double.valueOf(bonuses.get("weapons")
				.toString()));
		cBonuses.setWrmBonus(Double.valueOf(bonuses.get("wrm").toString()));

		cTaxes.setIncomeTax(Integer.valueOf(foodTaxes.get("income").toString()));
		cTaxes.setFoodVat(Integer.valueOf(foodTaxes.get("vat").toString()));
		cTaxes.setFoodImport(Integer
				.valueOf(foodTaxes.get("import").toString()));
		cTaxes.setFrmImport(Integer.valueOf(frmTaxes.get("import").toString()));

		cTaxes.setWeaponsVat(Integer.valueOf(weaponTaxes.get("vat").toString()));
		cTaxes.setWeaponsImport(Integer.valueOf(weaponTaxes.get("import")
				.toString()));
		cTaxes.setWrmImport(Integer.valueOf(wrmTaxes.get("import").toString()));

	}

	public void fillPartyMembers(PoliticalPartyStats stats) throws IOException,
			ParseException {
		String fetchResponse = fetchUrl(ErpkApiURL.PARTY.getUrl(stats
				.getParty().getPartyCode().toString()));
		JSONObject jsonObj = (JSONObject) parser.parse(fetchResponse);

		stats.setMembersCount(Integer
				.valueOf(jsonObj.get("members").toString()));

	}
}
