package mfs.robot.server.erepublik.erpk;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mfs.robot.server.Updater;
import mfs.robot.server.cache.CacheFacade;
import mfs.robot.shared.erepublik.data.JobMarketOffer;
import mfs.robot.shared.erepublik.data.Market;
import mfs.robot.shared.erepublik.data.MarketplaceOffer;
import mfs.robot.shared.erepublik.data.MonetaryOffer;
import mfs.robot.shared.erepublik.data.MonetaryType;
import mfs.robot.shared.erepublik.data.ProductQuality;
import mfs.robot.shared.erepublik.data.ProductType;

import org.json.simple.parser.ParseException;

import com.google.apphosting.api.ApiProxy.OverQuotaException;

public class ErpkUpdateTaskServlet extends HttpServlet {

	private static final long	serialVersionUID	= 1L;

	private static final Logger	log					= Logger.getLogger(ErpkUpdateTaskServlet.class
															.getName());

	private final ErpkConnector	connector			= new ErpkConnector();
	private final Updater		updater				= new Updater();

	@Override
	public void doPost(final HttpServletRequest req,
			final HttpServletResponse res) throws ServletException {

		Market market = Market.valueOf(req.getParameter("market"));

		Integer eRepDay = Integer.valueOf(req.getParameter("day"));
		Integer eRepHour = Integer.valueOf(req.getParameter("hour"));

		log.log(Level.INFO, "eTime: day " + eRepDay + ", " + eRepHour + "h");
		log.log(Level.INFO, "Starting update for " + market.name());
		try {
			switch (market) {
				case MARKETPLACE:
					updateMarketplace(req.getParameter("type"),
							req.getParameter("quality"), eRepDay, eRepHour);
					break;
				case JOB:
					updateJobMarket(eRepDay, eRepHour);
					break;
				case MONETARY:
					updateMonetaryMarket(req.getParameter("type"), eRepDay,
							eRepHour);
					break;
			}
		} catch (OverQuotaException e) {
			log.log(Level.SEVERE, "Over quota! " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			log.log(Level.SEVERE, e.getMessage());
			throw new ServletException(e);
		}

		log.log(Level.INFO, "Finished update");

	}

	private void updateMarketplace(String type, String quality,
			Integer eRepDay, Integer eRepHour) throws IOException,
			ParseException {
		log.log(Level.INFO, type + " " + quality);
		MarketplaceOffer offers = connector.getMarketplaceOffers(
				ProductType.valueOf(type), ProductQuality.valueOf(quality),
				eRepDay, eRepHour);
		updater.save(offers);

//		CacheFacade.put("marketplace" + type + quality + eRepDay + eRepHour,
//				offers);

		CacheFacade
				.remove("mfs.robot.server.erepublik.data.MarketDAO.getDailyMarketplaceOffer(ProductType="
						+ type
						+ ";ProductQuality="
						+ quality
						+ ";Integer="
						+ eRepDay + ";)");
	}

	private void updateJobMarket(Integer eRepDay, Integer eRepHour)
			throws IOException, ParseException {
		JobMarketOffer offers = connector.getJobMarketOffers(eRepDay, eRepHour);
		updater.save(offers);

//		CacheFacade.put("jobs" + eRepDay + eRepHour, offers);
		CacheFacade
				.remove("mfs.robot.server.erepublik.data.MarketDAO.getDailyJobMarketOffers(Integer="
						+ eRepDay + ";)");
	}

	private void updateMonetaryMarket(String type, Integer eRepDay,
			Integer eRepHour) throws ParseException, IOException {
		log.log(Level.INFO, type);
		MonetaryOffer offers = connector.getMonetaryMarketOffers(
				MonetaryType.valueOf(type), eRepDay, eRepHour);
		updater.save(offers);

		// Queue queue = QueueFactory.getDefaultQueue();
		// queue.add(withUrl("/bip/gold"));

//		CacheFacade.put("monetary" + type + eRepDay + eRepHour, offers);
		CacheFacade
				.remove("mfs.robot.server.erepublik.data.MarketDAO.getDailyMonetaryOffers(MonetaryType="
						+ type + ";Integer=" + eRepDay + ";)");
	}
}
