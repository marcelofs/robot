package mfs.robot.server.erepublik.erpk;

import java.net.MalformedURLException;
import java.net.URL;

import mfs.robot.shared.erepublik.data.MonetaryType;
import mfs.robot.shared.erepublik.data.ProductQuality;
import mfs.robot.shared.erepublik.data.ProductType;

public enum ErpkApiURL
{

	JOB_MARKET("{host}/jobmarket/{country}/1.json?key={key}"), 
	CURRENCY_MARKET("{host}/exchange/0/1.json?key={key}"), 
	GOLD_MARKET("{host}/exchange/1/1.json?key={key}"),

	WEAPON_Q1("{host}/market/{country}/weapons/1/1.json?key={key}"), 
	WEAPON_Q2("{host}/market/{country}/weapons/2/1.json?key={key}"), 
	WEAPON_Q3("{host}/market/{country}/weapons/3/1.json?key={key}"), 
	WEAPON_Q4("{host}/market/{country}/weapons/4/1.json?key={key}"), 
	WEAPON_Q5("{host}/market/{country}/weapons/5/1.json?key={key}"), 
	WEAPON_Q6("{host}/market/{country}/weapons/6/1.json?key={key}"), 
	WEAPON_Q7("{host}/market/{country}/weapons/7/1.json?key={key}"), 
	WEAPON_RAW("{host}/market/{country}/wrm/1/1.json?key={key}"),

	FOOD_Q1("{host}/market/{country}/food/1/1.json?key={key}"), 
	FOOD_Q2("{host}/market/{country}/food/2/1.json?key={key}"), 
	FOOD_Q3("{host}/market/{country}/food/3/1.json?key={key}"), 
	FOOD_Q4("{host}/market/{country}/food/4/1.json?key={key}"), 
	FOOD_Q5("{host}/market/{country}/food/5/1.json?key={key}"), 
	FOOD_Q6("{host}/market/{country}/food/6/1.json?key={key}"), 
	FOOD_Q7("{host}/market/{country}/food/7/1.json?key={key}"), 
	FOOD_RAW("{host}/market/{country}/frm/1/1.json?key={key}"),

	COUNTRY_SOCIETY("{host}/country/{country}/society.json?key={key}"), 
	COUNTRY_ECONOMY("{host}/country/{country}/economy.json?key={key}"),

	PARTY("{host}/party/{id}.json?key={key}")

	;


	private static final String	apiHost		= "http://api.bellenus.tk/web/app.php";
	// nW0lf
	private static final String	apiKey		= "";

	private static final String	countryCode	= "BR";

	private final String		url;

	private ErpkApiURL(String url) {
		this.url = url;
	}

	public URL getUrl() {
		try {
			return new URL(url.replace("{host}", apiHost)
					.replace("{country}", countryCode).replace("{key}", apiKey));
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public URL getUrl(String id) {
		try {
			return new URL(url.replace("{host}", apiHost)
					.replace("{country}", countryCode).replace("{id}", id)
					.replace("{key}", apiKey));
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static URL getUrl(ErpkApiURL url) {
		return url.getUrl();
	}

	public static URL getUrl(ProductType type, ProductQuality quality) {
		return ErpkApiURL.valueOf(type.name() + "_" + quality.name()).getUrl();
	}

	public static URL getUrl(MonetaryType type) {
		return ErpkApiURL.valueOf(type.name() + "_MARKET").getUrl();
	}

}
