package mfs.robot.server.erepublik.erpk;

import static com.google.appengine.api.taskqueue.TaskOptions.Builder.withUrl;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mfs.robot.server.Updater;
import mfs.robot.server.cache.CacheFacade;
import mfs.robot.server.erepublik.DailyUpdateServlet;
import mfs.robot.server.erepublik.eRepTimeCalculator;
import mfs.robot.server.erepublik.scrapper.ErepScrapperTask;
import mfs.robot.shared.erepublik.data.CountryBonuses;
import mfs.robot.shared.erepublik.data.CountryStats;
import mfs.robot.shared.erepublik.data.CountryTaxes;
import mfs.robot.shared.erepublik.data.PoliticalParty;
import mfs.robot.shared.erepublik.data.PoliticalPartyStats;

import org.json.simple.parser.ParseException;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.apphosting.api.ApiProxy.OverQuotaException;

@Deprecated
public class ErpkDailyUpdateTask extends HttpServlet {

	private static final long	serialVersionUID	= 1L;

	private static final Logger	log					= Logger.getLogger(DailyUpdateServlet.class
															.getName());

	private final ErpkConnector	connector			= new ErpkConnector();
	private final Updater		updater				= new Updater();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		log.log(Level.INFO, "eTime: day " + eRepTimeCalculator.geteRepDay()
				+ ", " + eRepTimeCalculator.geteRepHour() + "h");

		String op = req.getParameter("op");

		try {
//			Integer day = Integer.valueOf(req.getParameter("day"));
//			if ("COUNTRY".equals(op))
//				updateCountry(day);
//			if ("PARTY".equals(op))
//				updateParty(req.getParameter("id"), day);
		} catch (OverQuotaException e) {
			log.log(Level.SEVERE, "Over quota! " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			log.log(Level.SEVERE, e.getMessage());
			throw new ServletException(e);
		}

//		log.log(Level.INFO, "Finished update");

	}

	private void updateParty(String id, Integer day) throws IOException,
			ParseException {
//		log.log(Level.INFO, "Starting update for party " + id);
//
//		PoliticalParty party = PoliticalParty.fromId(id);
//		PoliticalPartyStats stats = new PoliticalPartyStats();
//
//		stats.seteRepDay(day);
//		stats.setParty(party);
//
//		connector.fillPartyMembers(stats);
//
//		updater.save(stats);
//
//		CacheFacade
//				.remove("mfs.robot.server.erepublik.data.CountryDAO.getPartyHistory(PoliticalParty="
//						+ party.name() + ";)");
//		CacheFacade
//				.remove("mfs.robot.server.erepublik.data.CountryDAO.getPoliticalHistory()");
//
//		log.log(Level.INFO, "Finished update");
	}

	private void updateCountry(Integer day) throws IOException, ParseException {
//		log.log(Level.INFO, "Starting update for country stats");

//		CountryStats stats = new CountryStats();
//		CountryTaxes taxes = new CountryTaxes();
//		CountryBonuses bonuses = new CountryBonuses();
//
//		stats.seteRepDay(day);
//		taxes.seteRepDay(day);
//		bonuses.seteRepDay(day);

//		connector.fillCountrySociety(stats);
//		connector.fillCountryEconomy(taxes, bonuses);

//		updater.save(stats);
//		updater.save(taxes);
//		updater.save(bonuses);

//		Queue defaultQueue = QueueFactory.getQueue("scrapper");
//		defaultQueue.add(withUrl("/erepublik/update/scrapper")
//				.param("operation",
//						ErepScrapperTask.Operation.COUNTRY_EGOV.name())
//				.param("statusId", stats.getId().toString())
//				.param("day", day.toString()));
//
//		CacheFacade.put("stats" + day, stats);
//		CacheFacade.put("taxes" + day, taxes);
//		CacheFacade.put("bonuses" + day, bonuses);
//		CacheFacade
//				.remove("mfs.robot.server.erepublik.data.CountryDAO.getCountryStatsHistory()");
//		CacheFacade
//				.remove("mfs.robot.server.erepublik.data.CountryDAO.getCountryStats(Integer="
//						+ day + ";)");
//		CacheFacade
//				.remove("mfs.robot.server.erepublik.data.CountryDAO.getCountryBonusesHistory()");
//		CacheFacade
//				.remove("mfs.robot.server.erepublik.data.CountryDAO.getCountryTaxesHistory()");
//		CacheFacade
//				.remove("mfs.robot.server.erepublik.RoBoTRPCServlet.getDayHour()");
	}

}
