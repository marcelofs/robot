package mfs.robot.server.erepublik;

import java.util.Collection;

import mfs.robot.client.erepublik.rpc.RoBoTOpenRPC;
import mfs.robot.server.erepublik.data.CountryDAO;
import mfs.robot.server.erepublik.data.ExpensesDAO;
import mfs.robot.server.erepublik.data.MarketDAO;
import mfs.robot.shared.erepublik.ExpensesInitialInfo;
import mfs.robot.shared.erepublik.data.CitizenAccount;
import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.CountryPresident;
import mfs.robot.shared.erepublik.data.CountryTreasury;
import mfs.robot.shared.erepublik.dto.BalanceDTO;
import mfs.robot.shared.erepublik.dto.CashFlowDTO;
import mfs.robot.shared.erepublik.dto.CountryExpenseSum;
import mfs.robot.shared.erepublik.dto.MonthlyRevenueDTO;
import mfs.robot.shared.erepublik.dto.TaxesOriginDTO;

import com.google.common.collect.Multimap;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class RoBoTOpenRPCServlet extends RemoteServiceServlet implements
		RoBoTOpenRPC {

	private static final long	serialVersionUID	= 1L;

	private CountryDAO			country				= new CountryDAO();
	private ExpensesDAO			expenses			= new ExpensesDAO();
	private MarketDAO			market				= new MarketDAO();

	// private final int taxesOriginDay = 1784;

	@Override
	public Collection<CountryTreasury> getCountryTreasuryHistory() {
		return country.getCountryTreasuryHistory();
	}

	@Override
	public Collection<CountryExpense> getExpenses(Integer day) {
		return expenses.getExpenses(day);
	}

	@Override
	public Collection<CountryExpense> getExpenses() {
		return expenses.getExpensesHistory();
	}

	@Override
	public Multimap<Integer, CountryExpenseSum> getExpensesSum() {
		return expenses.getExpensesSum();
	}

	@Override
	public TaxesOriginDTO getCountryTaxesOriginHistory() {
		TaxesOriginDTO dto = new TaxesOriginDTO();

		dto.eRepDay = eRepTimeCalculator.geteRepDay();
		dto.treasuries = country.getCountryTreasuryHistory();
		dto.citizens = country.getCountryStatsHistory();
		dto.jobMarkets = market.getAgregatedJobMarketOffers();
		dto.taxes = country.getCountryTaxesHistory();

		return dto;
	}

	@Override
	public ExpensesInitialInfo getExpensesInitialInfo() {
		ExpensesInitialInfo info = new ExpensesInitialInfo();

		info.eRepDay = eRepTimeCalculator.geteRepDay();
		info.presidents = country.getPresidents();

		return info;
	}

	@Override
	public BalanceDTO getBalanceData(CountryPresident cp) {
		BalanceDTO dto = new BalanceDTO();

		dto.expenses = expenses.getExpensesSum(); // FIXME
		dto.taxRevenues = country.getCountryTreasuryHistory(cp
				.getFirstErepDay() - 1);
		dto.incomes = expenses.getIncomesHistory(cp.getFirstErepDay() - 1);

		return dto;
	}

	@Override
	public MonthlyRevenueDTO getMonthlyRevenueData() {
		MonthlyRevenueDTO dto = new MonthlyRevenueDTO();

		dto.presidents = country.getPresidents();
		dto.taxRevenues = country.getCountryTreasuryHistory();

		return dto;
	}

	@Override
	public CashFlowDTO getCashFlowData(CountryPresident cp) {
		CashFlowDTO dto = new CashFlowDTO();
		dto.eRepDay = eRepTimeCalculator.geteRepDay();
		dto.expenses = expenses.getExpensesHistory(cp.getFirstErepDay() - 1);
		dto.incomes = expenses.getIncomesHistory(cp.getFirstErepDay() - 1);
		dto.transfers = expenses.getTransfersHistory(cp.getFirstErepDay() - 1);
		dto.treasuries = country
				.getCountryTreasuryHistory(cp.getFirstErepDay() - 1);
		return dto;
	}

	@Override
	@Deprecated
	public CashFlowDTO getCashFlowData(String selectedOrgName,
			String selectedOrgId) {
		CashFlowDTO dto = new CashFlowDTO();

		dto.eRepDay = eRepTimeCalculator.geteRepDay();
		dto.expenses = expenses.getExpensesHistory(selectedOrgName);
		dto.incomes = expenses.getIncomesHistory(selectedOrgName);
		dto.transfers = expenses.getTransfersHistory(0); // FIXME
		dto.countryOrg = country.getCitizenAccountHistory(Long
				.valueOf(selectedOrgId));

		return dto;
	}

	@Override
	public CashFlowDTO getCashFlowData(String selectedOrgName,
			String selectedOrgId, boolean loadTransfers, boolean loadOrgValue) {
		CashFlowDTO dto = new CashFlowDTO();

		dto.eRepDay = eRepTimeCalculator.geteRepDay();
		dto.expenses = expenses.getExpensesHistory(selectedOrgName);
		dto.incomes = expenses.getIncomesHistory(selectedOrgName);
		if (loadTransfers)
			dto.transfers = expenses.getTransfersHistory(0); // FIXME
		if (loadOrgValue)
			dto.countryOrg = country.getCitizenAccountHistory(Long
					.valueOf(selectedOrgId));

		return dto;
	}

	@Override
	public Collection<CitizenAccount> getCitizenAccountHistory(Long eRepId) {
		return country.getCitizenAccountHistory(eRepId);
	}

}
