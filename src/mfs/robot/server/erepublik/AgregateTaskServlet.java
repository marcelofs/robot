package mfs.robot.server.erepublik;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mfs.robot.server.PMF;
import mfs.robot.server.Updater;
import mfs.robot.server.cache.CacheFacade;
import mfs.robot.shared.erepublik.data.AgregatedJobMarketOffer;
import mfs.robot.shared.erepublik.data.AgregatedMarketplaceOffer;
import mfs.robot.shared.erepublik.data.AgregatedMonetaryOffer;
import mfs.robot.shared.erepublik.data.JobMarketOffer;
import mfs.robot.shared.erepublik.data.Market;
import mfs.robot.shared.erepublik.data.MarketplaceOffer;
import mfs.robot.shared.erepublik.data.MonetaryOffer;
import mfs.robot.shared.erepublik.data.MonetaryType;
import mfs.robot.shared.erepublik.data.ProductQuality;
import mfs.robot.shared.erepublik.data.ProductType;

import org.json.simple.parser.ParseException;

public class AgregateTaskServlet extends HttpServlet {

	private static final long	serialVersionUID	= 1L;

	private static final Logger	log					= Logger.getLogger(AgregateTaskServlet.class
															.getName());
	private final Updater		updater				= new Updater();

	@Override
	public void doPost(final HttpServletRequest req,
			final HttpServletResponse res) throws ServletException {

		Market market = Market.valueOf(req.getParameter("market"));
		Integer eRepDay = Integer.valueOf(req.getParameter("day"));

		log.log(Level.INFO, "Starting to agregate data for " + market.name()
				+ ", day " + eRepDay);
		try {
			switch (market) {
				case MARKETPLACE:
					agregateMarketplace(req.getParameter("type"),
							req.getParameter("quality"), eRepDay);
					break;
				case JOB:
					agregateJobMarket(eRepDay);
					break;
				case MONETARY:
					agregateMonetaryMarket(req.getParameter("type"), eRepDay);
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.log(Level.SEVERE, e.getMessage());
			throw new ServletException(e);
		}

		log.log(Level.INFO, "Finished agregation");

	}

	@SuppressWarnings("unchecked")
	private void agregateMarketplace(String type, String quality,
			Integer eRepDay) throws IOException, ParseException {
		log.log(Level.INFO, type + " " + quality);

		PersistenceManager pm = PMF.getNewPersistenceManager();

		String filter = "type == '" + type + "' && quality == '" + quality
				+ "' && eRepDay == " + eRepDay;
		Query q = pm.newQuery(MarketplaceOffer.class, filter);

		List<MarketplaceOffer> offers = (List<MarketplaceOffer>) q.execute();

		double average = 0;
		double maximum = Integer.MIN_VALUE;
		double minimum = Integer.MAX_VALUE;
		int quantity = 0;
		for (MarketplaceOffer offer : offers) {
			average += offer.getAverage() * offer.getQuantity();
			quantity += offer.getQuantity();

			if (offer.getMinimum() > maximum)
				maximum = offer.getMinimum();
			if (offer.getMinimum() < minimum)
				minimum = offer.getMinimum();
		}

		AgregatedMarketplaceOffer agregated = new AgregatedMarketplaceOffer();
		agregated.seteRepDay(eRepDay);
		agregated.setQuality(ProductQuality.valueOf(quality));
		agregated.setType(ProductType.valueOf(type));
		if (quantity > 0)
			agregated.setAveragePrice(average / quantity);
		agregated
				.setMinimumPrice(minimum == Integer.MIN_VALUE ? null : minimum);
		agregated
				.setMaximumPrice(maximum == Integer.MAX_VALUE ? null : maximum);

		updater.save(agregated);

		CacheFacade
				.remove("mfs.robot.server.erepublik.data.MarketDAO.getAgregatedMarketplaceOffer(ProductType="
						+ type + ";ProductQuality=" + quality + ";)");
	}

	@SuppressWarnings("unchecked")
	private void agregateJobMarket(Integer eRepDay) throws IOException,
			ParseException {
		PersistenceManager pm = PMF.getNewPersistenceManager();

		String filter = "eRepDay == " + eRepDay;
		Query q = pm.newQuery(JobMarketOffer.class, filter);

		List<JobMarketOffer> offers = (List<JobMarketOffer>) q.execute();

		double average = 0;
		double minimum = Integer.MAX_VALUE;
		double maximum = Integer.MIN_VALUE;
		for (JobMarketOffer offer : offers) {
			average += offer.getAverage();
			if (offer.getMaximum() < minimum)
				minimum = offer.getMaximum();
			if (offer.getMaximum() > maximum)
				maximum = offer.getMaximum();
		}

		AgregatedJobMarketOffer agregated = new AgregatedJobMarketOffer();
		agregated.seteRepDay(eRepDay);
		if (offers.size() > 0)
			agregated.setAverageSalary(average / offers.size());
		agregated.setMinimumSalary(minimum == Integer.MIN_VALUE ? null
				: minimum);
		agregated.setMaximum(maximum == Integer.MAX_VALUE ? null : maximum);

		updater.save(agregated);

		CacheFacade
				.remove("mfs.robot.server.erepublik.data.MarketDAO.getAgregatedJobMarketOffers()");
	}

	@SuppressWarnings("unchecked")
	private void agregateMonetaryMarket(String type, Integer eRepDay)
			throws ParseException, IOException {
		PersistenceManager pm = PMF.getNewPersistenceManager();

		String filter = "type == '" + type + "' && eRepDay == " + eRepDay;
		Query q = pm.newQuery(MonetaryOffer.class, filter);

		List<MonetaryOffer> offers = (List<MonetaryOffer>) q.execute();

		double average = 0;
		double minimum = Integer.MAX_VALUE;
		double maximum = Integer.MIN_VALUE;
		double quantity = 0;
		for (MonetaryOffer offer : offers) {
			average += offer.getAverage() * offer.getQuantity();
			quantity += offer.getQuantity();

			if (offer.getMinimum() > maximum)
				maximum = offer.getMinimum();
			if (offer.getMinimum() < minimum)
				minimum = offer.getMinimum();
		}

		AgregatedMonetaryOffer agregated = new AgregatedMonetaryOffer();
		agregated.seteRepDay(eRepDay);
		agregated.setType(MonetaryType.valueOf(type));
		agregated.setQuantity(quantity);

		if (quantity > 0)
			agregated.setAveragePrice(average / quantity);
		agregated
				.setMinimumPrice(minimum == Integer.MIN_VALUE ? null : minimum);
		agregated
				.setMaximumPrice(maximum == Integer.MAX_VALUE ? null : maximum);

		updater.save(agregated);
		CacheFacade
				.remove("mfs.robot.server.erepublik.data.MarketDAO.getAgregatedMonetaryOffers(MonetaryType="
						+ type + ";)");
	}

}
