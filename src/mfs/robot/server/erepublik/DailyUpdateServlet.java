package mfs.robot.server.erepublik;

import static com.google.appengine.api.taskqueue.TaskOptions.Builder.withUrl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mfs.robot.server.erepublik.scrapper.ErepScrapperTask.Operation;
import mfs.robot.shared.erepublik.CountryAccount;
import mfs.robot.shared.erepublik.data.PoliticalParty;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;

public class DailyUpdateServlet extends HttpServlet {

	private static final long	serialVersionUID	= 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String eRepDay = "" + eRepTimeCalculator.geteRepDay();

		Queue scrapperQueue = QueueFactory.getQueue("scrapper");

//		for (CountryAccount c : CountryAccount.values())
//			if (c.id != null && !c.id.trim().isEmpty())
//				scrapperQueue.add(withUrl("/erepublik/update/scrapper")
//						.param("operation", Operation.CITIZEN_ACCOUNT.name())
//						.param("id", c.id).param("day", eRepDay));

//		scrapperQueue.add(withUrl("/erepublik/update/scrapper").param(
//				"operation", Operation.COUNTRY_ECONOMY.name()).param("day",
//				eRepDay));

//		scrapperQueue.add(withUrl("/erepublik/update/scrapper").param(
//				"operation", Operation.COUNTRY_SOCIETY.name()).param("day",
//				eRepDay));

		for (PoliticalParty p : PoliticalParty.values())
			scrapperQueue.add(withUrl("/erepublik/update/scrapper")
					.param("operation",
							p.getTrackMembers() ? Operation.PARTY_MEMBERS
									.name() : Operation.PARTY_INFO.name())
					.param("id", p.getPartyCode().toString())
					.param("day", eRepDay));

		Queue defaultQueue = QueueFactory.getDefaultQueue();
		defaultQueue
				.add(withUrl("/erepublik/data/clean").param("day", eRepDay));

	}

}
