package mfs.robot.server.erepublik;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mfs.robot.server.PMF;
import mfs.robot.server.Updater;
import mfs.robot.server.cache.CacheFacade;
import mfs.robot.server.cache.Cacheable;
import mfs.robot.shared.erepublik.data.AgregatedMarketplaceProfit;
import mfs.robot.shared.erepublik.data.AgregatedRocketPrice;
import mfs.robot.shared.erepublik.data.CountryBonuses;
import mfs.robot.shared.erepublik.data.CountryTaxes;
import mfs.robot.shared.erepublik.data.JobMarketOffer;
import mfs.robot.shared.erepublik.data.MarketplaceOffer;
import mfs.robot.shared.erepublik.data.MarketplaceProfit;
import mfs.robot.shared.erepublik.data.MonetaryOffer;
import mfs.robot.shared.erepublik.data.MonetaryType;
import mfs.robot.shared.erepublik.data.ProductQuality;
import mfs.robot.shared.erepublik.data.ProductType;
import mfs.robot.shared.erepublik.data.RocketPrice;
import mfs.robot.shared.erepublik.data.RocketQuality;

public class ProfitServlet extends HttpServlet {

	private static final long	serialVersionUID	= 1L;

	private static final Logger	log					= Logger.getLogger(ProfitServlet.class
															.getName());

	private final Updater		updater				= new Updater();

	enum Operation
	{
		CALCULATE, AGREGATE, CALCULATE_ROCKET, AGREGATE_ROCKET
	}

	@Override
	public void doGet(final HttpServletRequest req,
			final HttpServletResponse res) throws ServletException {
		doPost(req, res);
	}

	@Override
	public void doPost(final HttpServletRequest req,
			final HttpServletResponse res) throws ServletException {

		Operation operation = Operation.valueOf(req.getParameter("operation"));
		switch (operation) {
			case CALCULATE:
				calculate(req.getParameter("type"),
						req.getParameter("quality"), req.getParameter("day"),
						req.getParameter("hour"));
				break;
			case CALCULATE_ROCKET:
				calculateRocket(req.getParameter("quality"),
						req.getParameter("day"), req.getParameter("hour"));
				break;
			case AGREGATE:
				agregate(req.getParameter("type"), req.getParameter("quality"),
						req.getParameter("day"));
				break;
			case AGREGATE_ROCKET:
				agregateRocket(req.getParameter("quality"),
						req.getParameter("day"));
				break;
		}

	}

	@SuppressWarnings("unchecked")
	private void agregateRocket(String quality, String eRepDay) {
		PersistenceManager pm = PMF.getNewPersistenceManager();

		Query q = pm.newQuery(RocketPrice.class);
		q.setFilter("eRepDay == " + eRepDay + " && quality == '" + quality
				+ "'");

		List<RocketPrice> offers = (List<RocketPrice>) q.execute();

		double averagePrice = 0;
		double averagePriceGold = 0;
		double minimumPrice = Integer.MAX_VALUE;
		double maximumPrice = Integer.MIN_VALUE;
		for (RocketPrice offer : offers) {
			averagePrice += offer.getAveragePrice();
			averagePriceGold += offer.getAveragePriceGold();
			if (offer.getAveragePrice() < minimumPrice)
				minimumPrice = offer.getAveragePrice();
			if (offer.getAveragePrice() > maximumPrice)
				maximumPrice = offer.getAveragePrice();
		}

		AgregatedRocketPrice agregated = new AgregatedRocketPrice();
		agregated.setAveragePrice(averagePrice / offers.size());
		agregated.setAveragePriceGold(averagePriceGold / offers.size());
		agregated.setMinimumPrice(minimumPrice);
		agregated.setMaximumPrice(maximumPrice);
		agregated.seteRepDay(Integer.valueOf(eRepDay));
		agregated.setQuality(RocketQuality.valueOf(quality));

		updater.save(agregated);

		CacheFacade
				.remove("mfs.robot.server.erepublik.data.MarketDAO.getAgregatedRocketPrice(RocketQuality="
						+ quality + ";)");

	}

	private void calculateRocket(String quality, String eRepDay, String eRepTime) {

		RocketQuality rQuality = RocketQuality.valueOf(quality);

		Double cost = 0d;
		cost += rQuality.getFixedPrice();

		ProductQuality[] neededWeapons = new ProductQuality[] {
				ProductQuality.Q1, ProductQuality.Q2, ProductQuality.Q3,
				ProductQuality.Q4, ProductQuality.Q5, ProductQuality.Q6 };

		for (ProductQuality q : neededWeapons) {
			MarketplaceOffer offer = getPrices(ProductType.WEAPON.name(),
					q.name(), eRepDay, eRepTime);
			cost += offer.getAverage() * rQuality.getRequiredWeapons();
		}

		MonetaryOffer monetary = getGoldPrices(eRepDay, eRepTime);

		RocketPrice rPrice = new RocketPrice();
		rPrice.seteRepDay(Integer.valueOf(eRepDay));
		rPrice.seteRepTime(Integer.valueOf(eRepTime));
		rPrice.setQuality(rQuality);
		rPrice.setAveragePrice(cost);
		rPrice.setAveragePriceGold(cost / monetary.getAverage());

		updater.save(rPrice);

		CacheFacade
				.remove("mfs.robot.server.erepublik.data.MarketDAO.getDailyRocketPrice(RocketQuality="
						+ quality + ";Integer=" + eRepDay + ";)");
	}

	private void calculate(String type, String quality, String day, String hour) {

		ProductQuality pQuality = ProductQuality.valueOf(quality);
		if (pQuality.equals(ProductQuality.RAW))
			return;

		MarketplaceOffer salePrices = getPrices(type, quality, day, hour);
		MarketplaceOffer rawPrices = getRawPrices(type, day, hour);
		JobMarketOffer salaryPrices = getSalaryPrices(day, hour);
		MonetaryOffer goldPrices = getGoldPrices(day, hour);
		CountryBonuses bonuses = getCountryBonuses(day);
		CountryTaxes taxes = getCountryTaxes(day);

		MarketplaceProfit profit = new MarketplaceProfit();
		profit.seteRepDay(Integer.valueOf(day));
		profit.seteRepTime(Integer.valueOf(hour));
		profit.setType(ProductType.valueOf(type));
		profit.setQuality(ProductQuality.valueOf(quality));

		Double salesPrice = ProductType.valueOf(type).equals(ProductType.FOOD) ? salePrices
				.getMinimum() : salePrices.getAverage();

		Double averageProfit = calculateProfit(salesPrice,
				rawPrices.getAverage(), salaryPrices.getAverage(),
				ProductType.valueOf(type), ProductQuality.valueOf(quality),
				bonuses, taxes);

		Double investmentNeeded = calculateInvestment(
				rawPrices.getAverage(), salaryPrices.getAverage(),
				ProductType.valueOf(type), ProductQuality.valueOf(quality),
				bonuses);

		profit.setInvestmentNeeded(investmentNeeded);
		profit.setAverageProfit(averageProfit);
		profit.setAverageProfitGold(averageProfit
				/ goldPrices.getAverage());
		profit.setAverageRoi(averageProfit / investmentNeeded * 100);

		updater.save(profit);

		CacheFacade
				.remove("mfs.robot.server.erepublik.data.MarketDAO.getDailyMarketplaceProfit(ProductType="
						+ type
						+ ";ProductQuality="
						+ quality
						+ ";Integer="
						+ day + ";)");
	}

	private Double calculateInvestment(Double rawPrice, Double averageSalary,
			ProductType type, ProductQuality quality, CountryBonuses bonuses) {

		Integer producedByWorker = getProducedQuantity(type, bonuses);
		Integer rawByWorker = getRawQuantity(type, quality, producedByWorker);
		Integer maxWorkers = getMaxWorkers(quality);

		Double costPerWorkers = ((rawByWorker * rawPrice) + averageSalary)
				* maxWorkers;

		Double costPerManager = rawByWorker * rawPrice;

		return costPerWorkers + costPerManager;

	}

	private Double calculateProfit(Double salesPrice, Double rawPrice,
			Double salary, ProductType type, ProductQuality quality,
			CountryBonuses bonuses, CountryTaxes taxes) {

		Integer producedByWorker = getProducedQuantity(type, bonuses);
		Integer rawByWorker = getRawQuantity(type, quality, producedByWorker);
		Integer maxWorkers = getMaxWorkers(quality);

		Double rawCost = rawByWorker * rawPrice;

		Double costPerProduct = (rawCost + salary) / producedByWorker;
		Double costManager = rawCost / producedByWorker;

		Double taxesDiv = getVATDiv(type, taxes);

		Double profitPerWeapon = (salesPrice / taxesDiv) - costPerProduct;
		Double profitManagerPerWeapon = (salesPrice / taxesDiv) - costManager;

		Double totalProfit = (profitPerWeapon * producedByWorker * maxWorkers)
				+ (profitManagerPerWeapon * producedByWorker);

		return totalProfit;
	}

	private Double getVATDiv(ProductType type, CountryTaxes taxes) {
		switch (type) {
			case FOOD:
				return (1d + (taxes.getFoodVat() / 100d));
			case WEAPON:
				return (1d + (taxes.getWeaponsVat() / 100d));
			default:
				return 0d;
		}
	}

	private Integer getMaxWorkers(ProductQuality quality) {
		switch (quality) {
			case Q1:
				return 1;
			case Q2:
				return 2;
			case Q3:
				return 3;
			case Q4:
				return 5;
			case Q5:
			case Q6:
			case Q7:
				return 10;
			default:
				return 0;
		}
	}

	private Integer getRawQuantity(ProductType type, ProductQuality quality,
			Integer producedByWorker) {
		int multiplier = 0;
		switch (quality) {
			case Q1:
				multiplier = 1;
				break;
			case Q2:
				multiplier = 2;
				break;
			case Q3:
				multiplier = 3;
				break;
			case Q4:
				multiplier = 4;
				break;
			case Q5:
				multiplier = 5;
				break;
			case Q6:
				multiplier = 6;
				break;
			case Q7:
				multiplier = 20;
				break;
			default:
				break;
		}
		if (type.equals(ProductType.WEAPON))
			multiplier *= 10;

		return producedByWorker * multiplier;
	}

	private Integer getProducedQuantity(ProductType type, CountryBonuses bonuses) {
		switch (type) {
			case FOOD:
				return (int) (100 + 100 * bonuses.getFoodBonus());
			case WEAPON:
				return (int) (10 + 10 * bonuses.getWeaponsBonus());
		}
		return 0;
	}

	@Cacheable
	private MarketplaceOffer getRawPrices(String type, String eRepDay,
			String eRepTime) {
		return getPrices(type, ProductQuality.RAW.name(), eRepDay, eRepTime);
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	private MarketplaceOffer getPrices(String type, String quality,
			String eRepDay, String eRepTime) {

		MarketplaceOffer offer = (MarketplaceOffer) CacheFacade
				.get("marketplace" + type + quality + eRepDay + eRepTime);

		if (offer == null) {
			PersistenceManager pm = PMF.getNewPersistenceManager();
			Query q = pm.newQuery(MarketplaceOffer.class);
			q.setFilter("type == '" + type + "' && quality == '" + quality
					+ "' && eRepDay == " + eRepDay + " && eRepTime == "
					+ eRepTime);
			List<MarketplaceOffer> offers = (List<MarketplaceOffer>) q
					.execute();
			if (offers.size() > 0)
				offer = offers.get(0);
		}

		if (offer == null)
			throw new IllegalStateException("Could not find price for " + type
					+ " " + quality + ", day " + eRepDay + " " + eRepTime + "h");

		return offer;
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	private JobMarketOffer getSalaryPrices(String eRepDay, String eRepTime) {

		JobMarketOffer offer = (JobMarketOffer) CacheFacade.get("jobs"
				+ eRepDay + eRepTime);

		if (offer == null) {
			PersistenceManager pm = PMF.getNewPersistenceManager();
			Query q = pm.newQuery(JobMarketOffer.class);
			q.setFilter("eRepDay == " + eRepDay + " && eRepTime == " + eRepTime);
			List<JobMarketOffer> offers = (List<JobMarketOffer>) q.execute();
			if (offers.size() > 0)
				offer = offers.get(0);
		}

		if (offer == null)
			throw new IllegalStateException("Could not find salaries for day "
					+ eRepDay + " " + eRepTime + "h");

		return offer;
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	private MonetaryOffer getGoldPrices(String eRepDay, String eRepTime) {

		MonetaryOffer offer = (MonetaryOffer) CacheFacade.get("monetary"
				+ MonetaryType.GOLD.name() + eRepDay + eRepTime);

		if (offer == null) {
			PersistenceManager pm = PMF.getNewPersistenceManager();
			Query q = pm.newQuery(MonetaryOffer.class);
			q.setFilter("type == '" + MonetaryType.GOLD.name()
					+ "' && eRepDay == " + eRepDay + " && eRepTime == "
					+ eRepTime);
			List<MonetaryOffer> offers = (List<MonetaryOffer>) q.execute();
			if (offers.size() > 0)
				offer = offers.get(0);
		}

		if (offer == null)
			throw new IllegalStateException(
					"Could not find gold value for day " + eRepDay + " "
							+ eRepTime + "h");

		return offer;
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	private CountryBonuses getCountryBonuses(String eRepDay) {

		Integer day = Integer.valueOf(eRepDay);
		day--;

		CountryBonuses offer = (CountryBonuses) CacheFacade
				.get("bonuses" + day);

		if (offer == null) {
			PersistenceManager pm = PMF.getNewPersistenceManager();
			Query q = pm.newQuery(CountryBonuses.class);
			q.setFilter("eRepDay == " + day);
			List<CountryBonuses> offers = (List<CountryBonuses>) q.execute();
			if (offers.size() > 0)
				offer = offers.get(0);
		}

		if (offer == null)
			throw new IllegalStateException("Could not find bonuses for day "
					+ day);

		return offer;
	}

	@Cacheable
	@SuppressWarnings("unchecked")
	private CountryTaxes getCountryTaxes(String eRepDay) {

		Integer day = Integer.valueOf(eRepDay);
		day--;

		CountryTaxes offer = (CountryTaxes) CacheFacade.get("taxes" + day);

		if (offer == null) {
			PersistenceManager pm = PMF.getNewPersistenceManager();
			Query q = pm.newQuery(CountryTaxes.class);
			q.setFilter("eRepDay == " + day);
			List<CountryTaxes> offers = (List<CountryTaxes>) q.execute();
			if (offers.size() > 0)
				offer = offers.get(0);
		}

		if (offer == null)
			throw new IllegalStateException("Could not find taxes for day "
					+ eRepDay);

		return offer;
	}

	@SuppressWarnings("unchecked")
	private void agregate(String type, String quality, String day) {

		log.log(Level.INFO, type + " " + quality);

		PersistenceManager pm = PMF.getNewPersistenceManager();

		String filter = "type == '" + type + "' && quality == '" + quality
				+ "' && eRepDay == " + day;
		Query q = pm.newQuery(MarketplaceProfit.class, filter);

		List<MarketplaceProfit> profits = (List<MarketplaceProfit>) q.execute();
		log.log(Level.INFO, profits.size()
				+ " items in agregator list for day " + day);

		double averageProfit = 0;
		double averageProfitGold = 0;
		double averageRoi = 0;
		double averageInvestment = 0;
		for (MarketplaceProfit profit : profits) {
			averageProfit += profit.getAverageProfit();
			averageProfitGold += profit.getAverageProfitGold();
			averageRoi += profit.getAverageRoi();
			averageInvestment += profit.getInvestmentNeeded();
		}

		log.log(Level.INFO, "avgProfit = " + averageProfit
				+ ", avgProfitGold = " + averageProfitGold + ", avgRoi = "
				+ averageRoi + ", avgInvestment = " + averageInvestment);

		AgregatedMarketplaceProfit agregated = new AgregatedMarketplaceProfit();
		agregated.seteRepDay(Integer.valueOf(day));
		agregated.setQuality(ProductQuality.valueOf(quality));
		agregated.setType(ProductType.valueOf(type));
		agregated.setAverageProfit(averageProfit / profits.size());
		agregated.setAverageProfitGold(averageProfitGold / profits.size());
		agregated.setAverageRoi(averageRoi / profits.size());
		agregated
				.setAverageInvestmentNeeded(averageInvestment / profits.size());
		updater.save(agregated);

		CacheFacade
				.remove("mfs.robot.server.erepublik.data.MarketDAO.getAgregatedMarketplaceProfit(ProductType="
						+ type + ";ProductQuality=" + quality + ";)");
	}
}
