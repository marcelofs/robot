package mfs.robot.server.erepublik;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mfs.robot.server.Mailer;
import mfs.robot.server.PMF;
import mfs.robot.server.erepublik.data.CountryDAO;
import mfs.robot.server.erepublik.data.PartyEmail;
import mfs.robot.shared.erepublik.CountryAccount;
import mfs.robot.shared.erepublik.data.CitizenAccount;
import mfs.robot.shared.erepublik.data.PartyMember;
import mfs.robot.shared.erepublik.data.PoliticalParty;
import mfs.robot.shared.erepublik.data.PoliticalPartyMembersDiff;

public class EmailTask extends HttpServlet {

	public enum Operation
	{
		PARTY_EMAIL, NOTIFY_GOLD_CHANGE;
	}

	private static final long	serialVersionUID	= 1L;

	private static final Logger	logger				= Logger.getLogger(EmailTask.class
															.getName());

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Operation op = Operation.valueOf(req.getParameter("operation"));
		switch (op) {
			case PARTY_EMAIL:
				sendPartyEmail(req.getParameter("day"), req.getParameter("id"));
				break;
			case NOTIFY_GOLD_CHANGE:
				notifyGoldChange(req.getParameter("day"),
						req.getParameter("id"));
				break;
		}
	}

	private void notifyGoldChange(String day, String id) throws IOException {
		CountryDAO dao = new CountryDAO();

		Integer eRDay = Integer.valueOf(day);
		Long accId = Long.valueOf(id);

		CitizenAccount dayStats = dao.getCitizenAccount(accId, eRDay);
		CitizenAccount previousDayStats = dao.getCitizenAccount(accId,
				eRDay - 1);

		if(dayStats == null || previousDayStats == null){
			logger.log(Level.SEVERE, "Info da org é null!", new Object[]{day, id, dayStats, previousDayStats});
			return;
		}
		if (!dayStats.getGold().equals(previousDayStats.getGold())
				|| !dayStats.getCurrency().equals(
						previousDayStats.getCurrency())) {
			CountryAccount acc = CountryAccount.fromId(id);

			StringBuilder builder = new StringBuilder("<html><body>")
					.append("<center><img src=\"http://myerepublikbot.appspot.com/logo-beta.cache.png\" /></center><br/>")
					.append("<h2>").append(acc.name).append(" - Dia ")
					.append(day).append("</h2><br>");

			builder.append(
					"<a href='http://www.erepublik.com/br/economy/citizen-accounts/")
					.append(acc.id).append("'>Link da Organização</a><br><br>");

			builder.append("<b>Entrada de Ouro:</b> <br>");

			builder.append("Dia ").append(previousDayStats.geteRepDay())
					.append(": ").append(previousDayStats.getGold())
					.append("<br>");

			builder.append("Dia ").append(dayStats.geteRepDay()).append(": ")
					.append(dayStats.getGold()).append("<br>");

			builder.append("<br><b>Entrada de BRL:</b> <br>");

			builder.append("Dia ").append(previousDayStats.geteRepDay())
					.append(": ").append(previousDayStats.getCurrency())
					.append("<br>");

			builder.append("Dia ").append(dayStats.geteRepDay()).append(": ")
					.append(dayStats.getCurrency()).append("<br>");

			new Mailer().mail("RoBoT - " + acc.name + " - Alteracoes Dia "
					+ day, builder.toString(), new String[] { "admin",
					"mr.marcelofs@gmail.com" });

			logger.log(Level.INFO,
					"Notificação de alteração enviada para admin");

		}

	}

	private void sendPartyEmail(String day, String partyId) throws IOException {

		logger.log(Level.INFO, "Preparando email (dia: " + day + ", partyId: "
				+ partyId + ")");

		PoliticalParty party = PoliticalParty.fromId(partyId);

		PersistenceManager pm = PMF.getNewPersistenceManager();

		try {

			PartyEmail email = pm.getObjectById(PartyEmail.class, party.name());

			Query q = pm.newQuery(PoliticalPartyMembersDiff.class);
			q.setFilter("eRepDay == " + day + " && party == '" + party.name()
					+ "'");
			q.setUnique(true);

			PoliticalPartyMembersDiff diff = (PoliticalPartyMembersDiff) q
					.execute();

			StringBuilder htmlBuilder = new StringBuilder("<html><body>")
					.append("<center><img src=\"http://myerepublikbot.appspot.com/logo-beta.cache.png\" /></center><br/>")
					.append("<h2>").append(party.getPartyName())
					.append(" - Dia ").append(day).append("</h2><br>");

			StringBuilder bbCodeBuilder = new StringBuilder("[B][U]RoBoT - ")
					.append(party.getPartyName()).append(" - ").append(day)
					.append("[/U][/B]<br />");

			htmlBuilder.append("<b>Novos partidários (")
					.append(diff.getNewMembers().size())
					.append("):</b><br /><br />");

			bbCodeBuilder.append("[SPOILER]Novos Partidários (")
					.append(diff.getNewMembers().size()).append("): <br />");

			for (PartyMember m : diff.getNewMembers()) {
				htmlBuilder
						.append("<a href=\"http://www.erepublik.com/en/citizen/profile/")
						.append(m.geteRepId()).append("\">")
						.append(m.geteRepName()).append("</a><br />");

				bbCodeBuilder
						.append("[url=http://www.erepublik.com/en/citizen/profile/")
						.append(m.geteRepId()).append("]")
						.append(m.geteRepName()).append("[/url]<br />");
			}

			htmlBuilder.append("<br /><b>Deixaram o Partido (")
					.append(diff.getLeftMembers().size())
					.append("):</b><br /><br />");

			bbCodeBuilder.append("<br /><b>Deixaram o Partido (")
					.append(diff.getLeftMembers().size()).append("): <br />");

			for (PartyMember m : diff.getLeftMembers()) {
				htmlBuilder
						.append("<a href=\"http://www.erepublik.com/en/citizen/profile/")
						.append(m.geteRepId()).append("\">")
						.append(m.geteRepName()).append("</a><br />");

				bbCodeBuilder
						.append("[url=http://www.erepublik.com/en/citizen/profile/")
						.append(m.geteRepId()).append("]")
						.append(m.geteRepName()).append("[/url]<br />");
			}

			if (email.getSendBBCode() != null && email.getSendBBCode()) {
				bbCodeBuilder.append("[/SPOILER]");
				htmlBuilder
						.append("<br /><br />========== BBCODE abaixo ==========<br /><br />");
				htmlBuilder.append(bbCodeBuilder);
			}

			htmlBuilder.append("</body></html>");

			String[] emails = email.getEmail().split(":");
			String[][] addresses = new String[emails.length][2];
			for (int i = 0; i < emails.length; i++) {
				addresses[i][0] = email.getRecipientName();
				addresses[i][1] = emails[i];
			}
			new Mailer().mail("RoBoT - " + party.getPartyName() + " - " + day,
					htmlBuilder.toString(), addresses);
			// new String[] { email.getRecipientName(),
			// email.getEmail() }

			logger.log(Level.WARNING,
					"Email enviado para " + email.getRecipientName());

		} finally {
			pm.close();
		}

	}
}
