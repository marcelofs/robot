package mfs.robot.server.erepublik;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mfs.robot.server.PMF;
import mfs.robot.shared.erepublik.data.JobMarketOffer;
import mfs.robot.shared.erepublik.data.MarketplaceOffer;
import mfs.robot.shared.erepublik.data.MarketplaceProfit;
import mfs.robot.shared.erepublik.data.MonetaryOffer;
import mfs.robot.shared.erepublik.data.PoliticalPartyMembers;
import mfs.robot.shared.erepublik.data.RocketPrice;

public class DataCleanerServlet extends HttpServlet {

	private static final long	serialVersionUID	= 1L;

	private static final Logger	log					= Logger.getLogger(DataCleanerServlet.class
															.getName());

	@Override
	public void doPost(final HttpServletRequest req,
			final HttpServletResponse res) throws ServletException {

		Integer today = Integer.valueOf(req.getParameter("day"));

		Integer deleteEarlierThan = today - 7;

		String filter = "eRepDay < " + deleteEarlierThan;

		log.log(Level.WARNING, "Deleting data older than day "
				+ deleteEarlierThan);

		PersistenceManager pm = PMF.getNewPersistenceManager();
		try {

			Query jobMarket = pm.newQuery(JobMarketOffer.class);
			jobMarket.setFilter(filter);
			jobMarket.deletePersistentAll();

			Query marketplaceOffer = pm.newQuery(MarketplaceOffer.class);
			marketplaceOffer.setFilter(filter);
			marketplaceOffer.deletePersistentAll();

//			Query marketplaceProfit = pm.newQuery(MarketplaceProfit.class);
//			marketplaceProfit.setFilter(filter);
//			marketplaceProfit.deletePersistentAll();

			Query monetaryOffer = pm.newQuery(MonetaryOffer.class);
			monetaryOffer.setFilter(filter);
			monetaryOffer.deletePersistentAll();

//			Query rocketPrice = pm.newQuery(RocketPrice.class);
//			rocketPrice.setFilter(filter);
//			rocketPrice.deletePersistentAll();

			Query partyMembers = pm.newQuery(PoliticalPartyMembers.class);
			partyMembers.setFilter(filter);
			partyMembers.deletePersistentAll();

		} finally {
			pm.close();
		}

		log.log(Level.WARNING, "Deleting completed");

	}

}
