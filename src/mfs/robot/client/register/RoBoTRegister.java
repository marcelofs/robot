package mfs.robot.client.register;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;

public class RoBoTRegister implements EntryPoint {

	private final RoBoTRegisterRPCAsync	rpcService	= GWT.create(RoBoTRegisterRPC.class);

	private final Panel					idPanel		= new FlowPanel();
	private final LabeledTextBox		idBox		= new LabeledTextBox(
															"ID numérica");
	private final TextBox				identBox	= new TextBox();
	private final Button				verifBtn	= new Button(
															">>> Verificar perfil >>>");

	public void onModuleLoad() {
		final Panel root = RootPanel.get("content");

		final Panel loading = new FlowPanel();
		loading.addStyleName("loading");
		loading.add(new Label("Carregando..."));

		root.add(loading);

		rpcService.getIdentString(new RegisterCallback<String>()
		{
			@Override
			public void onSuccess(String result) {
				root.remove(loading);
				onLoadIdentString(root, result);
			}
		});

	}

	private void onLoadIdentString(Panel root, String identString) {
		final Label initialText = new Label(
				"Já é cadastrado no jogo? Entre com sua ID abaixo:");

		idPanel.add(initialText);

		idPanel.add(idBox);

		Anchor identLabel = new Anchor(
				"Por favor edite seu perfil no jogo e adicione o texto abaixo à descrição ('about me'):",
				"http://www.erepublik.com/br/citizen/edit/profile");
		identBox.setText(identString);

		idPanel.add(new Label());
		idPanel.add(identLabel);
		idPanel.add(new Label());
		idPanel.add(identBox);

		verifBtn.addClickHandler(getVerifClickHandler());
		Panel buttonPanel = new FlowPanel();
		buttonPanel.add(verifBtn);
		idPanel.add(buttonPanel);

		root.add(idPanel);
	}

	private ClickHandler getVerifClickHandler() {
		return new ClickHandler()
		{

			@Override
			public void onClick(ClickEvent event) {

				final Panel loadingPanel = new FlowPanel();
				final Label loadingLabel = new Label(
						"Por favor aguarde. Seu usuário está sendo verificado...");
				final Image loadingGif = new Image("../../loading.cache.gif");

				if (!idBox.validate(4)) {
					idPanel.add(new Label(
							"Por favor, insira sua ID numérica (não o link do seu perfil)."));
					return;
				}

				setEnabled(false);

				loadingPanel.add(loadingLabel);
				loadingPanel.add(loadingGif);

				idPanel.add(loadingPanel);

				rpcService.verifUser(idBox.getText(),
						new AsyncCallback<String>()
						{

							@Override
							public void onSuccess(String result) {
								idPanel.clear();

								idPanel.add(new Label("Seja bem vindo, "
										+ result + "."));
								idPanel.add(new Label(
										"Usuário foi cadastrado com sucesso, você já pode remover o texto da sua descrição. "
												+ "Qualquer dúvida, entre em contato no canal #RoBoT."));

								Button goHome = new Button(
										">>> Começar a usar o RoBoT >>>");
								goHome.addClickHandler(new ClickHandler()
								{

									@Override
									public void onClick(ClickEvent event) {
										Window.Location.replace("http://"
												+ Window.Location.getHostName());

									}
								});
								idPanel.add(goHome);

							}

							@Override
							public void onFailure(Throwable caught) {
								setEnabled(true);
								loadingPanel.remove(loadingGif);
								loadingLabel
										.setText("Não foi possível verificar seu usuário. Por favor tente novamente em alguns instantes."
												+ "\n\n" + caught.getMessage());
							}
						});
			}
		};
	}

	private void setEnabled(boolean enabled) {
		idBox.setEnabled(enabled);
		identBox.setEnabled(enabled);
		verifBtn.setEnabled(enabled);
	}

}
