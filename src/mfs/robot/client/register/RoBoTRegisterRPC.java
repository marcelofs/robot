package mfs.robot.client.register;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("../login/register/rpc")
public interface RoBoTRegisterRPC extends RemoteService {

	String getIdentString() throws Exception;
	
	String verifUser(String eId) throws Exception;

}
