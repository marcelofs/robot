package mfs.robot.client.register;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface RoBoTRegisterRPCAsync {

	void getIdentString(AsyncCallback<String> callback);

	void verifUser(String eId, AsyncCallback<String> callback);

}
