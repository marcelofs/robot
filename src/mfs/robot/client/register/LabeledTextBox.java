package mfs.robot.client.register;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.client.ui.TextBox;

/**
 * Simple TextBox with Label Inside - onFocus removes the Label and replaces it
 * onBlur
 **/
public class LabeledTextBox extends TextBox {

	String	lb;
	boolean	isClicked	= false;

	@UiConstructor
	public LabeledTextBox(String label) {
		lb = label;

		superSetText(label);

		addFocusHandler(new FocusHandler()
		{
			@Override
			public void onFocus(FocusEvent event) {
				if (!isClicked) {
					isClicked = true;
					superSetText("");
					setStyleFilled();
				}
			}
		});

		addBlurHandler(new BlurHandler()
		{

			@Override
			public void onBlur(BlurEvent event) {
				if (isClicked)
					if (((TextBox) event.getSource()).getText().trim().length() < 1) {
						isClicked = false;
						superSetText(lb);
						setStyleClear();
					}
			}
		});
	}

	protected void setStyleClear() {
		addStyleName("light-text");
	}

	protected void setStyleFilled() {
		removeStyleName("light-text");
	}

	protected void superSetText(String text) {
		super.setText(text);
	}

	@Override
	public String getText() {
		return super.getText().equals(lb) ? "" : super.getText();
	}

	@Override
	public void setText(String text) {
		if ("".equals(text)) {
			isClicked = false;
			setStyleClear();
		} else {
			isClicked = true;
			setStyleFilled();
		}
		super.setText(text);
	}

	public boolean validate(int minSize) {
		boolean ok = true;
		if (getText().trim().length() < minSize || !getText().trim().matches("[0123456789]*")) {
			addStyleName("highlight");
			setFocus(true);
			ok = false;
		} else
			removeStyleName("highlight");

		return ok;
	}
}
