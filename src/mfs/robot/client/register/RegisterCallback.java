package mfs.robot.client.register;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.StatusCodeException;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;

public abstract class RegisterCallback<E> implements AsyncCallback<E> {

	@Override
	public void onFailure(Throwable caught) {
		try {
			throw caught;
		} catch (StatusCodeException e) {

			int code = e.getStatusCode();
			switch (code) {
				case 403:
					addError("Usuário não autorizado");
					break;
				default:
					addError("Erro desconhecido, status " + code);
			}

		} catch (Throwable e) {
			addError(e);
		}
	}

	private void addError(Throwable caught) {
		addError("ERROR " + caught.getClass() + ":  " + caught.getMessage());
	}

	private void addError(String message) {
		Label error = new Label(message);
		RootPanel.get("error").add(error);
	}
}
