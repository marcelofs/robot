package mfs.robot.client.erepublik.calculator;

import java.util.Collection;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import com.google.common.collect.Multimap;
import com.google.common.collect.Table;

import mfs.robot.client.erepublik.GraphDataOrganizer;
import mfs.robot.shared.erepublik.data.AgregatedMarketplaceOffer;
import mfs.robot.shared.erepublik.data.AgregatedMonetaryOffer;
import mfs.robot.shared.erepublik.data.MonetaryType;
import mfs.robot.shared.erepublik.data.ProductQuality;
import mfs.robot.shared.erepublik.data.RocketQuality;

public class RocketHistoryCalculator extends RocketCalculator {

	private Table<Integer, ProductQuality, AgregatedMarketplaceOffer>	prices;

	private Map<Integer, AgregatedMonetaryOffer>						goldValue;

	private SortedSet<Integer>											organizedKeys;

	public RocketHistoryCalculator(
			Multimap<ProductQuality, AgregatedMarketplaceOffer> prices,
			Collection<AgregatedMonetaryOffer> goldValue) {

		GraphDataOrganizer org = new GraphDataOrganizer();

		this.prices = org.tableOfPrices(prices);
		this.goldValue = org.mapOfMonetaryOffers(goldValue);
		this.organizedKeys = new TreeSet<Integer>(this.goldValue.keySet());
	}

	public Integer getFirstDay() {
		return organizedKeys.first();
	}

	public Integer getLastDay() {
		return organizedKeys.last();
	}

	public Integer getNbrOfDays() {
		return getLastDay() - getFirstDay() + 1;
	}

	public Double getValue(RocketQuality quality, Integer day,
			MonetaryType currency) {

		if (MonetaryType.GOLD.equals(currency)) {
			Double ccValue = getValue(quality, day, MonetaryType.CURRENCY);
			if (goldValue.get(day) != null)
				return ccValue / goldValue.get(day).getAverage();
			else
				return 0d;
		}

		Double price = 0d;

		price += getBasePrice(quality, day);

		int weaponsNeeded = getWeaponsNeeded(quality, day);
		for (ProductQuality q : ProductQuality.values())
			if (!ProductQuality.Q7.equals(q))
				if (prices.get(day, q) != null)
					price += prices.get(day, q).getAverage() * weaponsNeeded;

		return price;
	}

}
