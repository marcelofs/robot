package mfs.robot.client.erepublik.calculator;

import mfs.robot.shared.erepublik.data.RocketQuality;

/**
 * http://i.imm.io/1acoS.png
 * http://www.erepublik.com/en/main/latest-updates/0/34
 * http://www.erepublik.com/br/main/latest-updates/0/122
 */
public class RocketCalculator {
	protected Integer getBasePrice(RocketQuality quality, Integer day) {
		if (day >= 1926)
			return 0;
		else
			switch (quality) {
				case Q1:
					return 100;
				case Q2:
					return 250;
				case Q3:
					return 500;
				case Q4:
					return 1000;
				case Q5:
					return 1500;
			}
		return null;
	}

	protected Integer getWeaponsNeeded(RocketQuality quality, Integer day) {

		// http://www.erepublik.com/br/main/latest-updates/0/122
		if (day >= 2330 && day <= 2336) {
			switch (quality) {
				case Q1:
					return 3;
				case Q2:
					return 10;
				case Q3:
					return 15;
				case Q4:
					return 20;
				case Q5:
					return 20;
			}
		}

		// http://www.erepublik.com/en/main/latest-updates/0/34
		if (day >= 1926) {
			switch (quality) {
				case Q1:
					return 10;
				case Q2:
					return 20;
				case Q3:
					return 60;
				case Q4:
					return 120;
				case Q5:
					return 90;
			}
		}

		// http://i.imm.io/1acoS.png
		switch (quality) {
			case Q1:
				return 10;
			case Q2:
				return 25;
			case Q3:
				return 50;
			case Q4:
				return 100;
			case Q5:
				return 150;
		}

		return null;
	}
}
