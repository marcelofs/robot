package mfs.robot.client.erepublik.calculator;

import java.util.Collection;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import mfs.robot.client.erepublik.GraphDataOrganizer;
import mfs.robot.shared.erepublik.data.MarketplaceOffer;
import mfs.robot.shared.erepublik.data.MonetaryOffer;
import mfs.robot.shared.erepublik.data.MonetaryType;
import mfs.robot.shared.erepublik.data.ProductQuality;
import mfs.robot.shared.erepublik.data.RocketQuality;

import com.google.common.collect.Multimap;
import com.google.common.collect.Table;

public class RocketDailyCalculator extends RocketCalculator {

	private Table<Integer, ProductQuality, MarketplaceOffer>	prices;

	private Map<Integer, MonetaryOffer>							goldValue;

	private SortedSet<Integer>									organizedKeys;

	private Integer												day;

	public RocketDailyCalculator(
			Multimap<ProductQuality, MarketplaceOffer> prices,
			Collection<MonetaryOffer> goldValue, Integer day) {

		GraphDataOrganizer org = new GraphDataOrganizer();

		this.prices = org.tableOfDailyPrices(prices);
		this.goldValue = org.mapOfMonetaryDailyOffers(goldValue);
		this.organizedKeys = new TreeSet<Integer>(this.goldValue.keySet());
		this.day = day;
	}

	public Integer getLastHour() {
		return organizedKeys.last();
	}

	public Integer getDay() {
		return day;
	}

	public Double getValue(RocketQuality quality, Integer hour,
			MonetaryType currency) {

		if (MonetaryType.GOLD.equals(currency)) {
			Double ccValue = getValue(quality, hour, MonetaryType.CURRENCY);
			if (goldValue.get(hour) != null)
				return ccValue / goldValue.get(hour).getAverage();
			else
				return 0d;
		}

		Double price = 0d;

		price += getBasePrice(quality, day);

		int weaponsNeeded = getWeaponsNeeded(quality, day);
		for (ProductQuality q : ProductQuality.values())
			if (!ProductQuality.Q7.equals(q))
				if (prices.get(hour, q) != null)
					price += prices.get(hour, q).getAverage() * weaponsNeeded;

		return price;
	}
}
