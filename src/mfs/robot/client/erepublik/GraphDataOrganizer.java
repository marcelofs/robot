package mfs.robot.client.erepublik;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.Nullable;

import mfs.robot.shared.erepublik.IFinancialIndex;
import mfs.robot.shared.erepublik.data.AgregatedJobMarketOffer;
import mfs.robot.shared.erepublik.data.AgregatedMarketplaceOffer;
import mfs.robot.shared.erepublik.data.AgregatedMonetaryOffer;
import mfs.robot.shared.erepublik.data.CitizenAccount;
import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.CountryIncome;
import mfs.robot.shared.erepublik.data.CountryMoneyTransfer;
import mfs.robot.shared.erepublik.data.CountryPresident;
import mfs.robot.shared.erepublik.data.CountryStats;
import mfs.robot.shared.erepublik.data.CountryTaxes;
import mfs.robot.shared.erepublik.data.CountryTreasury;
import mfs.robot.shared.erepublik.data.ExpenseType;
import mfs.robot.shared.erepublik.data.MarketplaceOffer;
import mfs.robot.shared.erepublik.data.MonetaryOffer;
import mfs.robot.shared.erepublik.data.ProductQuality;
import mfs.robot.shared.erepublik.dto.CountryExpenseSum;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Table;

public class GraphDataOrganizer {

	public Set<CountryExpenseSum> agregateByType(CountryPresident cp,
			Multimap<Integer, CountryExpenseSum> allExpenses) {

		Map<ExpenseType, CountryExpenseSum> totalSums = mapOfExpenseTypes();

		if (cp != null)
			for (int i = cp.getFirstErepDay() + 1; i <= cp.getLastErepDay(); i++) {
				Collection<CountryExpenseSum> dailySums = allExpenses.get(i);
				for (CountryExpenseSum dailySum : dailySums)
					totalSums.get(dailySum.category).value += dailySum.value;

			}
		else
			for (CountryExpenseSum dailySum : allExpenses.values())
				totalSums.get(dailySum.category).value += dailySum.value;

		Set<CountryExpenseSum> orderedSums = new TreeSet<CountryExpenseSum>(
				expenseValueComparator());

		orderedSums.addAll(totalSums.values());

		return orderedSums;
	}

	public Double sumExpenses(CountryPresident cp,
			Multimap<Integer, CountryExpenseSum> allExpenses) {
		Double total = 0d;

		for (int i = cp.getFirstErepDay() + 1; i <= cp.getLastErepDay(); i++) {
			Collection<CountryExpenseSum> dailySums = allExpenses.get(i);
			for (CountryExpenseSum dailySum : dailySums)
				total += dailySum.value;
		}

		return total;
	}

	public Double sumTaxRevenue(CountryPresident cp,
			Collection<CountryTreasury> taxes) {

		Map<Integer, CountryTreasury> taxRevenues = mapOfTreasuries(taxes);
		Double total = 0d;

		for (int i = cp.getFirstErepDay(); i <= cp.getLastErepDay() - 1; i++) {
			CountryTreasury t = taxRevenues.get(i);
			if (t != null && t.getTaxRevenue() != null)
				total += t.getTaxRevenue();
		}

		return total;
	}

	public Double sumIncomes(CountryPresident cp,
			Collection<CountryIncome> incomes) {

		Multimap<Integer, CountryIncome> mapOfIncomes = mapOfIncomes(incomes);
		Double total = 0d;

		for (int i = cp.getFirstErepDay() + 1; i <= cp.getLastErepDay(); i++) {
			Collection<CountryIncome> dailySums = mapOfIncomes.get(i);
			for (CountryIncome dailySum : dailySums)
				total += dailySum.getValue();
		}

		return total;

	}

	private Map<ExpenseType, CountryExpenseSum> mapOfExpenseTypes() {
		Map<ExpenseType, CountryExpenseSum> totalSums = new HashMap<ExpenseType, CountryExpenseSum>();

		for (ExpenseType type : ExpenseType.values()) {
			CountryExpenseSum sum = new CountryExpenseSum();
			sum.category = type;
			sum.value = 0d;
			totalSums.put(type, sum);
		}
		return totalSums;
	}

	private Comparator<CountryExpenseSum> expenseValueComparator() {
		return new Comparator<CountryExpenseSum>()
		{

			@Override
			public int compare(CountryExpenseSum o1, CountryExpenseSum o2) {
				return (int) (o2.value - o1.value);
			}
		};
	}

	public Map<Integer, CountryStats> mapOfStats(Iterable<CountryStats> stats) {
		return Maps.uniqueIndex(stats, new Function<CountryStats, Integer>()
		{

			@Override
			@Nullable
			public Integer apply(@Nullable
			CountryStats stat) {
				return stat.geteRepDay();
			}
		});
	}

	public Map<Integer, CitizenAccount> mapOfCitizenAccount(
			Collection<CitizenAccount> citizenAcc) {

		if (citizenAcc == null)
			return new HashMap<Integer, CitizenAccount>();

		Set<CitizenAccount> unique = new HashSet<CitizenAccount>(citizenAcc);

		return Maps.uniqueIndex(unique, new Function<CitizenAccount, Integer>()
		{

			@Override
			@Nullable
			public Integer apply(@Nullable
			CitizenAccount account) {
				return account.geteRepDay();
			}
		});
	}

	public Map<Integer, CountryTaxes> mapOfTaxes(Iterable<CountryTaxes> taxes) {
		return Maps.uniqueIndex(taxes, new Function<CountryTaxes, Integer>()
		{

			@Override
			@Nullable
			public Integer apply(@Nullable
			CountryTaxes taxes) {
				return taxes.geteRepDay();
			}
		});
	}

	public Map<Integer, CountryTreasury> mapOfTreasuries(
			Iterable<CountryTreasury> treasury) {
		return Maps.uniqueIndex(treasury,
				new Function<CountryTreasury, Integer>()
				{

					@Override
					@Nullable
					public Integer apply(@Nullable
					CountryTreasury treasury) {
						if (treasury != null && treasury.geteRepDay() != null)
							return treasury.geteRepDay();
						return null;
					}
				});
	}

	public Multimap<Integer, CountryIncome> mapOfIncomes(
			Iterable<CountryIncome> incomes) {
		return Multimaps.index(incomes, new Function<CountryIncome, Integer>()
		{

			@Override
			@Nullable
			public Integer apply(@Nullable
			CountryIncome income) {
				return income.geteRepDay();
			}
		});
	}

	public Multimap<String, CountryIncome> mapOfIncomesByOrigin(
			Iterable<CountryIncome> incomes) {
		return Multimaps.index(incomes, new Function<CountryIncome, String>()
		{

			@Override
			@Nullable
			public String apply(@Nullable
			CountryIncome income) {
				return income.getOrigin();
			}
		});
	}

	public Iterable<CountryIncome> filterIncomesByCP(
			Iterable<CountryIncome> incomes, final CountryPresident cp) {

		return Iterables.filter(incomes, new Predicate<CountryIncome>()
		{

			@Override
			public boolean apply(@Nullable
			CountryIncome income) {
				if (cp == null)
					return true;
				return income.geteRepDay() > cp.getFirstErepDay()
						&& income.geteRepDay() <= cp.getLastErepDay();
			}
		});

	}

	public Iterable<CountryIncome> filterIncomesByOrigin(
			Iterable<CountryIncome> incomes, final String... origins) {

		return Iterables.filter(incomes, new Predicate<CountryIncome>()
		{

			@Override
			public boolean apply(@Nullable
			CountryIncome income) {
				for (String origin : origins)
					if (origin.equalsIgnoreCase(income.getOrigin()))
						return true;
				return false;
			}
		});

	}

	public Iterable<CountryIncome> filterIncomesByDestination(
			Iterable<CountryIncome> incomes, final String... orgs) {

		return Iterables.filter(incomes, new Predicate<CountryIncome>()
		{

			@Override
			public boolean apply(@Nullable
			CountryIncome income) {
				for (String org : orgs)
					if (org.equalsIgnoreCase(income.getDestination()))
						return true;
				return false;
			}
		});

	}

	public Multimap<Integer, CountryMoneyTransfer> mapOfTransfers(
			Iterable<CountryMoneyTransfer> transfers) {
		return Multimaps.index(transfers,
				new Function<CountryMoneyTransfer, Integer>()
				{

					@Override
					@Nullable
					public Integer apply(@Nullable
					CountryMoneyTransfer transfer) {
						return transfer.geteRepDay();
					}
				});
	}

	public Iterable<CountryMoneyTransfer> filterTransfers(
			Iterable<CountryMoneyTransfer> transfers, final String... orgs) {
		return Iterables.filter(transfers,
				new Predicate<CountryMoneyTransfer>()
				{

					@Override
					public boolean apply(@Nullable
					CountryMoneyTransfer transfer) {
						for (String org : orgs)
							if (org.equalsIgnoreCase(transfer.getOrigin())
									|| org.equalsIgnoreCase(transfer
											.getDestination()))
								return true;
						return false;
					}
				});
	}

	public Multimap<String, CountryExpense> mapOfExpensesByDestination(
			Iterable<CountryExpense> expenses) {

		return Multimaps.index(expenses, new Function<CountryExpense, String>()
		{

			@Override
			@Nullable
			public String apply(@Nullable
			CountryExpense expense) {
				return expense.getDestination();
			}
		});
	}

	public Multimap<Integer, CountryExpense> mapOfExpenses(
			Iterable<CountryExpense> expenses) {

		return Multimaps.index(expenses,
				new Function<CountryExpense, Integer>()
				{

					@Override
					@Nullable
					public Integer apply(@Nullable
					CountryExpense expense) {
						return expense.geteRepDay();
					}
				});
	}

	public Iterable<CountryExpense> filterExpensesByOrigin(
			Iterable<CountryExpense> expenses, final String... origins) {

		return Iterables.filter(expenses, new Predicate<CountryExpense>()
		{

			@Override
			public boolean apply(@Nullable
			CountryExpense expense) {
				for (String org : origins)
					if (org.equalsIgnoreCase(expense.getOrigin()))
						return true;
				return false;
			}
		});

	}

	public Iterable<CountryExpense> filterExpensesByDestination(
			Iterable<CountryExpense> expenses, final String... destinations) {

		return Iterables.filter(expenses, new Predicate<CountryExpense>()
		{

			@Override
			public boolean apply(@Nullable
			CountryExpense expense) {
				for (String destination : destinations)
					if (destination.equalsIgnoreCase(expense.getDestination()))
						return true;
				return false;
			}
		});

	}

	public Iterable<CountryExpense> filterExpensesByCP(
			Iterable<CountryExpense> expenses, final CountryPresident cp) {

		return Iterables.filter(expenses, new Predicate<CountryExpense>()
		{

			@Override
			public boolean apply(@Nullable
			CountryExpense expense) {
				if (cp == null)
					return true;
				return expense.geteRepDay() > cp.getFirstErepDay()
						&& expense.geteRepDay() <= cp.getLastErepDay();
			}
		});

	}

	public Map<Integer, AgregatedJobMarketOffer> mapOfJobMarket(
			Iterable<AgregatedJobMarketOffer> jobs) {
		return Maps.uniqueIndex(jobs,
				new Function<AgregatedJobMarketOffer, Integer>()
				{

					@Override
					@Nullable
					public Integer apply(@Nullable
					AgregatedJobMarketOffer stat) {
						return stat.geteRepDay();
					}
				});
	}

	public Map<Integer, AgregatedMonetaryOffer> mapOfMonetaryOffers(
			Collection<AgregatedMonetaryOffer> offers) {
		return Maps.uniqueIndex(offers,
				new Function<AgregatedMonetaryOffer, Integer>()
				{

					@Override
					@Nullable
					public Integer apply(@Nullable
					AgregatedMonetaryOffer stat) {
						return stat.geteRepDay();
					}
				});
	}

	public Map<Integer, MonetaryOffer> mapOfMonetaryDailyOffers(
			Collection<MonetaryOffer> offers) {
		return Maps.uniqueIndex(offers, new Function<MonetaryOffer, Integer>()
		{

			@Override
			@Nullable
			public Integer apply(@Nullable
			MonetaryOffer stat) {
				return stat.geteRepTime();
			}
		});
	}

	public Map<Integer, IFinancialIndex> mapOfIndexes(
			Collection<IFinancialIndex> indexes) {

		Set<IFinancialIndex> unique = new TreeSet<IFinancialIndex>(
				new Comparator<IFinancialIndex>()
				{

					@Override
					public int compare(IFinancialIndex o1, IFinancialIndex o2) {
						if (o1.geteRepDay().equals(o2.geteRepDay()))
							return 0;
						return o1.geteRepDay() > o2.geteRepDay() ? 1 : -1;
					}
				});

		unique.addAll(indexes);

		return Maps.uniqueIndex(unique,
				new Function<IFinancialIndex, Integer>()
				{

					@Override
					@Nullable
					public Integer apply(@Nullable
					IFinancialIndex index) {
						return index.geteRepDay();
					}
				});

	}

	public Table<Integer, ProductQuality, AgregatedMarketplaceOffer> tableOfPrices(
			Multimap<ProductQuality, AgregatedMarketplaceOffer> prices) {

		Table<Integer, ProductQuality, AgregatedMarketplaceOffer> offers = HashBasedTable
				.create();

		for (ProductQuality q : prices.keySet())
			for (AgregatedMarketplaceOffer o : prices.get(q))
				offers.put(o.geteRepDay(), q, o);

		return offers;
	}

	public Table<Integer, ProductQuality, MarketplaceOffer> tableOfDailyPrices(
			Multimap<ProductQuality, MarketplaceOffer> prices) {

		Table<Integer, ProductQuality, MarketplaceOffer> offers = HashBasedTable
				.create();

		for (ProductQuality q : prices.keySet())
			for (MarketplaceOffer o : prices.get(q))
				offers.put(o.geteRepTime(), q, o);

		return offers;
	}

}
