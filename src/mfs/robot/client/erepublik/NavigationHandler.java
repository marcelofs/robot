package mfs.robot.client.erepublik;

import java.util.HashMap;
import java.util.Map;

import mfs.robot.client.erepublik.components.ContentsPanel;
import mfs.robot.client.erepublik.components.MenuPanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;

public class NavigationHandler<T> implements ValueChangeHandler<String> {

	private final ContentsPanel	contents;
	private MenuPanel			menu;
	
	private Pages currentPage;

	public NavigationHandler(ContentsPanel contents, MenuPanel menu) {
		this.contents = contents;
		this.menu = menu;
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		changePage(History.getToken());
	}

	public void changePage(String token) {
		try {

			if (!token.contains("?"))
				changePage(Pages.valueOf(token));
			else
				// with parameters in url
				changePage(
						Pages.valueOf(token.substring(0, token.indexOf("?"))),
						parseParams(token.substring(token.indexOf("?") + 1)));

		} catch (IllegalArgumentException error) {
			GWT.log("page not found: token=" + token, error);
			History.newItem(Pages.notFound.name() + "?token=" + token
					+ "&error=" + error.getMessage());
		}
	}

	private Map<String, String> parseParams(String parameters) {

		try {

			Map<String, String> params = new HashMap<String, String>();

			String[] eachParam = parameters.split("&");
			for (String s : eachParam) {
				String[] p = s.split("=");
				params.put(p[0], p.length == 2 ? p[1] : "null");
			}

			return params;

		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}

	private void changePage(Pages page) {
		changePage(page, new HashMap<String, String>());
	}

	private void changePage(Pages page, Map<String, String> params) {
		contents.goToPage(page, params);
		menu.goToPage(currentPage, page);
		
		currentPage = page;
	}

}
