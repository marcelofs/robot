package mfs.robot.client.erepublik;

import mfs.robot.client.erepublik.pages.AContentsPage;
import mfs.robot.client.erepublik.pages.NotFoundPage;
import mfs.robot.client.erepublik.pages.accounts.AccountsPage;
import mfs.robot.client.erepublik.pages.admin.AdminPage;
import mfs.robot.client.erepublik.pages.bip.LibertyPage;
import mfs.robot.client.erepublik.pages.bonus.BonusPage;
import mfs.robot.client.erepublik.pages.citizens.CitizensPage;
import mfs.robot.client.erepublik.pages.expenses.ExpensesPage;
import mfs.robot.client.erepublik.pages.gold.GoldPage;
import mfs.robot.client.erepublik.pages.home.HomePage;
import mfs.robot.client.erepublik.pages.index.IndexesPage;
import mfs.robot.client.erepublik.pages.jobs.JobsPage;
import mfs.robot.client.erepublik.pages.market.MarketPage;
import mfs.robot.client.erepublik.pages.mdf.MdFPage;
import mfs.robot.client.erepublik.pages.politics.PoliticsPage;
import mfs.robot.client.erepublik.pages.profit.ProfitPage;
import mfs.robot.client.erepublik.pages.rockets.RocketPage;
import mfs.robot.client.erepublik.pages.taxes.TaxesPage;
import mfs.robot.client.erepublik.pages.treasury.TreasuryPage;

public enum Pages
{

	// lowercase because of page tokens on URL (looks better :)
	home(), liberty(), marketplace(), jobs(), gold(), profit(), rockets(), indexes(), orgs(), 
	citizens(), taxes(), bonus(), politics(), treasury(), mdf(), admin(), expenses(), notFound();

	public AContentsPage getPage() {
		return Pages.getPage(this);
	}

	@Override
	public String toString() {
		return name();
	}

	public static AContentsPage getPage(Pages page) {
		// switch since reflection is not possible
		switch (page) {
			case home:
				return new HomePage();
			case liberty:
				return new LibertyPage();
			case marketplace:
				return new MarketPage();
			case jobs:
				return new JobsPage();
			case gold:
				return new GoldPage();
			case profit:
				return new ProfitPage();
			case rockets:
				return new RocketPage();
			case indexes:
				return new IndexesPage();
			case orgs:
				return new AccountsPage();
			case citizens:
				return new CitizensPage();
			case taxes:
				return new TaxesPage();
			case bonus:
				return new BonusPage();
			case politics:
				return new PoliticsPage();
			case treasury:
				return new TreasuryPage();
			case expenses:
				return new ExpensesPage();
			case mdf:
				return new MdFPage();
			case admin:
				return new AdminPage();

			case notFound:
			default:
				return new NotFoundPage();

		}
	}
}
