package mfs.robot.client.erepublik.rpc;

import java.util.Collection;
import java.util.Date;

import mfs.robot.shared.admin.eRepUser;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("../admin/rpc")
public interface RoBoTAdminRPC extends RemoteService {

	Collection<eRepUser> getUsers();

	void saveUser(eRepUser user);

	boolean createPresident(String name, Date firstDay,
			Boolean showFinanceTables, Boolean showFinanceGraphs);

}
