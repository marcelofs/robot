package mfs.robot.client.erepublik.rpc;

import java.util.Collection;

import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.CountryIncome;
import mfs.robot.shared.erepublik.data.CountryMoneyTransfer;
import mfs.robot.shared.mdf.MdFInitialInfo;
import mfs.robot.shared.mdf.MdFLog;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("../mdf/rpc")
public interface RoBoTMdFRPC extends RemoteService {

	void saveManualRevenue(String day, String revenue, String comments)
			throws Exception;

	MdFInitialInfo getRevenueInitialInfo();

	MdFInitialInfo getRevenueInfo(Integer day);

	Collection<MdFLog> getTopLogs();

	Collection<CountryExpense> getExpenses(Integer day);

	boolean addExpense(CountryExpense e);

	boolean deleteExpense(CountryExpense e);

	boolean addIncome(CountryIncome income);

	boolean deleteIncome(CountryIncome e);

	Collection<CountryIncome> getIncomes(Integer day);

	Collection<CountryMoneyTransfer> getTransfers(Integer day);

	boolean addTransfer(CountryMoneyTransfer transfer);

	boolean deleteTransfer(CountryMoneyTransfer transfer);

}
