package mfs.robot.client.erepublik.rpc;

import java.util.Collection;
import java.util.Map;

import mfs.robot.shared.erepublik.FinancialIndexType;
import mfs.robot.shared.erepublik.IFinancialIndex;
import mfs.robot.shared.erepublik.InitialInfo;
import mfs.robot.shared.erepublik.data.AgregatedJobMarketOffer;
import mfs.robot.shared.erepublik.data.AgregatedMarketplaceOffer;
import mfs.robot.shared.erepublik.data.AgregatedMarketplaceProfit;
import mfs.robot.shared.erepublik.data.AgregatedMonetaryOffer;
import mfs.robot.shared.erepublik.data.AgregatedRocketPrice;
import mfs.robot.shared.erepublik.data.CountryBonuses;
import mfs.robot.shared.erepublik.data.CountryStats;
import mfs.robot.shared.erepublik.data.CountryTaxes;
import mfs.robot.shared.erepublik.data.JobMarketOffer;
import mfs.robot.shared.erepublik.data.MarketplaceOffer;
import mfs.robot.shared.erepublik.data.MarketplaceProfit;
import mfs.robot.shared.erepublik.data.MonetaryOffer;
import mfs.robot.shared.erepublik.data.MonetaryType;
import mfs.robot.shared.erepublik.data.PoliticalParty;
import mfs.robot.shared.erepublik.data.PoliticalPartyMembersDiff;
import mfs.robot.shared.erepublik.data.PoliticalPartyStats;
import mfs.robot.shared.erepublik.data.ProductQuality;
import mfs.robot.shared.erepublik.data.ProductType;
import mfs.robot.shared.erepublik.data.RocketPrice;
import mfs.robot.shared.erepublik.data.RocketQuality;
import mfs.robot.shared.erepublik.dto.JobHistoryDTO;
import mfs.robot.shared.erepublik.dto.RocketDailyPriceDTO;
import mfs.robot.shared.erepublik.dto.RocketHistoryPriceDTO;

import com.google.common.collect.Multimap;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface RoBoTRPCAsync {

	void geteRepDay(AsyncCallback<Integer> callback);

	void getCountryStatsHistory(AsyncCallback<Collection<CountryStats>> callback);

	void getAgregatedJobMarketOffers(
			AsyncCallback<Collection<AgregatedJobMarketOffer>> callback);

	void getAgregatedMonetaryOffers(MonetaryType type,
			AsyncCallback<Collection<AgregatedMonetaryOffer>> callback);

	void getAgregatedMarketplaceOffer(ProductType type, ProductQuality quality,
			AsyncCallback<Collection<AgregatedMarketplaceOffer>> callback);

	void getDailyJobMarketOffers(
			AsyncCallback<Collection<JobMarketOffer>> callback);

	void getDailyMonetaryOffers(MonetaryType type,
			AsyncCallback<Collection<MonetaryOffer>> callback);

	void getDailyMarketplaceOffer(ProductType type, ProductQuality quality,
			AsyncCallback<Collection<MarketplaceOffer>> callback);

	void getCountryBonusesHistory(
			AsyncCallback<Collection<CountryBonuses>> callback);

	void getCountryTaxesHistory(AsyncCallback<Collection<CountryTaxes>> callback);

	void getDailyMarketplaceProfit(ProductType type, ProductQuality quality,
			AsyncCallback<Collection<MarketplaceProfit>> callback);

	void getAgregatedMarketplaceProfit(ProductType type,
			ProductQuality quality,
			AsyncCallback<Collection<AgregatedMarketplaceProfit>> callback);

	void getDailyRocketPrice(RocketQuality quality,
			AsyncCallback<Collection<RocketPrice>> callback);

	void getAgregatedRocketPrice(RocketQuality quality,
			AsyncCallback<Collection<AgregatedRocketPrice>> callback);

	void getDailyJobMarketOffers(Integer day,
			AsyncCallback<Collection<JobMarketOffer>> callback);

	void getDailyMonetaryOffers(MonetaryType type, Integer day,
			AsyncCallback<Collection<MonetaryOffer>> callback);

	void getDailyMarketplaceOffer(ProductType type, ProductQuality quality,
			Integer day, AsyncCallback<Collection<MarketplaceOffer>> callback);

	void getDailyMarketplaceProfit(ProductType type, ProductQuality quality,
			Integer day, AsyncCallback<Collection<MarketplaceProfit>> callback);

	void getDailyRocketPrice(RocketQuality quality, Integer day,
			AsyncCallback<Collection<RocketPrice>> callback);

	void sendFeedback(String message, String location,
			AsyncCallback<Void> callback);

	void getPoliticalHistory(
			AsyncCallback<Map<PoliticalParty, Collection<PoliticalPartyStats>>> callback);

	void getInitialInfo(AsyncCallback<InitialInfo> callback);

	void getMembersDiff(PoliticalParty party, Integer day,
			AsyncCallback<PoliticalPartyMembersDiff> callback);

	void getMembersDiffHistory(PoliticalParty party,
			AsyncCallback<Collection<PoliticalPartyMembersDiff>> callback);

	void getIndex(
			FinancialIndexType[] types,
			AsyncCallback<Multimap<FinancialIndexType, IFinancialIndex>> callback);

	void getJobMarketHistory(AsyncCallback<JobHistoryDTO> callback);

	void getRocketPricesHistory(AsyncCallback<RocketHistoryPriceDTO> callback);

	void getRocketPricesDaily(Integer day,
			AsyncCallback<RocketDailyPriceDTO> callback);

}
