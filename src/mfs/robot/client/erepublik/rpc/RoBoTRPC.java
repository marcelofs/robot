package mfs.robot.client.erepublik.rpc;

import java.util.Collection;
import java.util.Map;

import mfs.robot.shared.erepublik.FinancialIndexType;
import mfs.robot.shared.erepublik.IFinancialIndex;
import mfs.robot.shared.erepublik.InitialInfo;
import mfs.robot.shared.erepublik.data.AgregatedJobMarketOffer;
import mfs.robot.shared.erepublik.data.AgregatedMarketplaceOffer;
import mfs.robot.shared.erepublik.data.AgregatedMarketplaceProfit;
import mfs.robot.shared.erepublik.data.AgregatedMonetaryOffer;
import mfs.robot.shared.erepublik.data.AgregatedRocketPrice;
import mfs.robot.shared.erepublik.data.CountryBonuses;
import mfs.robot.shared.erepublik.data.CountryStats;
import mfs.robot.shared.erepublik.data.CountryTaxes;
import mfs.robot.shared.erepublik.data.JobMarketOffer;
import mfs.robot.shared.erepublik.data.MarketplaceOffer;
import mfs.robot.shared.erepublik.data.MarketplaceProfit;
import mfs.robot.shared.erepublik.data.MonetaryOffer;
import mfs.robot.shared.erepublik.data.MonetaryType;
import mfs.robot.shared.erepublik.data.PoliticalParty;
import mfs.robot.shared.erepublik.data.PoliticalPartyMembersDiff;
import mfs.robot.shared.erepublik.data.PoliticalPartyStats;
import mfs.robot.shared.erepublik.data.ProductQuality;
import mfs.robot.shared.erepublik.data.ProductType;
import mfs.robot.shared.erepublik.data.RocketPrice;
import mfs.robot.shared.erepublik.data.RocketQuality;
import mfs.robot.shared.erepublik.dto.JobHistoryDTO;
import mfs.robot.shared.erepublik.dto.RocketDailyPriceDTO;
import mfs.robot.shared.erepublik.dto.RocketHistoryPriceDTO;

import com.google.common.collect.Multimap;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("../rpc")
public interface RoBoTRPC extends RemoteService {

	Integer geteRepDay();

	Collection<CountryStats> getCountryStatsHistory();

	Collection<CountryBonuses> getCountryBonusesHistory();

	Collection<CountryTaxes> getCountryTaxesHistory();

	Collection<AgregatedJobMarketOffer> getAgregatedJobMarketOffers();

	Collection<AgregatedMonetaryOffer> getAgregatedMonetaryOffers(
			MonetaryType type);

	Collection<AgregatedMarketplaceOffer> getAgregatedMarketplaceOffer(
			ProductType type, ProductQuality quality);

	Collection<JobMarketOffer> getDailyJobMarketOffers();

	Collection<JobMarketOffer> getDailyJobMarketOffers(Integer day);

	Collection<MonetaryOffer> getDailyMonetaryOffers(MonetaryType type);

	Collection<MonetaryOffer> getDailyMonetaryOffers(MonetaryType type,
			Integer day);

	Collection<MarketplaceOffer> getDailyMarketplaceOffer(ProductType type,
			ProductQuality quality);

	Collection<MarketplaceOffer> getDailyMarketplaceOffer(ProductType type,
			ProductQuality quality, Integer day);

	Collection<MarketplaceProfit> getDailyMarketplaceProfit(ProductType type,
			ProductQuality quality);

	Collection<MarketplaceProfit> getDailyMarketplaceProfit(ProductType type,
			ProductQuality quality, Integer day);

	Collection<AgregatedMarketplaceProfit> getAgregatedMarketplaceProfit(
			ProductType type, ProductQuality quality);

	Collection<RocketPrice> getDailyRocketPrice(RocketQuality quality);

	Collection<RocketPrice> getDailyRocketPrice(RocketQuality quality,
			Integer day);

	Collection<AgregatedRocketPrice> getAgregatedRocketPrice(
			RocketQuality quality);

	Map<PoliticalParty, Collection<PoliticalPartyStats>> getPoliticalHistory();

	void sendFeedback(String message, String location) throws Exception;

	InitialInfo getInitialInfo();

	PoliticalPartyMembersDiff getMembersDiff(PoliticalParty party, Integer day);

	Collection<PoliticalPartyMembersDiff> getMembersDiffHistory(
			PoliticalParty party);

	Multimap<FinancialIndexType, IFinancialIndex> getIndex(
			FinancialIndexType[] types);

	JobHistoryDTO getJobMarketHistory();

	RocketHistoryPriceDTO getRocketPricesHistory();

	RocketDailyPriceDTO getRocketPricesDaily(Integer day);
}
