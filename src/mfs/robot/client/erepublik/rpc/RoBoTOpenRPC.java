package mfs.robot.client.erepublik.rpc;

import java.util.Collection;

import mfs.robot.shared.erepublik.ExpensesInitialInfo;
import mfs.robot.shared.erepublik.data.CitizenAccount;
import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.CountryPresident;
import mfs.robot.shared.erepublik.data.CountryTreasury;
import mfs.robot.shared.erepublik.dto.BalanceDTO;
import mfs.robot.shared.erepublik.dto.CashFlowDTO;
import mfs.robot.shared.erepublik.dto.CountryExpenseSum;
import mfs.robot.shared.erepublik.dto.MonthlyRevenueDTO;
import mfs.robot.shared.erepublik.dto.TaxesOriginDTO;

import com.google.common.collect.Multimap;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("../openrpc")
public interface RoBoTOpenRPC extends RemoteService {

	Collection<CountryTreasury> getCountryTreasuryHistory();

	TaxesOriginDTO getCountryTaxesOriginHistory();

	Collection<CountryExpense> getExpenses(Integer day);

	Multimap<Integer, CountryExpenseSum> getExpensesSum();

	ExpensesInitialInfo getExpensesInitialInfo();

	BalanceDTO getBalanceData(CountryPresident newCP);

	MonthlyRevenueDTO getMonthlyRevenueData();

	Collection<CitizenAccount> getCitizenAccountHistory(Long eRepId);

	Collection<CountryExpense> getExpenses();

	CashFlowDTO getCashFlowData(CountryPresident cp);

	CashFlowDTO getCashFlowData(String selectedOrgName, String selectedOrgId);

	CashFlowDTO getCashFlowData(String selectedOrgName, String selectedOrgId,
			boolean loadTransfers, boolean loadOrgValue);

}
