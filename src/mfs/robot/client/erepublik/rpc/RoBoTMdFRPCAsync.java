package mfs.robot.client.erepublik.rpc;

import java.util.Collection;

import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.CountryIncome;
import mfs.robot.shared.erepublik.data.CountryMoneyTransfer;
import mfs.robot.shared.mdf.MdFInitialInfo;
import mfs.robot.shared.mdf.MdFLog;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface RoBoTMdFRPCAsync {

	void saveManualRevenue(String day, String revenue, String comments,
			AsyncCallback<Void> callback);

	void getRevenueInitialInfo(AsyncCallback<MdFInitialInfo> callback);

	void getRevenueInfo(Integer day, AsyncCallback<MdFInitialInfo> callback);

	void getTopLogs(AsyncCallback<Collection<MdFLog>> callback);

	void getExpenses(Integer day,
			AsyncCallback<Collection<CountryExpense>> callback);

	void getIncomes(Integer day,
			AsyncCallback<Collection<CountryIncome>> callback);

	void getTransfers(Integer day,
			AsyncCallback<Collection<CountryMoneyTransfer>> callback);

	void addExpense(CountryExpense e, AsyncCallback<Boolean> callback);

	void deleteExpense(CountryExpense e, AsyncCallback<Boolean> callback);

	void addIncome(CountryIncome income, AsyncCallback<Boolean> callback);

	void deleteIncome(CountryIncome e, AsyncCallback<Boolean> callback);

	void addTransfer(CountryMoneyTransfer transfer,
			AsyncCallback<Boolean> callback);

	void deleteTransfer(CountryMoneyTransfer transfer,
			AsyncCallback<Boolean> callback);

}
