package mfs.robot.client.erepublik.rpc;

import java.util.Collection;

import mfs.robot.shared.erepublik.ExpensesInitialInfo;
import mfs.robot.shared.erepublik.data.CitizenAccount;
import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.CountryPresident;
import mfs.robot.shared.erepublik.data.CountryTreasury;
import mfs.robot.shared.erepublik.dto.BalanceDTO;
import mfs.robot.shared.erepublik.dto.CashFlowDTO;
import mfs.robot.shared.erepublik.dto.CountryExpenseSum;
import mfs.robot.shared.erepublik.dto.MonthlyRevenueDTO;
import mfs.robot.shared.erepublik.dto.TaxesOriginDTO;

import com.google.common.collect.Multimap;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface RoBoTOpenRPCAsync {

	void getCountryTreasuryHistory(
			AsyncCallback<Collection<CountryTreasury>> callback);

	void getCountryTaxesOriginHistory(AsyncCallback<TaxesOriginDTO> callback);

	void getExpenses(Integer day,
			AsyncCallback<Collection<CountryExpense>> callback);

	void getExpensesSum(
			AsyncCallback<Multimap<Integer, CountryExpenseSum>> callback);

	void getBalanceData(CountryPresident newCP,
			AsyncCallback<BalanceDTO> callback);

	void getExpensesInitialInfo(AsyncCallback<ExpensesInitialInfo> callback);

	void getMonthlyRevenueData(AsyncCallback<MonthlyRevenueDTO> callback);

	void getCashFlowData(CountryPresident cp,
			AsyncCallback<CashFlowDTO> callback);

	void getCitizenAccountHistory(Long eRepId,
			AsyncCallback<Collection<CitizenAccount>> callback);

	void getExpenses(AsyncCallback<Collection<CountryExpense>> callback);

	void getCashFlowData(String selectedOrgName, String selectedOrgId,
			AsyncCallback<CashFlowDTO> callback);

	void getCashFlowData(String selectedOrgName, String selectedOrgId,
			boolean loadTransfers, boolean loadOrgValue,
			AsyncCallback<CashFlowDTO> callback);

}
