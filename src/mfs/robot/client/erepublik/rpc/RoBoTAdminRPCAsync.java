package mfs.robot.client.erepublik.rpc;

import java.util.Collection;
import java.util.Date;

import mfs.robot.shared.admin.eRepUser;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface RoBoTAdminRPCAsync {

	void getUsers(AsyncCallback<Collection<eRepUser>> callback);

	void saveUser(eRepUser user, AsyncCallback<Void> callback);

	void createPresident(String name, Date firstDay, Boolean showFinanceTables,
			Boolean showFinanceGraphs, AsyncCallback<Boolean> callback);

}
