package mfs.robot.client.erepublik;

import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.format.NumberFormat;
import com.googlecode.gwt.charts.client.format.NumberFormatOptions;

public enum DataTableFormatter
{
	BRL(2, " BRL"), BRL_CENTS(4, " BRL"), GOLD(4, " G"), PCT(2, "%"), TIME(0,
			"h00'"), INDEX(4, "");

	private NumberFormat	formatter;

	private DataTableFormatter(int fractionDigits, String suffix) {
		NumberFormatOptions opt = NumberFormatOptions.create();

		opt.setFractionDigits(fractionDigits);
		opt.setSuffix(suffix);

		this.formatter = NumberFormat.create(opt);
	}

	public void format(DataTable dt, Integer... cols) {
		for (Integer col : cols)
			formatter.format(dt, col);
	}
}
