package mfs.robot.client.erepublik.pages;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

public class NotFoundPage extends AContentsPage {

	@Override
	public Widget getWidget() {
		Panel panel = new FlowPanel();
		panel.addStyleName("auto-margin");
		panel.addStyleName("error404");

		panel.add(new Image("headless.cache.png"));
		panel.add(new Label("Erro 404:"));
		panel.add(new Label(
				"A página que você está procurando não foi encontrada."));
		return panel;
	}

	@Override
	protected String getPageName() {
		return "404-NotFound";
	}
}
