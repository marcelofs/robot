package mfs.robot.client.erepublik.pages.profit;

import java.util.Collection;

import mfs.robot.client.erepublik.DataTableFormatter;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AContentsPage;
import mfs.robot.shared.erepublik.data.MarketplaceProfit;
import mfs.robot.shared.erepublik.data.ProductQuality;
import mfs.robot.shared.erepublik.data.ProductType;
import static mfs.robot.client.erepublik.Utils.*;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.ColumnChart;
import com.googlecode.gwt.charts.client.corechart.ColumnChartOptions;
import com.googlecode.gwt.charts.client.options.HAxis;
import com.googlecode.gwt.charts.client.options.VAxis;

public class ProfitDailyInvestmentPage extends AContentsPage {

	private DockLayoutPanel		panel	= new DockLayoutPanel(Unit.EM);

	private final ListBox		type	= new ListBox();
	private final ListBox		quality	= new ListBox();
	private final ListBox		day		= new ListBox();

	// TODO chooser cc/gold, investment in gold
	// TODO lucro, investimento manager

	protected boolean			loaded	= false;

	protected final ColumnChart	cols	= new ColumnChart();

	@Override
	public Widget getWidget() {
		panel.addNorth(getHeader(), 2.5);
		panel.add(cols);
		return panel;
	}

	private Panel getHeader() {
		Panel header = new FlowPanel();
		addStyles(header, "auto-margin", "graph-header");

		addStyle("graph-chooser", type, quality, day);

		type.addItem("Weapon");
		type.addItem("Food");

		Integer someDay = initialInfo.eRepDay - 7;
		for (int i = 0; i <= 7; i++, someDay++)
			day.addItem(someDay + "");

		for (ProductQuality q : ProductQuality.values())
			if (!q.equals(ProductQuality.RAW))
				quality.addItem(q.name());

		if (initialInfo.isAdmin)
			header.add(type);

		header.add(quality);
		header.add(day);

		type.setSelectedIndex(0);
		quality.setSelectedIndex(6);
		day.setSelectedIndex(7);

		addChangeHandler(getUpdateChangeHandler(), type, quality, day);

		return header;
	}

	@Override
	protected ChangeHandler getUpdateChangeHandler() {
		return new ChangeHandler()
		{

			@Override
			public void onChange(ChangeEvent event) {
				loaded = false;
				update();
			}
		};
	}

	protected DataTable getDataTable() {
		final DataTable data = DataTable.create();

		data.addColumn(ColumnType.NUMBER, "Horário");
		data.addColumn(ColumnType.NUMBER, "Retorno médio (com funcionários)");
		addTooltipColumn(data);
		data.addColumn(ColumnType.NUMBER,
				"Investimento necessário (com funcionários)");
		addTooltipColumn(data);

		return data;
	}

	private native void addTooltipColumn(DataTable data) /*-{
		data.addColumn({
			type : 'string',
			role : 'tooltip'
		});
	}-*/;

	protected void draw(final ProductType type, final ProductQuality quality,
			final Integer day, final Runnable... callbacks) {

		final String name = type.name().toLowerCase() + " "
				+ quality.name().toLowerCase();

		final DataTable data = getDataTable();

		rpcService.getDailyMarketplaceProfit(type, quality, day,
				new SimpleCallback<Collection<MarketplaceProfit>>()
				{
					@Override
					public void onSuccess(Collection<MarketplaceProfit> result) {
						fillData(result, data);

						cols.draw(data, createColumnOpts("Fábrica de " + name
								+ " no dia " + day));

						for (Runnable r : callbacks)
							r.run();

						recordAnalyticsHit(day, type, quality);
					}
				});
	}

	protected void recordAnalyticsHit(Integer day, ProductType type,
			ProductQuality quality) {

		String dayTxt = "Unknown";

		if (initialInfo.eRepDay.equals(day))
			dayTxt = "Today";
		else
			dayTxt = "" + (day - initialInfo.eRepDay);

		recordAnalyticsHit("/Profit/Daily/" + type.name() + "/"
				+ quality.name() + "/" + dayTxt);
	}

	protected void fillData(Collection<MarketplaceProfit> result, DataTable data) {
		data.addRows(result.size());

		int row = 0;
		for (MarketplaceProfit offer : result) {
			String tooltipText = getTooltipText(offer);
			data.setValue(row, 0, offer.geteRepTime());
			data.setValue(row, 1, offer.getAverageProfit());
			data.setValue(row, 2, tooltipText);
			data.setValue(row, 3, offer.getInvestmentNeeded());
			data.setValue(row, 4, tooltipText);
			row++;
		}

		DataTableFormatter.TIME.format(data, 0);
		DataTableFormatter.BRL.format(data, 1, 3);
	}

	private String getTooltipText(MarketplaceProfit offer) {
		StringBuilder b = new StringBuilder();
		b.append(offer.geteRepTime()).append("h00'").append("\n");

		String invValue = NumberFormat.getFormat(".##").format(
				offer.getInvestmentNeeded());
		b.append("Investimento necessário: ").append(invValue).append(" BRL\n");

		String profValue = NumberFormat.getFormat(".##").format(
				offer.getAverageProfit());
		b.append("Retorno médio (com funcionários): ").append(profValue)
				.append(" BRL\n");

		String pct = NumberFormat.getFormat(".##")
				.format(offer.getAverageRoi());
		b.append("ROI: ").append(pct).append("%");

		return b.toString();
	}

	protected ColumnChartOptions createColumnOpts(String title) {
		ColumnChartOptions opts = super.createColumnOpts(title);

		opts.setVAxis(VAxis.create("Valor"));
		HAxis hAxis = HAxis.create("Horário do servidor");
		hAxis.setMaxValue(24);
		opts.setHAxis(hAxis);

		opts.setIsStacked(true);

		return opts;
	}

	@Override
	public void update() {
		super.update();
		if (!loaded) {

			disable(type, quality, day);

			ProductType pType = ProductType.valueOf(type.getItemText(
					type.getSelectedIndex()).toUpperCase());
			ProductQuality pQuality = ProductQuality.valueOf(quality
					.getItemText(quality.getSelectedIndex()).toUpperCase());
			Integer pDay = Integer.valueOf(day.getItemText(day
					.getSelectedIndex()));

			panel.remove(cols);

			Image loadingImg = getLoadingImage();
			panel.add(loadingImg);

			draw(pType, pQuality, pDay,
					removeLoadingImgCallback(loadingImg, panel),
					addChartCallback(panel),
					enableWidgetsCallback(day, quality, type));

			loaded = true;
		}

	}

	protected Runnable addChartCallback(final Panel panel) {
		return new Runnable()
		{

			@Override
			public void run() {
				panel.add(cols);
			}
		};
	}

	@Override
	protected String getPageName() {
		return "/Profit/Daily";
	}

}
