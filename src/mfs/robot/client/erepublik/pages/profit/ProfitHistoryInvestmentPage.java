package mfs.robot.client.erepublik.pages.profit;

import java.util.Collection;

import mfs.robot.client.erepublik.DataTableFormatter;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AHistoryPage;
import mfs.robot.shared.erepublik.data.AgregatedMarketplaceProfit;
import mfs.robot.shared.erepublik.data.ProductQuality;
import mfs.robot.shared.erepublik.data.ProductType;
import static mfs.robot.client.erepublik.Utils.*;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ChartType;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.ColumnChartOptions;
import com.googlecode.gwt.charts.client.options.AxisTitlesPosition;
import com.googlecode.gwt.charts.client.options.CoreOptions;
import com.googlecode.gwt.charts.client.options.HAxis;
import com.googlecode.gwt.charts.client.options.Legend;
import com.googlecode.gwt.charts.client.options.LegendPosition;
import com.googlecode.gwt.charts.client.options.VAxis;

public class ProfitHistoryInvestmentPage extends AHistoryPage {

	private DockLayoutPanel	panel	= new DockLayoutPanel(Unit.EM);

	private final ListBox	type	= new ListBox();
	private final ListBox	quality	= new ListBox();

	@Override
	protected void setChartType() {
		chart.setChartType(ChartType.COLUMN);
	}

	@Override
	public Widget getWidget() {

		panel.addNorth(dashboard, 0);
		panel.addNorth(getHeader(), 3);
		panel.addSouth(numberRangeFilter, 6.25);
		panel.add(chart);

		return panel;
	}

	@Override
	protected DataTable getDataTable() {
		final DataTable data = DataTable.create();
		data.addColumn(ColumnType.NUMBER, "Dia");
		data.addColumn(ColumnType.NUMBER, "Retorno médio (com funcionários)");
		addTooltipColumn(data);
		data.addColumn(ColumnType.NUMBER,
				"Investimento necessário (com funcionários)");
		addTooltipColumn(data);

		return data;
	}

	private native void addTooltipColumn(DataTable data) /*-{
		data.addColumn({
			type : 'string',
			role : 'tooltip'
		});
	}-*/;

	private void fillProfitData(DataTable data,
			Collection<AgregatedMarketplaceProfit> result) {
		data.addRows(result.size());

		int row = 0;
		for (AgregatedMarketplaceProfit offer : result) {
			String tooltipText = getTooltipText(offer);
			data.setValue(row, 0, offer.geteRepDay());
			data.setValue(row, 1, offer.getAverageProfit());
			data.setValue(row, 2, tooltipText);
			data.setValue(row, 3, offer.getAverageInvestmentNeeded());
			data.setValue(row, 4, tooltipText);

			row++;

			if (row >= result.size()) // last element
				lastDay = offer.geteRepDay();
		}

		DataTableFormatter.BRL.format(data, 1, 3);
	}

	private String getTooltipText(AgregatedMarketplaceProfit offer) {
		StringBuilder b = new StringBuilder();
		b.append(offer.geteRepDay()).append("\n");

		String invValue = NumberFormat.getFormat(".##").format(
				offer.getAverageInvestmentNeeded());
		b.append("Investimento necessário: ").append(invValue).append(" BRL\n");

		String profValue = NumberFormat.getFormat(".##").format(
				offer.getAverageProfit());
		b.append("Retorno médio (com funcionários): ").append(profValue)
				.append(" BRL\n");

		String pct = NumberFormat.getFormat(".##")
				.format(offer.getAverageRoi());
		b.append("ROI: ").append(pct).append("%");

		return b.toString();
	}

	@Override
	protected CoreOptions getControlChartOptions() {
		ColumnChartOptions controlChartOptions = ColumnChartOptions.create();
		controlChartOptions.setHeight(100);
		controlChartOptions.setBackgroundColor("#EDF4F5");
		return controlChartOptions;
	}

	@Override
	protected CoreOptions getChartOptions() {
		ColumnChartOptions chartOptions = ColumnChartOptions.create();
		chartOptions.setLegend(Legend.create(LegendPosition.TOP));
		chartOptions.setBackgroundColor("#EDF4F5");
		chartOptions.setAxisTitlesPosition(AxisTitlesPosition.OUT);
		chartOptions.setVAxis(VAxis.create("Valor"));
		chartOptions.setHAxis(HAxis.create("Dia no eRepublik"));
		chartOptions.setIsStacked(true);
		return chartOptions;
	}

	private Panel getHeader() {
		Panel header = new FlowPanel();
		addStyles(header, "auto-margin", "graph-header");

		addStyle("graph-chooser", type, quality);

		type.addItem("Weapon");
		type.addItem("Food");

		for (ProductQuality q : ProductQuality.values())
			quality.addItem(q.name());

		if (initialInfo.isAdmin)
			header.add(type);

		header.add(quality);

		type.setSelectedIndex(0);
		quality.setSelectedIndex(6);

		addChangeHandler(getUpdateChangeHandler(), type, quality);

		return header;
	}

	protected void draw(final ProductType type, final ProductQuality quality,
			final Runnable... callbacks) {

		final DataTable data = getDataTable();

		final String name = type.name().toLowerCase() + " "
				+ quality.name().toLowerCase();

		rpcService.getAgregatedMarketplaceProfit(type, quality,
				new SimpleCallback<Collection<AgregatedMarketplaceProfit>>()
				{
					@Override
					public void onSuccess(
							Collection<AgregatedMarketplaceProfit> result) {
						fillProfitData(data, result);

						setControlOptions(name);
						dashboard.draw(data);

						for (Runnable r : callbacks)
							r.run();

						recordAnalyticsHit("/Profit/History/" + type.name()
								+ "/" + quality.name());
					}
				});
	}

	@Override
	public void update() {
		super.update();
		if (!loaded) {

			disable(type, quality);

			ProductType pType = ProductType.valueOf(type.getItemText(
					type.getSelectedIndex()).toUpperCase());
			ProductQuality pQuality = ProductQuality.valueOf(quality
					.getItemText(quality.getSelectedIndex()).toUpperCase());

			panel.remove(chart);
			panel.remove(numberRangeFilter);

			Image loadingImg = getLoadingImage();
			panel.add(loadingImg);
			draw(pType, pQuality, removeLoadingImgCallback(loadingImg, panel),
					addChartCallback(panel),
					enableWidgetsCallback(type, quality));
			loaded = true;
		}
	}

	@Override
	protected String getPageName() {
		return "/Profit/History";
	}

}
