package mfs.robot.client.erepublik.pages.citizens;

import mfs.robot.client.erepublik.pages.AContentsPage;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;
import static mfs.robot.client.erepublik.Utils.*;

public class CitizensPage extends AContentsPage {

	private NewCitizensHistoryPage		newCitizens;
	private ActiveCitizensHistoryPage	activeCitizens;

	@Override
	public Widget getWidget() {
		TabLayoutPanel root = new TabLayoutPanel(2.3, Unit.EM);

		newCitizens = new NewCitizensHistoryPage();
		activeCitizens = new ActiveCitizensHistoryPage();

		setInitialInfo(newCitizens, activeCitizens);

		root.add(activeCitizens.getWidget(), "Cidadãos Ativos");
		root.add(newCitizens.getWidget(), "Novos Cidadãos");

		root.addSelectionHandler(getLoaderSelectionHandler(activeCitizens,
				newCitizens));

		// TODO load based on params
		activeCitizens.update();

		return root;
	}
}
