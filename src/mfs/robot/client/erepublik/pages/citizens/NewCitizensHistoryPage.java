package mfs.robot.client.erepublik.pages.citizens;

import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import static mfs.robot.client.erepublik.Utils.removeLoadingImgCallback;

import java.util.Collection;

import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AHistoryPage;
import mfs.robot.shared.erepublik.data.CountryStats;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.options.VAxis;

public class NewCitizensHistoryPage extends AHistoryPage {

	private DockLayoutPanel	panel	= new DockLayoutPanel(Unit.EM);

	@Override
	public Widget getWidget() {

		panel.addNorth(dashboard, 0);
		panel.addSouth(numberRangeFilter, 6.25);
		panel.add(chart);

		return panel;
	}

	protected DataTable getDataTable() {
		final DataTable data = DataTable.create();

		data.addColumn(ColumnType.NUMBER, "Dia");
		data.addColumn(ColumnType.NUMBER, "Novos Cidadãos");

		return data;
	}

	protected VAxis getVAxis() {
		return VAxis.create("Novos Cidadãos");
	}

	protected void fillCitizenData(DataTable data,
			Collection<CountryStats> result) {
		data.addRows(result.size());

		int row = 0;
		for (CountryStats offer : result) {
			data.setValue(row, 0, offer.geteRepDay());
			if (offer.getNewCitizens() != null)
				data.setValue(row, 1, offer.getNewCitizens());
			row++;

			if (row >= result.size()) // last element
				lastDay = offer.geteRepDay();
		}

	}

	protected void draw(final Runnable... callbacks) {

		final DataTable data = getDataTable();

		rpcService
				.getCountryStatsHistory(new SimpleCallback<Collection<CountryStats>>()
				{
					@Override
					public void onSuccess(Collection<CountryStats> result) {
						fillCitizenData(data, result);

						setControlOptions("Novos Cidadãos");
						dashboard.draw(data);

						for (Runnable r : callbacks)
							r.run();
					}
				});
	}

	@Override
	public void update() {
		super.update();
		if (!loaded) {

			panel.remove(chart);
			panel.remove(numberRangeFilter);

			Image loadingImg = getLoadingImage();
			panel.add(loadingImg);
			draw(removeLoadingImgCallback(loadingImg, panel),
					addChartCallback(panel));
			loaded = true;
		}
	}

	@Override
	protected String getPageName() {
		return "/Citizens/New";
	}
}
