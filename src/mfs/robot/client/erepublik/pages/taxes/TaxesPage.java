package mfs.robot.client.erepublik.pages.taxes;

import mfs.robot.client.erepublik.pages.AContentsPage;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;
import static mfs.robot.client.erepublik.Utils.*;

public class TaxesPage extends AContentsPage {

	private TaxesHistoryPage	history;

	@Override
	public Widget getWidget() {
		TabLayoutPanel root = new TabLayoutPanel(2.3, Unit.EM);

		history = new TaxesHistoryPage();

		setInitialInfo(history);

		root.add(history.getWidget(), "Histórico");

		root.addSelectionHandler(getLoaderSelectionHandler(history));

		history.update();

		return root;
	}
}
