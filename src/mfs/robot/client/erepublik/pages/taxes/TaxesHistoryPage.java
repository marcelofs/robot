package mfs.robot.client.erepublik.pages.taxes;

import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import static mfs.robot.client.erepublik.Utils.removeLoadingImgCallback;

import java.util.Collection;

import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AHistoryPage;
import mfs.robot.shared.erepublik.data.CountryTaxes;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;

public class TaxesHistoryPage extends AHistoryPage {

	private DockLayoutPanel	panel	= new DockLayoutPanel(Unit.EM);

	@Override
	public Widget getWidget() {

		panel.addNorth(dashboard, 0);
		panel.addSouth(numberRangeFilter, 6.25);
		panel.add(chart);

		return panel;
	}

	protected DataTable getDataTable() {
		final DataTable data = DataTable.create();

		data.addColumn(ColumnType.NUMBER, "Dia");
		data.addColumn(ColumnType.NUMBER, "Income");
		data.addColumn(ColumnType.NUMBER, "VAT: Food");
		data.addColumn(ColumnType.NUMBER, "VAT: Weapons");
		data.addColumn(ColumnType.NUMBER, "Import: Food");
		data.addColumn(ColumnType.NUMBER, "Import: Weapons");
		data.addColumn(ColumnType.NUMBER, "Import: Food raw");
		data.addColumn(ColumnType.NUMBER, "Import: Weapons raw");

		return data;
	}

	protected void fillTaxesData(DataTable data, Collection<CountryTaxes> result) {
		data.addRows(result.size());

		int row = 0;
		for (CountryTaxes tax : result) {
			data.setValue(row, 0, tax.geteRepDay());
			data.setValue(row, 1, tax.getIncomeTax());
			data.setValue(row, 2, tax.getFoodVat());
			data.setValue(row, 3, tax.getWeaponsVat());
			data.setValue(row, 4, tax.getFoodImport());
			data.setValue(row, 5, tax.getWeaponsImport());
			data.setValue(row, 6, tax.getFrmImport());
			data.setValue(row, 7, tax.getWrmImport());
			row++;

			if (row >= result.size()) // last element
				lastDay = tax.geteRepDay();
		}

	}

	protected void draw(final Runnable... callbacks) {

		final DataTable data = getDataTable();

		rpcService
				.getCountryTaxesHistory(new SimpleCallback<Collection<CountryTaxes>>()
				{
					@Override
					public void onSuccess(Collection<CountryTaxes> result) {
						fillTaxesData(data, result);

						setControlOptions("Impostos");
						dashboard.draw(data);

						for (Runnable r : callbacks)
							r.run();
					}
				});
	}

	@Override
	public void update() {
		super.update();
		if (!loaded) {

			panel.remove(chart);
			panel.remove(numberRangeFilter);

			Image loadingImg = getLoadingImage();
			panel.add(loadingImg);
			draw(removeLoadingImgCallback(loadingImg, panel),
					addChartCallback(panel));
			loaded = true;
		}
	}

	@Override
	protected String getPageName() {
		return "/Taxes/History";
	}
}
