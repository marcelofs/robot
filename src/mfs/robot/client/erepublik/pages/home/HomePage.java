package mfs.robot.client.erepublik.pages.home;

import static mfs.robot.client.erepublik.Utils.addStyle;
import static mfs.robot.client.erepublik.Utils.getLogoImage;
import mfs.robot.client.erepublik.Pages;
import mfs.robot.client.erepublik.pages.AContentsPage;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

public class HomePage extends AContentsPage {

	@Override
	public Widget getWidget() {
		DockLayoutPanel root = new DockLayoutPanel(Unit.EM);

		if (initialInfo != null)
			buildLoggedIn(root);
		else
			buildNotLoggedIn(root);

		return root;
	}

	private void buildLoggedIn(DockLayoutPanel root) {
		Label lbl = new Label("<~~");
		root.addWest(lbl, 3);

		// DockLayoutPanel bip = new DockLayoutPanel(Unit.PX);
		// {
		// Hyperlink link = new Hyperlink(
		// "O RoBoT recomenda o investimento no Banco Liberty",
		// Pages.liberty.name());
		// link.setWidth("24em");
		//
		// Image img = new Image("libertylogo.cache.jpeg");
		// img.addClickHandler(new ClickHandler()
		// {
		//
		// @Override
		// public void onClick(ClickEvent event) {
		// History.newItem(Pages.liberty.name());
		// }
		// });
		//
		// addStyle("dark-green", link);
		// addStyle("auto-margin", link, img);
		//
		// bip.addNorth(img, 115);
		// bip.add(link);
		// }
		// root.addSouth(bip, 12);

		Panel logoPanel = new FlowPanel();
		{
			logoPanel.setHeight("120px");
			logoPanel.setWidth("120px");
			logoPanel.add(getLogoImage());
		}

		addStyle("auto-margin", logoPanel);

		recordAnalyticsHit("/Home/LoggedIn");
		root.add(logoPanel);
	}

	private void buildNotLoggedIn(DockLayoutPanel root) {
		Panel infoPanel = new FlowPanel();
		infoPanel.addStyleName("welcome");

		HTML html = new HTML(
				"<p>O RoBoT é uma ferramenta de análise de mercado para o "
						+ "<a target='_blank' href='http://www.erepublik.com/br/referrer/nW0lf'>eRepublik Brasil</a>, "
						+ "atualmente disponível gratuitamente para todos os eBrasileiros.</p>"
						+ "<p>Também conta com dados fornecidos diretamente pelo Ministério da Fazenda eBrasileiro.</p>"
						+ "<p>Ainda não conhece o jogo? Nele você pode se transformar em general, congressista, "
						+ "ministro, diplomata, jornalista, empresário, e até Presidente da República! "
						+ "<a target='_blank' href='http://www.erepublik.com/br/referrer/nW0lf'>Cadastre-se aqui</a>!</p>"
						+ "<h2>Para se registrar...</h2> <ul> <li>Faça login com sua conta do Google. "
						+ "<a target='_blank' href='http://support.google.com/accounts/bin/answer.py?hl=pt-br&answer=112802'>É completamente seguro</a>!"
						+ " </li> <li>Verifique seu usuário no eRepublik. Isso é necessário para confirmar que você é eBrasileiro.</li>"
						+ " <li>Pronto! Você já pode acessar todas as informações do RoBoT!</li></ul>");

		HTML btn = new HTML(
				"<input type='button' class='gwt-Button welcome-btn' value='Registrar'"
						+ "onclick=\"window.location.href = 'login/check';\">");
		btn.addStyleName("welcome-btn");

		infoPanel.add(html);
		infoPanel.add(btn);

		recordAnalyticsHit("/Home/NotLoggedIn");

		root.add(infoPanel);
	}

	@Override
	protected String getPageName() {
		return "Home";
	}

}
