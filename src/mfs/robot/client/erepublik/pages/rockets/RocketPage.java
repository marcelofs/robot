package mfs.robot.client.erepublik.pages.rockets;

import mfs.robot.client.erepublik.pages.AContentsPage;
import static mfs.robot.client.erepublik.Utils.*;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

public class RocketPage extends AContentsPage {

	private RocketDailyPage		daily;
	private RocketHistoryPage	history;

	@Override
	public Widget getWidget() {
		TabLayoutPanel root = new TabLayoutPanel(2.3, Unit.EM);

		daily = new RocketDailyPage();
		history = new RocketHistoryPage();

		setInitialInfo(daily, history);

//		root.add(daily.getWidget(), "Diário");
		root.add(history.getWidget(), "Histórico");

		root.addSelectionHandler(getLoaderSelectionHandler(daily, history));

//		daily.update();
		history.update();

		return root;
	}
}
