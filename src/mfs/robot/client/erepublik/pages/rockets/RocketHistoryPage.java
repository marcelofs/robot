package mfs.robot.client.erepublik.pages.rockets;

import static mfs.robot.client.erepublik.Utils.addChangeHandler;
import static mfs.robot.client.erepublik.Utils.addStyle;
import static mfs.robot.client.erepublik.Utils.addStyles;
import static mfs.robot.client.erepublik.Utils.disable;
import static mfs.robot.client.erepublik.Utils.enableWidgetsCallback;
import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import static mfs.robot.client.erepublik.Utils.removeLoadingImgCallback;

import mfs.robot.client.erepublik.DataTableFormatter;
import mfs.robot.client.erepublik.calculator.RocketHistoryCalculator;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AHistoryPage;
import mfs.robot.shared.erepublik.data.MonetaryType;
import mfs.robot.shared.erepublik.data.RocketQuality;
import mfs.robot.shared.erepublik.dto.RocketHistoryPriceDTO;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.options.VAxis;

public class RocketHistoryPage extends AHistoryPage {

	private DockLayoutPanel			panel		= new DockLayoutPanel(Unit.EM);

	private final ListBox			quality		= new ListBox();
	private final ListBox			currency	= new ListBox();

	private RocketHistoryCalculator	calculator;

	@Override
	public Widget getWidget() {

		panel.addNorth(dashboard, 0);
		panel.addNorth(getHeader(), 3);
		panel.addSouth(numberRangeFilter, 6.25);
		panel.add(chart);

		return panel;
	}

	private Panel getHeader() {
		Panel header = new FlowPanel();

		addStyles(header, "auto-margin", "graph-header");

		addStyle("graph-chooser", currency, quality);

		for (MonetaryType t : MonetaryType.values())
			currency.addItem(t.name().toLowerCase());

		for (RocketQuality q : RocketQuality.values())
			quality.addItem(q.name());

		header.add(quality);
		header.add(currency);

		currency.setSelectedIndex(1);
		quality.setSelectedIndex(2);
		addChangeHandler(getUpdateChangeHandler(), currency, quality);

		return header;
	}

	protected VAxis getVAxis() {
		return VAxis.create("Preço");
	}

	protected DataTable getDataTable(MonetaryType type) {
		if (type == MonetaryType.CURRENCY)
			return super.getDataTable();
		else {
			final DataTable data = DataTable.create();
			data.addColumn(ColumnType.NUMBER, "Dia");
			data.addColumn(ColumnType.NUMBER, "Valor Médio");
			return data;
		}
	}

	protected void fillData(RocketQuality quality, MonetaryType type,
			DataTable data) {

		data.addRows(calculator.getNbrOfDays());

		int row = 0;

		for (int i = calculator.getFirstDay(); i <= calculator.getLastDay(); i++) {
			data.setValue(row, 0, i);
			Double value = calculator.getValue(quality, i, type);
			if (value != null && value != 0)
				data.setValue(row, 1, value);
			row++;
		}

		lastDay = calculator.getLastDay();

		if (type == MonetaryType.GOLD)
			DataTableFormatter.GOLD.format(data, 1);
		else
			DataTableFormatter.BRL.format(data, 1);
	}

	protected void downloadAndDraw(final MonetaryType type,
			final RocketQuality quality, final Runnable... callbacks) {
		if (calculator == null)
			rpcService
					.getRocketPricesHistory(new SimpleCallback<RocketHistoryPriceDTO>()
					{

						@Override
						public void onSuccess(RocketHistoryPriceDTO result) {
							calculator = new RocketHistoryCalculator(
									result.prices, result.goldValue);
							draw(type, quality, callbacks);
						}

					});
		else
			draw(type, quality, callbacks);
	}

	protected void draw(final MonetaryType type, final RocketQuality quality,
			final Runnable... callbacks) {

		final DataTable data = getDataTable(type);

		fillData(quality, type, data);

		setControlOptions("rockets " + quality.name().toLowerCase());
		dashboard.draw(data);

		for (Runnable r : callbacks)
			r.run();

		recordAnalyticsHit("/Rockets/History/" + quality.name());

	}

	@Override
	public void update() {
		super.update();
		if (!loaded) {

			disable(quality, currency);

			MonetaryType pType = MonetaryType.valueOf(currency.getItemText(
					currency.getSelectedIndex()).toUpperCase());
			RocketQuality pQuality = RocketQuality.valueOf(quality.getItemText(
					quality.getSelectedIndex()).toUpperCase());

			panel.remove(chart);
			panel.remove(numberRangeFilter);

			Image loadingImg = getLoadingImage();
			panel.add(loadingImg);
			downloadAndDraw(pType, pQuality,
					removeLoadingImgCallback(loadingImg, panel),
					addChartCallback(panel),
					enableWidgetsCallback(quality, currency));
			loaded = true;
		}
	}

	@Override
	protected String getPageName() {
		return "/Rockets/History";
	}
}
