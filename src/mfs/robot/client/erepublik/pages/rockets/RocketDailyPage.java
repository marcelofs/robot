package mfs.robot.client.erepublik.pages.rockets;

import static mfs.robot.client.erepublik.Utils.addChangeHandler;
import static mfs.robot.client.erepublik.Utils.addStyle;
import static mfs.robot.client.erepublik.Utils.addStyles;
import static mfs.robot.client.erepublik.Utils.disable;
import static mfs.robot.client.erepublik.Utils.enableWidgetsCallback;
import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import static mfs.robot.client.erepublik.Utils.removeLoadingImgCallback;
import mfs.robot.client.erepublik.DataTableFormatter;
import mfs.robot.client.erepublik.Utils;
import mfs.robot.client.erepublik.calculator.RocketDailyCalculator;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.ADailyPage;
import mfs.robot.shared.erepublik.data.MonetaryType;
import mfs.robot.shared.erepublik.data.RocketQuality;
import mfs.robot.shared.erepublik.dto.RocketDailyPriceDTO;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.options.VAxis;

public class RocketDailyPage extends ADailyPage {

	private DockLayoutPanel			panel		= new DockLayoutPanel(Unit.EM);

	private final ListBox			quality		= new ListBox();
	private final ListBox			day			= new ListBox();
	private final ListBox			currency	= new ListBox();

	private RocketDailyCalculator	calculator;

	@Override
	public Widget getWidget() {
		panel.addNorth(getHeader(), 2.5);
		panel.add(lines);
		return panel;
	}

	private Panel getHeader() {
		Panel header = new FlowPanel();

		addStyles(header, "auto-margin", "graph-header");
		addStyle("graph-chooser", currency, quality, day);

		for (MonetaryType t : MonetaryType.values())
			currency.addItem(t.name().toLowerCase());

		Integer someDay = initialInfo.eRepDay - 7;
		for (int i = 0; i <= 7; i++, someDay++)
			day.addItem(someDay + "");

		for (RocketQuality q : RocketQuality.values())
			quality.addItem(q.name());

		header.add(quality);
		header.add(day);
		header.add(currency);

		currency.setSelectedIndex(1);
		quality.setSelectedIndex(2);
		day.setSelectedIndex(7);

		addChangeHandler(getUpdateChangeHandler(), currency, quality, day);

		return header;
	}

	protected DataTable getDataTable() {
		final DataTable data = DataTable.create();

		data.addColumn(ColumnType.NUMBER, "Horário");
		data.addColumn(ColumnType.NUMBER, "Preço Médio");

		return data;
	}

	protected VAxis getVAxis() {
		return VAxis.create("Preço");
	}

	protected void fillRocketData(MonetaryType type, RocketQuality quality,
			DataTable data) {

		data.addRows(calculator.getLastHour() + 1);

		// Utils.firebugLog("last hour: " + calculator.getLastHour());

		int row = 0;
		for (int i = 0; i <= 24; i++) {
			data.setValue(row, 0, i);
			if (i <= calculator.getLastHour()) {
				Double value = calculator.getValue(quality, i, type);
				if (value != null && value != 0)
					data.setValue(row, 1, value);
				// Utils.firebugLog("i = " + i + " | value = " + value);
			}
		}

		DataTableFormatter.TIME.format(data, 0);
		if (MonetaryType.CURRENCY == type)
			DataTableFormatter.BRL.format(data, 1);
		else
			DataTableFormatter.GOLD.format(data, 1);
	}

	protected void downloadAndDraw(final MonetaryType type,
			final RocketQuality quality, final Integer day,
			final Runnable... callbacks) {

		if (calculator == null || calculator.getDay() != day)
			rpcService.getRocketPricesDaily(day,
					new SimpleCallback<RocketDailyPriceDTO>()
					{

						@Override
						public void onSuccess(RocketDailyPriceDTO result) {
							calculator = new RocketDailyCalculator(
									result.prices, result.goldValue, result.day);
							draw(type, quality, day, callbacks);
						}

					});
		else
			draw(type, quality, day, callbacks);
	}

	protected void draw(final MonetaryType type, final RocketQuality quality,
			final Integer day, final Runnable... callbacks) {

		final DataTable data = getDataTable();

		fillRocketData(type, quality, data);

		lines.draw(data, createDailyLineOpts("Preço em "
				+ type.name().toLowerCase() + " de rockets "
				+ quality.name().toLowerCase() + " no dia " + day));

		for (Runnable r : callbacks)
			r.run();

		recordAnalyticsHit(day, quality);

	}

	protected void recordAnalyticsHit(Integer day, RocketQuality quality) {

		String dayTxt = "Unknown";

		if (initialInfo.eRepDay.equals(day))
			dayTxt = "Today";
		else
			dayTxt = "" + (day - initialInfo.eRepDay);

		recordAnalyticsHit("/Rockets/Daily/" + quality.name() + "/" + dayTxt);
	}

	@Override
	public void update() {
		super.update();
		if (!loaded) {

			disable(currency, quality, day);

			MonetaryType pType = MonetaryType.valueOf(currency.getItemText(
					currency.getSelectedIndex()).toUpperCase());
			RocketQuality pQuality = RocketQuality.valueOf(quality.getItemText(
					quality.getSelectedIndex()).toUpperCase());
			Integer pDay = Integer.valueOf(day.getItemText(day
					.getSelectedIndex()));

			panel.remove(lines);

			Image loadingImg = getLoadingImage();
			panel.add(loadingImg);

			downloadAndDraw(pType, pQuality, pDay,
					removeLoadingImgCallback(loadingImg, panel),
					addChartCallback(panel),
					enableWidgetsCallback(day, quality, currency));

			loaded = true;
		}

	}

	@Override
	protected String getPageName() {
		return "/Rockets/Daily";
	}

}
