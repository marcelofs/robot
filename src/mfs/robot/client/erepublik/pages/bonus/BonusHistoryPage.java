package mfs.robot.client.erepublik.pages.bonus;

import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import static mfs.robot.client.erepublik.Utils.removeLoadingImgCallback;

import java.util.Collection;

import mfs.robot.client.erepublik.DataTableFormatter;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AHistoryPage;
import mfs.robot.shared.erepublik.data.CountryBonuses;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ChartType;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.ColumnChartOptions;
import com.googlecode.gwt.charts.client.options.AxisTitlesPosition;
import com.googlecode.gwt.charts.client.options.CoreOptions;
import com.googlecode.gwt.charts.client.options.Legend;
import com.googlecode.gwt.charts.client.options.LegendPosition;
import com.googlecode.gwt.charts.client.options.VAxis;

public class BonusHistoryPage extends AHistoryPage {

	private DockLayoutPanel	panel	= new DockLayoutPanel(Unit.EM);

	@Override
	public Widget getWidget() {

		panel.addNorth(dashboard, 0);
		panel.addSouth(numberRangeFilter, 6.25);
		panel.add(chart);

		return panel;
	}

	@Override
	protected ChartType getChartType() {
		return ChartType.COLUMN;
	}

	@Override
	protected CoreOptions getChartOptions() {
		ColumnChartOptions chartOptions = ColumnChartOptions.create();
		chartOptions.setLegend(Legend.create(LegendPosition.TOP));
		chartOptions.setBackgroundColor("#EDF4F5");
		chartOptions.setAxisTitlesPosition(AxisTitlesPosition.OUT);
		chartOptions.setVAxis(getVAxis());
		chartOptions.setHAxis(getHAxis());
		return chartOptions;
	}

	@Override
	protected VAxis getVAxis() {
		VAxis axis = VAxis.create("Valor");
		axis.setMaxValue(100);
		return axis;
	}

	@Override
	protected DataTable getDataTable() {
		final DataTable data = DataTable.create();

		data.addColumn(ColumnType.NUMBER, "Dia");
		data.addColumn(ColumnType.NUMBER, "Bônus: Food");
		data.addColumn(ColumnType.NUMBER, "Bônus: Weapons");

		return data;
	}

	protected void fillBonusesData(DataTable data,
			Collection<CountryBonuses> result) {
		data.addRows(result.size());

		int row = 0;
		for (CountryBonuses bonuses : result) {
			data.setValue(row, 0, bonuses.geteRepDay());
			data.setValue(row, 1, bonuses.getFoodBonus() * 100);
			data.setValue(row, 2, bonuses.getWeaponsBonus() * 100);
			row++;

			if (row >= result.size()) // last element
				lastDay = bonuses.geteRepDay();
		}

		DataTableFormatter.PCT.format(data, 1, 2);

	}

	protected void draw(final Runnable... callbacks) {

		final DataTable data = getDataTable();

		rpcService
				.getCountryBonusesHistory(new SimpleCallback<Collection<CountryBonuses>>()
				{
					@Override
					public void onSuccess(Collection<CountryBonuses> result) {
						fillBonusesData(data, result);

						setControlOptions("Bônus");
						dashboard.draw(data);

						for (Runnable r : callbacks)
							r.run();
					}
				});
	}

	@Override
	public void update() {
		super.update();
		if (!loaded) {

			panel.remove(chart);
			panel.remove(numberRangeFilter);

			Image loadingImg = getLoadingImage();
			panel.add(loadingImg);
			draw(removeLoadingImgCallback(loadingImg, panel),
					addChartCallback(panel));
			loaded = true;
		}
	}

	@Override
	protected String getPageName() {
		return "/Bonus/History";
	}
}
