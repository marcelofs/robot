package mfs.robot.client.erepublik.pages.admin;

import java.util.Date;

import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AContentsPage;
import mfs.robot.client.erepublik.rpc.RoBoTAdminRPCAsync;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

public class AdminPresidentsPage extends AContentsPage {

	private RoBoTAdminRPCAsync	adminRpcService;
	private Panel				root				= new FlowPanel();

	private TextBox				name				= new TextBox();
	private DateBox				firstDay			= new DateBox();
	private CheckBox			showFinanceTables	= new CheckBox(
															"Mostrar nas tabelas");
	private CheckBox			showFinanceGraphs	= new CheckBox(
															"Mostrar nos gráficos");

	private Button				save				= new Button("Salvar");

	public AdminPresidentsPage(RoBoTAdminRPCAsync rpcService) {
		this.adminRpcService = rpcService;

		build();
	}

	private void build() {

		root.add(new Label("Nome:"));
		root.add(name);

		root.add(new Label("Primeiro dia de mandato (dia 5 do mês):"));

		DateTimeFormat format = DateTimeFormat.getFormat("yyyy.MM.dd");
		firstDay.setFormat(new DateBox.DefaultFormat(format));
		root.add(firstDay);

		root.add(showFinanceTables);

		root.add(showFinanceGraphs);

		root.add(save);

		save.addClickHandler(new ClickHandler()
		{

			@Override
			public void onClick(ClickEvent event) {
				save.setEnabled(false);
				adminRpcService.createPresident(name.getText().trim(),
						firstDay.getValue(), showFinanceTables.getValue(),
						showFinanceGraphs.getValue(),
						new SimpleCallback<Boolean>()
						{

							@Override
							public void onSuccess(Boolean result) {
								if (result) {
									clearForm();
									save.setEnabled(true);
								}
							}
						});

			}
		});
	}

	private void clearForm() {
		name.setText("");
		firstDay.setValue(new Date());
		showFinanceTables.setValue(false);
		showFinanceGraphs.setValue(false);
	}

	@Override
	public Widget getWidget() {
		return root;
	}

	@Override
	public void update() {}

	@Override
	protected String getPageName() {
		return "/Admin/Presidents";
	}

}
