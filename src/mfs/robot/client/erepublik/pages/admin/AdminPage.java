package mfs.robot.client.erepublik.pages.admin;

import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import mfs.robot.client.erepublik.pages.AContentsPage;
import mfs.robot.client.erepublik.rpc.RoBoTAdminRPC;
import mfs.robot.client.erepublik.rpc.RoBoTAdminRPCAsync;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

public class AdminPage extends AContentsPage {

	@Override
	public Widget getWidget() {
		final TabLayoutPanel root = new TabLayoutPanel(2.3, Unit.EM);

		final Image img = getLoadingImage();
		root.add(img, "Carregando...");

		GWT.runAsync(new RunAsyncCallback()
		{

			@Override
			public void onSuccess() {
				load(root, img);
			}

			@Override
			public void onFailure(Throwable reason) {}
		});
		return root;
	}

	private void load(final TabLayoutPanel root, final Image loadingImg) {
		final RoBoTAdminRPCAsync rpcService = GWT.create(RoBoTAdminRPC.class);

		AdminUsersPage users = new AdminUsersPage(rpcService);
		AdminPresidentsPage cps = new AdminPresidentsPage(rpcService);

		setInitialInfo(users, cps);

		root.remove(loadingImg);
		root.add(users.getWidget(), "Administração de Usuários");
		root.add(cps.getWidget(), "Presidentes");

		users.update();

	}
}
