package mfs.robot.client.erepublik.pages.admin;

import mfs.robot.client.erepublik.components.LabeledTextBox;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.rpc.RoBoTAdminRPCAsync;
import mfs.robot.shared.admin.eRepUser;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class UserRow {

	private final eRepUser				user;
	private final Panel					panel;
	private final boolean				enabled;
	private final RoBoTAdminRPCAsync	rpcService;

	private TextBox						eRepId;
	private TextBox						eRepName;
	private CheckBox					isAuthorized;
	private CheckBox					isMdF;
	private Button						saveButton;

	public UserRow(eRepUser user, boolean enabled, RoBoTAdminRPCAsync rpcService) {
		this.user = user;
		this.panel = new FlowPanel();
		panel.addStyleName("admin-row");
		this.enabled = enabled;
		this.rpcService = rpcService;
		build();
	}

	private void build() {

		Label email = new Label(user.getgEmail()
				+ "   -   "
				+ DateTimeFormat.getFormat("yyyy.MM.dd, HH:mm:ss").format(
						user.getRegistered()));

		eRepId = new LabeledTextBox("eRepublik ID");
		if (user.geteRepId() != null)
			eRepId.setText(user.geteRepId());
		eRepId.setEnabled(enabled);

		eRepName = new LabeledTextBox("eRepublik Name");
		if (user.geteRepName() != null)
			eRepName.setText(user.geteRepName());
		eRepName.setEnabled(enabled);

		isAuthorized = new CheckBox("Autorizado");
		isAuthorized.setValue(user.isAuthorized());

		isMdF = new CheckBox("MdF");
		isMdF.setValue(user.getIsMdF());

		saveButton = new Button("Salvar");
		saveButton.addClickHandler(getSaveHandler());

		if (user.geteRepId() != null)
			panel.add(new Anchor(user.geteRepName(),
					"http://www.erepublik.com/en/citizen/profile/"
							+ user.geteRepId()));

		panel.add(email);
		panel.add(eRepId);
		panel.add(eRepName);
		panel.add(isAuthorized);
		panel.add(isMdF);
		panel.add(saveButton);

	}

	private ClickHandler getSaveHandler() {
		return new ClickHandler()
		{

			@Override
			public void onClick(ClickEvent event) {
				saveButton.setEnabled(false);

				user.seteRepId(eRepId.getText().trim());
				user.seteRepName(eRepName.getText().trim());
				user.setIsAuthorized(isAuthorized.getValue());
				user.setIsMdF(isMdF.getValue());

				rpcService.saveUser(user, new SimpleCallback<Void>()
				{
					@Override
					public void onSuccess(Void result) {
						saveButton.setEnabled(true);
					}
				});
			}

		};
	}

	public Widget getWidget() {
		return panel;
	}

}
