package mfs.robot.client.erepublik.pages.admin;

import java.util.Collection;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

import mfs.robot.client.erepublik.Utils;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AContentsPage;
import mfs.robot.client.erepublik.rpc.RoBoTAdminRPCAsync;
import mfs.robot.shared.admin.eRepUser;

public class AdminUsersPage extends AContentsPage {

	private RoBoTAdminRPCAsync	adminRpcService;
	private Panel				root	= new DockLayoutPanel(Unit.EM);

	public AdminUsersPage(RoBoTAdminRPCAsync rpcService) {
		this.adminRpcService = rpcService;
	}

	@Override
	public Widget getWidget() {
		return root;
	}

	@Override
	public void update() {
		root.clear();
		root.add(Utils.getLoadingImage());

		adminRpcService.getUsers(new SimpleCallback<Collection<eRepUser>>()
		{
			@Override
			public void onSuccess(Collection<eRepUser> result) {
				root.clear();
				draw(result);
			}
		});

	}

	protected void draw(Collection<eRepUser> users) {
		Panel unverifPanel = new FlowPanel();
		Panel newUsersPanel = new FlowPanel();
		Panel unauthPanel = new FlowPanel();
		Panel verifPanel = new FlowPanel();
		Panel mdfPanel = new FlowPanel();

		int unverif = 0;
		int newU = 0;
		int unauth = 0;
		int verif = 0;
		int mdf = 0;

		for (eRepUser user : users) {
			if (user.geteRepId() == null) {
				unverif++;
				unverifPanel.add(new UserRow(user, true, adminRpcService)
						.getWidget());
			} else
				if (user.isAuthorized() == null) {
					newU++;
					newUsersPanel.add(new UserRow(user, true, adminRpcService)
							.getWidget());
				} else
					if (user.isAuthorized() == false) {
						unauth++;
						unauthPanel
								.add(new UserRow(user, true, adminRpcService)
										.getWidget());
					} else {
						verif++;
						verifPanel.add(new UserRow(user, true, adminRpcService)
								.getWidget());
					}

			if (user.getIsMdF()) {
				mdf++;
				mdfPanel.add(new UserRow(user, true, adminRpcService)
						.getWidget());
			}
		}

		DisclosurePanel unverifiedUsers = new DisclosurePanel(
				"Usuários não verificados (" + unverif + ")");
		unverifiedUsers.add(unverifPanel);

		DisclosurePanel newUsers = new DisclosurePanel("Novos usuários ("
				+ newU + ")");
		newUsers.add(newUsersPanel);

		DisclosurePanel unauthorizedUsers = new DisclosurePanel(
				"Usuários não autorizados (" + unauth + ")");
		unauthorizedUsers.add(unauthPanel);

		DisclosurePanel verifiedUsers = new DisclosurePanel(
				"Usuários autorizados (" + verif + ")");
		verifiedUsers.add(verifPanel);

		DisclosurePanel mdfUsers = new DisclosurePanel("Usuários do MdF ("
				+ mdf + ")");
		mdfUsers.add(mdfPanel);

		Panel contents = new FlowPanel();
		contents.add(newUsers);
		contents.add(unverifiedUsers);
		contents.add(unauthorizedUsers);
		contents.add(verifiedUsers);
		contents.add(mdfUsers);

		ScrollPanel scroll = new ScrollPanel();
		scroll.add(contents);

		root.add(scroll);
	}

	@Override
	protected String getPageName() {
		return "/Admin/Users";
	}

}
