package mfs.robot.client.erepublik.pages;

import mfs.robot.client.erepublik.rpc.RoBoTOpenRPCAsync;

public interface IOpenPage {
	void setOpenRpc(RoBoTOpenRPCAsync openRpcService);
}
