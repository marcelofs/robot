package mfs.robot.client.erepublik.pages.bip;

import mfs.robot.client.erepublik.pages.AContentsPage;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

public class LibertyPage extends AContentsPage {

	@Override
	public Widget getWidget() {
		DockLayoutPanel root = new DockLayoutPanel(Unit.EM);

		build(root);

		return root;
	}

	private void build(DockLayoutPanel root) {

		final String url = "http://nwolf.bancoliberty.com";

		HTML html = new HTML(
				"<p>O <a target='_blank' href='"
						+ url
						+ "'>Banco Liberty</a> "
						+ "é um banco do eRepublik diferente: ao invés de colocar seu dinheiro na mão de estranhos e pagar taxas altíssimas de "
						+ "administração, você pode investir como achar melhor no site, de acordo com seu perfil de jogo!</p> "
						+ "<p>É aberto para todos os eBrasileiros, sendo facílimo de investir: "
						+ "<a target='_blank' href='"
						+ url
						+ "'>Cadastre-se aqui</a>!</p>"
						+ "<h2>Para Abrir sua Conta...</h2><p><ul> "
						+ "<li>Visite o <a target='_blank' href='"
						+ url
						+ "'>site do banco</a></li> "
						+ "<li>No menu superior, clique em \"Abra já sua conta\"</li>"
						+ "<li>Agora você já pode visualizar todas as opções de investimento e rendimentos.</li></ul>"
						+ "<h2>Para Investir...</h2><ul>"
						+ "<li>No menu 'Fazer um depósito na conta', informe quanto você deseja investir</li>"
						+ "<li>Transfira essa quantidade para a jogadora <a target='_blank' href='http://www.erepublik.com/en/citizen/profile/6753971'>Loritita</a>, no jogo</li>"
						+ "<ul><li>Você pode investir até 100.000 BRLs semanais</li></ul>"
						+ "<li> No site do banco, informe seu nome de usuário e a hora em que a transferência foi feita"
						+ "<li>Seu dinheiro do jogo será transformado em cotas do site em até 24h</li>"
						+ "<li>Você receberá um email avisando quando suas cotas estiverem disponíveis!</li>"
						+ "<li>Após receber suas cotas, você poderá escolher onde investir de acordo com seu perfil:</li>"
						+ "<ul><li>Vencimentos: Quanto mais tempo você deixar seu dinheiro investido, mais você ganha. Você pode escolher rendimentos diários, semanais, bissemanais, ou mensais.</li>"
						+ "<li>Retenção: É a taxa que você pagará se quiser tirar seu investimento do site rapidamente, sem vender o seu investimento para outro jogador. Quanto maior a taxa de retenção, maior sua 'fidelidade' ao banco e mais você lucra!</li>"
						+ "</ul>"
						+ "<li>Você também pode pegar empréstimos no banco, com taxas de juros acessíveis!</ul></p>"
						+ "<h2>Está esperando o quê? <a target='_blank' href='"
						+ url + "'>Invista agora!</a></h2>");

		html.addStyleName("welcome");

		Panel p = new FlowPanel();
		p.add(html);

		ScrollPanel scroll = new ScrollPanel();
		scroll.add(p);
		root.add(scroll);
	}

	@Override
	protected String getPageName() {
		return "/BIP/AdPage";
	}

}
