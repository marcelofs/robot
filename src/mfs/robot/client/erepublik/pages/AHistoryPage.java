package mfs.robot.client.erepublik.pages;

import java.util.Collection;

import mfs.robot.client.erepublik.DataTableFormatter;
import mfs.robot.shared.erepublik.IHistorySeries;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.googlecode.gwt.charts.client.ChartType;
import com.googlecode.gwt.charts.client.ChartWrapper;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.controls.Dashboard;
import com.googlecode.gwt.charts.client.controls.filter.ChartRangeFilter;
import com.googlecode.gwt.charts.client.controls.filter.ChartRangeFilterOptions;
import com.googlecode.gwt.charts.client.controls.filter.ChartRangeFilterState;
import com.googlecode.gwt.charts.client.controls.filter.ChartRangeFilterStateRange;
import com.googlecode.gwt.charts.client.controls.filter.ChartRangeFilterUi;
import com.googlecode.gwt.charts.client.corechart.LineChartOptions;
import com.googlecode.gwt.charts.client.options.AxisTitlesPosition;
import com.googlecode.gwt.charts.client.options.ChartArea;
import com.googlecode.gwt.charts.client.options.CoreOptions;
import com.googlecode.gwt.charts.client.options.HAxis;
import com.googlecode.gwt.charts.client.options.Legend;
import com.googlecode.gwt.charts.client.options.LegendPosition;
import com.googlecode.gwt.charts.client.options.VAxis;

public abstract class AHistoryPage extends AContentsPage {

	protected Dashboard					dashboard			= new Dashboard();
	protected ChartWrapper<CoreOptions>	chart				= new ChartWrapper<CoreOptions>();
	protected ChartRangeFilter			numberRangeFilter	= new ChartRangeFilter();

	protected boolean					loaded				= false;

	protected Integer					lastDay;

	protected AHistoryPage() {
		setChartType();
	}

	protected void setChartType() {
		chart.setChartType(getChartType());
	}

	protected ChartType getChartType() {
		return ChartType.LINE;
	}

	protected DataTable getDataTable() {
		final DataTable data = DataTable.create();

		data.addColumn(ColumnType.NUMBER, "Dia");
		data.addColumn(ColumnType.NUMBER, "Valor Médio das Ofertas");
		data.addColumn(ColumnType.NUMBER, "Maior Valor de Venda");
		data.addColumn(ColumnType.NUMBER, "Menor Valor de Venda");

		return data;
	}

	protected void fillData(DataTable data,
			Collection<? extends IHistorySeries> result) {
		data.addRows(result.size());

		int row = 0;
		for (IHistorySeries offer : result) {
			data.setValue(row, 0, offer.geteRepDay());
			if (offer.getAverage() != null && offer.getAverage() != 0)
				data.setValue(row, 1, offer.getAverage());
			if (offer.getMaximum() != null && offer.getMaximum() != 0)
				data.setValue(row, 2, offer.getMaximum());
			if (offer.getMinimum() != null && offer.getMinimum() != 0)
				data.setValue(row, 3, offer.getMinimum());
			row++;

			if (row >= result.size()) // last element
				lastDay = offer.geteRepDay();
		}

		DataTableFormatter.BRL_CENTS.format(data, 1);
		DataTableFormatter.BRL.format(data, 2, 3);
	}

	protected void setControlOptions(String title) {

		ChartRangeFilterOptions chartRangeFilterOptions = getChartRangeFilterOptions();
		CoreOptions controlChartOptions = getControlChartOptions();
		ChartArea chartArea = getChartArea();
		ChartRangeFilterUi chartRangeFilterUi = getChartRangeFilterUi();
		ChartRangeFilterState controlState = getChartRangeFilterState();
		ChartRangeFilterStateRange stateRange = getChartRangeFilterStateRange();
		CoreOptions chartOptions = getChartOptions();

		controlChartOptions.setChartArea(chartArea);

		chartRangeFilterUi.setChartOptions(controlChartOptions);
		chartRangeFilterOptions.setUi(chartRangeFilterUi);

		if (lastDay != null) {
			stateRange.setStart(lastDay - 14);
			stateRange.setEnd(lastDay);
		}

		controlState.setRange(stateRange);
		numberRangeFilter.setState(controlState);
		numberRangeFilter.setOptions(chartRangeFilterOptions);

		chartOptions.setChartArea(chartArea);
		chartOptions.setTitle("Histórico de " + title);
		chart.setOptions(chartOptions);

		dashboard.bind(numberRangeFilter, chart);

	}

	protected ChartRangeFilterOptions getChartRangeFilterOptions() {
		ChartRangeFilterOptions chartRangeFilterOptions = ChartRangeFilterOptions
				.create();
		chartRangeFilterOptions.setFilterColumnIndex(0);
		return chartRangeFilterOptions;
	}

	protected CoreOptions getControlChartOptions() {
		LineChartOptions controlChartOptions = LineChartOptions.create();
		controlChartOptions.setHeight(100);
		controlChartOptions.setBackgroundColor("#EDF4F5");
		return controlChartOptions;
	}

	protected CoreOptions getChartOptions() {
		LineChartOptions chartOptions = LineChartOptions.create();
		chartOptions.setLegend(Legend.create(LegendPosition.TOP));
		chartOptions.setBackgroundColor("#EDF4F5");
		chartOptions.setAxisTitlesPosition(AxisTitlesPosition.OUT);
		chartOptions.setInterpolateNulls(false);
		chartOptions.setVAxis(getVAxis());
		chartOptions.setHAxis(getHAxis());
		return chartOptions;
	}

	protected VAxis getVAxis() {
		return VAxis.create("Valor");
	}

	protected HAxis getHAxis() {
		return HAxis.create("Dia no eRepublik");
	}

	protected ChartArea getChartArea() {
		ChartArea chartArea = ChartArea.create();
		chartArea.setWidth("80%");
		chartArea.setHeight("70%");

		return chartArea;
	}

	protected ChartRangeFilterUi getChartRangeFilterUi() {
		ChartRangeFilterUi chartRangeFilterUi = ChartRangeFilterUi.create();
		chartRangeFilterUi.setChartType(getChartType());

		chartRangeFilterUi.setMinRangeSize(7);
		chartRangeFilterUi.setSnapToData(true);
		return chartRangeFilterUi;
	}

	protected ChartRangeFilterStateRange getChartRangeFilterStateRange() {
		return ChartRangeFilterStateRange.create();
	}

	protected ChartRangeFilterState getChartRangeFilterState() {
		return ChartRangeFilterState.create();
	}

	protected ChangeHandler getUpdateChangeHandler() {
		return new ChangeHandler()
		{

			@Override
			public void onChange(ChangeEvent event) {
				loaded = false;
				update();
			}
		};
	}

	protected Runnable addChartCallback(final DockLayoutPanel panel) {
		return new Runnable()
		{

			@Override
			public void run() {
				panel.addSouth(numberRangeFilter, 6.25);
				panel.add(chart);
			}
		};
	}

}
