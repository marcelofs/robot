package mfs.robot.client.erepublik.pages;

import java.util.Collection;

import mfs.robot.client.erepublik.DataTableFormatter;
import mfs.robot.shared.erepublik.IDailySeries;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.Panel;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.LineChart;
import com.googlecode.gwt.charts.client.corechart.LineChartOptions;
import com.googlecode.gwt.charts.client.options.HAxis;
import com.googlecode.gwt.charts.client.options.VAxis;

public abstract class ADailyPage extends AContentsPage {

	protected final LineChart	lines	= new LineChart();
	protected boolean			loaded	= false;

	protected DataTable getDataTable() {
		final DataTable data = DataTable.create();

		data.addColumn(ColumnType.NUMBER, "Horário");
		data.addColumn(ColumnType.NUMBER, "Valor Médio das Ofertas");
		data.addColumn(ColumnType.NUMBER, "Menor Valor de Venda");

		return data;
	}

	protected void fillData(Collection<? extends IDailySeries> result,
			DataTable data) {
		data.addRows(result.size());

		int row = 0;
		for (IDailySeries offer : result) {
			data.setValue(row, 0, offer.geteRepTime());
			data.setValue(row, 1, offer.getAverage());
			data.setValue(row, 2, offer.getMinimum());
			row++;
		}

		DataTableFormatter.TIME.format(data, 0);
		DataTableFormatter.BRL_CENTS.format(data, 1);
		DataTableFormatter.BRL.format(data, 2);
	}

	protected LineChartOptions createDailyLineOpts(String title) {
		LineChartOptions opts = super.createLineOpts(title);

		opts.setVAxis(getVAxis());

		HAxis hAxis = HAxis.create("Horário do servidor");
		hAxis.setMaxValue(24);
		opts.setHAxis(hAxis);
		opts.setInterpolateNulls(false);
		return opts;
	}

	protected VAxis getVAxis() {
		return VAxis.create("Valor");
	}

	protected Runnable addChartCallback(final Panel panel) {
		return new Runnable()
		{

			@Override
			public void run() {
				panel.add(lines);
			}
		};
	}

	protected ChangeHandler getUpdateChangeHandler() {
		return new ChangeHandler()
		{

			@Override
			public void onChange(ChangeEvent event) {
				loaded = false;
				update();
			}
		};
	}

}
