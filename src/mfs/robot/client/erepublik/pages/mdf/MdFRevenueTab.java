package mfs.robot.client.erepublik.pages.mdf;

import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AContentsPage;
import mfs.robot.client.erepublik.rpc.RoBoTMdFRPCAsync;
import mfs.robot.shared.mdf.MdFInitialInfo;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class MdFRevenueTab extends AContentsPage {

	private final RoBoTMdFRPCAsync	rpcService;

	private final TextBox			day					= new TextBox();
	private final Label				exists				= new Label(
																"ATENÇÃO! Os dados existentes serão sobrescritos!");
	private final Label				wrongDay			= new Label(
																"O dia ainda não passou, não existem dados de arrecadação!");

	private final Label				wrongCP				= new Label(
																"Não é possível definir a arrecadação de outros presidentes");

	private final TextBox			treasuryPreviousDay	= new TextBox();

	private final TextBox			treasuryInTheDay	= new TextBox();

	private final TextBox			donations			= new TextBox();

	private final TextBox			mpps				= new TextBox();

	private final TextBox			incomes				= new TextBox();

	private final TextBox			revenue				= new TextBox();

	private final TextBox			comments			= new TextBox();

	private final Button			save				= new Button("Salvar");

	private final MdFInitialInfo	mdfInitialInfo;

	public MdFRevenueTab(RoBoTMdFRPCAsync rpcService, MdFInitialInfo initialInfo) {
		this.rpcService = rpcService;
		this.mdfInitialInfo = initialInfo;
	}

	public Widget getWidget() {
		final Panel content = new FlowPanel();

		if (mdfInitialInfo.treasuryInTheDay == null
				|| mdfInitialInfo.treasuryInTheDay.getCalculatedTaxRevenue() == null) {
			content.add(new Label("Os dados do dia " + mdfInitialInfo.eRepDay
					+ " ainda não estão disponíveis"));
			return content;
		}

		save.addClickHandler(getSaveHandler());
		day.addBlurHandler(getDayChangedHandler(content));

		donations.addBlurHandler(getCalculateHandler());
		mpps.addBlurHandler(getCalculateHandler());
		incomes.addBlurHandler(getCalculateHandler());

		build(content);
		buildInitialInfo(content);
		exists.setVisible(mdfInitialInfo.alreadyExists);

		return content;

	}

	private void build(Panel content) {
		content.add(new Label("Dia no eRepublik:"));
		content.add(day);

		exists.addStyleName("warning");
		content.add(exists);
		exists.setVisible(false);

		wrongDay.addStyleName("warning");
		content.add(wrongDay);
		wrongDay.setVisible(false);

		wrongCP.addStyleName("warning");
		content.add(wrongCP);
		wrongCP.setVisible(false);

		content.add(new Label("Doações (BRL):"));
		content.add(donations);
		donations.setText("");
		content.add(new Label("MPPs (BRL):"));
		content.add(mpps);
		mpps.setText("");
		content.add(new Label(
				"+10000 BRL para MPPs lançadas - ainda em votação ou aprovadas;"));
		content.add(new Label("-10000 BRL para MPPs lançadas e rejeitadas."));
		content.add(new Anchor(" Verificar MPPs aqui!",
				"http://www.erepublik.com/en/country-administration/Brazil/1"));

		content.add(new Label("Outras entradas no TN:"));
		content.add(incomes);
		incomes.setText("");

		content.add(new Label("Tesouro no final do dia anterior (BRL):"));
		content.add(treasuryPreviousDay);
		treasuryPreviousDay.setReadOnly(true);
		content.add(new Label("Tesouro no final do dia (BRL):"));
		content.add(treasuryInTheDay);
		treasuryInTheDay.setReadOnly(true);
		content.add(new Label("Arrecadação total (BRL):"));
		content.add(revenue);
		revenue.setReadOnly(true);
		content.add(new Label(
				"Comentários/Eventos que podem alterar a arrecadação:"));
		content.add(comments);
		content.add(save);
	}

	private ClickHandler getSaveHandler() {
		return new ClickHandler()
		{

			@Override
			public void onClick(ClickEvent event) {
				setFormEnabled(false);

				// calculate();

				Integer dayT = Integer.valueOf(day.getText().trim());
				Double revenueValue = Double.valueOf(revenue.getText().trim());

				rpcService.saveManualRevenue(dayT.toString(),
						revenueValue.toString(), comments.getText().trim(),
						new SimpleCallback<Void>()
						{

							@Override
							public void onSuccess(Void result) {
								clearForm();
								setFormEnabled(true);
								day.setFocus(true);
							}

							@Override
							public void onFailure(Throwable caught) {
								setFormEnabled(true);
								super.onFailure(caught);
							}
						});

			}
		};
	}

	private BlurHandler getCalculateHandler() {
		return new BlurHandler()
		{
			@Override
			public void onBlur(BlurEvent event) {
				calculate();
			}
		};
	}

	private BlurHandler getDayChangedHandler(final Panel content) {
		return new BlurHandler()
		{

			@Override
			public void onBlur(BlurEvent event) {
				Integer intDay = Integer.valueOf(day.getText().trim());
				if (intDay < mdfInitialInfo.currentPresident.getFirstErepDay()
						|| intDay > mdfInitialInfo.currentPresident
								.getLastErepDay()) {
					wrongCP.setVisible(true);
					exists.setVisible(false);
					clearForm();
					day.setFocus(true);
					save.setEnabled(false);
					return;
				}

				if (intDay > mdfInitialInfo.eRepDay) {
					wrongDay.setVisible(true);
					exists.setVisible(false);
					clearForm();
					day.setFocus(true);
					save.setEnabled(false);
					return;
				} else {
					wrongDay.setVisible(false);
				}

				save.setEnabled(true);
				setFormEnabled(false);
				rpcService.getRevenueInfo(
						Integer.valueOf(day.getText().trim()),
						new SimpleCallback<MdFInitialInfo>()
						{

							@Override
							public void onSuccess(MdFInitialInfo result) {
								treasuryPreviousDay
										.setText(result.treasuryTheDayBefore
												.getCurrencyTreasury()
												.toString());
								treasuryInTheDay
										.setText(result.treasuryInTheDay
												.getCurrencyTreasury()
												.toString());
								donations.setText("");
								mpps.setText("");
								// calculate();
								revenue.setText(result.treasuryInTheDay
										.getCalculatedTaxRevenue() + "");

								exists.setVisible(result.alreadyExists);

								setFormEnabled(true);
							}
						});
			}
		};
	}

	private void setFormEnabled(boolean enabled) {
		day.setEnabled(enabled);
		donations.setEnabled(enabled);
		mpps.setEnabled(enabled);
		comments.setEnabled(enabled);
		incomes.setEnabled(enabled);

		save.setEnabled(enabled);
		if (enabled)
			save.setText("Salvar");
		else
			save.setText("Enviando...");
	}

	private void clearForm() {
		day.setText("");
		donations.setText("");
		mpps.setText("");
		incomes.setText("");
		treasuryPreviousDay.setText("");
		treasuryInTheDay.setText("");
		revenue.setText("");
		comments.setText("");
		exists.setVisible(false);
		wrongDay.setVisible(false);
	}

	private void buildInitialInfo(Panel content) {

		day.setText(mdfInitialInfo.eRepDay + "");

		treasuryPreviousDay.setText(mdfInitialInfo.treasuryTheDayBefore
				.getCurrencyTreasury().toString());
		treasuryInTheDay.setText(mdfInitialInfo.treasuryInTheDay
				.getCurrencyTreasury().toString());

		revenue.setText(mdfInitialInfo.treasuryInTheDay
				.getCalculatedTaxRevenue().toString());

		// calculate();
	}

	// ((caixa do dia + doações propostas) - caixa do dia anterior) + (mpps
	// aprovadas ou propostas pelo brasil) - (mpps reprovadas) + (novos cidadãos
	// * 5)
	private void calculate() {
		Double totalRevenue = 0d;

		totalRevenue += Double.valueOf(treasuryInTheDay.getText().trim());
		totalRevenue += Double.valueOf(donations.getText().trim());
		totalRevenue -= Double.valueOf(treasuryPreviousDay.getText().trim());
		totalRevenue += Double.valueOf(mpps.getText().trim());
		totalRevenue -= Double.valueOf(incomes.getText().trim());

		revenue.setText(totalRevenue.toString());

	}

	@Override
	protected String getPageName() {
		return "/MdF/Revenue";
	}

}
