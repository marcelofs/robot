package mfs.robot.client.erepublik.pages.mdf;

import java.util.Collection;

import mfs.robot.client.erepublik.components.DeletionHandler;
import mfs.robot.client.erepublik.components.MdFUsersSuggestBox;
import mfs.robot.client.erepublik.components.OrgSuggestBox;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AContentsPage;
import mfs.robot.client.erepublik.rpc.RoBoTMdFRPCAsync;
import mfs.robot.shared.erepublik.ITransactionWithCategory;
import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.CountryPresident;
import mfs.robot.shared.erepublik.data.ExpenseType;
import mfs.robot.shared.erepublik.data.MonetaryType;
import mfs.robot.shared.mdf.MdFInitialInfo;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class MdFExpenseTab extends AContentsPage {

	private final RoBoTMdFRPCAsync					rpcService;

	private final TextBox							day					= new TextBox();
	private final Label								wrongDay			= new Label(
																				"Não é possível lançar saídas futuras");
	private final Label								wrongCP				= new Label(
																				"Não é possível lançar saídas de outros presidentes");

	private final TextBox							value				= new TextBox();

	private final ListBox							type				= getMoneyTypes();

	private final SuggestBox						personResponsible;

	private final SuggestBox						origin;

	private final SuggestBox						destination;

	private final ListBox							cats				= getCategories();

	private final TextBox							comments			= new TextBox();

	private final Button							save				= new Button(
																				"Salvar");

	private final MdFTransactionWithCategoryTable	existingExpenses	= new MdFTransactionWithCategoryTable();

	private final int								today;

	private final CountryPresident					president;

	public MdFExpenseTab(final RoBoTMdFRPCAsync rpcService,
			MdFInitialInfo initialInfo) {
		this.rpcService = rpcService;
		today = initialInfo.eRepDay + 1;
		this.president = initialInfo.currentPresident;

		personResponsible = new MdFUsersSuggestBox(initialInfo);
		origin = new OrgSuggestBox(initialInfo);
		destination = new OrgSuggestBox(initialInfo);

		existingExpenses
				.setOnDeleteCallback(new DeletionHandler<ITransactionWithCategory>()
				{

					@Override
					public void onDelete(final ITransactionWithCategory object) {
						rpcService.deleteExpense((CountryExpense) object,
								new SimpleCallback<Boolean>()
								{

									@Override
									public void onSuccess(Boolean result) {
										refreshExpenses(object.geteRepDay());
									}
								});
					}
				});

	}

	public Widget getWidget() {

		final Panel content = new FlowPanel();

		save.addClickHandler(getSaveHandler());
		cats.addChangeHandler(getCatsChangeHandler());
		day.addBlurHandler(getDayChangedHandler(content));

		build(content);

		return new ScrollPanel(content);

	}

	public void update() {
		refreshExpenses(today);
	}

	private void build(Panel content) {
		content.add(new Label("Dia no eRepublik:"));
		content.add(day);
		day.setText(today + "");

		wrongDay.addStyleName("warning");
		content.add(wrongDay);
		wrongDay.setVisible(false);

		wrongCP.addStyleName("warning");
		content.add(wrongCP);
		wrongCP.setVisible(false);

		content.add(new Label("Valor do gasto:"));
		content.add(value);
		content.add(type);

		content.add(new Label("Pessoa responsável:"));
		content.add(personResponsible);

		content.add(new Label("Origem do dinheiro:"));
		// origin.setText("Controle Nacional");
		content.add(origin);

		content.add(new Label("Destino do dinheiro:"));
		content.add(destination);
		content.add(cats);

		content.add(new Label("Comentários:"));
		content.add(comments);
		content.add(save);

		Panel expenses = new FlowPanel();
		expenses.addStyleName("expenses");
		expenses.add(existingExpenses.getTable());
		content.add(expenses);
	}

	private ClickHandler getSaveHandler() {
		return new ClickHandler()
		{

			@Override
			public void onClick(ClickEvent event) {
				setFormEnabled(false);
				try {
					final CountryExpense exp = getCurrentExpense();
					rpcService.addExpense(exp, new SimpleCallback<Boolean>()
					{

						@Override
						public void onSuccess(Boolean result) {
							clearForm();
							setFormEnabled(true);
							value.setFocus(true);
							refreshExpenses(exp.geteRepDay());
						}

						@Override
						public void onFailure(Throwable caught) {
							setFormEnabled(true);
							super.onFailure(caught);
						}
					});
				} catch (Exception e) {
					setFormEnabled(true);
					RootPanel.get("error").add(new Label(e.getMessage()));
				}
			}
		};
	}

	private ChangeHandler getCatsChangeHandler() {
		return new ChangeHandler()
		{

			@Override
			public void onChange(ChangeEvent event) {
				if (ExpenseType.MPP.getName().equals(
						cats.getItemText(cats.getSelectedIndex()))) {
					origin.setText("Tesouro Nacional");
					value.setText("10000");
				}
				// else
				// origin.setText("Controle Nacional");

			}
		};
	}

	private CountryExpense getCurrentExpense() {
		CountryExpense e = new CountryExpense();

		e.seteRepDay(Integer.valueOf(day.getText().trim()));
		e.setValue(Double.valueOf(value.getText().trim()));
		e.setType(MonetaryType.valueOf(type
				.getItemText(type.getSelectedIndex()).toUpperCase()));
		e.setPersonResponsible(personResponsible.getText().trim());
		e.setOrigin(origin.getText().trim());
		e.setDestination(destination.getText().trim());
		e.setObservations(comments.getText().trim());
		e.setCategory(ExpenseType.fromName(cats.getItemText(cats
				.getSelectedIndex())));

		return e;
	}

	private BlurHandler getDayChangedHandler(final Panel content) {
		return new BlurHandler()
		{

			@Override
			public void onBlur(BlurEvent event) {
				Integer intDay = Integer.valueOf(day.getText().trim());
				if (intDay > today) {
					wrongDay.setVisible(true);
					day.setFocus(true);
					save.setEnabled(false);
					return;
				} else
					if (intDay < president.getFirstErepDay()
							|| intDay > president.getLastErepDay()) {
						wrongCP.setVisible(true);
						day.setFocus(true);
						save.setEnabled(false);
						return;
					} else {
						wrongDay.setVisible(false);
						wrongCP.setVisible(false);
						save.setEnabled(true);
					}

				setFormEnabled(false);
				rpcService.getExpenses(Integer.valueOf(day.getText().trim()),
						new SimpleCallback<Collection<CountryExpense>>()
						{
							@Override
							public void onSuccess(
									Collection<CountryExpense> result) {
								existingExpenses.set(result);
								setFormEnabled(true);
							}
						});
			}
		};
	}

	private void setFormEnabled(boolean enabled) {
		day.setEnabled(enabled);
		value.setEnabled(enabled);
		personResponsible.setEnabled(enabled);
		origin.setEnabled(enabled);
		destination.setEnabled(enabled);
		comments.setEnabled(enabled);
		type.setEnabled(enabled);
		cats.setEnabled(enabled);

		save.setEnabled(enabled);
		if (enabled)
			save.setText("Salvar");
		else
			save.setText("Enviando...");
	}

	private void clearForm() {
		day.setText(today + "");
		value.setText("");
		// personResponsible.setText("");

		if (ExpenseType.MPP.getName().equals(
				cats.getItemText(cats.getSelectedIndex()))) {
			origin.setText("Tesouro Nacional");
			value.setText("10000");
		}
		// else
		// origin.setText("Controle Nacional");

		// destination.setText("");
		// comments.setText("");
	}

	private void refreshExpenses(int expDay) {
		day.setText(expDay + "");
		rpcService.getExpenses(expDay,
				new SimpleCallback<Collection<CountryExpense>>()
				{
					@Override
					public void onSuccess(Collection<CountryExpense> result) {
						existingExpenses.set(result);
					}
				});
	}

	private ListBox getMoneyTypes() {
		ListBox dropdown = new ListBox(false);

		for (MonetaryType t : MonetaryType.values())
			dropdown.addItem(t.name().toLowerCase());

		return dropdown;
	}

	private ListBox getCategories() {
		ListBox dropdown = new ListBox(false);

		for (ExpenseType t : ExpenseType.values())
			if (t.isManual())
				dropdown.addItem(t.getName());

		return dropdown;
	}

	@Override
	protected String getPageName() {
		return "/MdF/Expenses";
	}
}
