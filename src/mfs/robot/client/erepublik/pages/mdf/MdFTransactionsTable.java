package mfs.robot.client.erepublik.pages.mdf;

import mfs.robot.client.erepublik.components.DeletionHandler;
import mfs.robot.client.erepublik.components.tables.TransactionTable;
import mfs.robot.shared.erepublik.ITransaction;

import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.IdentityColumn;
import com.google.gwt.user.client.Window;

public class MdFTransactionsTable extends TransactionTable {

	private DeletionHandler<ITransaction>	onDeleteCallback;

	public void setOnDeleteCallback(
			DeletionHandler<ITransaction> onDeleteCallback) {
		this.onDeleteCallback = onDeleteCallback;
	}

	public void build() {
		super.build();
		ActionCell<ITransaction> delCell = new ActionCell<ITransaction>(
				"Excluir", new ActionCell.Delegate<ITransaction>()
				{
					@Override
					public void execute(ITransaction object) {
						boolean isDelete = Window
								.confirm("Você tem certeza que deseja excluir a transação de "
										+ object.getValue()
										+ " "
										+ object.getType().name().toLowerCase()
										+ "?");
						if (isDelete && onDeleteCallback != null)
							onDeleteCallback.onDelete(object);
					}

				});
		@SuppressWarnings({ "rawtypes", "unchecked" })
		Column<ITransaction, ActionCell> delCol = new IdentityColumn(delCell);
		grid.addColumn(delCol);

	}

}
