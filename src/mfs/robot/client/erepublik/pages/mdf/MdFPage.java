package mfs.robot.client.erepublik.pages.mdf;

import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AContentsPage;
import mfs.robot.client.erepublik.rpc.RoBoTMdFRPC;
import mfs.robot.client.erepublik.rpc.RoBoTMdFRPCAsync;
import mfs.robot.shared.mdf.MdFInitialInfo;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;
import static mfs.robot.client.erepublik.Utils.*;

public class MdFPage extends AContentsPage {

	private AContentsPage	logs;
	private AContentsPage	revenue;
	private AContentsPage	expenses;
	private AContentsPage	incomes;
	private AContentsPage	transfers;

	@Override
	public Widget getWidget() {
		final TabLayoutPanel root = new TabLayoutPanel(2.3, Unit.EM);

		final Image img = getLoadingImage();
		root.add(img, "Carregando...");

		GWT.runAsync(new RunAsyncCallback()
		{

			@Override
			public void onSuccess() {
				load(root, img);
			}

			@Override
			public void onFailure(Throwable reason) {}
		});
		return root;
	}

	private void load(final TabLayoutPanel root, final Image loadingImg) {
		final RoBoTMdFRPCAsync rpcService = GWT.create(RoBoTMdFRPC.class);
		rpcService.getRevenueInitialInfo(new SimpleCallback<MdFInitialInfo>()
		{

			@Override
			public void onSuccess(MdFInitialInfo result) {

				logs = new MdFLogsTab(rpcService);
				revenue = new MdFRevenueTab(rpcService, result);
				expenses = new MdFExpenseTab(rpcService, result);
				incomes = new MdFIncomeTab(rpcService, result);
				transfers = new MdFTransferTab(rpcService, result);

				root.remove(loadingImg);

				root.add(logs.getWidget(), "Logs");
				root.add(revenue.getWidget(), "Correção da Arrecadação");
				root.add(expenses.getWidget(), "Cadastro de Saídas");
				root.add(incomes.getWidget(), "Cadastro de Entradas");
				root.add(transfers.getWidget(), "Cadastro de Transferências");

				setInitialInfo(logs, revenue, expenses, incomes, transfers);

				root.addSelectionHandler(getLoaderSelectionHandler(logs,
						revenue, expenses, incomes, transfers));

				// TODO load based on params
				logs.update();
			}
		});
	}
}
