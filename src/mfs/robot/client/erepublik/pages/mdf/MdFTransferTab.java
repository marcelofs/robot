package mfs.robot.client.erepublik.pages.mdf;

import java.util.Collection;

import mfs.robot.client.erepublik.Utils;
import mfs.robot.client.erepublik.components.DeletionHandler;
import mfs.robot.client.erepublik.components.MdFUsersSuggestBox;
import mfs.robot.client.erepublik.components.OrgSuggestBox;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AContentsPage;
import mfs.robot.client.erepublik.rpc.RoBoTMdFRPCAsync;
import mfs.robot.shared.erepublik.ITransaction;
import mfs.robot.shared.erepublik.data.CountryMoneyTransfer;
import mfs.robot.shared.erepublik.data.CountryPresident;
import mfs.robot.shared.erepublik.data.MonetaryType;
import mfs.robot.shared.mdf.MdFInitialInfo;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class MdFTransferTab extends AContentsPage {

	private final RoBoTMdFRPCAsync		rpcService;

	private final TextBox				day					= new TextBox();
	private final Label					wrongDay			= new Label(
																	"Não é possível lançar transferências futuras");

	private final Label					wrongCP				= new Label(
																	"Não é possível lançar transferências de outros presidentes");

	private final TextBox				value				= new TextBox();

	private final ListBox				type				= getMoneyTypes();

	private final SuggestBox			personResponsible;

	private final SuggestBox			origin;

	private final SuggestBox			destination;

	private final TextBox				comments			= new TextBox();

	private final Button				save				= new Button(
																	"Salvar");

	private final MdFTransactionsTable	existingTransfers	= new MdFTransactionsTable();

	private final int					today;

	private final CountryPresident		president;

	public MdFTransferTab(final RoBoTMdFRPCAsync rpcService,
			MdFInitialInfo initialInfo) {
		this.rpcService = rpcService;
		today = initialInfo.eRepDay + 1;
		this.president = initialInfo.currentPresident;

		personResponsible = new MdFUsersSuggestBox(initialInfo);
		origin = new OrgSuggestBox(initialInfo);
		destination = new OrgSuggestBox(initialInfo);

		existingTransfers
				.setOnDeleteCallback(new DeletionHandler<ITransaction>()
				{

					@Override
					public void onDelete(final ITransaction object) {
						rpcService.deleteTransfer(
								(CountryMoneyTransfer) object,
								new SimpleCallback<Boolean>()
								{

									@Override
									public void onSuccess(Boolean result) {
										refreshTransfers(object.geteRepDay());
									}
								});
					}
				});

	}

	public Widget getWidget() {

		final Panel content = new FlowPanel();

		save.addClickHandler(getSaveHandler());
		day.addBlurHandler(getDayChangedHandler(content));

		build(content);

		return new ScrollPanel(content);

	}

	public void update() {
		refreshTransfers(today);
	}

	private void build(Panel content) {
		content.add(new Label("Dia no eRepublik:"));
		content.add(day);
		day.setText(today + "");

		wrongDay.addStyleName("warning");
		content.add(wrongDay);
		wrongDay.setVisible(false);

		wrongCP.addStyleName("warning");
		content.add(wrongCP);
		wrongCP.setVisible(false);

		content.add(new Label("Valor da transferência:"));
		content.add(value);
		content.add(type);

		content.add(new Label("Pessoa responsável:"));
		content.add(personResponsible);

		content.add(new Label("Origem do dinheiro:"));
		// origin.setText("Tesouro Nacional");
		content.add(origin);

		content.add(new Label("Destino do dinheiro:"));
		// destination.setText("Controle Nacional");
		content.add(destination);

		content.add(new Label("Comentários:"));
		content.add(comments);
		content.add(save);

		Panel expenses = new FlowPanel();
		expenses.addStyleName("expenses");
		expenses.add(existingTransfers.getTable());
		content.add(expenses);
	}

	private ClickHandler getSaveHandler() {
		return new ClickHandler()
		{

			@Override
			public void onClick(ClickEvent event) {
				setFormEnabled(false);
				try {
					final CountryMoneyTransfer transfer = getCurrentTransfer();
					rpcService.addTransfer(transfer,
							new SimpleCallback<Boolean>()
							{

								@Override
								public void onSuccess(Boolean result) {
									clearForm();
									setFormEnabled(true);
									value.setFocus(true);
									refreshTransfers(transfer.geteRepDay());
								}

								@Override
								public void onFailure(Throwable caught) {
									setFormEnabled(true);
									super.onFailure(caught);
								}
							});
				} catch (Exception e) {
					setFormEnabled(true);
					Utils.firebugError(e.getMessage());
					Window.alert(e.getMessage());
				}
			}
		};
	}

	private CountryMoneyTransfer getCurrentTransfer() {
		CountryMoneyTransfer e = new CountryMoneyTransfer();

		e.seteRepDay(Integer.valueOf(day.getText().trim()));
		e.setValue(Double.valueOf(value.getText().trim()));
		e.setType(MonetaryType.valueOf(type
				.getItemText(type.getSelectedIndex()).toUpperCase()));
		e.setPersonResponsible(personResponsible.getText().trim());
		e.setOrigin(origin.getText().trim());
		e.setDestination(destination.getText().trim());
		e.setObservations(comments.getText().trim());

		return e;
	}

	private BlurHandler getDayChangedHandler(final Panel content) {
		return new BlurHandler()
		{

			@Override
			public void onBlur(BlurEvent event) {
				Integer intDay = Integer.valueOf(day.getText().trim());
				if (intDay > today) {
					wrongDay.setVisible(true);
					day.setFocus(true);
					save.setEnabled(false);
					return;
				} else
					if (intDay < president.getFirstErepDay()
							|| intDay > president.getLastErepDay()) {
						wrongCP.setVisible(true);
						day.setFocus(true);
						save.setEnabled(false);
						return;
					} else {
						wrongDay.setVisible(false);
						wrongCP.setVisible(false);
						save.setEnabled(true);
					}

				setFormEnabled(false);
				rpcService.getTransfers(Integer.valueOf(day.getText().trim()),
						new SimpleCallback<Collection<CountryMoneyTransfer>>()
						{
							@Override
							public void onSuccess(
									Collection<CountryMoneyTransfer> result) {
								existingTransfers.set(result);
								setFormEnabled(true);
							}
						});
			}
		};
	}

	private void setFormEnabled(boolean enabled) {
		day.setEnabled(enabled);
		value.setEnabled(enabled);
		// origin.setEnabled(enabled);
		// destination.setEnabled(enabled);
		personResponsible.setEnabled(enabled);
		comments.setEnabled(enabled);
		type.setEnabled(enabled);

		save.setEnabled(enabled);
		if (enabled)
			save.setText("Salvar");
		else
			save.setText("Enviando...");
	}

	private void clearForm() {
		day.setText(today + "");
		value.setText("");
		// origin.setText("");
		// destination.setText("");
		// personResponsible.setText("");
		// comments.setText("");
	}

	private void refreshTransfers(int expDay) {
		day.setText(expDay + "");
		rpcService.getTransfers(expDay,
				new SimpleCallback<Collection<CountryMoneyTransfer>>()
				{
					@Override
					public void onSuccess(
							Collection<CountryMoneyTransfer> result) {
						existingTransfers.set(result);
					}
				});
	}

	private ListBox getMoneyTypes() {
		ListBox dropdown = new ListBox(false);

		for (MonetaryType t : MonetaryType.values())
			dropdown.addItem(t.name().toLowerCase());

		return dropdown;
	}

	@Override
	protected String getPageName() {
		return "/MdF/Transfers";
	}
}
