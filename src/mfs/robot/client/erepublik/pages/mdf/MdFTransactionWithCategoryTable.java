package mfs.robot.client.erepublik.pages.mdf;

import mfs.robot.client.erepublik.components.DeletionHandler;
import mfs.robot.client.erepublik.components.tables.TransactionWithCategoryTable;
import mfs.robot.shared.erepublik.ITransactionWithCategory;

import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.IdentityColumn;
import com.google.gwt.user.client.Window;

public class MdFTransactionWithCategoryTable extends
		TransactionWithCategoryTable {

	private DeletionHandler<ITransactionWithCategory>	onDeleteCallback;

	public void setOnDeleteCallback(
			DeletionHandler<ITransactionWithCategory> onDeleteCallback) {
		this.onDeleteCallback = onDeleteCallback;
	}

	protected void build() {

		super.build();
		ActionCell<ITransactionWithCategory> delCell = new ActionCell<ITransactionWithCategory>(
				"Excluir", new ActionCell.Delegate<ITransactionWithCategory>()
				{
					@Override
					public void execute(ITransactionWithCategory object) {
						boolean isDelete = Window
								.confirm("Você tem certeza que deseja excluir o gasto de "
										+ object.getValue()
										+ " "
										+ object.getType().name().toLowerCase()
										+ "?");
						if (isDelete && onDeleteCallback != null)
							onDeleteCallback.onDelete(object);
					}

				});
		@SuppressWarnings({ "rawtypes", "unchecked" })
		Column<ITransactionWithCategory, ActionCell> delCol = new IdentityColumn(
				delCell);
		grid.addColumn(delCol);

	}
}
