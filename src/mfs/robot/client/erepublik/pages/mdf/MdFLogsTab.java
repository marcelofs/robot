package mfs.robot.client.erepublik.pages.mdf;

import java.util.Collection;

import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AContentsPage;
import mfs.robot.client.erepublik.rpc.RoBoTMdFRPCAsync;
import mfs.robot.shared.mdf.MdFLog;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

public class MdFLogsTab extends AContentsPage {

	private final RoBoTMdFRPCAsync	rpcService;

	private final Panel				panel	= new FlowPanel();

	public MdFLogsTab(RoBoTMdFRPCAsync rpcService) {
		this.rpcService = rpcService;
	}

	public void update() {

		panel.add(new Label("Carregando logs..."));

		rpcService.getTopLogs(new SimpleCallback<Collection<MdFLog>>()
		{

			@Override
			public void onSuccess(Collection<MdFLog> result) {

				panel.clear();

				DateTimeFormat formatter = DateTimeFormat
						.getFormat("yyyy.MM.dd, HH:mm:ss");

				if (result == null || result.isEmpty()) {
					panel.add(new Label("Não existem logs disponíveis"));
					return;
				}

				for (MdFLog log : result) {
					String date = formatter.format(log.getRecordedOn());
					panel.add(new Label(date + " - " + log.getUserName()
							+ " - " + log.getMessage()));
				}
			}
		});
	}

	@Override
	public Widget getWidget() {
		return panel;
	}

	@Override
	protected String getPageName() {
		return "/MdF/Logs";
	}

}
