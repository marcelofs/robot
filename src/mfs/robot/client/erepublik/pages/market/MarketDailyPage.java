package mfs.robot.client.erepublik.pages.market;

import static mfs.robot.client.erepublik.Utils.addChangeHandler;
import static mfs.robot.client.erepublik.Utils.addStyle;
import static mfs.robot.client.erepublik.Utils.addStyles;
import static mfs.robot.client.erepublik.Utils.disable;
import static mfs.robot.client.erepublik.Utils.enableWidgetsCallback;
import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import static mfs.robot.client.erepublik.Utils.removeLoadingImgCallback;

import java.util.Collection;

import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.ADailyPage;
import mfs.robot.shared.erepublik.data.MarketplaceOffer;
import mfs.robot.shared.erepublik.data.ProductQuality;
import mfs.robot.shared.erepublik.data.ProductType;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.DataTable;

public class MarketDailyPage extends ADailyPage {

	private DockLayoutPanel	panel	= new DockLayoutPanel(Unit.EM);

	private final ListBox	type	= new ListBox();
	private final ListBox	quality	= new ListBox();
	private final ListBox	day		= new ListBox();

	@Override
	public Widget getWidget() {
		panel.addNorth(getHeader(), 2.5);
		panel.add(lines);
		return panel;
	}

	private Panel getHeader() {
		Panel header = new FlowPanel();
		addStyles(header, "auto-margin", "graph-header");

		addStyle("graph-chooser", type, quality, day);

		type.addItem("Weapon");
		type.addItem("Food");

		Integer someDay = initialInfo.eRepDay - 7;
		for (int i = 0; i <= 7; i++, someDay++)
			day.addItem(someDay + "");

		for (ProductQuality q : ProductQuality.values())
			quality.addItem(q.name());

		header.add(type);
		header.add(quality);
		header.add(day);

		type.setSelectedIndex(0);
		quality.setSelectedIndex(6);
		day.setSelectedIndex(7);

		addChangeHandler(getUpdateChangeHandler(), type, quality, day);

		return header;
	}

	protected void draw(final ProductType type, final ProductQuality quality,
			final Integer day, final Runnable... callbacks) {

		final String name = type.name().toLowerCase() + " "
				+ quality.name().toLowerCase();

		final DataTable data = getDataTable();

		rpcService.getDailyMarketplaceOffer(type, quality, day,
				new SimpleCallback<Collection<MarketplaceOffer>>()
				{
					@Override
					public void onSuccess(Collection<MarketplaceOffer> result) {
						fillData(result, data);

						lines.draw(data, createDailyLineOpts("Valores de "
								+ name + " no dia " + day));

						for (Runnable r : callbacks)
							r.run();

						recordAnalyticsHit(day, type, quality);
					}
				});
	}

	protected void recordAnalyticsHit(Integer day, ProductType type,
			ProductQuality quality) {

		String dayTxt = "Unknown";

		if (initialInfo.eRepDay.equals(day))
			dayTxt = "Today";
		else
			dayTxt = "" + (day - initialInfo.eRepDay);

		recordAnalyticsHit("/Market/Daily/" + type.name() + "/"
				+ quality.name() + "/" + dayTxt);
	}

	@Override
	public void update() {
		super.update();
		if (!loaded) {

			disable(type, quality, day);

			ProductType pType = ProductType.valueOf(type.getItemText(
					type.getSelectedIndex()).toUpperCase());
			ProductQuality pQuality = ProductQuality.valueOf(quality
					.getItemText(quality.getSelectedIndex()).toUpperCase());
			Integer pDay = Integer.valueOf(day.getItemText(day
					.getSelectedIndex()));

			panel.remove(lines);

			Image loadingImg = getLoadingImage();
			panel.add(loadingImg);

			draw(pType, pQuality, pDay,
					removeLoadingImgCallback(loadingImg, panel),
					addChartCallback(panel),
					enableWidgetsCallback(day, quality, type));

			loaded = true;
		}

	}

	@Override
	protected String getPageName() {
		return "/Market/Daily";
	}
}
