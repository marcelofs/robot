package mfs.robot.client.erepublik.pages.market;

import mfs.robot.client.erepublik.pages.AContentsPage;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;
import static mfs.robot.client.erepublik.Utils.*;

public class MarketPage extends AContentsPage {

	private MarketDailyPage		daily;
	private MarketHistoryPage	history;

	@Override
	public Widget getWidget() {
		TabLayoutPanel root = new TabLayoutPanel(2.3, Unit.EM);

		daily = new MarketDailyPage();
		history = new MarketHistoryPage();

		setInitialInfo(daily, history);

		root.add(daily.getWidget(), "Diário");
		root.add(history.getWidget(), "Histórico");

		root.addSelectionHandler(getLoaderSelectionHandler(daily, history));

		// TODO load based on params
		daily.update();

		return root;
	}
}
