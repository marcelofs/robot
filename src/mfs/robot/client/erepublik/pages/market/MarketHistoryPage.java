package mfs.robot.client.erepublik.pages.market;

import java.util.Collection;

import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AHistoryPage;
import mfs.robot.shared.erepublik.data.AgregatedMarketplaceOffer;
import mfs.robot.shared.erepublik.data.ProductQuality;
import mfs.robot.shared.erepublik.data.ProductType;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.DataTable;
import static mfs.robot.client.erepublik.Utils.*;

public class MarketHistoryPage extends AHistoryPage {

	private DockLayoutPanel	panel	= new DockLayoutPanel(Unit.EM);

	private final ListBox	type	= new ListBox();
	private final ListBox	quality	= new ListBox();

	@Override
	public Widget getWidget() {

		panel.addNorth(dashboard, 0);
		panel.addNorth(getHeader(), 3);
		panel.addSouth(numberRangeFilter, 6.25);
		panel.add(chart);

		return panel;
	}

	private Panel getHeader() {
		Panel header = new FlowPanel();
		addStyles(header, "auto-margin", "graph-header");

		addStyle("graph-chooser", type, quality);

		type.addItem("Weapon");
		type.addItem("Food");

		for (ProductQuality q : ProductQuality.values())
			quality.addItem(q.name());

		header.add(type);
		header.add(quality);

		type.setSelectedIndex(0);
		quality.setSelectedIndex(6);

		addChangeHandler(getUpdateChangeHandler(), type, quality);

		return header;
	}

	protected void draw(final ProductType type, final ProductQuality quality,
			final Runnable... callbacks) {

		final DataTable data = getDataTable();

		final String name = type.name().toLowerCase() + " "
				+ quality.name().toLowerCase();

		rpcService.getAgregatedMarketplaceOffer(type, quality,
				new SimpleCallback<Collection<AgregatedMarketplaceOffer>>()
				{
					@Override
					public void onSuccess(
							Collection<AgregatedMarketplaceOffer> result) {
						fillData(data, result);

						setControlOptions(name);
						dashboard.draw(data);

						for (Runnable r : callbacks)
							r.run();

						recordAnalyticsHit("/Market/History/" + type.name()
								+ "/" + quality.name());
					}
				});
	}

	@Override
	public void update() {
		super.update();
		if (!loaded) {

			disable(type, quality);

			ProductType pType = ProductType.valueOf(type.getItemText(
					type.getSelectedIndex()).toUpperCase());
			ProductQuality pQuality = ProductQuality.valueOf(quality
					.getItemText(quality.getSelectedIndex()).toUpperCase());

			panel.remove(chart);
			panel.remove(numberRangeFilter);

			Image loadingImg = getLoadingImage();
			panel.add(loadingImg);
			draw(pType, pQuality, removeLoadingImgCallback(loadingImg, panel),
					addChartCallback(panel),
					enableWidgetsCallback(type, quality));
			loaded = true;
		}
	}

	@Override
	protected String getPageName() {
		return "/Market/History";
	}
}
