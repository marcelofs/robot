package mfs.robot.client.erepublik.pages;

import java.util.HashMap;
import java.util.Map;

import mfs.robot.client.erepublik.rpc.RoBoTRPCAsync;
import mfs.robot.shared.erepublik.InitialInfo;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.googlecode.gwt.charts.client.corechart.ColumnChartOptions;
import com.googlecode.gwt.charts.client.corechart.ComboChartOptions;
import com.googlecode.gwt.charts.client.corechart.LineChartOptions;
import com.googlecode.gwt.charts.client.options.HAxis;
import com.googlecode.gwt.charts.client.options.VAxis;

public abstract class AContentsPage implements IPage {

	protected Map<String, String>	urlParams	= new HashMap<String, String>();
	protected RoBoTRPCAsync			rpcService;
	protected InitialInfo			initialInfo;

	public AContentsPage setUrlParams(Map<String, String> urlParams) {
		if (urlParams != null)
			this.urlParams.putAll(urlParams);
		return this;
	}

	public AContentsPage setRpcService(RoBoTRPCAsync rpcService) {
		this.rpcService = rpcService;
		return this;
	}

	public AContentsPage setInitialInfo(InitialInfo initialInfo) {
		this.initialInfo = initialInfo;
		return this;
	}

	@Override
	public void update() {
		if (getPageName() != null)
			recordAnalyticsHit(getPageName());
	}

	protected ChangeHandler getUpdateChangeHandler() {
		return new ChangeHandler()
		{

			@Override
			public void onChange(ChangeEvent event) {
				update();
			}
		};
	}

	/**
	 * Override to record analytics
	 * 
	 * @return The page name to be used on the analytics hits
	 */
	protected String getPageName() {
		return null;
	}

	protected native void recordAnalyticsHit(String pageName) /*-{
		$wnd._gaq.push([ '_trackPageview', pageName ]);
	}-*/;

	// TODO this does not belong here
	protected LineChartOptions createLineOpts(String title) {
		LineChartOptions opts = LineChartOptions.create();
		opts.setTitle(title);
		// TODO test with nulls
		opts.setInterpolateNulls(false);
		opts.setBackgroundColor("#EDF4F5");
		return opts;
	}

	protected ColumnChartOptions createColumnOpts(String title) {
		ColumnChartOptions opts = ColumnChartOptions.create();
		opts.setTitle(title);
		opts.setBackgroundColor("#EDF4F5");
		return opts;
	}

	protected ComboChartOptions createComboOpts(String title) {
		ComboChartOptions opts = ComboChartOptions.create();
		opts.setTitle(title);
		opts.setInterpolateNulls(false);
		opts.setBackgroundColor("#EDF4F5");
		return opts;
	}

	protected LineChartOptions createLineOpts(String title, String vName,
			String hName) {
		LineChartOptions opts = createLineOpts(title);

		opts.setVAxis(VAxis.create(vName));
		opts.setHAxis(HAxis.create(hName));

		return opts;
	}

	protected void setInitialInfo(AContentsPage... pages) {
		for (AContentsPage page : pages)
			page.setInitialInfo(initialInfo).setRpcService(rpcService)
					.setUrlParams(urlParams);
	}
}
