package mfs.robot.client.erepublik.pages.expenses.cashflow;

import static mfs.robot.client.erepublik.Utils.addStyles;
import static mfs.robot.client.erepublik.Utils.getLoadingImage;

import java.util.Collection;

import mfs.robot.client.erepublik.Formatters;
import mfs.robot.client.erepublik.components.TransactionDetailsTablePopup;

import com.google.gwt.cell.client.ClickableTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.text.shared.AbstractSafeHtmlRenderer;
import com.google.gwt.text.shared.SafeHtmlRenderer;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.view.client.ListDataProvider;

public class CashFlowTable {

	private final DataGrid<CashFlowRow>			grid			= new DataGrid<CashFlowRow>();
	private final ListDataProvider<CashFlowRow>	dataProvider	= new ListDataProvider<CashFlowRow>();
	private final TransactionDetailsTablePopup	details			= new TransactionDetailsTablePopup();

	private static final NumberFormat			MONETARY_FORMAT	= Formatters.CURRENCY;

	private static final NumberFormat			DAY_FORMAT		= Formatters.DAY;

	public CashFlowTable() {

		grid.setHeaderBuilder(new CustomHeaderBuilder(grid));
		grid.setFooterBuilder(new CustomFooterBuilder(grid));
		grid.setLoadingIndicator(getLoadingImage());
		grid.setEmptyTableWidget(new Label("Não existem dados disponíveis ):"));
		addStyles(grid, "expense-grid", "auto-0margin");

		build();

		dataProvider.addDataDisplay(grid);
	}

	private void build() {
		SafeHtmlRenderer<String> anchorRenderer = new AbstractSafeHtmlRenderer<String>()
		{
			@Override
			public SafeHtml render(String object) {
				SafeHtmlBuilder sb = new SafeHtmlBuilder();
				sb.appendHtmlConstant("<a href=\"javascript:;\">")
						.appendEscaped(object).appendHtmlConstant("</a>");
				return sb.toSafeHtml();
			}
		};

		Column<CashFlowRow, Number> dayCol = new Column<CashFlowRow, Number>(
				new NumberCell(DAY_FORMAT))
		{

			@Override
			public Integer getValue(CashFlowRow object) {
				return object.getDay();
			}
		};

		grid.addColumn(dayCol, "Dia");
		grid.setColumnWidth(dayCol, 5, Unit.EM);

		Column<CashFlowRow, Number> revenueCol = new Column<CashFlowRow, Number>(
				new NumberCell(MONETARY_FORMAT))
		{

			@Override
			public Double getValue(CashFlowRow object) {
				return object.getTaxRevenue();
			}
		};
		revenueCol.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

		grid.addColumn(revenueCol, "BRL");

		Column<CashFlowRow, Number> revenueGoldCol = new Column<CashFlowRow, Number>(
				new NumberCell(MONETARY_FORMAT))
		{

			@Override
			public Double getValue(CashFlowRow object) {
				if (object.getTaxRevenueG() == null
						|| object.getTaxRevenueG() == 0)
					return null;

				return object.getTaxRevenueG();
			}
		};
		revenueGoldCol
				.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		grid.addColumn(revenueGoldCol, "Gold");
		grid.setColumnWidth(revenueGoldCol, 6, Unit.EM);

		Column<CashFlowRow, Number> treasuryCol = new Column<CashFlowRow, Number>(
				new NumberCell(MONETARY_FORMAT))
		{

			@Override
			public Double getValue(CashFlowRow object) {
				return object.getTreasury();
			}
		};
		treasuryCol.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		grid.addColumn(treasuryCol, "BRL");

		Column<CashFlowRow, Number> treasuryGCol = new Column<CashFlowRow, Number>(
				new NumberCell(MONETARY_FORMAT))
		{

			@Override
			public Double getValue(CashFlowRow object) {
				return object.getGTreasury();
			}
		};
		treasuryGCol.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		grid.addColumn(treasuryGCol, "Gold");
		grid.setColumnWidth(treasuryGCol, 6, Unit.EM);

		Column<CashFlowRow, String> transfersCol = new Column<CashFlowRow, String>(
				new ClickableTextCell(anchorRenderer))
		{

			@Override
			public String getValue(CashFlowRow object) {
				if (object.getTransfersValue() != null)
					return MONETARY_FORMAT.format(object.getTransfersValue());
				return null;
			}
		};
		transfersCol.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

		transfersCol.setFieldUpdater(new FieldUpdater<CashFlowRow, String>()
		{

			@Override
			public void update(int index, CashFlowRow object, String value) {
				details.loadTransfers(object.getTransfers());
			}
		});
		grid.addColumn(transfersCol, "BRL");

		Column<CashFlowRow, String> transfersGCol = new Column<CashFlowRow, String>(
				new ClickableTextCell(anchorRenderer))
		{

			@Override
			public String getValue(CashFlowRow object) {
				if (object.getTransfersGValue() != null)
					return MONETARY_FORMAT.format(object.getTransfersGValue());
				return null;
			}
		};
		transfersGCol
				.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

		transfersGCol.setFieldUpdater(new FieldUpdater<CashFlowRow, String>()
		{

			@Override
			public void update(int index, CashFlowRow object, String value) {
				details.loadTransfers(object.getTransfers());
			}
		});
		grid.addColumn(transfersGCol, "Gold");
		grid.setColumnWidth(transfersGCol, 6, Unit.EM);

		Column<CashFlowRow, String> incomesCCCol = new Column<CashFlowRow, String>(
				new ClickableTextCell(anchorRenderer))
		{

			@Override
			public String getValue(CashFlowRow object) {
				if (object.getIncomesCCValue() != null)
					return MONETARY_FORMAT.format(object.getIncomesCCValue());
				return null;
			}
		};
		incomesCCCol.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

		incomesCCCol.setFieldUpdater(new FieldUpdater<CashFlowRow, String>()
		{

			@Override
			public void update(int index, CashFlowRow object, String value) {
				details.loadIncomes(object.getIncomes());
			}
		});
		grid.addColumn(incomesCCCol, "BRL");

		Column<CashFlowRow, String> incomesGCol = new Column<CashFlowRow, String>(
				new ClickableTextCell(anchorRenderer))
		{

			@Override
			public String getValue(CashFlowRow object) {
				if (object.getIncomesGValue() != null)
					return MONETARY_FORMAT.format(object.getIncomesGValue());
				return null;
			}
		};
		incomesGCol.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

		incomesGCol.setFieldUpdater(new FieldUpdater<CashFlowRow, String>()
		{

			@Override
			public void update(int index, CashFlowRow object, String value) {
				details.loadIncomes(object.getIncomes());
			}
		});
		grid.addColumn(incomesGCol, "Gold");
		grid.setColumnWidth(incomesGCol, 6, Unit.EM);

		// Column<CashFlowRow, Number> nationalOrgCol = new Column<CashFlowRow,
		// Number>(
		// new NumberCell(MONETARY_FORMAT))
		// {
		//
		// @Override
		// public Double getValue(CashFlowRow object) {
		// return object.getCn();
		// }
		// };
		// nationalOrgCol
		// .setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		// grid.addColumn(nationalOrgCol, "BRL");
		//
		// Column<CashFlowRow, Number> nationalOrgGCol = new Column<CashFlowRow,
		// Number>(
		// new NumberCell(MONETARY_FORMAT))
		// {
		//
		// @Override
		// public Double getValue(CashFlowRow object) {
		// return object.getCnG();
		// }
		// };
		// nationalOrgGCol
		// .setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		// grid.addColumn(nationalOrgGCol, "Gold");
		// grid.setColumnWidth(nationalOrgGCol, 6, Unit.EM);

		Column<CashFlowRow, String> expensesCCCol = new Column<CashFlowRow, String>(
				new ClickableTextCell(anchorRenderer))
		{

			@Override
			public String getValue(CashFlowRow object) {
				if (object.getExpensesCCValue() != null)
					return MONETARY_FORMAT.format(object.getExpensesCCValue());
				return null;
			}
		};
		expensesCCCol
				.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		expensesCCCol.setFieldUpdater(new FieldUpdater<CashFlowRow, String>()
		{

			@Override
			public void update(int index, CashFlowRow object, String value) {
				details.loadExpenses(object.getExpenses());
			}
		});
		grid.addColumn(expensesCCCol, "BRL");

		Column<CashFlowRow, String> expensesGCol = new Column<CashFlowRow, String>(
				new ClickableTextCell(anchorRenderer))
		{

			@Override
			public String getValue(CashFlowRow object) {
				if (object.getExpensesGValue() != null)
					return MONETARY_FORMAT.format(object.getExpensesGValue());
				return null;
			}
		};
		expensesGCol.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

		expensesGCol.setFieldUpdater(new FieldUpdater<CashFlowRow, String>()
		{

			@Override
			public void update(int index, CashFlowRow object, String value) {
				details.loadExpenses(object.getExpenses());
			}
		});
		grid.addColumn(expensesGCol, "Gold");
		grid.setColumnWidth(expensesGCol, 6, Unit.EM);
	}

	public void set(Collection<CashFlowRow> data) {
		dataProvider.getList().clear();
		if (data != null)
			dataProvider.getList().addAll(data);
	}

	public DataGrid<CashFlowRow> getTable() {
		return grid;
	}

}
