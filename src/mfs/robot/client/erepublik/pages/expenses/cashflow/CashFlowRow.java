package mfs.robot.client.erepublik.pages.expenses.cashflow;

import java.util.Collection;

import mfs.robot.shared.erepublik.data.CitizenAccount;
import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.CountryIncome;
import mfs.robot.shared.erepublik.data.CountryMoneyTransfer;
import mfs.robot.shared.erepublik.data.CountryTreasury;
import mfs.robot.shared.erepublik.data.MonetaryType;

public class CashFlowRow {

	private Integer								day;

	private CountryTreasury						treasury;

	private Collection<CountryMoneyTransfer>	transfers;

	private Collection<CountryIncome>			incomes;

	private CitizenAccount						cn;

	private Collection<CountryExpense>			expenses;

	private boolean								showTaxRevenue;

	private boolean								showNationalOrg;

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Double getTaxRevenue() {
		if (treasury == null || !showTaxRevenue)
			return null;
		return treasury.getTaxRevenue();
	}

	public Double getTaxRevenueG() {
		if (treasury == null || !showTaxRevenue)
			return null;
		return treasury.getGoldTaxRevenue();
	}

	public Double getTreasury() {
		if (treasury == null || !showTaxRevenue)
			return null;
		return treasury.getCurrencyTreasury();
	}

	public Double getGTreasury() {
		if (treasury == null || !showTaxRevenue)
			return null;
		return treasury.getGoldTreasury();
	}

	public void setTreasury(CountryTreasury treasury, boolean showTaxRevenue) {
		this.treasury = treasury;
		this.showTaxRevenue = showTaxRevenue;
	}

	public Double getTransfersValue() {
		if (transfers != null && !transfers.isEmpty()) {
			double value = 0;
			for (CountryMoneyTransfer t : transfers) {
				if (MonetaryType.CURRENCY.equals(t.getType())) {
					value += t.getValue();
				}

			}
			if (value != 0)
				return value;
		}
		return null;
	}

	public Double getTransfersGValue() {
		if (transfers != null && !transfers.isEmpty()) {
			double value = 0;
			for (CountryMoneyTransfer t : transfers) {
				if (MonetaryType.GOLD.equals(t.getType())) {
					value += t.getValue();
				}
			}
			if (value != 0)
				return value;
		}
		return null;
	}

	public Collection<CountryMoneyTransfer> getTransfers() {
		return transfers;
	}

	public void setTransfers(Collection<CountryMoneyTransfer> transfers) {
		this.transfers = transfers;
	}

	public Double getIncomesCCValue() {
		if (incomes != null && !incomes.isEmpty()) {
			double value = 0;
			for (CountryIncome t : incomes)
				if (MonetaryType.CURRENCY.equals(t.getType()))
					value += t.getValue();
			if (value != 0)
				return value;
		}
		return null;
	}

	public Double getIncomesGValue() {
		if (incomes != null && !incomes.isEmpty()) {
			double value = 0;
			for (CountryIncome t : incomes)
				if (MonetaryType.GOLD.equals(t.getType()))
					value += t.getValue();
			if (value != 0)
				return value;
		}
		return null;
	}

	public Collection<CountryIncome> getIncomes() {
		return incomes;
	}

	public void setIncomes(Collection<CountryIncome> incomes) {
		this.incomes = incomes;
	}

	public Double getCn() {
		if (cn == null || !showNationalOrg)
			return null;
		return cn.getCurrency();
	}

	public Double getCnG() {
		if (cn == null || !showNationalOrg)
			return null;
		return cn.getGold();
	}

	public void setCn(CitizenAccount citizenAccount, boolean showNationalOrg) {
		this.cn = citizenAccount;
		this.showNationalOrg = showNationalOrg;
	}

	public Double getExpensesCCValue() {
		if (expenses != null && !expenses.isEmpty()) {
			double value = 0;
			for (CountryExpense t : expenses)
				if (MonetaryType.CURRENCY.equals(t.getType()))
					value += t.getValue();
			if (value != 0)
				return value;
		}
		return null;
	}

	public Double getExpensesGValue() {
		if (expenses != null && !expenses.isEmpty()) {
			double value = 0;
			for (CountryExpense t : expenses)
				if (MonetaryType.GOLD.equals(t.getType()))
					value += t.getValue();
			if (value != 0)
				return value;
		}
		return null;
	}

	public Collection<CountryExpense> getExpenses() {
		return expenses;
	}

	public void setExpenses(Collection<CountryExpense> expenses) {
		this.expenses = expenses;
	}

}
