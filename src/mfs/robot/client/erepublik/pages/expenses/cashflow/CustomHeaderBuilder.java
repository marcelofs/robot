package mfs.robot.client.erepublik.pages.expenses.cashflow;

import com.google.gwt.dom.builder.shared.TableCellBuilder;
import com.google.gwt.dom.builder.shared.TableRowBuilder;
import com.google.gwt.user.cellview.client.AbstractCellTable;
import com.google.gwt.user.cellview.client.DefaultHeaderOrFooterBuilder;

public class CustomHeaderBuilder extends
		DefaultHeaderOrFooterBuilder<CashFlowRow> {

	public CustomHeaderBuilder(AbstractCellTable<CashFlowRow> table) {
		super(table, false);

	}

	@Override
	protected boolean buildHeaderOrFooterImpl() {

		TableRowBuilder tr = startRow();
		buildFirstLineHeader(tr, 1, "");
		buildFirstLineHeader(tr, 2, "Arrecadação");
		buildFirstLineHeader(tr, 2, "Tesouro Nacional");
		buildFirstLineHeader(tr, 2, "Transferências");
		buildFirstLineHeader(tr, 2, "Entradas");
//		buildFirstLineHeader(tr, 2, "Controle Nacional");
		buildFirstLineHeader(tr, 2, "Saídas");
		tr.endTR();

		super.buildHeaderOrFooterImpl();

		return true;
	}

	private void buildFirstLineHeader(TableRowBuilder tr, int colSpan,
			String title) {
		TableCellBuilder th = tr.startTH().colSpan(colSpan);
		// th.style().trustedProperty("border-right", "10px solid white")
		// .endStyle();
		th.text(title).endTH();
	}

}
