package mfs.robot.client.erepublik.pages.expenses.cashflow;

import static mfs.robot.client.erepublik.Utils.addChangeHandler;
import static mfs.robot.client.erepublik.Utils.addStyle;
import static mfs.robot.client.erepublik.Utils.addStyles;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mfs.robot.client.erepublik.GraphDataOrganizer;
import mfs.robot.client.erepublik.Utils;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AContentsPage;
import mfs.robot.client.erepublik.pages.IOpenPage;
import mfs.robot.client.erepublik.rpc.RoBoTOpenRPCAsync;
import mfs.robot.shared.erepublik.ExpensesInitialInfo;
import mfs.robot.shared.erepublik.data.CitizenAccount;
import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.CountryIncome;
import mfs.robot.shared.erepublik.data.CountryMoneyTransfer;
import mfs.robot.shared.erepublik.data.CountryPresident;
import mfs.robot.shared.erepublik.data.CountryTreasury;
import mfs.robot.shared.erepublik.dto.CashFlowDTO;

import com.google.common.collect.Multimap;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

public class CashFlowPage extends AContentsPage implements IOpenPage {

	private DockLayoutPanel							panel		= new DockLayoutPanel(
																		Unit.EM);

	private CashFlowTable							table		= new CashFlowTable();

	private final ListBox							president	= new ListBox();
	private CountryPresident						selectedCP;

	private final ExpensesInitialInfo				expensesInitialInfo;

	private Widget									loading		= Utils.getLoadingImage();

	protected RoBoTOpenRPCAsync						openRpcService;

	private Multimap<Integer, CountryExpense>		expenses;
	private Multimap<Integer, CountryIncome>		incomes;
	private Multimap<Integer, CountryMoneyTransfer>	transfers;
	private Map<Integer, CitizenAccount>			countryOrg;
	private Map<Integer, CountryTreasury>			treasuries;

	public CashFlowPage(ExpensesInitialInfo result) {
		this.expensesInitialInfo = result;
	}

	@Override
	public void setOpenRpc(RoBoTOpenRPCAsync openRpcService) {
		this.openRpcService = openRpcService;
	}

	public Widget getWidget() {
		panel.addNorth(getHeader(), 2.5);
		panel.add(loading);
		return panel;
	}

	private Panel getHeader() {
		Panel header = new FlowPanel();
		addStyles(header, "auto-margin", "graph-header");

		addStyle("graph-chooser", president);

		for (CountryPresident cp : expensesInitialInfo.presidents)
			if (cp.getShowFinanceTables())
				president.addItem(cp.getName(), cp.getId() + "");

		header.add(president);

		president.setSelectedIndex(0);

		addChangeHandler(getUpdateChangeHandler(), president);

		return header;
	}

	@Override
	public void update() {
		super.update();

		final String newCPId = president.getValue(president.getSelectedIndex());

		CountryPresident newCP = null;
		for (CountryPresident cp : expensesInitialInfo.presidents)
			if (cp.getId().equals(Long.valueOf(newCPId)))
				newCP = cp;

		final CountryPresident temp = newCP;

		if (selectedCP == null || !newCP.getId().equals(selectedCP.getId()))
			openRpcService.getCashFlowData(newCP,
					new SimpleCallback<CashFlowDTO>()
					{

						@Override
						public void onSuccess(CashFlowDTO result) {
							selectedCP = temp;
							organizeData(result);
							draw();
						}
					});
		else {
			table.set(null);
			draw();
		}
	}

	private void organizeData(CashFlowDTO allData) {
		GraphDataOrganizer organizer = new GraphDataOrganizer();

		this.expenses = organizer.mapOfExpenses(allData.expenses);
		this.incomes = organizer.mapOfIncomes(allData.incomes);
		this.transfers = organizer.mapOfTransfers(allData.transfers);
		this.countryOrg = organizer.mapOfCitizenAccount(allData.countryOrg);
		this.treasuries = organizer.mapOfTreasuries(allData.treasuries);
	}

	private void draw() {
		CountryPresident cp = getSelectedCP();
		List<CashFlowRow> adapters = new ArrayList<CashFlowRow>();
		for (int i = cp.getFirstErepDay(); i <= cp.getLastErepDay(); i++) {
			CashFlowRow row = new CashFlowRow();
			row.setDay(i);
			if (i > cp.getFirstErepDay())
				row.setExpenses(expenses.get(i));

			row.setIncomes(incomes.get(i));
			row.setTransfers(transfers.get(i));
			row.setCn(countryOrg.get(i), i != cp.getFirstErepDay());
			row.setTreasury(treasuries.get(i), i < cp.getLastErepDay());

			adapters.add(row);
		}
		table.set(adapters);
		panel.remove(loading);
		panel.add(table.getTable());
	}

	private CountryPresident getSelectedCP() {
		String cp = president.getItemText(president.getSelectedIndex());

		for (CountryPresident p : expensesInitialInfo.presidents)
			if (p.getName().equalsIgnoreCase(cp))
				return p;

		return null;
	}

	@Override
	protected String getPageName() {
		return "/Expenses/CashFlow/"
				+ president.getItemText(president.getSelectedIndex());
	}

}
