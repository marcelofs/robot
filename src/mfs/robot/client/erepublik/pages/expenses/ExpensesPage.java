package mfs.robot.client.erepublik.pages.expenses;

import static mfs.robot.client.erepublik.Utils.getLoaderSelectionHandler;
import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AContentsPage;
import mfs.robot.client.erepublik.pages.IOpenPage;
import mfs.robot.client.erepublik.pages.expenses.cashflow.CashFlowPage;
import mfs.robot.client.erepublik.rpc.RoBoTOpenRPCAsync;
import mfs.robot.shared.erepublik.ExpensesInitialInfo;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

public class ExpensesPage extends AContentsPage implements IOpenPage {

	private ExpensesHistoryPage			history;
	private CashFlowPage				cashFlow;
	private ExpensesDistributionPage	distribution;
	private BalancePage					balance;

	private TabLayoutPanel				root	= new TabLayoutPanel(2.3,
														Unit.EM);
	protected RoBoTOpenRPCAsync			openRpcService;

	@Override
	public void setOpenRpc(RoBoTOpenRPCAsync openRpcService) {
		this.openRpcService = openRpcService;
	}

	@Override
	public Widget getWidget() {

		final Image img = getLoadingImage();
		root.add(img, "Carregando...");

		load(img);

		return root;
	}

	private void load(final Image loadingImg) {
		openRpcService
				.getExpensesInitialInfo(new SimpleCallback<ExpensesInitialInfo>()
				{

					@Override
					public void onSuccess(ExpensesInitialInfo result) {

						root.remove(loadingImg);

						// history = new ExpensesHistoryPage();
						cashFlow = new CashFlowPage(result);
						distribution = new ExpensesDistributionPage(result);
						balance = new BalancePage(result);

						setInitialInfo(/* history, */cashFlow, distribution,
								balance);
						setOpenInfo(/* history, */cashFlow, distribution,
								balance);

						root.add(distribution.getWidget(),
								"Distribuição das Saídas");
						root.add(cashFlow.getWidget(), "Fluxo de Caixa");
						root.add(balance.getWidget(), "Balanço");
						// root.add(history.getWidget(), "Histórico");

						root.addSelectionHandler(getLoaderSelectionHandler(
								distribution, cashFlow, balance/* , history */));

						if (urlParams.containsKey("tab")) {
							Integer tabIndex = Integer.valueOf(urlParams.get(
									"tab").trim());
							root.selectTab(tabIndex);
						} else
							distribution.update();

					}
				});
	}

	protected void setOpenInfo(IOpenPage... pages) {
		for (IOpenPage page : pages)
			page.setOpenRpc(openRpcService);
	}

}
