package mfs.robot.client.erepublik.pages.expenses;

import static mfs.robot.client.erepublik.Utils.addChangeHandler;
import static mfs.robot.client.erepublik.Utils.addStyle;
import static mfs.robot.client.erepublik.Utils.addStyles;
import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import static mfs.robot.client.erepublik.Utils.removeLoadingImgCallback;
import mfs.robot.client.erepublik.DataTableFormatter;
import mfs.robot.client.erepublik.GraphDataOrganizer;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AContentsPage;
import mfs.robot.client.erepublik.pages.IOpenPage;
import mfs.robot.client.erepublik.rpc.RoBoTOpenRPCAsync;
import mfs.robot.shared.erepublik.ExpensesInitialInfo;
import mfs.robot.shared.erepublik.data.CountryPresident;
import mfs.robot.shared.erepublik.dto.BalanceDTO;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.ColumnChart;
import com.googlecode.gwt.charts.client.corechart.ColumnChartOptions;
import com.googlecode.gwt.charts.client.options.VAxis;

public class BalancePage extends AContentsPage implements IOpenPage {

	private DockLayoutPanel				panel		= new DockLayoutPanel(
															Unit.EM);

	private final ListBox				president	= new ListBox();
	private CountryPresident			selectedCP;

	protected ColumnChart				chart		= new ColumnChart();

	protected RoBoTOpenRPCAsync			openRpcService;

	private final ExpensesInitialInfo	expensesInitialInfo;

	// private BalanceDTO allData;

	public BalancePage(ExpensesInitialInfo result) {
		this.expensesInitialInfo = result;
	}

	@Override
	public void setOpenRpc(RoBoTOpenRPCAsync openRpcService) {
		this.openRpcService = openRpcService;
	}

	@Override
	public Widget getWidget() {

		panel.addNorth(getHeader(), 2.5);
		panel.add(chart);
		return panel;
	}

	private Panel getHeader() {
		Panel header = new FlowPanel();
		addStyles(header, "auto-margin", "graph-header");

		addStyle("graph-chooser", president);

		for (CountryPresident cp : expensesInitialInfo.presidents)
			if (cp.getShowFinanceGraphs())
				president.addItem(cp.getName());

		header.add(president);

		president.setSelectedIndex(0);

		addChangeHandler(getUpdateChangeHandler(), president);

		return header;
	}

	protected ColumnChartOptions getColumnChartOptions() {
		ColumnChartOptions chartOptions = ColumnChartOptions.create();
		chartOptions.setBackgroundColor("#EDF4F5");
		chartOptions.setTitle("Balanço Mensal");
		chartOptions.setVAxis(createVAxis());
		chartOptions.setIsStacked(true);
		return chartOptions;
	}

	private VAxis createVAxis() {
		VAxis axis = VAxis.create("Valor (BRL)");

		axis.setMinValue(0);

		return axis;
	}

	protected DataTable getDataTable() {
		final DataTable data = DataTable.create();

		// data.addColumn(ColumnType.STRING, "Presidente");
		// data.addColumn(ColumnType.NUMBER, "Arrecadação");
		// data.addColumn(ColumnType.NUMBER, "Gastos");

		data.addColumn(ColumnType.STRING, "");
		data.addColumn(ColumnType.NUMBER, "Arrecadação");
		data.addColumn(ColumnType.NUMBER, "Entradas");
		data.addColumn(ColumnType.NUMBER, "Saídas");

		return data;
	}

	protected void downloadAndDraw(final Runnable... callbacks) {

		final CountryPresident newCP = getSelectedCP();

		openRpcService.getBalanceData(newCP, new SimpleCallback<BalanceDTO>()
		{
			@Override
			public void onSuccess(BalanceDTO result) {

				selectedCP = newCP;
				doDraw(result, callbacks);

			}
		});
	}

	private void doDraw(BalanceDTO allData, Runnable[] callbacks) {

		final DataTable data = getDataTable();

		data.addRows(2);

		GraphDataOrganizer organizer = new GraphDataOrganizer();

		Double revenue = organizer.sumTaxRevenue(selectedCP,
				allData.taxRevenues);
		Double incomes = organizer.sumIncomes(selectedCP, allData.incomes);
		Double expenses = organizer.sumExpenses(selectedCP, allData.expenses);

		// data.setValue(0, 0, cp.getName());
		// data.setValue(0, 1, revenue);
		// data.setValue(0, 2, expenses);

		data.setValue(0, 0, "Arrecadação + Entradas");
		data.setValue(0, 1, revenue);
		data.setValue(0, 2, incomes);
		data.setValue(0, 3, 0); // expenses

		data.setValue(1, 0, "Saídas");
		data.setValue(1, 1, 0);
		data.setValue(1, 2, 0);
		data.setValue(1, 3, expenses);

		DataTableFormatter.BRL.format(data, 1, 2, 3);

		chart.draw(data, getColumnChartOptions());

		for (Runnable r : callbacks)
			r.run();

	}

	private CountryPresident getSelectedCP() {
		String cp = president.getItemText(president.getSelectedIndex());

		for (CountryPresident p : expensesInitialInfo.presidents)
			if (p.getName().equalsIgnoreCase(cp))
				return p;

		return null;
	}

	@Override
	public void update() {
		super.update();

		if (!selectedCP.equals(getSelectedCP())) {
			panel.remove(chart);

			Image loadingImg = getLoadingImage();
			panel.add(loadingImg);
			downloadAndDraw(removeLoadingImgCallback(loadingImg, panel),
					addChartCallback(panel));
		}

	}

	protected Runnable addChartCallback(final DockLayoutPanel panel) {
		return new Runnable()
		{

			@Override
			public void run() {
				panel.add(chart);
			}
		};
	}

	@Override
	protected String getPageName() {
		return "/Expenses/Balance";
	}

}
