package mfs.robot.client.erepublik.pages.expenses;

import static mfs.robot.client.erepublik.Utils.firebugError;
import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import static mfs.robot.client.erepublik.Utils.removeLoadingImgCallback;

import java.util.Collection;

import mfs.robot.client.erepublik.DataTableFormatter;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.components.TransactionDetailsTablePopup;
import mfs.robot.client.erepublik.pages.AHistoryPage;
import mfs.robot.client.erepublik.pages.IOpenPage;
import mfs.robot.client.erepublik.rpc.RoBoTOpenRPCAsync;
import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.ExpenseType;
import mfs.robot.shared.erepublik.dto.CountryExpenseSum;

import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ChartType;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.Selection;
import com.googlecode.gwt.charts.client.controls.filter.ChartRangeFilterUi;
import com.googlecode.gwt.charts.client.corechart.ColumnChartOptions;
import com.googlecode.gwt.charts.client.corechart.ComboChartOptions;
import com.googlecode.gwt.charts.client.event.SelectEvent;
import com.googlecode.gwt.charts.client.event.SelectHandler;
import com.googlecode.gwt.charts.client.options.AxisTitlesPosition;
import com.googlecode.gwt.charts.client.options.CoreOptions;
import com.googlecode.gwt.charts.client.options.Legend;
import com.googlecode.gwt.charts.client.options.LegendPosition;
import com.googlecode.gwt.charts.client.options.SeriesType;

@Deprecated
public class ExpensesHistoryPage extends AHistoryPage implements IOpenPage {

	private DockLayoutPanel		panel			= new DockLayoutPanel(Unit.EM);

	private Integer				lastSelectedDay	= 0;

	protected RoBoTOpenRPCAsync	openRpcService;

	@Override
	public void setOpenRpc(RoBoTOpenRPCAsync openRpcService) {
		this.openRpcService = openRpcService;
	}

	@Override
	public Widget getWidget() {

		panel.addNorth(dashboard, 0);
		panel.addSouth(numberRangeFilter, 6.25);
		panel.add(chart);
		chart.addSelectHandler(buildSelectHandler());
		return panel;
	}

	@Override
	protected ChartType getChartType() {
		return ChartType.COLUMN;
	}

	@Override
	protected CoreOptions getControlChartOptions() {
		ComboChartOptions controlChartOptions = ComboChartOptions.create();
		controlChartOptions.setHeight(100);
		controlChartOptions.setBackgroundColor("#EDF4F5");
		controlChartOptions.setIsStacked(true);
		controlChartOptions.setSeriesType(SeriesType.BARS);
		return controlChartOptions;
	}

	@Override
	protected ChartRangeFilterUi getChartRangeFilterUi() {
		ChartRangeFilterUi chartRangeFilterUi = ChartRangeFilterUi.create();
		chartRangeFilterUi.setChartType(ChartType.COMBO);

		chartRangeFilterUi.setMinRangeSize(7);
		chartRangeFilterUi.setSnapToData(true);
		return chartRangeFilterUi;
	}

	@Override
	protected CoreOptions getChartOptions() {
		ColumnChartOptions chartOptions = ColumnChartOptions.create();
		chartOptions.setLegend(Legend.create(LegendPosition.TOP));
		chartOptions.setBackgroundColor("#EDF4F5");
		chartOptions.setAxisTitlesPosition(AxisTitlesPosition.OUT);
		chartOptions.setVAxis(getVAxis());
		chartOptions.setHAxis(getHAxis());
		chartOptions.setIsStacked(true);
		return chartOptions;
	}

	@Override
	protected DataTable getDataTable() {
		final DataTable data = DataTable.create();

		data.addColumn(ColumnType.NUMBER, "Dia");
		data.addColumn(ColumnType.NUMBER, ExpenseType.NEW_USERS.getName());
		data.addColumn(ColumnType.NUMBER, ExpenseType.MPP.getName());
		data.addColumn(ColumnType.NUMBER, ExpenseType.ARMY.getName());
		data.addColumn(ColumnType.NUMBER, ExpenseType.FARMER.getName());
		data.addColumn(ColumnType.NUMBER, ExpenseType.SAM.getName());
		data.addColumn(ColumnType.NUMBER, ExpenseType.DIST.getName());
		data.addColumn(ColumnType.NUMBER, ExpenseType.DIST_EXTRA.getName());
		data.addColumn(ColumnType.NUMBER, ExpenseType.RENT.getName());
		data.addColumn(ColumnType.NUMBER, ExpenseType.AIRSTRIKE.getName());
		data.addColumn(ColumnType.NUMBER, ExpenseType.OTHER.getName());

		return data;
	}

	private void setData(DataTable data, int row,
			Collection<CountryExpenseSum> result) {
		for (CountryExpenseSum sum : result) {
			int col;
			switch (sum.category) {
				case NEW_USERS:
					col = 1;
					break;
				case MPP:
					col = 2;
					break;
				case ARMY:
					col = 3;
					break;
				case FARMER:
					col = 4;
					break;
				case SAM:
					col = 5;
					break;
				case DIST:
					col = 6;
					break;
				case DIST_EXTRA:
					col = 7;
					break;
				case RENT:
					col = 8;
					break;
				case AIRSTRIKE:
					col = 9;
					break;
				case OTHER:
					col = 10;
					break;
				default:
					firebugError("Could not find column for some expense type");
					col = -1;
					break;
			}
			data.setValue(row, col, sum.value);

		}

		DataTableFormatter.BRL.format(data, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

	}

	protected void draw(final Runnable... callbacks) {

		final DataTable data = getDataTable();

		openRpcService
				.getExpensesSum(new SimpleCallback<Multimap<Integer, CountryExpenseSum>>()
				{
					@Override
					public void onSuccess(
							Multimap<Integer, CountryExpenseSum> result) {

						data.addRows(result.keySet().size());

						int row = 0;
						for (Integer day : Sets.newTreeSet(result.keySet())) {
							data.setValue(row, 0, day);
							setData(data, row, result.get(day));
							row++;

							if (row >= result.keySet().size()) // last element
								lastDay = day;
						}

						setControlOptions("Gastos");
						dashboard.draw(data);

						for (Runnable r : callbacks)
							r.run();
					}
				});
	}

	@Override
	public void update() {
		super.update();
		if (!loaded) {

			panel.remove(chart);
			panel.remove(numberRangeFilter);

			Image loadingImg = getLoadingImage();
			panel.add(loadingImg);
			draw(removeLoadingImgCallback(loadingImg, panel),
					addChartCallback(panel));
			loaded = true;
		}
	}

	@Override
	protected String getPageName() {
		return "/Expenses/History";
	}

	private SelectHandler buildSelectHandler() {
		return new SelectHandler()
		{

			@Override
			public void onSelect(SelectEvent event) {
				Integer day = getSelectedDay();
				openRpcService.getExpenses(day,
						new SimpleCallback<Collection<CountryExpense>>()
						{

							@Override
							public void onSuccess(
									Collection<CountryExpense> result) {
								new TransactionDetailsTablePopup()
										.loadExpenses(result);
							}
						});
			}
		};
	}

	protected Integer getSelectedDay() {
		try {
			JsArray<Selection> selections = chart.getSelection();
			if (selections.length() > 0) {
				Selection selection = selections.get(0);
				Integer row = selection.getRow();
				lastSelectedDay = (int) chart.getDataTable().getValueNumber(
						row, 0);
			}
		} catch (Exception e) {
			firebugError(e.getMessage());
		}
		return lastSelectedDay;
	}
}
