package mfs.robot.client.erepublik.pages.expenses;

import static mfs.robot.client.erepublik.Utils.addChangeHandler;
import static mfs.robot.client.erepublik.Utils.addStyle;
import static mfs.robot.client.erepublik.Utils.addStyles;
import static mfs.robot.client.erepublik.Utils.firebugError;
import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import static mfs.robot.client.erepublik.Utils.removeLoadingImgCallback;

import java.util.Collection;
import java.util.Set;

import javax.annotation.Nullable;

import mfs.robot.client.erepublik.DataTableFormatter;
import mfs.robot.client.erepublik.GraphDataOrganizer;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.components.TransactionDetailsTablePopup;
import mfs.robot.client.erepublik.pages.AContentsPage;
import mfs.robot.client.erepublik.pages.IOpenPage;
import mfs.robot.client.erepublik.rpc.RoBoTOpenRPCAsync;
import mfs.robot.shared.erepublik.ExpensesInitialInfo;
import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.CountryPresident;
import mfs.robot.shared.erepublik.data.ExpenseType;
import mfs.robot.shared.erepublik.dto.CountryExpenseSum;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Multimap;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.Selection;
import com.googlecode.gwt.charts.client.corechart.PieChart;
import com.googlecode.gwt.charts.client.corechart.PieChartOptions;
import com.googlecode.gwt.charts.client.event.SelectEvent;
import com.googlecode.gwt.charts.client.event.SelectHandler;

public class ExpensesDistributionPage extends AContentsPage implements
		IOpenPage {

	private DockLayoutPanel							panel		= new DockLayoutPanel(
																		Unit.EM);

	private final ListBox							president	= new ListBox();
	protected PieChart								chart		= new PieChart();

	protected RoBoTOpenRPCAsync						openRpcService;

	private final ExpensesInitialInfo				expensesInitialInfo;

	private Multimap<Integer, CountryExpenseSum>	allExpensesSums;

	private Collection<CountryExpense>				allExpenses;

	public ExpensesDistributionPage(ExpensesInitialInfo result) {
		this.expensesInitialInfo = result;
	}

	@Override
	public void setOpenRpc(RoBoTOpenRPCAsync openRpcService) {
		this.openRpcService = openRpcService;
	}

	@Override
	public Widget getWidget() {

		panel.addNorth(getHeader(), 2.5);
		panel.add(chart);

		return panel;
	}

	private Panel getHeader() {
		Panel header = new FlowPanel();
		addStyles(header, "auto-margin", "graph-header");

		addStyle("graph-chooser", president);

		for (CountryPresident cp : expensesInitialInfo.presidents)
			if (cp.getShowFinanceGraphs())
				president.addItem(cp.getName());

		header.add(president);

		president.setSelectedIndex(0);

		addChangeHandler(getUpdateChangeHandler(), president);

		return header;
	}

	protected PieChartOptions getPieChartOptions() {
		PieChartOptions chartOptions = PieChartOptions.create();
		chartOptions.setBackgroundColor("#EDF4F5");
		chartOptions.setIs3D(true);
		chartOptions.setTitle("Distribuição das Saídas por Categoria");
		return chartOptions;
	}

	protected DataTable getDataTable() {
		final DataTable data = DataTable.create();

		data.addColumn(ColumnType.STRING, "Categoria");
		data.addColumn(ColumnType.NUMBER, "Valor");
		data.addRows(ExpenseType.values().length);

		return data;
	}

	protected void downloadAndDraw(final Runnable... callbacks) {
		if (allExpensesSums != null)
			doDraw(callbacks);

		else
			openRpcService
					.getExpensesSum(new SimpleCallback<Multimap<Integer, CountryExpenseSum>>()
					{
						@Override
						public void onSuccess(
								Multimap<Integer, CountryExpenseSum> result) {

							allExpensesSums = result;
							doDraw(callbacks);

						}
					});
	}

	private void doDraw(Runnable[] callbacks) {

		final DataTable data = getDataTable();

		CountryPresident cp = getSelectedCP();

		Set<CountryExpenseSum> totalSums = new GraphDataOrganizer()
				.agregateByType(cp, allExpensesSums);

		int row = 0;
		for (CountryExpenseSum sum : totalSums) {
			if (sum.category != ExpenseType.MON_MARKET) {
				data.setValue(row, 0, sum.category.getName());
				data.setValue(row, 1, sum.value);
				row++;
			}
		}

		DataTableFormatter.BRL.format(data, 1);

		chart.draw(data, getPieChartOptions());

		chart.removeAllHandlers();
		chart.addSelectHandler(buildSelectHandler(data));

		for (Runnable r : callbacks)
			r.run();

	}

	private CountryPresident getSelectedCP() {
		String cp = president.getItemText(president.getSelectedIndex());

		for (CountryPresident p : expensesInitialInfo.presidents)
			if (p.getName().equalsIgnoreCase(cp))
				return p;

		return null;
	}

	@Override
	public void update() {
		super.update();

		panel.remove(chart);

		Image loadingImg = getLoadingImage();
		panel.add(loadingImg);
		downloadAndDraw(removeLoadingImgCallback(loadingImg, panel),
				addChartCallback(panel));

	}

	protected Runnable addChartCallback(final DockLayoutPanel panel) {
		return new Runnable()
		{

			@Override
			public void run() {
				panel.add(chart);
			}
		};
	}

	private SelectHandler buildSelectHandler(final DataTable data) {
		return new SelectHandler()
		{

			@Override
			public void onSelect(SelectEvent event) {
				final ExpenseType type = getSelectedType(data);

				if (allExpenses == null) {
					openRpcService
							.getExpenses(new SimpleCallback<Collection<CountryExpense>>()
							{

								@Override
								public void onSuccess(
										Collection<CountryExpense> result) {
									allExpenses = result;
									filterAndPopup(type);
								}
							});
				} else {
					filterAndPopup(type);
				}
			}
		};
	}

	protected void filterAndPopup(final ExpenseType category) {

		final CountryPresident cp = getSelectedCP();
		Collection<CountryExpense> filtered = Collections2.filter(allExpenses,
				new Predicate<CountryExpense>()
				{

					@Override
					public boolean apply(@Nullable
					CountryExpense object) {
						return category.equals(object.getCategory())
								&& object.geteRepDay() > cp.getFirstErepDay()
								&& object.geteRepDay() <= cp.getLastErepDay();
					}
				});

		new TransactionDetailsTablePopup().loadExpenses(filtered);
	}

	protected ExpenseType getSelectedType(DataTable data) {
		try {
			JsArray<Selection> selections = chart.getSelection();
			if (selections.length() > 0) {
				Selection selection = selections.get(0);
				Integer row = selection.getRow();

				String label = data.getValueString(row, 0);

				return ExpenseType.fromName(label);

			}
		} catch (Exception e) {
			firebugError(e.getMessage());
		}
		return null;
	}

	@Override
	protected String getPageName() {
		return "/Expenses/Distribution";
	}

}
