package mfs.robot.client.erepublik.pages.politics;

import static mfs.robot.client.erepublik.Utils.addChangeHandler;
import static mfs.robot.client.erepublik.Utils.addStyle;
import static mfs.robot.client.erepublik.Utils.addStyles;
import static mfs.robot.client.erepublik.Utils.setVisible;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AContentsPage;
import mfs.robot.shared.erepublik.data.PoliticalParty;
import mfs.robot.shared.erepublik.data.PoliticalPartyMembersDiff;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

@Deprecated
public class PartyMembersPage extends AContentsPage {

	private PartyMembersTable	newMembers	= new PartyMembersTable(
													"Jogadores que entraram no partido:");
	private PartyMembersTable	leftMembers	= new PartyMembersTable(
													"Jogadores que saíram do partido:");

	private final ListBox		dayList		= new ListBox();
	private final ListBox		partyList	= new ListBox();

	@Override
	public Widget getWidget() {
		DockLayoutPanel root = new DockLayoutPanel(Unit.EM);

		root.addNorth(getHeader(), 3);

		DockLayoutPanel tablesPanel = new DockLayoutPanel(Unit.PCT);
		tablesPanel.addWest(newMembers.getTable(), 50);
		tablesPanel.addEast(leftMembers.getTable(), 50);
		root.add(tablesPanel);

		return root;

	}

	private Panel getHeader() {
		Panel header = new FlowPanel();

		addStyles(header, "auto-margin", "graph-header");

		addStyle("graph-chooser", dayList, partyList);

		int limit = initialInfo.isAdmin ? initialInfo.eRepDay - 1
				: initialInfo.eRepDay - 8;
		for (int i = limit; i >= 1925; i--)
			dayList.addItem(i + "", i + "");

		for (PoliticalParty party : PoliticalParty.values())
			if (party.getTrackMembers())
				partyList.addItem(party.getPartyName(), party.getPartyCode()
						+ "");

		header.add(dayList);
		header.add(partyList);

		dayList.setSelectedIndex(0);
		partyList.setSelectedIndex(0);

		addChangeHandler(getUpdateChangeHandler(), dayList, partyList);

		return header;
	}

	@Override
	public void update() {
		super.update();

		Integer selectedDay = Integer.valueOf(dayList.getValue(dayList
				.getSelectedIndex()));
		PoliticalParty selectedParty = PoliticalParty.fromId(partyList
				.getValue(partyList.getSelectedIndex()));

		loadExpenses(selectedDay, selectedParty);
	}

	protected void loadExpenses(final Integer day, PoliticalParty party) {
		setVisible(false, newMembers.getTable(), leftMembers.getTable());

		rpcService.getMembersDiff(party, day,
				new SimpleCallback<PoliticalPartyMembersDiff>()
				{

					@Override
					public void onSuccess(PoliticalPartyMembersDiff result) {
						setVisible(true, newMembers.getTable(),
								leftMembers.getTable());
						newMembers.set(day, result.getNewMembers());
						leftMembers.set(day, result.getLeftMembers());
					}
				});
	}

	protected ChangeHandler getUpdateChangeHandler() {
		return new ChangeHandler()
		{

			@Override
			public void onChange(ChangeEvent event) {
				update();
			}
		};
	}

	@Override
	protected String getPageName() {
		return "/Politics/PartyMembers";
	}
}
