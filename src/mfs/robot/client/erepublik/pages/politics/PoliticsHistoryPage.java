package mfs.robot.client.erepublik.pages.politics;

import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import static mfs.robot.client.erepublik.Utils.removeLoadingImgCallback;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AHistoryPage;
import mfs.robot.shared.erepublik.data.PoliticalParty;
import mfs.robot.shared.erepublik.data.PoliticalPartyStats;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.options.VAxis;

public class PoliticsHistoryPage extends AHistoryPage {

	private DockLayoutPanel	panel	= new DockLayoutPanel(Unit.EM);

	@Override
	public Widget getWidget() {

		panel.addNorth(dashboard, 0);
		panel.addSouth(numberRangeFilter, 6.25);
		panel.add(chart);

		return panel;
	}

	@Override
	protected DataTable getDataTable() {
		final DataTable data = DataTable.create();

		data.addColumn(ColumnType.NUMBER, "Dia");

		for (PoliticalParty p : PoliticalParty.values())
			data.addColumn(ColumnType.NUMBER, p.getPartyName());

		return data;
	}

	protected void fillPoliticalData(DataTable data,
			Map<PoliticalParty, Collection<PoliticalPartyStats>> result) {

		int resultSize = result.values().iterator().next().size();

		data.addRows(resultSize + 1);

		int col = 1;
		for (PoliticalParty p : PoliticalParty.values()) {
			int row = 0;
			if (result.get(p) != null)
				for (PoliticalPartyStats s : result.get(p)) {
					data.setValue(row, 0, s.geteRepDay());
					if (s.getMembersCount() > 0)
						data.setValue(row, col, s.getMembersCount());
					row++;

					if (row >= resultSize) // last element
						lastDay = s.geteRepDay();
				}
			col++;
		}
	}

	protected VAxis getVAxis() {
		return VAxis.create("Número de Membros");
	}

	protected void draw(final Runnable... callbacks) {

		final DataTable data = getDataTable();

		rpcService
				.getPoliticalHistory(new SimpleCallback<Map<PoliticalParty, Collection<PoliticalPartyStats>>>()
				{
					@Override
					public void onSuccess(
							Map<PoliticalParty, Collection<PoliticalPartyStats>> result) {
						fillPoliticalData(data, result);

						setControlOptions("Partidos");

						chart.setView("{\"columns\":[0, 1, 2, 3, 4, 5]}");
						dashboard.draw(data);

						for (Runnable r : callbacks)
							r.run();

					}
				});
	}

	@Override
	public void update() {
		super.update();
		if (!loaded) {

			panel.remove(chart);
			panel.remove(numberRangeFilter);

			Image loadingImg = getLoadingImage();
			panel.add(loadingImg);
			draw(removeLoadingImgCallback(loadingImg, panel),
					addPartyListCallback(panel), addChartCallback(panel));
			loaded = true;
		}
	}

	private Runnable addPartyListCallback(final DockLayoutPanel panel) {
		return new Runnable()
		{

			@Override
			public void run() {
				final List<CheckBox> partiesList = new ArrayList<CheckBox>();
				Panel cbPanel = new FlowPanel();
				int i = 1;

				for (PoliticalParty p : PoliticalParty.values()) {
					CheckBox cb = new CheckBox(p.getPartyName());
					if (i <= 5)
						cb.setValue(true);
					cb.setFormValue("" + i++);
					cb.addClickHandler(new ClickHandler()
					{

						@Override
						public void onClick(ClickEvent event) {
							updateChartView(partiesList);
						}
					});
					partiesList.add(cb);
					cb.addStyleName("partyList-cb");
					cbPanel.add(cb);
				}
				cbPanel.addStyleName("partyList");
				panel.addWest(cbPanel, 10);
			}
		};
	}

	private void updateChartView(List<CheckBox> partiesList) {
		String viewConfigString = "{\"columns\":[0";

		for (CheckBox cb : partiesList)
			if (cb.getValue())
				viewConfigString += ", " + cb.getFormValue();

		viewConfigString += "]}";

		chart.setView(viewConfigString);
		chart.draw();
	}

	@Override
	protected String getPageName() {
		return "/Politics/History";
	}
}
