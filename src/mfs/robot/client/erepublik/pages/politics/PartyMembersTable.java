package mfs.robot.client.erepublik.pages.politics;

import static mfs.robot.client.erepublik.Utils.addStyles;
import static mfs.robot.client.erepublik.Utils.getLoadingImage;

import java.util.ArrayList;
import java.util.Collection;

import mfs.robot.client.erepublik.components.AnchorCell;
import mfs.robot.shared.erepublik.data.PartyMember;

import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.ui.Anchor;

public class PartyMembersTable {

	private final CellTable<PartyMember>	grid		= new CellTable<PartyMember>();
	private Integer							day;
	private final String					action;

	private static final NumberFormat		DAY_FORMAT	= NumberFormat
																.getFormat("####");

	public PartyMembersTable(String action) {
		this.action = action;
		build();
		grid.setLoadingIndicator(getLoadingImage());
		addStyles(grid, "expense-grid", "auto-0margin");
	}

	private void build() {
		Column<PartyMember, Number> dayCol = new Column<PartyMember, Number>(
				new NumberCell(DAY_FORMAT))
		{

			@Override
			public Integer getValue(PartyMember object) {
				return day;
			}
		};
		grid.addColumn(dayCol, "Dia");

		Column<PartyMember, Anchor> membersCol = new Column<PartyMember, Anchor>(
				new AnchorCell())
		{

			@Override
			public Anchor getValue(PartyMember object) {
				Anchor link = new Anchor(object.geteRepName(),
						"http://www.erepublik.com/en/citizen/profile/"
								+ object.geteRepId());
				return link;
			}
		};
		grid.addColumn(membersCol, action);

	}

	public void set(Integer day, Collection<PartyMember> members) {
		this.day = day;
		grid.setRowCount(members.size(), true);
		grid.setRowData(new ArrayList<PartyMember>(members));
	}

	public CellTable<PartyMember> getTable() {
		return grid;
	}

}
