package mfs.robot.client.erepublik.pages.politics;

import static mfs.robot.client.erepublik.Utils.addChangeHandler;
import static mfs.robot.client.erepublik.Utils.addStyle;
import static mfs.robot.client.erepublik.Utils.addStyles;
import static mfs.robot.client.erepublik.Utils.firebugError;
import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import static mfs.robot.client.erepublik.Utils.removeLoadingImgCallback;

import java.util.Collection;

import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AHistoryPage;
import mfs.robot.shared.erepublik.data.PoliticalParty;
import mfs.robot.shared.erepublik.data.PoliticalPartyMembersDiff;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ChartType;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.Selection;
import com.googlecode.gwt.charts.client.corechart.ColumnChartOptions;
import com.googlecode.gwt.charts.client.event.SelectEvent;
import com.googlecode.gwt.charts.client.event.SelectHandler;
import com.googlecode.gwt.charts.client.options.AxisTitlesPosition;
import com.googlecode.gwt.charts.client.options.CoreOptions;
import com.googlecode.gwt.charts.client.options.Legend;
import com.googlecode.gwt.charts.client.options.LegendPosition;
import com.googlecode.gwt.charts.client.options.VAxis;

public class PartyMembersHistoryPage extends AHistoryPage {

	private DockLayoutPanel	panel		= new DockLayoutPanel(Unit.EM);
	private final ListBox	partyList	= new ListBox();

	@Override
	public Widget getWidget() {

		panel.addNorth(dashboard, 0);
		panel.addNorth(getHeader(), 3);
		panel.addSouth(numberRangeFilter, 6.25);
		panel.add(chart);

		chart.addSelectHandler(buildSelectHandler());

		return panel;
	}

	private Panel getHeader() {
		Panel header = new FlowPanel();

		addStyles(header, "auto-margin", "graph-header");
		addStyle("graph-chooser", partyList);

		for (PoliticalParty party : PoliticalParty.values())
			if (party.getTrackMembers())
				partyList.addItem(party.getPartyName(), party.getPartyCode()
						+ "");

		header.add(partyList);

		partyList.setSelectedIndex(0);

		addChangeHandler(getUpdateChangeHandler(), partyList);

		return header;
	}

	protected ChartType getChartType() {
		return ChartType.COLUMN;
	}

	protected CoreOptions getControlChartOptions() {
		ColumnChartOptions controlChartOptions = ColumnChartOptions.create();
		controlChartOptions.setHeight(100);
		controlChartOptions.setBackgroundColor("#EDF4F5");
		return controlChartOptions;
	}

	protected CoreOptions getChartOptions() {
		ColumnChartOptions chartOptions = ColumnChartOptions.create();
		chartOptions.setLegend(Legend.create(LegendPosition.TOP));
		chartOptions.setBackgroundColor("#EDF4F5");
		chartOptions.setAxisTitlesPosition(AxisTitlesPosition.OUT);
		chartOptions.setVAxis(getVAxis());
		chartOptions.setHAxis(getHAxis());
		return chartOptions;
	}

	@Override
	protected DataTable getDataTable() {
		final DataTable data = DataTable.create();

		data.addColumn(ColumnType.NUMBER, "Dia");
		data.addColumn(ColumnType.NUMBER, "Entraram no Partido");
		data.addColumn(ColumnType.NUMBER, "Saíram do Partido");

		return data;
	}

	protected void fillPoliticalData(DataTable data,
			Collection<PoliticalPartyMembersDiff> result) {

		data.addRows(result.size());

		int limit = initialInfo.isAdmin ? initialInfo.eRepDay - 1
				: initialInfo.eRepDay - 8;

		int row = 0;
		for (PoliticalPartyMembersDiff diff : result) {
			data.setValue(row, 0, diff.geteRepDay());
			if (diff.geteRepDay() <= limit) {
				data.setValue(row, 1, diff.getNewMembersCount());
				data.setValue(row, 2, diff.getLeftMembersCount());
			}
			row++;
		}
		lastDay = limit;

	}

	protected VAxis getVAxis() {
		return VAxis.create("Número de Membros");
	}

	protected void draw(final Runnable... callbacks) {

		final DataTable data = getDataTable();

		PoliticalParty party = PoliticalParty.fromId(partyList
				.getValue(partyList.getSelectedIndex()));

		rpcService.getMembersDiffHistory(party,
				new SimpleCallback<Collection<PoliticalPartyMembersDiff>>()
				{

					@Override
					public void onSuccess(
							Collection<PoliticalPartyMembersDiff> result) {

						fillPoliticalData(data, result);
						setControlOptions("Movimentação de Partidários");
						dashboard.draw(data);
						for (Runnable r : callbacks)
							r.run();
					}
				});
	}

	@Override
	public void update() {
		super.update();
		if (!loaded) {

			panel.remove(chart);
			panel.remove(numberRangeFilter);

			Image loadingImg = getLoadingImage();
			panel.add(loadingImg);
			draw(removeLoadingImgCallback(loadingImg, panel),
					addChartCallback(panel));
			loaded = true;
		}
	}

	private SelectHandler buildSelectHandler() {
		return new SelectHandler()
		{

			@Override
			public void onSelect(SelectEvent event) {
				final Integer day = getSelectedDay();
				final String action = getSelectedAction();
				PoliticalParty party = PoliticalParty.fromId(partyList
						.getValue(partyList.getSelectedIndex()));

				if (day != null && action != null)
					rpcService.getMembersDiff(party, day,
							new SimpleCallback<PoliticalPartyMembersDiff>()
							{

								@Override
								public void onSuccess(
										PoliticalPartyMembersDiff result) {
									PartyMembersTablePopup popup = new PartyMembersTablePopup(
											action);
									if ("Entraram no Partido".equals(action))
										popup.showMembers(day,
												result.getNewMembers());
									if ("Saíram do Partido".equals(action))
										popup.showMembers(day,
												result.getLeftMembers());
								}
							});
			}
		};
	}

	protected Integer getSelectedDay() {
		try {
			JsArray<Selection> selections = chart.getSelection();
			if (selections.length() > 0) {
				Selection selection = selections.get(0);
				Integer row = selection.getRow();
				return (int) chart.getDataTable().getValueNumber(row, 0);
			}
		} catch (Exception e) {
			firebugError(e.getMessage());
		}
		return null;
	}

	protected String getSelectedAction() {
		try {
			JsArray<Selection> selections = chart.getSelection();
			if (selections.length() > 0) {
				Selection selection = selections.get(0);
				Integer col = selection.getColumn();
				if (col == 1)
					return "Entraram no Partido";
				if (col == 2)
					return "Saíram do Partido";
			}
		} catch (Exception e) {
			firebugError(e.getMessage());
		}
		return null;
	}

	@Override
	protected String getPageName() {
		return "/Politics/PartyMembers2";
	}
}
