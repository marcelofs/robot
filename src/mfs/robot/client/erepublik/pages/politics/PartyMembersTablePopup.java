package mfs.robot.client.erepublik.pages.politics;

import java.util.Collection;

import mfs.robot.shared.erepublik.data.PartyMember;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ScrollPanel;

public class PartyMembersTablePopup {

	private final PartyMembersTable	membersTable;
	private final PopupPanel		root	= new PopupPanel(true, true);

	public PartyMembersTablePopup(String action) {

		membersTable = new PartyMembersTable(action);

		root.setAnimationEnabled(true);
		root.setGlassEnabled(true);

		FlowPanel panel = new FlowPanel();
		panel.add(membersTable.getTable());

		ScrollPanel scroll = new ScrollPanel(panel);
		scroll.setHeight(Window.getClientHeight() / 2 + "px");

		root.setWidget(scroll);

	}

	protected void showMembers(Integer day, Collection<PartyMember> objects) {
		membersTable.set(day, objects);
		root.show();
		root.center();
	}

}
