package mfs.robot.client.erepublik.pages.politics;

import mfs.robot.client.erepublik.pages.AContentsPage;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;
import static mfs.robot.client.erepublik.Utils.*;

public class PoliticsPage extends AContentsPage {

	private PoliticsHistoryPage		history;
	private PartyMembersHistoryPage	inOut;

	@Override
	public Widget getWidget() {
		TabLayoutPanel root = new TabLayoutPanel(2.3, Unit.EM);

		history = new PoliticsHistoryPage();
		inOut = new PartyMembersHistoryPage();

		setInitialInfo(history, inOut);

		root.add(history.getWidget(), "Histórico");
		root.add(inOut.getWidget(), "Partidários");

		root.addSelectionHandler(getLoaderSelectionHandler(history, inOut));

		history.update();

		return root;
	}
}
