package mfs.robot.client.erepublik.pages.jobs;

import static mfs.robot.client.erepublik.Utils.addStyles;
import static mfs.robot.client.erepublik.Utils.disable;
import static mfs.robot.client.erepublik.Utils.enableWidgetsCallback;
import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import static mfs.robot.client.erepublik.Utils.removeLoadingImgCallback;

import java.util.Collection;

import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.ADailyPage;
import mfs.robot.shared.erepublik.data.JobMarketOffer;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;

public class JobsDailyPage extends ADailyPage {

	private DockLayoutPanel	panel	= new DockLayoutPanel(Unit.EM);

	private final ListBox	day		= new ListBox();

	@Override
	public Widget getWidget() {
		panel.addNorth(getHeader(), 2.5);
		panel.add(lines);
		return panel;
	}

	private Panel getHeader() {
		Panel header = new FlowPanel();
		addStyles(header, "auto-margin", "graph-header");

		day.addStyleName("graph-chooser");

		Integer someDay = initialInfo.eRepDay - 7;
		for (int i = 0; i <= 7; i++, someDay++)
			day.addItem(someDay + "");

		header.add(day);

		day.setSelectedIndex(7);

		day.addChangeHandler(getUpdateChangeHandler());

		return header;
	}

	@Override
	protected DataTable getDataTable() {
		final DataTable data = DataTable.create();

		data.addColumn(ColumnType.NUMBER, "Horário");
		data.addColumn(ColumnType.NUMBER, "Valor Médio das Ofertas");
		data.addColumn(ColumnType.NUMBER, "Maior Valor Contratado");

		return data;
	}

	protected void draw(final Integer day, final Runnable... callbacks) {

		final DataTable data = getDataTable();

		rpcService.getDailyJobMarketOffers(day,
				new SimpleCallback<Collection<JobMarketOffer>>()
				{
					@Override
					public void onSuccess(Collection<JobMarketOffer> result) {
						fillData(result, data);

						lines.draw(data, createDailyLineOpts("Salários no dia "
								+ day));

						for (Runnable r : callbacks)
							r.run();

						recordAnalyticsHit(day);
					}
				});
	}

	protected void recordAnalyticsHit(Integer day) {

		String dayTxt = "Unknown";

		if (initialInfo.eRepDay.equals(day))
			dayTxt = "Today";
		else
			dayTxt = "" + (day - initialInfo.eRepDay);

		recordAnalyticsHit("/Jobs/Daily/" + dayTxt);
	}

	@Override
	public void update() {
		super.update();
		if (!loaded) {

			disable(day);

			Integer pDay = Integer.valueOf(day.getItemText(day
					.getSelectedIndex()));

			panel.remove(lines);

			Image loadingImg = getLoadingImage();
			panel.add(loadingImg);

			draw(pDay, removeLoadingImgCallback(loadingImg, panel),
					addChartCallback(panel), enableWidgetsCallback(day));

			loaded = true;
		}

	}

	@Override
	protected String getPageName() {
		return "/Jobs/Daily";
	}
}
