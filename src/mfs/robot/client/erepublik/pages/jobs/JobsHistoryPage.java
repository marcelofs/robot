package mfs.robot.client.erepublik.pages.jobs;

import static mfs.robot.client.erepublik.Utils.addStyles;
import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import static mfs.robot.client.erepublik.Utils.removeLoadingImgCallback;

import java.util.Collection;
import java.util.Map;

import mfs.robot.client.erepublik.DataTableFormatter;
import mfs.robot.client.erepublik.GraphDataOrganizer;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AHistoryPage;
import mfs.robot.shared.erepublik.IHistorySeries;
import mfs.robot.shared.erepublik.data.AgregatedJobMarketOffer;
import mfs.robot.shared.erepublik.data.CountryTaxes;
import mfs.robot.shared.erepublik.dto.JobHistoryDTO;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;

public class JobsHistoryPage extends AHistoryPage {

	enum Type
	{
		RAW("Bruto"), NET("Líquido");

		public String	uiName;

		private Type(String uiName) {
			this.uiName = uiName;
		}
	}

	private DockLayoutPanel	panel	= new DockLayoutPanel(Unit.EM);
	private final ListBox	type	= new ListBox();

	private JobHistoryDTO	dto;

	@Override
	public Widget getWidget() {

		panel.addNorth(dashboard, 0);
		panel.addNorth(getHeader(), 2.5);

		panel.addSouth(numberRangeFilter, 6.25);
		panel.add(chart);

		return panel;
	}

	private Panel getHeader() {
		Panel header = new FlowPanel();
		addStyles(header, "auto-margin", "graph-header");

		type.addStyleName("graph-chooser");

		for (Type t : Type.values())
			type.addItem(t.uiName, t.name());

		header.add(type);

		type.addChangeHandler(getUpdateChangeHandler());

		return header;
	}

	protected DataTable getDataTable() {
		final DataTable data = DataTable.create();

		data.addColumn(ColumnType.NUMBER, "Dia");
		data.addColumn(ColumnType.NUMBER, "Valor Médio das Ofertas");
		data.addColumn(ColumnType.NUMBER, "Maior Valor Contratado");
		data.addColumn(ColumnType.NUMBER, "Menor Valor Contratado");
		data.addColumn(ColumnType.NUMBER, "Média Oficial do País");
		return data;
	}

	protected void draw(Runnable... callbacks) {
		DataTable data = getDataTable();

		if (Type.RAW.equals(getSelectedType()))
			fillData(data, dto.jobs);
		else
			fillData(data, dto);

		setControlOptions("Salários (" + getSelectedType().uiName + ")");
		dashboard.draw(data);

		for (Runnable r : callbacks)
			r.run();
	}

	protected void downloadAndDraw(final Runnable... callbacks) {
		if (dto == null)
			rpcService.getJobMarketHistory(new SimpleCallback<JobHistoryDTO>()
			{
				@Override
				public void onSuccess(JobHistoryDTO result) {
					dto = result;
					draw(callbacks);
				}
			});
		else
			draw(callbacks);
	}

	protected void fillData(DataTable data,
			Collection<? extends IHistorySeries> result) {
		data.addRows(result.size());

		int row = 0;
		for (IHistorySeries offer : result) {
			data.setValue(row, 0, offer.geteRepDay());
			if (offer.getAverage() != null)
				data.setValue(row, 1, offer.getAverage());
			if (offer.getMaximum() != null)
				data.setValue(row, 2, offer.getMaximum());
			if (offer.getMinimum() != null)
				data.setValue(row, 3, offer.getMinimum());

			Double oficialAvg = ((AgregatedJobMarketOffer) offer)
					.getOficialAvgSalary();
			if (oficialAvg != null)
				data.setValue(row, 4, oficialAvg);

			row++;

			if (row >= result.size()) // last element
				lastDay = offer.geteRepDay();
		}

		DataTableFormatter.BRL_CENTS.format(data, 1);
		DataTableFormatter.BRL.format(data, 2, 3);
	}

	protected void fillData(DataTable data, JobHistoryDTO result) {

		GraphDataOrganizer organizer = new GraphDataOrganizer();

		Map<Integer, CountryTaxes> taxes = organizer.mapOfTaxes(result.taxes);

		data.addRows(result.jobs.size());

		int row = 0;
		for (AgregatedJobMarketOffer offer : result.jobs) {
			CountryTaxes tax = taxes.get(offer.geteRepDay());
			Double irMultiplier = null;
			if (tax != null && tax.getIncomeTax() != null)
				irMultiplier = 1 - (tax.getIncomeTax() / 100d);

			data.setValue(row, 0, offer.geteRepDay());
			if (irMultiplier != null) {
				if (offer.getAverage() != null)
					data.setValue(row, 1, offer.getAverage() * irMultiplier);
				if (offer.getMaximum() != null)
					data.setValue(row, 2, offer.getMaximum() * irMultiplier);
				if (offer.getMinimum() != null)
					data.setValue(row, 3, offer.getMinimum() * irMultiplier);

				if (offer.getOficialAvgSalary() != null)
					data.setValue(row, 4, offer.getOficialAvgSalary());
			}
			row++;

			if (row >= result.jobs.size()) // last element
				lastDay = tax.geteRepDay();
		}

		DataTableFormatter.BRL_CENTS.format(data, 1);
		DataTableFormatter.BRL.format(data, 2, 3);
	}

	private Type getSelectedType() {
		return Type.valueOf(type.getValue(type.getSelectedIndex()));
	}

	@Override
	public void update() {
		super.update();
		if (!loaded) {

			panel.remove(chart);
			panel.remove(numberRangeFilter);

			Image loadingImg = getLoadingImage();
			panel.add(loadingImg);
			downloadAndDraw(removeLoadingImgCallback(loadingImg, panel),
					addChartCallback(panel));
			loaded = true;
		}
	}

	@Override
	protected String getPageName() {
		return "/Jobs/History/" + type.getValue(type.getSelectedIndex());
	}
}
