package mfs.robot.client.erepublik.pages.index;

import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import static mfs.robot.client.erepublik.Utils.removeLoadingImgCallback;

import java.util.Collection;
import java.util.Map;

import mfs.robot.client.erepublik.DataTableFormatter;
import mfs.robot.client.erepublik.GraphDataOrganizer;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AHistoryPage;
import mfs.robot.shared.erepublik.FinancialIndexType;
import mfs.robot.shared.erepublik.IFinancialIndex;

import com.google.common.collect.Multimap;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;

public class IndexesMainPage extends AHistoryPage {

	private DockLayoutPanel		panel	= new DockLayoutPanel(Unit.EM);

	private FinancialIndexType	toBeDivided;
	private FinancialIndexType	toDivide;

	@Override
	public Widget getWidget() {

		panel.addNorth(dashboard, 0);
		// panel.addSouth(numberRangeFilter, 6.25);
		panel.addWest(getIndexNames(), 15);
		// panel.add(chart);

		return panel;
	}

	private Widget getIndexNames() {

		Panel cbPanel = new FlowPanel();
		Panel top = new FlowPanel();
		Panel bottom = new FlowPanel();

		for (FinancialIndexType index : FinancialIndexType.values()) {
			if (index.isCanBeDivided()) {
				RadioButton radio = createButton("canBeDivided", index,
						toBeDivided == null);
				top.add(radio);
				if (toBeDivided == null)
					toBeDivided = index;
			}

			if (index.isCanDivide()) {
				RadioButton radio = createButton("canDivide", index,
						toDivide == null);
				bottom.add(radio);
				if (toDivide == null)
					toDivide = index;
			}
		}
		cbPanel.addStyleName("partyList");

		cbPanel.add(new Label("Escolha dois índices:"));
		cbPanel.add(top);
		cbPanel.add(new Label(" por: "));
		cbPanel.add(bottom);

		return cbPanel;
	}

	private RadioButton createButton(String group, FinancialIndexType index,
			boolean preSelected) {
		RadioButton radio = new RadioButton(group, index.getTitle());
		radio.setFormValue(index.name());
		radio.setValue(preSelected);
		radio.addValueChangeHandler(getUpdateValueChangeHandler(group, index));
		radio.addStyleName("partyList-cb");
		return radio;
	}

	@Override
	protected DataTable getDataTable() {
		final DataTable data = DataTable.create();

		data.addColumn(ColumnType.NUMBER, "Dia");
		data.addColumn(ColumnType.NUMBER, toBeDivided.getTitle() + " / "
				+ toDivide.getTitle());

		return data;
	}

	private void fillData(DataTable data,
			Collection<IFinancialIndex> colToBeDivided,
			Collection<IFinancialIndex> colToDivide) {

		data.addRows(colToBeDivided.size());

		GraphDataOrganizer organizer = new GraphDataOrganizer();
		Map<Integer, IFinancialIndex> mapToDivide = organizer
				.mapOfIndexes(colToDivide);

		int row = 0;
		for (IFinancialIndex indexToBeDivided : colToBeDivided) {
			data.setValue(row, 0, indexToBeDivided.geteRepDay());

			IFinancialIndex indexToDivide = mapToDivide.get(indexToBeDivided
					.geteRepDay());
			if (indexToDivide != null)
				data.setValue(row, 1, indexToBeDivided.getIndexValue()
						/ indexToDivide.getIndexValue());

			row++;
			if (row >= colToBeDivided.size()) // last element
				lastDay = indexToBeDivided.geteRepDay();
		}

		DataTableFormatter.INDEX.format(data, 1);
	}

	protected void draw(final Runnable... callbacks) {

		final DataTable data = getDataTable();

		rpcService
				.getIndex(
						new FinancialIndexType[] { toBeDivided, toDivide },
						new SimpleCallback<Multimap<FinancialIndexType, IFinancialIndex>>()
						{

							@Override
							public void onSuccess(
									Multimap<FinancialIndexType, IFinancialIndex> result) {

								fillData(data, result.get(toBeDivided),
										result.get(toDivide));

								setControlOptions(toBeDivided.getTitle()
										+ " / " + toDivide.getTitle());

								dashboard.draw(data);

								for (Runnable r : callbacks)
									r.run();

							}

						});

	}

	@Override
	public void update() {
		super.update();
		if (!loaded) {

			panel.remove(chart);
			panel.remove(numberRangeFilter);

			Image loadingImg = getLoadingImage();
			panel.add(loadingImg);

			draw(removeLoadingImgCallback(loadingImg, panel),
					addChartCallback(panel));
			loaded = true;
		}

	}

	private ValueChangeHandler<Boolean> getUpdateValueChangeHandler(
			final String group, final FinancialIndexType index) {
		return new ValueChangeHandler<Boolean>()
		{

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				loaded = false;

				if ("canBeDivided".equals(group))
					toBeDivided = index;
				else
					if ("canDivide".equals(group))
						toDivide = index;

				update();

			}
		};
	}

	@Override
	protected String getPageName() {
		if (toBeDivided == null || toDivide == null)
			return "/Indexes/Main/";

		return "/Indexes/Main/" + toBeDivided.name() + "/" + toDivide.name();
	}
}
