package mfs.robot.client.erepublik.pages.index;

import static mfs.robot.client.erepublik.Utils.getLoaderSelectionHandler;
import mfs.robot.client.erepublik.pages.AContentsPage;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

public class IndexesPage extends AContentsPage {

	private IndexesMainPage	indexes;

	@Override
	public Widget getWidget() {
		TabLayoutPanel root = new TabLayoutPanel(2.3, Unit.EM);

		indexes = new IndexesMainPage();

		setInitialInfo(indexes);

		root.add(indexes.getWidget(), "Índices");

		root.addSelectionHandler(getLoaderSelectionHandler(indexes));

		indexes.update();

		return root;
	}

}
