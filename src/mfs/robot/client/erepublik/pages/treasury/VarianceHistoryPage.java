package mfs.robot.client.erepublik.pages.treasury;

import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import static mfs.robot.client.erepublik.Utils.removeLoadingImgCallback;

import java.util.Collection;

import mfs.robot.client.erepublik.DataTableFormatter;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AHistoryPage;
import mfs.robot.client.erepublik.pages.IOpenPage;
import mfs.robot.client.erepublik.rpc.RoBoTOpenRPCAsync;
import mfs.robot.shared.erepublik.data.CountryTreasury;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;

public class VarianceHistoryPage extends AHistoryPage implements IOpenPage {

	private DockLayoutPanel		panel	= new DockLayoutPanel(Unit.EM);

	protected RoBoTOpenRPCAsync	openRpcService;

	@Override
	public void setOpenRpc(RoBoTOpenRPCAsync openRpcService) {
		this.openRpcService = openRpcService;
	}

	@Override
	public Widget getWidget() {

		panel.addNorth(dashboard, 0);
		panel.addSouth(numberRangeFilter, 6.25);
		panel.add(chart);

		return panel;
	}

	protected DataTable getDataTable() {
		final DataTable data = DataTable.create();

		data.addColumn(ColumnType.NUMBER, "Dia");
		data.addColumn(ColumnType.NUMBER, "Variação (%)");

		return data;
	}

	protected void fillTreasuryData(DataTable data,
			Collection<CountryTreasury> result) {
		data.addRows(result.size());

		int row = 0;
		Double yesterdayRevenue = null;
		for (CountryTreasury treasury : result) {

			data.setValue(row, 0, treasury.geteRepDay());
			Double todayRevenue = treasury.getTaxRevenue();
			setRevenueVariationData(row, data, yesterdayRevenue, todayRevenue);
			yesterdayRevenue = todayRevenue;

			row++;

			if (row >= result.size()) // last element
				lastDay = treasury.geteRepDay();
		}

		DataTableFormatter.PCT.format(data, 1);
	}

	private void setRevenueVariationData(int row,
			DataTable revenueVariationData, Double yesterdayRevenue,
			Double todayRevenue) {
		if (yesterdayRevenue != null)
			revenueVariationData.setValue(row, 1,
					getRevenueVariation(yesterdayRevenue, todayRevenue));
	}

	private Double getRevenueVariation(Double yesterdayRevenue,
			Double todayRevenue) {
		return ((todayRevenue / yesterdayRevenue) - 1) * 100;
	}

	protected void draw(final Runnable... callbacks) {

		final DataTable data = getDataTable();

		openRpcService
				.getCountryTreasuryHistory(new SimpleCallback<Collection<CountryTreasury>>()
				{
					@Override
					public void onSuccess(Collection<CountryTreasury> result) {
						fillTreasuryData(data, result);

						setControlOptions("variação da arrecadação em relação ao dia anterior");
						dashboard.draw(data);

						for (Runnable r : callbacks)
							r.run();
					}
				});
	}

	@Override
	public void update() {
		super.update();
		if (!loaded) {

			panel.remove(chart);
			panel.remove(numberRangeFilter);

			Image loadingImg = getLoadingImage();
			panel.add(loadingImg);
			draw(removeLoadingImgCallback(loadingImg, panel),
					addChartCallback(panel));
			loaded = true;
		}
	}

	@Override
	protected String getPageName() {
		return "/Treasury/Revenue/Variation";
	}
}
