package mfs.robot.client.erepublik.pages.treasury;

import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import static mfs.robot.client.erepublik.Utils.removeLoadingImgCallback;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mfs.robot.client.erepublik.DataTableFormatter;
import mfs.robot.client.erepublik.GraphDataOrganizer;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AContentsPage;
import mfs.robot.client.erepublik.pages.IOpenPage;
import mfs.robot.client.erepublik.rpc.RoBoTOpenRPCAsync;
import mfs.robot.shared.erepublik.data.CountryPresident;
import mfs.robot.shared.erepublik.dto.MonthlyRevenueDTO;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.ColumnChart;
import com.googlecode.gwt.charts.client.corechart.ColumnChartOptions;
import com.googlecode.gwt.charts.client.options.HAxis;
import com.googlecode.gwt.charts.client.options.VAxis;

public class MonthlyRevenuePage extends AContentsPage implements IOpenPage {

	private DockLayoutPanel		panel	= new DockLayoutPanel(Unit.EM);

	protected ColumnChart		chart	= new ColumnChart();

	protected RoBoTOpenRPCAsync	openRpcService;

	private MonthlyRevenueDTO	allData;

	@Override
	public void setOpenRpc(RoBoTOpenRPCAsync openRpcService) {
		this.openRpcService = openRpcService;
	}

	@Override
	public Widget getWidget() {
		panel.add(chart);
		return panel;
	}

	protected ColumnChartOptions getColumnChartOptions() {
		ColumnChartOptions chartOptions = ColumnChartOptions.create();
		chartOptions.setBackgroundColor("#EDF4F5");
		chartOptions.setTitle("Arrecadação Mensal");
		chartOptions.setVAxis(createVAxis());
		chartOptions.setHAxis(HAxis.create("Presidente"));
		return chartOptions;
	}

	private VAxis createVAxis() {
		VAxis axis = VAxis.create("Valor (BRL)");
		axis.setMinValue(0);
		return axis;
	}

	protected DataTable getDataTable() {
		final DataTable data = DataTable.create();

		data.addColumn(ColumnType.STRING, "Presidente");
		data.addColumn(ColumnType.NUMBER, "Arrecadação");

		return data;
	}

	protected void downloadAndDraw(final Runnable... callbacks) {
		if (allData != null)
			doDraw(callbacks);

		else
			openRpcService
					.getMonthlyRevenueData(new SimpleCallback<MonthlyRevenueDTO>()
					{
						@Override
						public void onSuccess(MonthlyRevenueDTO result) {

							allData = result;
							doDraw(callbacks);

						}
					});
	}

	private void doDraw(Runnable[] callbacks) {

		final DataTable data = getDataTable();

		data.addRows(allData.presidents.size());

		GraphDataOrganizer organizer = new GraphDataOrganizer();

		List<CountryPresident> reversePresidents = new ArrayList<CountryPresident>(
				allData.presidents);
		Collections.reverse(reversePresidents);

		int row = 0;
		for (CountryPresident cp : reversePresidents) {
			Double revenue = organizer.sumTaxRevenue(cp, allData.taxRevenues);
			data.setValue(row, 0, cp.getName());
			data.setValue(row, 1, revenue);
			row++;
		}

		DataTableFormatter.BRL.format(data, 1);

		chart.draw(data, getColumnChartOptions());

		for (Runnable r : callbacks)
			r.run();

	}

	@Override
	public void update() {
		super.update();

		panel.remove(chart);

		Image loadingImg = getLoadingImage();
		panel.add(loadingImg);
		downloadAndDraw(removeLoadingImgCallback(loadingImg, panel),
				addChartCallback(panel));

	}

	protected Runnable addChartCallback(final DockLayoutPanel panel) {
		return new Runnable()
		{

			@Override
			public void run() {
				panel.add(chart);
			}
		};
	}

	@Override
	protected String getPageName() {
		return "/Expenses/MonthlyRevenue";
	}

}
