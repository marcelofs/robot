package mfs.robot.client.erepublik.pages.treasury;

import static mfs.robot.client.erepublik.Utils.getLoaderSelectionHandler;
import mfs.robot.client.erepublik.pages.AContentsPage;
import mfs.robot.client.erepublik.pages.IOpenPage;
import mfs.robot.client.erepublik.rpc.RoBoTOpenRPCAsync;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

public class TreasuryPage extends AContentsPage implements IOpenPage {

	private TreasuryHistoryPage treasuryHistory;
	private RevenueHistoryPage revenueHistory;
	// private VarianceHistoryPage varianceHistory;
	// private WeeklyRevenueHistoryPage weeklyHistory;
	// private OriginHistoryPage originHistory;
	private MonthlyRevenuePage monthlyRevenue;

	protected RoBoTOpenRPCAsync openRpcService;

	@Override
	public Widget getWidget() {
		TabLayoutPanel root = new TabLayoutPanel(2.3, Unit.EM);

		treasuryHistory = new TreasuryHistoryPage();
		revenueHistory = new RevenueHistoryPage();
		// varianceHistory = new VarianceHistoryPage();
		// weeklyHistory = new WeeklyRevenueHistoryPage();
		// originHistory = new OriginHistoryPage();
		monthlyRevenue = new MonthlyRevenuePage();

		setInitialInfo(treasuryHistory, revenueHistory, monthlyRevenue);
		setOpenInfo(treasuryHistory, revenueHistory, monthlyRevenue);

		root.add(treasuryHistory.getWidget(), "Tesouro Nacional");
		root.add(revenueHistory.getWidget(), "Arrecadação");
		// root.add(varianceHistory.getWidget(),
		// "Variação Diária na Arrecadação");
		// root.add(weeklyHistory.getWidget(), "Arrecadação Média");
		root.add(monthlyRevenue.getWidget(), "Arrecadação Mensal");
		// if (isLoggedInAsMdF())
		// root.add(originHistory.getWidget(), "Estimativa de Origem");

		root.addSelectionHandler(getLoaderSelectionHandler(treasuryHistory,
				revenueHistory, monthlyRevenue));

		// TODO load based on params
		treasuryHistory.update();

		return root;
	}

	private boolean isLoggedInAsMdF() {
		return initialInfo != null && initialInfo.isMDF;
	}

	@Override
	public void setOpenRpc(RoBoTOpenRPCAsync openRpcService) {
		this.openRpcService = openRpcService;
	}

	protected void setOpenInfo(IOpenPage... pages) {
		for (IOpenPage page : pages)
			page.setOpenRpc(openRpcService);
	}
}
