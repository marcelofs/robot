package mfs.robot.client.erepublik.pages.treasury;

import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import static mfs.robot.client.erepublik.Utils.removeLoadingImgCallback;

import java.util.Map;

import mfs.robot.client.erepublik.DataTableFormatter;
import mfs.robot.client.erepublik.GraphDataOrganizer;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AHistoryPage;
import mfs.robot.client.erepublik.pages.IOpenPage;
import mfs.robot.client.erepublik.rpc.RoBoTOpenRPCAsync;
import mfs.robot.shared.erepublik.data.AgregatedJobMarketOffer;
import mfs.robot.shared.erepublik.data.CountryStats;
import mfs.robot.shared.erepublik.data.CountryTaxes;
import mfs.robot.shared.erepublik.data.CountryTreasury;
import mfs.robot.shared.erepublik.dto.TaxesOriginDTO;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ChartType;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.AreaChartOptions;
import com.googlecode.gwt.charts.client.options.AxisTitlesPosition;
import com.googlecode.gwt.charts.client.options.CoreOptions;
import com.googlecode.gwt.charts.client.options.Legend;
import com.googlecode.gwt.charts.client.options.LegendPosition;

public class OriginHistoryPage extends AHistoryPage implements IOpenPage {

	private DockLayoutPanel		panel	= new DockLayoutPanel(Unit.EM);

	protected RoBoTOpenRPCAsync	openRpcService;

	@Override
	public void setOpenRpc(RoBoTOpenRPCAsync openRpcService) {
		this.openRpcService = openRpcService;
	}

	protected ChartType getChartType() {
		return ChartType.AREA;
	}

	protected CoreOptions getControlChartOptions() {
		AreaChartOptions controlChartOptions = AreaChartOptions.create();
		controlChartOptions.setHeight(100);
		controlChartOptions.setIsStacked(true);
		controlChartOptions.setBackgroundColor("#EDF4F5");
		return controlChartOptions;
	}

	protected CoreOptions getChartOptions() {
		AreaChartOptions chartOptions = AreaChartOptions.create();
		chartOptions.setLegend(Legend.create(LegendPosition.TOP));
		chartOptions.setBackgroundColor("#EDF4F5");
		chartOptions.setAxisTitlesPosition(AxisTitlesPosition.OUT);
		chartOptions.setIsStacked(true);
		chartOptions.setVAxis(getVAxis());
		chartOptions.setHAxis(getHAxis());
		return chartOptions;
	}

	@Override
	public Widget getWidget() {

		panel.addNorth(dashboard, 0);
		panel.addSouth(numberRangeFilter, 6.25);
		panel.add(chart);

		return panel;
	}

	protected DataTable getDataTable() {
		final DataTable data = DataTable.create();

		data.addColumn(ColumnType.NUMBER, "Dia");
		data.addColumn(ColumnType.NUMBER,
				"Estimativa de arrecadação com IR (BRL)");
		data.addColumn(ColumnType.NUMBER,
				"Estimativa de arrecadação com VAT + Import (BRL)");

		return data;
	}

	protected void fillTreasuryData(DataTable data, TaxesOriginDTO result) {

		GraphDataOrganizer organizer = new GraphDataOrganizer();

		Map<Integer, CountryStats> citizensMap = organizer
				.mapOfStats(result.citizens);
		Map<Integer, AgregatedJobMarketOffer> jobsMap = organizer
				.mapOfJobMarket(result.jobMarkets);
		Map<Integer, CountryTreasury> treasurytMap = organizer
				.mapOfTreasuries(result.treasuries);
		Map<Integer, CountryTaxes> taxesMap = organizer
				.mapOfTaxes(result.taxes);

		int max = result.treasuries.size();
		data.addRows(max);

		int row = 0;
		for (int i = 1784; i < result.eRepDay; i++) {

			CountryStats citizens = citizensMap.get(i);
			AgregatedJobMarketOffer jobMarket = jobsMap.get(i);
			CountryTreasury treasury = treasurytMap.get(i);
			CountryTaxes taxes = taxesMap.get(i);

			data.setValue(row, 0, treasury.geteRepDay());

			Double irEstimate = calculateIREstimate(citizens, jobMarket, taxes);

			if (irEstimate != null) {
				Double vatEstimate = treasury.getTaxRevenue() - irEstimate;

				data.setValue(row, 1, irEstimate);
				data.setValue(row, 2, vatEstimate < 0 ? 0 : vatEstimate);
			}

			row++;

			if (row >= max) // last element
				lastDay = treasury.geteRepDay();
		}

		DataTableFormatter.BRL.format(data, 1);
		DataTableFormatter.BRL.format(data, 2);
	}

	private Double calculateIREstimate(CountryStats citizens,
			AgregatedJobMarketOffer jobMarket, CountryTaxes taxes) {

		Double militaryUnitEstimate = calculateMUEstimate(taxes);
		Double marketWorkers = calculateMarketWorkersEstimate(citizens);

		if (marketWorkers == null)
			return null;

		Double jobMarketEstimate = marketWorkers * jobMarket.getAverage()
				* (taxes.getIncomeTax() / 100d);

		return jobMarketEstimate + militaryUnitEstimate;
	}

	private Double calculateMUEstimate(CountryTaxes taxes) {

		int muMembersEstimate = getMUMembersEstimate(taxes.geteRepDay());

		return getMinimumWage(taxes.geteRepDay())
				* (taxes.getIncomeTax() / 100d) * muMembersEstimate;
	}

	private Double calculateMarketWorkersEstimate(CountryStats citizens) {

		if (citizens.getMiners() != null)
			return (citizens.getMiners() * 1.05)
					- getMUMembersEstimate(citizens.geteRepDay());

		if (citizens.getEgov4youFighters() != null)
			return (citizens.getEgov4youFighters() * 1.45)
					- getMUMembersEstimate(citizens.geteRepDay());

		return null;
	}

	private int getMUMembersEstimate(Integer day) {
		if (day < 1936)
			return 428;
		else
			return 368;
	}

	private Double getMinimumWage(int day) {
		if (day < 1884)
			return 3d;
		else
			return 1d;
	}

	protected void draw(final Runnable... callbacks) {

		final DataTable data = getDataTable();

		openRpcService
				.getCountryTaxesOriginHistory(new SimpleCallback<TaxesOriginDTO>()
				{
					@Override
					public void onSuccess(TaxesOriginDTO result) {
						fillTreasuryData(data, result);

						setControlOptions("estimativa de origem da arrecadação");
						dashboard.draw(data);

						for (Runnable r : callbacks)
							r.run();
					}
				});
	}

	@Override
	public void update() {
		super.update();
		if (!loaded) {

			panel.remove(chart);
			panel.remove(numberRangeFilter);

			Image loadingImg = getLoadingImage();
			panel.add(loadingImg);
			draw(removeLoadingImgCallback(loadingImg, panel),
					addChartCallback(panel));
			loaded = true;
		}
	}

	@Override
	protected String getPageName() {
		return "/Treasury/Revenue/Origin";
	}
}
