package mfs.robot.client.erepublik.pages.treasury;

import static mfs.robot.client.erepublik.Utils.addChangeHandler;
import static mfs.robot.client.erepublik.Utils.addStyle;
import static mfs.robot.client.erepublik.Utils.addStyles;
import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import static mfs.robot.client.erepublik.Utils.removeLoadingImgCallback;

import java.util.Collection;

import mfs.robot.client.erepublik.DataTableFormatter;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AHistoryPage;
import mfs.robot.client.erepublik.pages.IOpenPage;
import mfs.robot.client.erepublik.rpc.RoBoTOpenRPCAsync;
import mfs.robot.shared.erepublik.data.CountryTreasury;
import mfs.robot.shared.erepublik.data.MonetaryType;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;

public class TreasuryHistoryPage extends AHistoryPage implements IOpenPage {

	private DockLayoutPanel		panel		= new DockLayoutPanel(Unit.EM);
	private final ListBox		currency	= new ListBox();

	protected RoBoTOpenRPCAsync	openRpcService;

	@Override
	public void setOpenRpc(RoBoTOpenRPCAsync openRpcService) {
		this.openRpcService = openRpcService;
	}

	@Override
	public Widget getWidget() {

		panel.addNorth(dashboard, 0);
		panel.addNorth(getHeader(), 3);
		panel.addSouth(numberRangeFilter, 6.25);
		panel.add(chart);

		return panel;
	}

	private Panel getHeader() {
		Panel header = new FlowPanel();

		addStyles(header, "auto-margin", "graph-header");

		addStyle("graph-chooser", currency);

		for (MonetaryType t : MonetaryType.values())
			currency.addItem(t.name());

		header.add(currency);

		addChangeHandler(getUpdateChangeHandler(), currency);

		return header;
	}

	protected DataTable getDataTable() {
		final DataTable data = DataTable.create();

		data.addColumn(ColumnType.NUMBER, "Dia");
		data.addColumn(ColumnType.NUMBER, "Tesouro: "
				+ getSelectedCurrency().name());

		return data;
	}

	private MonetaryType getSelectedCurrency() {
		return MonetaryType.valueOf(currency.getItemText(currency
				.getSelectedIndex()));
	}

	protected void fillTreasuryData(DataTable data,
			Collection<CountryTreasury> result) {
		data.addRows(result.size());

		int row = 0;
		for (CountryTreasury treasury : result) {
			data.setValue(row, 0, treasury.geteRepDay());
			if (MonetaryType.CURRENCY.equals(getSelectedCurrency()))
				data.setValue(row, 1, treasury.getCurrencyTreasury());
			if (MonetaryType.GOLD.equals(getSelectedCurrency()))
				data.setValue(row, 1, treasury.getGoldTreasury());
			row++;

			if (row >= result.size()) // last element
				lastDay = treasury.geteRepDay();
		}

		DataTableFormatter.BRL.format(data, 1);
	}

	protected void draw(final Runnable... callbacks) {

		final DataTable data = getDataTable();

		openRpcService
				.getCountryTreasuryHistory(new SimpleCallback<Collection<CountryTreasury>>()
				{
					@Override
					public void onSuccess(Collection<CountryTreasury> result) {
						fillTreasuryData(data, result);

						setControlOptions(getSelectedCurrency().name()
								+ " no Tesouro Nacional");
						dashboard.draw(data);

						for (Runnable r : callbacks)
							r.run();
					}
				});
	}

	@Override
	public void update() {
		super.update();
		if (!loaded) {

			panel.remove(chart);
			panel.remove(numberRangeFilter);

			Image loadingImg = getLoadingImage();
			panel.add(loadingImg);
			draw(removeLoadingImgCallback(loadingImg, panel),
					addChartCallback(panel));
			loaded = true;
		}
	}

	@Override
	protected String getPageName() {
		return "/Treasury/TreasuryHistory" + getSelectedCurrency().name();
	}
}
