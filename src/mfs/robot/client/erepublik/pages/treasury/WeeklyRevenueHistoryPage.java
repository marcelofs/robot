package mfs.robot.client.erepublik.pages.treasury;

import static mfs.robot.client.erepublik.Utils.addChangeHandler;
import static mfs.robot.client.erepublik.Utils.addStyle;
import static mfs.robot.client.erepublik.Utils.addStyles;
import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import static mfs.robot.client.erepublik.Utils.removeLoadingImgCallback;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;

import mfs.robot.client.erepublik.DataTableFormatter;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AHistoryPage;
import mfs.robot.client.erepublik.pages.IOpenPage;
import mfs.robot.client.erepublik.rpc.RoBoTOpenRPCAsync;
import mfs.robot.shared.erepublik.data.CountryTreasury;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;

public class WeeklyRevenueHistoryPage extends AHistoryPage implements IOpenPage {

	private DockLayoutPanel				panel	= new DockLayoutPanel(Unit.EM);
	private final ListBox				period	= new ListBox();

	protected RoBoTOpenRPCAsync			openRpcService;

	private Collection<CountryTreasury>	revenueData;

	@Override
	public void setOpenRpc(RoBoTOpenRPCAsync openRpcService) {
		this.openRpcService = openRpcService;
	}

	@Override
	public Widget getWidget() {

		panel.addNorth(dashboard, 0);
		panel.addNorth(getHeader(), 3);
		panel.addSouth(numberRangeFilter, 6.25);
		panel.add(chart);

		return panel;
	}

	private Panel getHeader() {
		Panel header = new FlowPanel();

		addStyles(header, "auto-margin", "graph-header");

		addStyle("graph-chooser", period);

		period.addItem("30 dias", "30");
		period.addItem("14 dias", "14");
		period.addItem("7 dias", "7");
		period.addItem("5 dias", "5");
		period.addItem("3 dias", "3");

		header.add(period);

		period.setSelectedIndex(2);

		addChangeHandler(getUpdateChangeHandler(), period);

		return header;
	}

	protected DataTable getDataTable() {
		final DataTable data = DataTable.create();

		data.addColumn(ColumnType.NUMBER, "Dia");
		data.addColumn(ColumnType.NUMBER, "Arrecadação média do período (BRL)");
		return data;
	}

	protected void fillTreasuryData(DataTable data,
			Collection<CountryTreasury> result) {
		data.addRows(result.size());

		int row = 0;
		Queue<CountryTreasury> averagesQueue = new LinkedList<CountryTreasury>();
		for (CountryTreasury treasury : result) {
			setAverageRevenueData(row, data, averagesQueue, treasury);
			row++;

			if (row >= result.size()) // last element
				lastDay = treasury.geteRepDay();
		}

		DataTableFormatter.BRL.format(data, 1);
	}

	private void setAverageRevenueData(int row, DataTable averagesData,
			Queue<CountryTreasury> averagesQueue, CountryTreasury todayTreasury) {

		int periodValue = Integer.valueOf(period.getValue(period
				.getSelectedIndex()));

		if (averagesQueue.size() == periodValue)
			averagesQueue.poll();
		averagesQueue.offer(todayTreasury);

		Double averageValue = 0d;
		for (CountryTreasury t : averagesQueue)
			averageValue += t.getTaxRevenue();
		averageValue /= averagesQueue.size();

		averagesData.setValue(row, 0, todayTreasury.geteRepDay());
		averagesData.setValue(row, 1, averageValue);

	}

	protected void downloadAndDraw(final Runnable... callbacks) {
		if (revenueData != null)
			draw(callbacks);
		else
			openRpcService
					.getCountryTreasuryHistory(new SimpleCallback<Collection<CountryTreasury>>()
					{
						@Override
						public void onSuccess(Collection<CountryTreasury> result) {
							revenueData = result;
							draw(callbacks);
						}
					});
	}

	protected void draw(final Runnable... callbacks) {

		final DataTable data = getDataTable();

		fillTreasuryData(data, revenueData);

		setControlOptions("arrecadação média");
		dashboard.draw(data);

		for (Runnable r : callbacks)
			r.run();

	}

	@Override
	public void update() {
		super.update();
		if (!loaded) {

			panel.remove(chart);
			panel.remove(numberRangeFilter);

			Image loadingImg = getLoadingImage();
			panel.add(loadingImg);
			downloadAndDraw(removeLoadingImgCallback(loadingImg, panel),
					addChartCallback(panel));
			loaded = true;
		}
	}

	@Override
	protected String getPageName() {
		return "/Treasury/Revenue/Weekly";
	}
}
