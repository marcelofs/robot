package mfs.robot.client.erepublik.pages;

import com.google.gwt.user.client.ui.Widget;

public interface IPage {

	public Widget getWidget();

	public void update();

}
