package mfs.robot.client.erepublik.pages.gold;

import java.util.Collection;

import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AHistoryPage;
import mfs.robot.shared.erepublik.data.AgregatedMonetaryOffer;
import mfs.robot.shared.erepublik.data.MonetaryType;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.DataTable;
import static mfs.robot.client.erepublik.Utils.*;

public class GoldHistoryPage extends AHistoryPage {

	private DockLayoutPanel	panel	= new DockLayoutPanel(Unit.EM);

	@Override
	public Widget getWidget() {

		panel.addNorth(dashboard, 0);
		panel.addSouth(numberRangeFilter, 6.25);
		panel.add(chart);

		return panel;
	}

	protected void draw(final Runnable... callbacks) {

		final DataTable data = getDataTable();

		rpcService.getAgregatedMonetaryOffers(MonetaryType.GOLD,
				new SimpleCallback<Collection<AgregatedMonetaryOffer>>()
				{
					@Override
					public void onSuccess(
							Collection<AgregatedMonetaryOffer> result) {
						fillData(data, result);

						setControlOptions("Gold");
						dashboard.draw(data);

						for (Runnable r : callbacks)
							r.run();
					}
				});
	}

	@Override
	public void update() {
		super.update();
		if (!loaded) {

			panel.remove(chart);
			panel.remove(numberRangeFilter);

			Image loadingImg = getLoadingImage();
			panel.add(loadingImg);
			draw(removeLoadingImgCallback(loadingImg, panel),
					addChartCallback(panel));
			loaded = true;
		}
	}

	@Override
	protected String getPageName() {
		return "/Gold/History";
	}
}
