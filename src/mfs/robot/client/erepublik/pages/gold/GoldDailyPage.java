package mfs.robot.client.erepublik.pages.gold;

import static mfs.robot.client.erepublik.Utils.addStyles;
import static mfs.robot.client.erepublik.Utils.disable;
import static mfs.robot.client.erepublik.Utils.enableWidgetsCallback;
import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import static mfs.robot.client.erepublik.Utils.removeLoadingImgCallback;

import java.util.Collection;

import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.ADailyPage;
import mfs.robot.shared.erepublik.data.MonetaryOffer;
import mfs.robot.shared.erepublik.data.MonetaryType;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.DataTable;

public class GoldDailyPage extends ADailyPage {

	private DockLayoutPanel	panel	= new DockLayoutPanel(Unit.EM);

	private final ListBox	day		= new ListBox();

	@Override
	public Widget getWidget() {
		panel.addNorth(getHeader(), 2.5);
		panel.add(lines);
		return panel;
	}

	private Panel getHeader() {
		Panel header = new FlowPanel();
		addStyles(header, "auto-margin", "graph-header");

		day.addStyleName("graph-chooser");

		Integer someDay = initialInfo.eRepDay - 7;
		for (int i = 0; i <= 7; i++, someDay++)
			day.addItem(someDay + "");

		header.add(day);

		day.setSelectedIndex(7);

		day.addChangeHandler(getUpdateChangeHandler());

		return header;
	}

	protected void draw(final Integer day, final Runnable... callbacks) {

		final DataTable data = getDataTable();

		rpcService.getDailyMonetaryOffers(MonetaryType.GOLD, day,
				new SimpleCallback<Collection<MonetaryOffer>>()
				{
					@Override
					public void onSuccess(Collection<MonetaryOffer> result) {
						fillData(result, data);

						lines.draw(data, createDailyLineOpts("Gold no dia "
								+ day));

						for (Runnable r : callbacks)
							r.run();
						recordAnalyticsHit(day);
					}
				});
	}

	protected void recordAnalyticsHit(Integer day) {

		String dayTxt = "Unknown";

		if (initialInfo.eRepDay.equals(day))
			dayTxt = "Today";
		else
			dayTxt = "" + (day - initialInfo.eRepDay);

		recordAnalyticsHit("/Gold/Daily/" + dayTxt);
	}

	@Override
	public void update() {
		super.update();
		if (!loaded) {

			disable(day);

			Integer pDay = Integer.valueOf(day.getItemText(day
					.getSelectedIndex()));

			panel.remove(lines);

			Image loadingImg = getLoadingImage();
			panel.add(loadingImg);

			draw(pDay, removeLoadingImgCallback(loadingImg, panel),
					addChartCallback(panel), enableWidgetsCallback(day));

			loaded = true;
		}

	}

	@Override
	protected String getPageName() {
		return "/Gold/Daily";
	}
}
