package mfs.robot.client.erepublik.pages.accounts;

import static mfs.robot.client.erepublik.Utils.getLoaderSelectionHandler;
import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AContentsPage;
import mfs.robot.client.erepublik.pages.IOpenPage;
import mfs.robot.client.erepublik.pages.accounts.destinations.DestinationPage;
import mfs.robot.client.erepublik.pages.accounts.transactions2.TransactionsPage;
import mfs.robot.client.erepublik.rpc.RoBoTOpenRPCAsync;
import mfs.robot.shared.erepublik.ExpensesInitialInfo;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

public class AccountsPage extends AContentsPage implements IOpenPage {

	private AccountsHistoryPage		history;
	private TransactionsPage		transactions;
	private DestinationPage			destinations;

	private final TabLayoutPanel	root	= new TabLayoutPanel(2.3, Unit.EM);

	protected RoBoTOpenRPCAsync		openRpcService;

	@Override
	public void setOpenRpc(RoBoTOpenRPCAsync openRpcService) {
		this.openRpcService = openRpcService;
	}

	@Override
	public Widget getWidget() {

		final Image img = getLoadingImage();
		root.add(img, "Carregando...");

		load(img);

		return root;
	}

	private void load(final Image loadingImg) {
		openRpcService
				.getExpensesInitialInfo(new SimpleCallback<ExpensesInitialInfo>()
				{

					@Override
					public void onSuccess(ExpensesInitialInfo result) {
						root.remove(loadingImg);

						history = new AccountsHistoryPage();
						transactions = new TransactionsPage(result);
						destinations = new DestinationPage(result);

						setInitialInfo(history, transactions, destinations);
						setOpenInfo(history, transactions, destinations);

						root.add(history.getWidget(), "Histórico");
						root.add(transactions.getWidget(), "Transações");
						root.add(destinations.getWidget(), "Destinatários");

						root.addSelectionHandler(getLoaderSelectionHandler(
								history, transactions, destinations));

						if (urlParams.containsKey("tab")) {
							Integer tabIndex = Integer.valueOf(urlParams.get(
									"tab").trim());
							root.selectTab(tabIndex);
						} else
							history.update();
					}
				});
	}

	protected void setOpenInfo(IOpenPage... pages) {
		for (IOpenPage page : pages)
			page.setOpenRpc(openRpcService);
	}
}
