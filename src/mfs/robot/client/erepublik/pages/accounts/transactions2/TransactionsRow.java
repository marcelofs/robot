package mfs.robot.client.erepublik.pages.accounts.transactions2;

import java.util.Collection;

import mfs.robot.shared.erepublik.data.CitizenAccount;
import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.CountryIncome;
import mfs.robot.shared.erepublik.data.CountryMoneyTransfer;
import mfs.robot.shared.erepublik.data.MonetaryType;

public class TransactionsRow {

	private Integer								day;

	private Collection<CountryMoneyTransfer>	transfers;

	private Collection<CountryIncome>			incomes;

	private CitizenAccount						cn;

	private Collection<CountryExpense>			expenses;

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Double getTransfersValue() {
		if (transfers != null && !transfers.isEmpty()) {
			double value = 0;
			for (CountryMoneyTransfer t : transfers) {
				if (MonetaryType.CURRENCY.equals(t.getType())) {
					value += t.getValue();
				}

			}
			if (value != 0)
				return value;
		}
		return null;
	}

	public Double getTransfersGValue() {
		if (transfers != null && !transfers.isEmpty()) {
			double value = 0;
			for (CountryMoneyTransfer t : transfers) {
				if (MonetaryType.GOLD.equals(t.getType())) {
					value += t.getValue();
				}
			}
			if (value != 0)
				return value;
		}
		return null;
	}

	public Collection<CountryMoneyTransfer> getTransfers() {
		return transfers;
	}

	public void setTransfers(Collection<CountryMoneyTransfer> transfers) {
		this.transfers = transfers;
	}

	public Double getIncomesCCValue() {
		if (incomes != null && !incomes.isEmpty()) {
			double value = 0;
			for (CountryIncome t : incomes)
				if (MonetaryType.CURRENCY.equals(t.getType()))
					value += t.getValue();
			if (value != 0)
				return value;
		}
		return null;
	}

	public Double getIncomesGValue() {
		if (incomes != null && !incomes.isEmpty()) {
			double value = 0;
			for (CountryIncome t : incomes)
				if (MonetaryType.GOLD.equals(t.getType()))
					value += t.getValue();
			if (value != 0)
				return value;
		}
		return null;
	}

	public Collection<CountryIncome> getIncomes() {
		return incomes;
	}

	public void setIncomes(Collection<CountryIncome> incomes) {
		this.incomes = incomes;
	}

	public Double getCn() {
		if (cn == null)
			return null;
		return cn.getCurrency();
	}

	public Double getCnG() {
		if (cn == null)
			return null;
		return cn.getGold();
	}

	public void setCn(CitizenAccount citizenAccount) {
		this.cn = citizenAccount;

	}

	public Double getExpensesCCValue() {
		if (expenses != null && !expenses.isEmpty()) {
			double value = 0;
			for (CountryExpense t : expenses)
				if (MonetaryType.CURRENCY.equals(t.getType()))
					value += t.getValue();
			if (value != 0)
				return value;
		}
		return null;
	}

	public Double getExpensesGValue() {
		if (expenses != null && !expenses.isEmpty()) {
			double value = 0;
			for (CountryExpense t : expenses)
				if (MonetaryType.GOLD.equals(t.getType()))
					value += t.getValue();
			if (value != 0)
				return value;
		}
		return null;
	}

	public Collection<CountryExpense> getExpenses() {
		return expenses;
	}

	public void setExpenses(Collection<CountryExpense> expenses) {
		this.expenses = expenses;
	}

}
