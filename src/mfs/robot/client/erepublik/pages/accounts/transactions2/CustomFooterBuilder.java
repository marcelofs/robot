package mfs.robot.client.erepublik.pages.accounts.transactions2;

import java.util.List;

import com.google.gwt.dom.builder.shared.TableRowBuilder;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.AbstractHeaderOrFooterBuilder;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;

public class CustomFooterBuilder extends
		AbstractHeaderOrFooterBuilder<TransactionsRow> {

	private static final NumberFormat	MONETARY_FORMAT	= NumberFormat
																.getFormat("#,##0.00");

	public CustomFooterBuilder(DataGrid<TransactionsRow> dataGrid) {
		super(dataGrid, true);
	}

	@Override
	protected boolean buildHeaderOrFooterImpl() {
		String footerStyle = getTable().getResources().style().footer();

		String transfersSum = "";
		String transfersGSum = "";
		String incomeCCSum = "";
		String incomeGSum = "";
		String expensesCCSum = "";
		String expensesGSum = "";
		List<TransactionsRow> items = getTable().getVisibleItems();
		if (items.size() > 0) {
			double totalTransfer = 0;
			double totalTransferG = 0;
			double totalIncomeCC = 0;
			double totalIncomeG = 0;
			double totalExpensesCC = 0;
			double totalExpensesG = 0;
			for (TransactionsRow item : items) {
				if (item.getTransfersValue() != null)
					totalTransfer += item.getTransfersValue();
				if (item.getTransfersGValue() != null)
					totalTransferG += item.getTransfersGValue();
				if (item.getIncomesCCValue() != null)
					totalIncomeCC += item.getIncomesCCValue();
				if (item.getIncomesGValue() != null)
					totalIncomeG += item.getIncomesGValue();
				if (item.getExpensesCCValue() != null)
					totalExpensesCC += item.getExpensesCCValue();
				if (item.getExpensesGValue() != null)
					totalExpensesG += item.getExpensesGValue();
			}

			transfersSum = MONETARY_FORMAT.format(totalTransfer);
			transfersGSum = MONETARY_FORMAT.format(totalTransferG);
			incomeCCSum = MONETARY_FORMAT.format(totalIncomeCC);
			incomeGSum = MONETARY_FORMAT.format(totalIncomeG);
			expensesCCSum = MONETARY_FORMAT.format(totalExpensesCC);
			expensesGSum = MONETARY_FORMAT.format(totalExpensesG);
		}

		TableRowBuilder tr = startRow();
		tr.startTH()
				.className(footerStyle)
				.align(HasHorizontalAlignment.ALIGN_CENTER.getTextAlignString())
				.text("SOMA: ").endTH();

		tr.startTH().className(footerStyle).colSpan(2).endTH();

		tr.startTH()
				.className(footerStyle)
				.align(HasHorizontalAlignment.ALIGN_CENTER.getTextAlignString())
				.text(transfersSum).endTH();

		tr.startTH()
				.className(footerStyle)
				.align(HasHorizontalAlignment.ALIGN_CENTER.getTextAlignString())
				.text(transfersGSum).endTH();

		tr.startTH()
				.className(footerStyle)
				.align(HasHorizontalAlignment.ALIGN_CENTER.getTextAlignString())
				.text(incomeCCSum).endTH();

		tr.startTH()
				.className(footerStyle)
				.align(HasHorizontalAlignment.ALIGN_CENTER.getTextAlignString())
				.text(incomeGSum).endTH();

		tr.startTH()
				.className(footerStyle)
				.align(HasHorizontalAlignment.ALIGN_CENTER.getTextAlignString())
				.text(expensesCCSum).endTH();

		tr.startTH()
				.className(footerStyle)
				.align(HasHorizontalAlignment.ALIGN_CENTER.getTextAlignString())
				.text(expensesGSum).endTH();

		tr.endTR();

		return true;
	}
}
