package mfs.robot.client.erepublik.pages.accounts.transactions2;

import static mfs.robot.client.erepublik.Utils.addStyles;
import static mfs.robot.client.erepublik.Utils.getLoadingImage;

import java.util.Collection;

import mfs.robot.client.erepublik.Formatters;
import mfs.robot.client.erepublik.components.TransactionDetailsTablePopup;

import com.google.gwt.cell.client.ClickableTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.text.shared.AbstractSafeHtmlRenderer;
import com.google.gwt.text.shared.SafeHtmlRenderer;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.view.client.ListDataProvider;

public class TransactionsTable {

	private final DataGrid<TransactionsRow>			grid			= new DataGrid<TransactionsRow>();
	private final ListDataProvider<TransactionsRow>	dataProvider	= new ListDataProvider<TransactionsRow>();
	private final TransactionDetailsTablePopup		details			= new TransactionDetailsTablePopup();

	private static final NumberFormat				MONETARY_FORMAT	= Formatters.CURRENCY;

	private static final NumberFormat				DAY_FORMAT		= Formatters.DAY;

	public TransactionsTable() {

		grid.setHeaderBuilder(new CustomHeaderBuilder(grid));
		grid.setFooterBuilder(new CustomFooterBuilder(grid));
		grid.setLoadingIndicator(getLoadingImage());
		grid.setEmptyTableWidget(new Label("Não existem dados disponíveis ):"));
		addStyles(grid, "expense-grid", "auto-0margin");

		build();

		dataProvider.addDataDisplay(grid);
	}

	private void build() {
		SafeHtmlRenderer<String> anchorRenderer = new AbstractSafeHtmlRenderer<String>()
		{
			@Override
			public SafeHtml render(String object) {
				SafeHtmlBuilder sb = new SafeHtmlBuilder();
				sb.appendHtmlConstant("<a href=\"javascript:;\">")
						.appendEscaped(object).appendHtmlConstant("</a>");
				return sb.toSafeHtml();
			}
		};

		Column<TransactionsRow, Number> dayCol = new Column<TransactionsRow, Number>(
				new NumberCell(DAY_FORMAT))
		{

			@Override
			public Integer getValue(TransactionsRow object) {
				return object.getDay();
			}
		};

		grid.addColumn(dayCol, "Dia");
		grid.setColumnWidth(dayCol, 5, Unit.EM);

		Column<TransactionsRow, Number> nationalOrgCol = new Column<TransactionsRow, Number>(
				new NumberCell(MONETARY_FORMAT))
		{

			@Override
			public Double getValue(TransactionsRow object) {
				return object.getCn();
			}
		};
		nationalOrgCol
				.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		grid.addColumn(nationalOrgCol, "BRL");

		Column<TransactionsRow, Number> nationalOrgGCol = new Column<TransactionsRow, Number>(
				new NumberCell(MONETARY_FORMAT))
		{

			@Override
			public Double getValue(TransactionsRow object) {
				return object.getCnG();
			}
		};
		nationalOrgGCol
				.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		grid.addColumn(nationalOrgGCol, "Gold");

		Column<TransactionsRow, String> transfersCol = new Column<TransactionsRow, String>(
				new ClickableTextCell(anchorRenderer))
		{

			@Override
			public String getValue(TransactionsRow object) {
				if (object.getTransfersValue() != null)
					return MONETARY_FORMAT.format(object.getTransfersValue());
				return null;
			}
		};
		transfersCol.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

		transfersCol
				.setFieldUpdater(new FieldUpdater<TransactionsRow, String>()
				{

					@Override
					public void update(int index, TransactionsRow object,
							String value) {
						details.loadTransfers(object.getTransfers());
					}
				});
		grid.addColumn(transfersCol, "BRL");

		Column<TransactionsRow, String> transfersGCol = new Column<TransactionsRow, String>(
				new ClickableTextCell(anchorRenderer))
		{

			@Override
			public String getValue(TransactionsRow object) {
				if (object.getTransfersGValue() != null)
					return MONETARY_FORMAT.format(object.getTransfersGValue());
				return null;
			}
		};
		transfersGCol
				.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

		transfersGCol
				.setFieldUpdater(new FieldUpdater<TransactionsRow, String>()
				{

					@Override
					public void update(int index, TransactionsRow object,
							String value) {
						details.loadTransfers(object.getTransfers());
					}
				});
		grid.addColumn(transfersGCol, "Gold");

		Column<TransactionsRow, String> incomesCCCol = new Column<TransactionsRow, String>(
				new ClickableTextCell(anchorRenderer))
		{

			@Override
			public String getValue(TransactionsRow object) {
				if (object.getIncomesCCValue() != null)
					return MONETARY_FORMAT.format(object.getIncomesCCValue());
				return null;
			}
		};
		incomesCCCol.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

		incomesCCCol
				.setFieldUpdater(new FieldUpdater<TransactionsRow, String>()
				{

					@Override
					public void update(int index, TransactionsRow object,
							String value) {
						details.loadIncomes(object.getIncomes());
					}
				});
		grid.addColumn(incomesCCCol, "BRL");

		Column<TransactionsRow, String> incomesGCol = new Column<TransactionsRow, String>(
				new ClickableTextCell(anchorRenderer))
		{

			@Override
			public String getValue(TransactionsRow object) {
				if (object.getIncomesGValue() != null)
					return MONETARY_FORMAT.format(object.getIncomesGValue());
				return null;
			}
		};
		incomesGCol.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

		incomesGCol.setFieldUpdater(new FieldUpdater<TransactionsRow, String>()
		{

			@Override
			public void update(int index, TransactionsRow object, String value) {
				details.loadIncomes(object.getIncomes());
			}
		});
		grid.addColumn(incomesGCol, "Gold");

		Column<TransactionsRow, String> expensesCCCol = new Column<TransactionsRow, String>(
				new ClickableTextCell(anchorRenderer))
		{

			@Override
			public String getValue(TransactionsRow object) {
				if (object.getExpensesCCValue() != null)
					return MONETARY_FORMAT.format(object.getExpensesCCValue());
				return null;
			}
		};
		expensesCCCol
				.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		expensesCCCol
				.setFieldUpdater(new FieldUpdater<TransactionsRow, String>()
				{

					@Override
					public void update(int index, TransactionsRow object,
							String value) {
						details.loadExpenses(object.getExpenses());
					}
				});
		grid.addColumn(expensesCCCol, "BRL");

		Column<TransactionsRow, String> expensesGCol = new Column<TransactionsRow, String>(
				new ClickableTextCell(anchorRenderer))
		{

			@Override
			public String getValue(TransactionsRow object) {
				if (object.getExpensesGValue() != null)
					return MONETARY_FORMAT.format(object.getExpensesGValue());
				return null;
			}
		};
		expensesGCol.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

		expensesGCol
				.setFieldUpdater(new FieldUpdater<TransactionsRow, String>()
				{

					@Override
					public void update(int index, TransactionsRow object,
							String value) {
						details.loadExpenses(object.getExpenses());
					}
				});
		grid.addColumn(expensesGCol, "Gold");

	}

	public void set(Collection<TransactionsRow> data) {
		dataProvider.getList().clear();
		if (data != null)
			dataProvider.getList().addAll(data);
	}

	public DataGrid<TransactionsRow> getTable() {
		return grid;
	}

}
