package mfs.robot.client.erepublik.pages.accounts.transactions2;

import com.google.gwt.dom.builder.shared.TableCellBuilder;
import com.google.gwt.dom.builder.shared.TableRowBuilder;
import com.google.gwt.user.cellview.client.AbstractCellTable;
import com.google.gwt.user.cellview.client.DefaultHeaderOrFooterBuilder;

public class CustomHeaderBuilder extends
		DefaultHeaderOrFooterBuilder<TransactionsRow> {

	public CustomHeaderBuilder(AbstractCellTable<TransactionsRow> table) {
		super(table, false);

	}

	@Override
	protected boolean buildHeaderOrFooterImpl() {

		TableRowBuilder tr = startRow();
		buildFirstLineHeader(tr, 1, "");
		buildFirstLineHeader(tr, 2, "Valor de Fechamento");
		buildFirstLineHeader(tr, 2, "Transferências");
		buildFirstLineHeader(tr, 2, "Entradas");
		buildFirstLineHeader(tr, 2, "Saídas");
		tr.endTR();

		super.buildHeaderOrFooterImpl();

		return true;
	}

	private void buildFirstLineHeader(TableRowBuilder tr, int colSpan,
			String title) {
		TableCellBuilder th = tr.startTH().colSpan(colSpan);
		th.text(title).endTH();
	}

}
