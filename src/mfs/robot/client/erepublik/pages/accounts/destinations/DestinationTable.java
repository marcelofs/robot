package mfs.robot.client.erepublik.pages.accounts.destinations;

import static mfs.robot.client.erepublik.Utils.addStyles;
import static mfs.robot.client.erepublik.Utils.getLoadingImage;

import java.util.Collection;

import mfs.robot.client.erepublik.Formatters;
import mfs.robot.client.erepublik.components.TransactionDetailsTablePopup;

import com.google.gwt.cell.client.ClickableTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.text.shared.AbstractSafeHtmlRenderer;
import com.google.gwt.text.shared.SafeHtmlRenderer;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.view.client.ListDataProvider;

public class DestinationTable {

	private final DataGrid<DestinationRow>			grid			= new DataGrid<DestinationRow>();
	private final ListDataProvider<DestinationRow>	dataProvider	= new ListDataProvider<DestinationRow>();
	private final TransactionDetailsTablePopup		details			= new TransactionDetailsTablePopup();

	private static final NumberFormat				MONETARY_FORMAT	= Formatters.CURRENCY;

	public DestinationTable() {

		grid.setHeaderBuilder(new CustomHeaderBuilder(grid));
		grid.setFooterBuilder(new CustomFooterBuilder(grid));
		grid.setLoadingIndicator(getLoadingImage());
		grid.setEmptyTableWidget(new Label("Não existem dados disponíveis ):"));
		addStyles(grid, "expense-grid", "auto-0margin");

		build();

		dataProvider.addDataDisplay(grid);
	}

	private void build() {
		SafeHtmlRenderer<String> anchorRenderer = new AbstractSafeHtmlRenderer<String>()
		{
			@Override
			public SafeHtml render(String object) {
				SafeHtmlBuilder sb = new SafeHtmlBuilder();
				sb.appendHtmlConstant("<a href=\"javascript:;\">")
						.appendEscaped(object).appendHtmlConstant("</a>");
				return sb.toSafeHtml();
			}
		};

		Column<DestinationRow, String> nameCol = new TextColumn<DestinationRow>()
		{

			@Override
			public String getValue(DestinationRow object) {
				return object.getName();
			}
		};
		grid.addColumn(nameCol, "Jogador");

		Column<DestinationRow, String> expensesCCCol = new Column<DestinationRow, String>(
				new ClickableTextCell(anchorRenderer))
		{

			@Override
			public String getValue(DestinationRow object) {
				if (object.getExpensesCCValue() != null)
					return MONETARY_FORMAT.format(object.getExpensesCCValue());
				return null;
			}
		};
		expensesCCCol
				.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		expensesCCCol
				.setFieldUpdater(new FieldUpdater<DestinationRow, String>()
				{

					@Override
					public void update(int index, DestinationRow object,
							String value) {
						details.loadExpenses(object.getExpenses());
					}
				});
		grid.addColumn(expensesCCCol, "BRL");

		Column<DestinationRow, String> expensesGCol = new Column<DestinationRow, String>(
				new ClickableTextCell(anchorRenderer))
		{

			@Override
			public String getValue(DestinationRow object) {
				if (object.getExpensesGValue() != null)
					return MONETARY_FORMAT.format(object.getExpensesGValue());
				return null;
			}
		};
		expensesGCol.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

		expensesGCol.setFieldUpdater(new FieldUpdater<DestinationRow, String>()
		{

			@Override
			public void update(int index, DestinationRow object, String value) {
				details.loadExpenses(object.getExpenses());
			}
		});
		grid.addColumn(expensesGCol, "Gold");

		Column<DestinationRow, String> incomesCCCol = new Column<DestinationRow, String>(
				new ClickableTextCell(anchorRenderer))
		{

			@Override
			public String getValue(DestinationRow object) {
				if (object.getIncomesCCValue() != null)
					return MONETARY_FORMAT.format(object.getIncomesCCValue());
				return null;
			}
		};
		incomesCCCol.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

		incomesCCCol.setFieldUpdater(new FieldUpdater<DestinationRow, String>()
		{

			@Override
			public void update(int index, DestinationRow object, String value) {
				details.loadIncomes(object.getIncomes());
			}
		});
		grid.addColumn(incomesCCCol, "BRL");

		Column<DestinationRow, String> incomesGCol = new Column<DestinationRow, String>(
				new ClickableTextCell(anchorRenderer))
		{

			@Override
			public String getValue(DestinationRow object) {
				if (object.getIncomesGValue() != null)
					return MONETARY_FORMAT.format(object.getIncomesGValue());
				return null;
			}
		};
		incomesGCol.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

		incomesGCol.setFieldUpdater(new FieldUpdater<DestinationRow, String>()
		{

			@Override
			public void update(int index, DestinationRow object, String value) {
				details.loadIncomes(object.getIncomes());
			}
		});
		grid.addColumn(incomesGCol, "Gold");

	}

	public void set(Collection<DestinationRow> data) {
		dataProvider.getList().clear();
		if (data != null)
			dataProvider.getList().addAll(data);
	}

	public DataGrid<DestinationRow> getTable() {
		return grid;
	}

}
