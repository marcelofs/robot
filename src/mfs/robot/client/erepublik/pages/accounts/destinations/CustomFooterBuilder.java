package mfs.robot.client.erepublik.pages.accounts.destinations;

import java.util.List;

import com.google.gwt.dom.builder.shared.TableRowBuilder;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.AbstractHeaderOrFooterBuilder;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;

public class CustomFooterBuilder extends
		AbstractHeaderOrFooterBuilder<DestinationRow> {

	private static final NumberFormat	MONETARY_FORMAT	= NumberFormat
																.getFormat("#,##0.00");

	public CustomFooterBuilder(DataGrid<DestinationRow> dataGrid) {
		super(dataGrid, true);
	}

	@Override
	protected boolean buildHeaderOrFooterImpl() {
		String footerStyle = getTable().getResources().style().footer();

		String incomeCCSum = "";
		String incomeGSum = "";
		String expensesCCSum = "";
		String expensesGSum = "";
		List<DestinationRow> items = getTable().getVisibleItems();
		if (items.size() > 0) {

			double totalIncomeCC = 0;
			double totalIncomeG = 0;
			double totalExpensesCC = 0;
			double totalExpensesG = 0;
			for (DestinationRow item : items) {
				if (item.getIncomesCCValue() != null)
					totalIncomeCC += item.getIncomesCCValue();
				if (item.getIncomesGValue() != null)
					totalIncomeG += item.getIncomesGValue();
				if (item.getExpensesCCValue() != null)
					totalExpensesCC += item.getExpensesCCValue();
				if (item.getExpensesGValue() != null)
					totalExpensesG += item.getExpensesGValue();
			}

			incomeCCSum = MONETARY_FORMAT.format(totalIncomeCC);
			incomeGSum = MONETARY_FORMAT.format(totalIncomeG);
			expensesCCSum = MONETARY_FORMAT.format(totalExpensesCC);
			expensesGSum = MONETARY_FORMAT.format(totalExpensesG);
		}

		TableRowBuilder tr = startRow();
		tr.startTH()
				.className(footerStyle)
				.align(HasHorizontalAlignment.ALIGN_RIGHT.getTextAlignString())
				.text("SOMA: ").endTH();

		tr.startTH()
				.className(footerStyle)
				.align(HasHorizontalAlignment.ALIGN_RIGHT.getTextAlignString())
				.text(expensesCCSum).endTH();

		tr.startTH()
				.className(footerStyle)
				.align(HasHorizontalAlignment.ALIGN_RIGHT.getTextAlignString())
				.text(expensesGSum).endTH();

		tr.startTH()
				.className(footerStyle)
				.align(HasHorizontalAlignment.ALIGN_RIGHT.getTextAlignString())
				.text(incomeCCSum).endTH();

		tr.startTH()
				.className(footerStyle)
				.align(HasHorizontalAlignment.ALIGN_RIGHT.getTextAlignString())
				.text(incomeGSum).endTH();

		tr.endTR();

		return true;
	}
}
