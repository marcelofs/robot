package mfs.robot.client.erepublik.pages.accounts.destinations;

import static mfs.robot.client.erepublik.Utils.addChangeHandler;
import static mfs.robot.client.erepublik.Utils.addStyle;
import static mfs.robot.client.erepublik.Utils.addStyles;

import java.util.HashMap;
import java.util.Map;

import mfs.robot.client.erepublik.GraphDataOrganizer;
import mfs.robot.client.erepublik.Utils;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AContentsPage;
import mfs.robot.client.erepublik.pages.IOpenPage;
import mfs.robot.client.erepublik.rpc.RoBoTOpenRPCAsync;
import mfs.robot.shared.erepublik.CountryAccount;
import mfs.robot.shared.erepublik.ExpensesInitialInfo;
import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.CountryIncome;
import mfs.robot.shared.erepublik.data.CountryPresident;
import mfs.robot.shared.erepublik.dto.CashFlowDTO;

import com.google.common.collect.Multimap;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

public class DestinationPage extends AContentsPage implements IOpenPage {

	private DockLayoutPanel						panel		= new DockLayoutPanel(
																	Unit.EM);

	private DestinationTable					table		= new DestinationTable();

	private final ListBox						orgName		= new ListBox();
	private final ListBox						president	= new ListBox();

	private String								selectedOrgId;

	private final ExpensesInitialInfo			expensesInitialInfo;

	private Widget								loading		= Utils.getLoadingImage();

	protected RoBoTOpenRPCAsync					openRpcService;

	private Multimap<String, CountryExpense>	expenses;
	private Multimap<String, CountryIncome>		incomes;

	public DestinationPage(ExpensesInitialInfo result) {
		this.expensesInitialInfo = result;
	}

	@Override
	public void setOpenRpc(RoBoTOpenRPCAsync openRpcService) {
		this.openRpcService = openRpcService;
	}

	public Widget getWidget() {
		panel.addNorth(getHeader(), 2.5);
		panel.add(loading);
		return panel;
	}

	private Panel getHeader() {
		Panel header = new FlowPanel();
		addStyles(header, "auto-margin", "graph-header");

		addStyle("graph-chooser", president, orgName);

		for (CountryAccount c : CountryAccount.values())
			if (c.trackTransactions)
				orgName.addItem(c.name, c.id);

		for (CountryPresident cp : expensesInitialInfo.presidents)
			if (cp.getShowFinanceTables())
				president.addItem(cp.getName(), cp.getId() + "");
		
		president.addItem("Todos os Presidentes", "");

		header.add(orgName);
		header.add(president);

		orgName.setSelectedIndex(0);
		president.setSelectedIndex(0);

		addChangeHandler(getUpdateChangeHandler(), orgName, president);

		return header;
	}

	@Override
	public void update() {
		super.update();

		String newSelectedOrgName = orgName.getItemText(orgName
				.getSelectedIndex());
		String newSelectedOrgId = orgName.getValue(orgName.getSelectedIndex());

		if (!newSelectedOrgId.equals(selectedOrgId)) {
			selectedOrgId = newSelectedOrgId;
			openRpcService.getCashFlowData(newSelectedOrgName, selectedOrgId,
					false, false, new SimpleCallback<CashFlowDTO>()
					{

						@Override
						public void onSuccess(CashFlowDTO result) {
							organizeData(result);
							draw();
						}
					});
		} else {
			table.set(null);
			draw();
		}

	}

	private void organizeData(CashFlowDTO allData) {
		GraphDataOrganizer organizer = new GraphDataOrganizer();

		this.expenses = organizer.mapOfExpensesByDestination(allData.expenses);
		this.incomes = organizer.mapOfIncomesByOrigin(allData.incomes);
	}

	private void draw() {
		CountryPresident cp = getSelectedCP();

		Map<String, DestinationRow> adapters = new HashMap<String, DestinationRow>();

		for (String key : expenses.keySet()) {
			DestinationRow row = new DestinationRow(cp);
			row.setExpenses(expenses.get(key));
			row.setName(key);

			if (row.getExpensesCCValue() != null
					|| row.getExpensesGValue() != null)
				adapters.put(key, row);
		}

		for (String key : incomes.keySet()) {
			DestinationRow row = adapters.get(key);
			if (row == null) {
				row = new DestinationRow(cp);
				row.setName(key);
			}
			row.setIncomes(incomes.get(key));

			if (row.getIncomesCCValue() != null
					|| row.getIncomesGValue() != null)
				adapters.put(key, row);
		}

		table.set(adapters.values());
		panel.remove(loading);
		panel.add(table.getTable());
	}

	private CountryPresident getSelectedCP() {
		String cp = president.getItemText(president.getSelectedIndex());

		for (CountryPresident p : expensesInitialInfo.presidents)
			if (p.getName().equalsIgnoreCase(cp))
				return p;

		return null;
	}

	@Override
	protected String getPageName() {
		return "/Expenses/Accounts/Destination/"
				+ president.getItemText(president.getSelectedIndex());
	}

}
