package mfs.robot.client.erepublik.pages.accounts.destinations;

import com.google.gwt.dom.builder.shared.TableCellBuilder;
import com.google.gwt.dom.builder.shared.TableRowBuilder;
import com.google.gwt.user.cellview.client.AbstractCellTable;
import com.google.gwt.user.cellview.client.DefaultHeaderOrFooterBuilder;

public class CustomHeaderBuilder extends
		DefaultHeaderOrFooterBuilder<DestinationRow> {

	public CustomHeaderBuilder(AbstractCellTable<DestinationRow> table) {
		super(table, false);

	}

	@Override
	protected boolean buildHeaderOrFooterImpl() {

		TableRowBuilder tr = startRow();
		buildFirstLineHeader(tr, 1, "");
		buildFirstLineHeader(tr, 2, "Saídas");
		buildFirstLineHeader(tr, 2, "Entradas");
		tr.endTR();

		super.buildHeaderOrFooterImpl();

		return true;
	}

	private void buildFirstLineHeader(TableRowBuilder tr, int colSpan,
			String title) {
		TableCellBuilder th = tr.startTH().colSpan(colSpan);
		th.text(title).endTH();
	}

}
