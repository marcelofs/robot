package mfs.robot.client.erepublik.pages.accounts.destinations;

import java.util.Collection;

import com.google.common.collect.Lists;

import mfs.robot.client.erepublik.GraphDataOrganizer;
import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.CountryIncome;
import mfs.robot.shared.erepublik.data.CountryPresident;
import mfs.robot.shared.erepublik.data.MonetaryType;

public class DestinationRow {

	private final CountryPresident		cp;

	private String						name;

	private Iterable<CountryIncome>		incomes;

	private Iterable<CountryExpense>	expenses;

	public DestinationRow(CountryPresident cp) {
		this.cp = cp;
	}

	public Double getIncomesCCValue() {
		if (incomes != null) {
			double value = 0;
			for (CountryIncome t : incomes)
				if (MonetaryType.CURRENCY.equals(t.getType()))
					value += t.getValue();
			if (value != 0)
				return value;
		}
		return null;
	}

	public Double getIncomesGValue() {
		if (incomes != null) {
			double value = 0;
			for (CountryIncome t : incomes)
				if (MonetaryType.GOLD.equals(t.getType()))
					value += t.getValue();
			if (value != 0)
				return value;
		}
		return null;
	}

	public Collection<CountryIncome> getIncomes() {
		return Lists.newArrayList(incomes);
	}

	public void setIncomes(Collection<CountryIncome> incomes) {
		this.incomes = new GraphDataOrganizer().filterIncomesByCP(incomes, cp);
	}

	public Double getExpensesCCValue() {
		if (expenses != null) {
			double value = 0;
			for (CountryExpense t : expenses)
				if (MonetaryType.CURRENCY.equals(t.getType()))
					value += t.getValue();
			if (value != 0)
				return value;
		}
		return null;
	}

	public Double getExpensesGValue() {
		if (expenses != null) {
			double value = 0;
			for (CountryExpense t : expenses)
				if (MonetaryType.GOLD.equals(t.getType()))
					value += t.getValue();
			if (value != 0)
				return value;
		}
		return null;
	}

	public Collection<CountryExpense> getExpenses() {
		return Lists.newArrayList(expenses);
	}

	public void setExpenses(Collection<CountryExpense> expenses) {
		this.expenses = new GraphDataOrganizer().filterExpensesByCP(expenses,
				cp);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
