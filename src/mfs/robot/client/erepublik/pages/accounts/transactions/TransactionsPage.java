package mfs.robot.client.erepublik.pages.accounts.transactions;

import static mfs.robot.client.erepublik.Utils.addChangeHandler;
import static mfs.robot.client.erepublik.Utils.addStyle;
import static mfs.robot.client.erepublik.Utils.addStyles;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import mfs.robot.client.erepublik.GraphDataOrganizer;
import mfs.robot.client.erepublik.Utils;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AContentsPage;
import mfs.robot.client.erepublik.pages.IOpenPage;
import mfs.robot.client.erepublik.pages.accounts.transactions.adapters.MainRowAdapter;
import mfs.robot.client.erepublik.rpc.RoBoTOpenRPCAsync;
import mfs.robot.shared.erepublik.CountryAccount;
import mfs.robot.shared.erepublik.ExpensesInitialInfo;
import mfs.robot.shared.erepublik.data.CitizenAccount;
import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.CountryIncome;
import mfs.robot.shared.erepublik.data.CountryMoneyTransfer;
import mfs.robot.shared.erepublik.data.CountryPresident;
import mfs.robot.shared.erepublik.dto.CashFlowDTO;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

public class TransactionsPage extends AContentsPage implements IOpenPage {

	private DockLayoutPanel							panel		= new DockLayoutPanel(
																		Unit.EM);

	private TransactionsDataGrid					table		= new TransactionsDataGrid();

	private final ListBox							orgName		= new ListBox();
	private final ListBox							president	= new ListBox();

	private String									selectedOrgId;

	private final ExpensesInitialInfo				expensesInitialInfo;

	private Widget									loading		= Utils.getLoadingImage();

	protected RoBoTOpenRPCAsync						openRpcService;

	private CashFlowDTO								allData;
	private Multimap<Integer, CountryExpense>		expenses;
	private Multimap<Integer, CountryIncome>		incomes;
	private Multimap<Integer, CountryMoneyTransfer>	transfers;
	private Map<Integer, CitizenAccount>			countryOrg;

	public TransactionsPage(ExpensesInitialInfo result) {
		this.expensesInitialInfo = result;
	}

	@Override
	public void setOpenRpc(RoBoTOpenRPCAsync openRpcService) {
		this.openRpcService = openRpcService;
	}

	public Widget getWidget() {
		panel.addNorth(getHeader(), 2.5);
		panel.add(loading);
		return panel;
	}

	private Panel getHeader() {
		Panel header = new FlowPanel();
		addStyles(header, "auto-margin", "graph-header");

		addStyle("graph-chooser", president);

		for (CountryAccount c : CountryAccount.values())
			if (c.trackTransactions)
				orgName.addItem(c.name, c.id);

		for (CountryPresident cp : expensesInitialInfo.presidents)
			if (cp.getShowFinanceTables())
				president.addItem(cp.getName());

		header.add(orgName);
		header.add(president);

		orgName.setSelectedIndex(0);
		president.setSelectedIndex(0);

		addChangeHandler(getUpdateChangeHandler(), orgName, president);

		return header;
	}

	@Override
	public void update() {
		super.update();
		String newSelectedOrgName = orgName.getItemText(orgName
				.getSelectedIndex());
		String newSelectedOrgId = orgName.getValue(orgName.getSelectedIndex());
		if (allData == null || !newSelectedOrgId.equals(selectedOrgId)) {
			selectedOrgId = newSelectedOrgId;
			openRpcService.getCashFlowData(newSelectedOrgName, selectedOrgId,
					new SimpleCallback<CashFlowDTO>()
					{

						@Override
						public void onSuccess(CashFlowDTO result) {
							allData = result;
							organizeData();
							draw();
						}
					});
		} else {
			table.set(null);
			draw();
		}
	}

	private void organizeData() {
		GraphDataOrganizer organizer = new GraphDataOrganizer();

		this.expenses = organizer.mapOfExpenses(Iterables.filter(
				allData.expenses, new Predicate<CountryExpense>()
				{

					@Override
					public boolean apply(@Nullable
					CountryExpense object) {
						return orgName.getItemText(orgName.getSelectedIndex())
								.equalsIgnoreCase(object.getOrigin());
					}
				}));

		this.incomes = organizer.mapOfIncomes(Iterables.filter(allData.incomes,
				new Predicate<CountryIncome>()
				{

					@Override
					public boolean apply(@Nullable
					CountryIncome object) {
						return orgName.getItemText(orgName.getSelectedIndex())
								.equalsIgnoreCase(object.getDestination());
					}
				}));

		this.transfers = organizer.mapOfTransfers(Iterables.filter(
				allData.transfers, new Predicate<CountryMoneyTransfer>()
				{

					@Override
					public boolean apply(@Nullable
					CountryMoneyTransfer object) {
						return orgName.getItemText(orgName.getSelectedIndex())
								.equalsIgnoreCase(object.getOrigin())
								|| orgName.getItemText(
										orgName.getSelectedIndex())
										.equalsIgnoreCase(
												object.getDestination());
					}
				}));
		this.countryOrg = organizer.mapOfCitizenAccount(allData.countryOrg);
	}

	private void draw() {
		CountryPresident cp = getSelectedCP();
		List<ITransactionRow> adapters = new ArrayList<ITransactionRow>();
		for (int i = cp.getFirstErepDay(); i <= cp.getLastErepDay(); i++) {
			MainRowAdapter row = new MainRowAdapter(orgName.getItemText(orgName
					.getSelectedIndex()));
			row.setDay(i);
			if (i > cp.getFirstErepDay())
				row.setExpenses(expenses.get(i));

			row.setIncomes(incomes.get(i));
			row.setTransfers(transfers.get(i));

			CitizenAccount cOrg = countryOrg.get(i);
			row.setOrgCC(cOrg != null ? cOrg.getCurrency() : null);
			row.setOrgG(cOrg != null ? cOrg.getGold() : null);

			adapters.add(row);
		}
		table.set(adapters);
		panel.remove(loading);
		panel.add(table.getWidget());
	}

	private CountryPresident getSelectedCP() {
		String cp = president.getItemText(president.getSelectedIndex());

		for (CountryPresident p : expensesInitialInfo.presidents)
			if (p.getName().equalsIgnoreCase(cp))
				return p;

		return null;
	}

	@Override
	protected String getPageName() {
		return "/Accounts/Transactions/"
				+ president.getItemText(president.getSelectedIndex());
	}

}
