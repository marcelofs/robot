package mfs.robot.client.erepublik.pages.accounts.transactions.adapters;

import java.util.List;

import mfs.robot.client.erepublik.pages.accounts.transactions.ITransactionRow;
import mfs.robot.client.erepublik.pages.accounts.transactions.TransactionsDataGrid;
import mfs.robot.shared.erepublik.data.CountryMoneyTransfer;
import mfs.robot.shared.erepublik.data.MonetaryType;

public class TransferRowAdapter implements ITransactionRow {

	private CountryMoneyTransfer	transfer;
	private String					orgName;

	public TransferRowAdapter(String orgName, CountryMoneyTransfer transfer) {
		this.orgName = orgName;
		this.transfer = transfer;
	}

	@Override
	public Long getId() {
		return transfer.getId();
	}

	@Override
	public String geteRepDay() {
		if (transfer.getRecordedOn() != null)
			return TransactionsDataGrid.DATE_FORMAT.format(transfer
					.getRecordedOn());
		return "";
	}

	@Override
	public String getIncomesCC() {
		if (orgName.equalsIgnoreCase(transfer.getDestination()))
			if (MonetaryType.CURRENCY.equals(transfer.getType()))
				return TransactionsDataGrid.MONETARY_FORMAT.format(transfer
						.getValue());
		return "";
	}

	@Override
	public String getIncomesGold() {
		if (orgName.equalsIgnoreCase(transfer.getDestination()))
			if (MonetaryType.GOLD.equals(transfer.getType()))
				return TransactionsDataGrid.MONETARY_FORMAT.format(transfer
						.getValue());
		return null;
	}

	@Override
	public String getOrgCC() {
		return "";
	}
	
	@Override
	public String getOrgG() {
		return "";
	}

	@Override
	public String getExpensesCC() {
		if (orgName.equalsIgnoreCase(transfer.getOrigin()))
			if (MonetaryType.CURRENCY.equals(transfer.getType()))
				return TransactionsDataGrid.MONETARY_FORMAT.format(transfer
						.getValue());
		return "";
	}

	@Override
	public String getExpensesG() {
		if (orgName.equalsIgnoreCase(transfer.getOrigin()))
			if (MonetaryType.GOLD.equals(transfer.getType()))
				return TransactionsDataGrid.MONETARY_FORMAT.format(transfer
						.getValue());
		return null;
	}

	@Override
	public String getPersonResponsible() {
		return transfer.getPersonResponsible();
	}

	@Override
	public String getOrigin() {
		return transfer.getOrigin();
	}

	@Override
	public String getDestination() {
		return transfer.getDestination();
	}

	@Override
	public String getComments() {
		return transfer.getObservations();
	}

	@Override
	public List<ITransactionRow> getChildren() {
		return null;
	}

}
