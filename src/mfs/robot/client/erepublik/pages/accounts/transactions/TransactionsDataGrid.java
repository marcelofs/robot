package mfs.robot.client.erepublik.pages.accounts.transactions;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gwt.cell.client.ClickableTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.text.shared.AbstractSafeHtmlRenderer;
import com.google.gwt.text.shared.SafeHtmlRenderer;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.SelectionModel;

public class TransactionsDataGrid {

	public static final NumberFormat			MONETARY_FORMAT	= NumberFormat
																		.getFormat("#,###.00");

	public static final DateTimeFormat			DATE_FORMAT		= DateTimeFormat
																		.getFormat("yyyy.MM.dd, HH:mm:ss");

	static final ProvidesKey<ITransactionRow>	KEY_PROVIDER	= new ProvidesKey<ITransactionRow>()
																{
																	@Override
																	public Object getKey(
																			ITransactionRow item) {
																		return item == null ? null
																				: item.getId();
																	}
																};

	/*
	 * Set a key provider that provides a unique key for each contact. If key is
	 * used to identify contacts when fields (such as the name and address)
	 * change.
	 */
	DataGrid<ITransactionRow>					dataGrid		= new DataGrid<ITransactionRow>(
																		KEY_PROVIDER);

	private ListDataProvider<ITransactionRow>	dataProvider	= new ListDataProvider<ITransactionRow>();

	/**
	 * Contains the contact id for each row in the table where the friends list
	 * is currently expanded.
	 */
	private final Set<Long>						showingDetails	= new HashSet<Long>();

	private Column<ITransactionRow, String>		viewDetailsColumn;
	private Column<ITransactionRow, String>		dayColumn;
	private Column<ITransactionRow, String>		incomesCCColumn;
	private Column<ITransactionRow, String>		incomesGColumn;
	private Column<ITransactionRow, String>		orgCCColumn;
	private Column<ITransactionRow, String>		orgGColumn;
	private Column<ITransactionRow, String>		expensesCCColumn;
	private Column<ITransactionRow, String>		expensesGColumn;
	private Column<ITransactionRow, String>		originColumn;
	private Column<ITransactionRow, String>		destinationColumn;
	private Column<ITransactionRow, String>		commentsColumn;
	private Column<ITransactionRow, String>		responsibleColumn;

	public TransactionsDataGrid() {
		dataGrid.setAutoHeaderRefreshDisabled(true);
		dataGrid.setAutoFooterRefreshDisabled(true);

		// Set the message to display when the table is empty.
		dataGrid.setEmptyTableWidget(new Label("Sem dados ):"));

		// Add a selection model so we can select cells.
		final SelectionModel<ITransactionRow> selectionModel = new MultiSelectionModel<ITransactionRow>(
				KEY_PROVIDER);
		dataGrid.setSelectionModel(selectionModel, DefaultSelectionEventManager
				.<ITransactionRow> createCheckboxManager());

		initializeColumns();

		// Specify a custom table.
		dataGrid.setTableBuilder(new CustomTableBuilder(dataGrid,
				showingDetails, viewDetailsColumn, dayColumn, incomesCCColumn,
				incomesGColumn, orgCCColumn, orgGColumn, expensesCCColumn,
				expensesGColumn, responsibleColumn, originColumn,
				destinationColumn, commentsColumn));
		dataGrid.setHeaderBuilder(new CustomHeaderBuilder(dataGrid, dayColumn,
				incomesCCColumn, incomesGColumn, orgCCColumn, orgGColumn,
				expensesCCColumn, expensesGColumn, responsibleColumn,
				originColumn, destinationColumn, commentsColumn));
		dataGrid.setFooterBuilder(new CustomFooterBuilder(dataGrid));

		// Add the CellList to the adapter in the database.
		dataProvider.addDataDisplay(dataGrid);
	}

	public Widget getWidget() {
		return dataGrid;
	}

	/**
	 * Defines the columns in the custom table. Maps the data in the ContactInfo
	 * for each row into the appropriate column in the table, and defines
	 * handlers for each column.
	 */
	private void initializeColumns() {

		// View friends.
		SafeHtmlRenderer<String> anchorRenderer = new AbstractSafeHtmlRenderer<String>()
		{
			@Override
			public SafeHtml render(String object) {
				SafeHtmlBuilder sb = new SafeHtmlBuilder();
				sb.appendHtmlConstant("(<a href=\"javascript:;\">")
						.appendEscaped(object).appendHtmlConstant("</a>)");
				return sb.toSafeHtml();
			}
		};
		viewDetailsColumn = new Column<ITransactionRow, String>(
				new ClickableTextCell(anchorRenderer))
		{
			@Override
			public String getValue(ITransactionRow object) {
				if (showingDetails.contains(object.getId())) {
					return "esconder";
				} else {
					return "mostrar";
				}
			}
		};
		viewDetailsColumn
				.setFieldUpdater(new FieldUpdater<ITransactionRow, String>()
				{
					@Override
					public void update(int index, ITransactionRow object,
							String value) {
						if (showingDetails.contains(object.getId())) {
							showingDetails.remove(object.getId());
						} else {
							showingDetails.add(object.getId());
						}

						// Redraw the modified row.
						dataGrid.redrawRow(index);
					}
				});
		dataGrid.setColumnWidth(0, 6.5, Unit.EM);

		dayColumn = new Column<ITransactionRow, String>(new TextCell())
		{
			@Override
			public String getValue(ITransactionRow object) {
				return object.geteRepDay();
			}
		};
		dataGrid.setColumnWidth(1, 5, Unit.EM);

		orgCCColumn = new Column<ITransactionRow, String>(new TextCell())
		{
			@Override
			public String getValue(ITransactionRow object) {
				return object.getOrgCC();
			}
		};
		dataGrid.setColumnWidth(2, 9, Unit.EM);
		
		orgGColumn = new Column<ITransactionRow, String>(new TextCell())
				{
					@Override
					public String getValue(ITransactionRow object) {
						return object.getOrgG();
					}
				};
		dataGrid.setColumnWidth(3, 7, Unit.EM);

		incomesCCColumn = new Column<ITransactionRow, String>(new TextCell())
		{
			@Override
			public String getValue(ITransactionRow object) {
				return object.getIncomesCC();
			}
		};
		dataGrid.setColumnWidth(4, 8, Unit.EM);

		incomesGColumn = new Column<ITransactionRow, String>(new TextCell())
		{
			@Override
			public String getValue(ITransactionRow object) {
				return object.getIncomesGold();
			}
		};
		dataGrid.setColumnWidth(5, 7, Unit.EM);

		expensesCCColumn = new Column<ITransactionRow, String>(new TextCell())
		{
			@Override
			public String getValue(ITransactionRow object) {
				return object.getExpensesCC();
			}
		};
		dataGrid.setColumnWidth(6, 8, Unit.EM);

		expensesGColumn = new Column<ITransactionRow, String>(new TextCell())
		{
			@Override
			public String getValue(ITransactionRow object) {
				return object.getExpensesG();
			}
		};
		dataGrid.setColumnWidth(7, 7, Unit.EM);

		originColumn = new Column<ITransactionRow, String>(new TextCell())
		{
			@Override
			public String getValue(ITransactionRow object) {
				return object.getOrigin();
			}
		};
		dataGrid.setColumnWidth(8, 10, Unit.EM);

		destinationColumn = new Column<ITransactionRow, String>(new TextCell())
		{
			@Override
			public String getValue(ITransactionRow object) {
				return object.getDestination();
			}
		};
		dataGrid.setColumnWidth(9, 10, Unit.EM);

		commentsColumn = new Column<ITransactionRow, String>(new TextCell())
		{
			@Override
			public String getValue(ITransactionRow object) {
				return object.getComments();
			}
		};
		dataGrid.setColumnWidth(10, 100, Unit.PCT);

		responsibleColumn = new Column<ITransactionRow, String>(new TextCell())
		{
			@Override
			public String getValue(ITransactionRow object) {
				return object.getPersonResponsible();
			}
		};
		dataGrid.setColumnWidth(11, 8, Unit.EM);

	}

	public void set(List<ITransactionRow> adapters) {
		dataProvider.getList().clear();
		if (adapters != null)
			dataProvider.getList().addAll(adapters);
	}
}
