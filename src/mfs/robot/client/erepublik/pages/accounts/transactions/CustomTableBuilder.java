package mfs.robot.client.erepublik.pages.accounts.transactions;

import java.util.Collection;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.builder.shared.TableCellBuilder;
import com.google.gwt.dom.builder.shared.TableRowBuilder;
import com.google.gwt.dom.client.Style.OutlineStyle;
import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.user.cellview.client.AbstractCellTable.Style;
import com.google.gwt.user.cellview.client.AbstractCellTableBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.view.client.SelectionModel;

/**
 * Renders the data rows that display each contact in the table.
 */
public class CustomTableBuilder extends
		AbstractCellTableBuilder<ITransactionRow> {

	/**
	 * The resources used by this example.
	 */
	interface Resources extends ClientBundle {

		@Source("CustomDataGrid.css")
		Styles styles();
	}

	interface Styles extends CssResource {
		/**
		 * Indents cells in child rows.
		 */
		String childCell();

	}

	/**
	 * The resources used by this example.
	 */
	private final Resources						resources;
	private final DataGrid<ITransactionRow>		dataGrid;
	private final Set<Long>						showingDetails;

	private final Column<ITransactionRow, ?>	viewDetailsColumn;
	private final Column<ITransactionRow, ?>	dayColumn;
	private final Column<ITransactionRow, ?>	incomesCCColumn;
	private final Column<ITransactionRow, ?>	incomesGColumn;
	private final Column<ITransactionRow, ?>	orgCCColumn;
	private final Column<ITransactionRow, ?>	orgGColumn;
	private final Column<ITransactionRow, ?>	expensesCCColumn;
	private final Column<ITransactionRow, ?>	expensesGColumn;
	private final Column<ITransactionRow, ?>	responsibleColumn;
	private final Column<ITransactionRow, ?>	originColumn;
	private final Column<ITransactionRow, ?>	destinationColumn;
	private final Column<ITransactionRow, ?>	commentsColumn;

	private final String						childCell;
	private final String						rowStyle;
	private final String						selectedRowStyle;
	private final String						cellStyle;
	private final String						selectedCellStyle;

	public CustomTableBuilder(DataGrid<ITransactionRow> dataGrid,
			Set<Long> showingDetails,
			Column<ITransactionRow, ?> viewDetailsColumn,
			Column<ITransactionRow, ?> dayColumn,
			Column<ITransactionRow, ?> incomesCCColumn,
			Column<ITransactionRow, ?> incomesGColumn,
			Column<ITransactionRow, ?> orgCCColumn,
			Column<ITransactionRow, ?> orgGColumn,
			Column<ITransactionRow, ?> expensesCCColumn,
			Column<ITransactionRow, ?> expensesGColumn,
			Column<ITransactionRow, ?> responsibleColumn,
			Column<ITransactionRow, ?> originColumn,
			Column<ITransactionRow, ?> destinationColumn,
			Column<ITransactionRow, ?> commentsColumn) {

		super(dataGrid);

		resources = GWT.create(Resources.class);
		resources.styles().ensureInjected();

		this.dataGrid = dataGrid;
		this.showingDetails = showingDetails;

		this.viewDetailsColumn = viewDetailsColumn;
		this.dayColumn = dayColumn;
		this.incomesCCColumn = incomesCCColumn;
		this.incomesGColumn = incomesGColumn;
		this.orgCCColumn = orgCCColumn;
		this.orgGColumn = orgGColumn;
		this.expensesCCColumn = expensesCCColumn;
		this.expensesGColumn = expensesGColumn;
		this.responsibleColumn = responsibleColumn;
		this.originColumn = originColumn;
		this.destinationColumn = destinationColumn;
		this.commentsColumn = commentsColumn;

		// Cache styles for faster access.
		Style style = dataGrid.getResources().style();
		rowStyle = style.evenRow();
		selectedRowStyle = " " + style.selectedRow();
		cellStyle = style.cell() + " " + style.evenRowCell();
		selectedCellStyle = " " + style.selectedRowCell();
		childCell = " " + resources.styles().childCell();

	}

	@Override
	public void buildRowImpl(ITransactionRow rowValue, int absRowIndex) {
		buildContactRow(rowValue, absRowIndex, false);

		// Display information about the user in another row that spans the
		// entire
		// table.
		// Date dob = rowValue.getBirthday();
		// if (dob.getMonth() == todayMonth) {
		// TableRowBuilder row = startRow();
		// TableCellBuilder td = row.startTD().colSpan(7)
		// .className(cellStyle);
		// td.style().trustedBackgroundColor("#ccf").endStyle();
		// td.text(rowValue.getFirstName() + "'s birthday is this month!")
		// .endTD();
		// row.endTR();
		// }

		if (showingDetails.contains(rowValue.getId())) {
			Collection<ITransactionRow> children = rowValue.getChildren();
			for (ITransactionRow child : children) {
				buildContactRow(child, absRowIndex, true);
			}
		}
	}

	/**
	 * Build a row.
	 * 
	 * @param rowValue
	 *            the contact info
	 * @param absRowIndex
	 *            the absolute row index
	 * @param isChild
	 *            true if this is a subrow, false if a top level row
	 */
	private void buildContactRow(ITransactionRow rowValue, int absRowIndex,
			boolean isChild) {
		// Calculate the row styles.
		SelectionModel<? super ITransactionRow> selectionModel = dataGrid
				.getSelectionModel();
		boolean isSelected = (selectionModel == null || rowValue == null) ? false
				: selectionModel.isSelected(rowValue);
		// boolean isEven = absRowIndex % 2 == 0;
		StringBuilder trClasses = new StringBuilder(rowStyle);
		if (isSelected) {
			trClasses.append(selectedRowStyle);
		}

		// Calculate the cell styles.
		String cellStyles = cellStyle;
		if (isSelected) {
			cellStyles += selectedCellStyle;
		}
		if (isChild) {
			cellStyles += childCell;
		}

		TableRowBuilder row = startRow();
		row.className(trClasses.toString());

		TableCellBuilder td = row.startTD();
		td.className(cellStyles);
		if (!isChild && rowValue.getChildren() != null
				&& !rowValue.getChildren().isEmpty()) {
			td.style().outlineStyle(OutlineStyle.NONE).endStyle();
			renderCell(td, createContext(1), viewDetailsColumn, rowValue);
		}
		td.endTD();

		appendCell(row, cellStyles, rowValue, 2, !isChild ? 1 : 3, dayColumn,
				TextAlign.CENTER);
		if (!isChild) {
			appendCell(row, cellStyles, rowValue, 3, orgCCColumn,
					TextAlign.RIGHT);
			appendCell(row, cellStyles, rowValue, 4, orgGColumn,
					TextAlign.RIGHT);
		}
		appendCell(row, cellStyles, rowValue, 5, incomesCCColumn,
				TextAlign.RIGHT);
		appendCell(row, cellStyles, rowValue, 6, incomesGColumn,
				TextAlign.RIGHT);
		appendCell(row, cellStyles, rowValue, 7, expensesCCColumn,
				TextAlign.RIGHT);
		appendCell(row, cellStyles, rowValue, 8, expensesGColumn,
				TextAlign.RIGHT);
		appendCell(row, cellStyles, rowValue, 9, originColumn, TextAlign.CENTER);
		appendCell(row, cellStyles, rowValue, 10, destinationColumn,
				TextAlign.CENTER);
		appendCell(row, cellStyles, rowValue, 11, commentsColumn,
				TextAlign.CENTER);
		appendCell(row, cellStyles, rowValue, 12, responsibleColumn,
				TextAlign.CENTER);

		row.endTR();
	}

	private void appendCell(TableRowBuilder row, String cellStyles,
			ITransactionRow rowValue, int colNumber,
			Column<ITransactionRow, ?> column, TextAlign align) {
		appendCell(row, cellStyles, rowValue, colNumber, 1, column, align);
	}

	private void appendCell(TableRowBuilder row, String cellStyles,
			ITransactionRow rowValue, int colNumber, int colSpan,
			Column<ITransactionRow, ?> column, TextAlign align) {
		TableCellBuilder td = row.startTD();
		td.className(cellStyles);
		td.colSpan(colSpan);
		td.style().outlineStyle(OutlineStyle.NONE).textAlign(align).endStyle();
		renderCell(td, createContext(colNumber), column, rowValue);
		td.endTD();
	}
}
