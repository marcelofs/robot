package mfs.robot.client.erepublik.pages.accounts.transactions.adapters;

import java.util.List;

import mfs.robot.client.erepublik.pages.accounts.transactions.ITransactionRow;
import mfs.robot.client.erepublik.pages.accounts.transactions.TransactionsDataGrid;
import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.MonetaryType;

public class ExpenseRowAdapter implements ITransactionRow {

	private CountryExpense	expense;
	private String			orgName;

	public ExpenseRowAdapter(String orgName, CountryExpense expense) {
		this.orgName = orgName;
		this.expense = expense;
	}

	@Override
	public Long getId() {
		return expense.getId();
	}

	@Override
	public String geteRepDay() {
		if (expense.getRecordedOn() != null)
			return TransactionsDataGrid.DATE_FORMAT.format(expense
					.getRecordedOn());
		return "";
	}

	@Override
	public String getIncomesCC() {
		return "";
	}

	@Override
	public String getIncomesGold() {
		return "";
	}

	@Override
	public String getOrgCC() {
		return "";
	}

	@Override
	public String getOrgG() {
		return "";
	}

	@Override
	public String getExpensesCC() {
		if (orgName.equalsIgnoreCase(expense.getOrigin()))
			if (MonetaryType.CURRENCY.equals(expense.getType()))
				return TransactionsDataGrid.MONETARY_FORMAT.format(expense
						.getValue());
		return "";
	}

	@Override
	public String getExpensesG() {
		if (orgName.equalsIgnoreCase(expense.getOrigin()))
			if (MonetaryType.GOLD.equals(expense.getType()))
				return TransactionsDataGrid.MONETARY_FORMAT.format(expense
						.getValue());
		return "";
	}

	@Override
	public String getPersonResponsible() {
		return expense.getPersonResponsible();
	}

	@Override
	public String getOrigin() {
		return expense.getOrigin();
	}

	@Override
	public String getDestination() {
		return expense.getDestination();
	}

	@Override
	public String getComments() {
		return expense.getCategory().getName() + ": "
				+ expense.getObservations();
	}

	@Override
	public List<ITransactionRow> getChildren() {
		return null;
	}

}
