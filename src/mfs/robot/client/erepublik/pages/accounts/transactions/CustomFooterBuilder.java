package mfs.robot.client.erepublik.pages.accounts.transactions;

import com.google.gwt.dom.builder.shared.TableRowBuilder;
import com.google.gwt.user.cellview.client.AbstractHeaderOrFooterBuilder;
import com.google.gwt.user.cellview.client.DataGrid;

public class CustomFooterBuilder extends
		AbstractHeaderOrFooterBuilder<ITransactionRow> {

	public CustomFooterBuilder(DataGrid<ITransactionRow> dataGrid) {
		super(dataGrid, true);
	}

	@Override
	protected boolean buildHeaderOrFooterImpl() {
		String footerStyle = getTable().getResources().style().footer();
		TableRowBuilder tr = startRow();
		tr.startTH().colSpan(12).className(footerStyle).endTH();
		tr.endTR();
		return true;
	}
}
