package mfs.robot.client.erepublik.pages.accounts.transactions;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.dom.builder.shared.TableCellBuilder;
import com.google.gwt.dom.builder.shared.TableRowBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.AbstractCellTable.Style;
import com.google.gwt.user.cellview.client.AbstractHeaderOrFooterBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortList;
import com.google.gwt.user.cellview.client.ColumnSortList.ColumnSortInfo;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.cellview.client.SafeHtmlHeader;
import com.google.gwt.user.cellview.client.TextHeader;

/**
 * Renders custom table headers. The top header row includes the groups "Name"
 * and "Information", each of which spans multiple columns. The second row of
 * the headers includes the contacts' first and last names grouped under the
 * "Name" category. The second row also includes the age, category, and address
 * of the contacts grouped under the "Information" category.
 */
public class CustomHeaderBuilder extends
		AbstractHeaderOrFooterBuilder<ITransactionRow> {

	private Header<String>						dayHeader			= new TextHeader(
																			"Dia");

	private SafeHtmlHeader						orgCCHeader			= new SafeHtmlHeader(
																			new SafeHtmlBuilder()
																					.appendEscaped(
																							"Fechamento")
																					.appendHtmlConstant(
																							"<br/>")
																					.appendEscaped(
																							"(BRL)")
																					.toSafeHtml());
	private SafeHtmlHeader						orgGHeader			= new SafeHtmlHeader(
																			new SafeHtmlBuilder()
																					.appendEscaped(
																							"Fechamento")
																					.appendHtmlConstant(
																							"<br/>")
																					.appendEscaped(
																							"(Gold)")
																					.toSafeHtml());
	private SafeHtmlHeader						incomesCCHeader		= new SafeHtmlHeader(
																			new SafeHtmlBuilder()
																					.appendEscaped(
																							"Entradas")
																					.appendHtmlConstant(
																							"<br/>")
																					.appendEscaped(
																							"(BRL)")
																					.toSafeHtml());
	private SafeHtmlHeader						incomesGHeader		= new SafeHtmlHeader(
																			new SafeHtmlBuilder()
																					.appendEscaped(
																							"Entradas")
																					.appendHtmlConstant(
																							"<br/>")
																					.appendEscaped(
																							"(Gold)")
																					.toSafeHtml());
	private SafeHtmlHeader						expensesCCHeader	= new SafeHtmlHeader(
																			new SafeHtmlBuilder()
																					.appendEscaped(
																							"Saídas")
																					.appendHtmlConstant(
																							"<br/>")
																					.appendEscaped(
																							"(BRL)")
																					.toSafeHtml());

	private SafeHtmlHeader						expensesGoldHeader	= new SafeHtmlHeader(
																			new SafeHtmlBuilder()
																					.appendEscaped(
																							"Saídas")
																					.appendHtmlConstant(
																							"<br/>")
																					.appendEscaped(
																							"(Gold)")
																					.toSafeHtml());
	private Header<String>						responsibleHeader	= new TextHeader(
																			"Responsável");
	private Header<String>						originHeader		= new TextHeader(
																			"Origem");
	private Header<String>						destinationHeader	= new TextHeader(
																			"Destino");
	private Header<String>						commentsHeader		= new TextHeader(
																			"Observações");

	private final Column<ITransactionRow, ?>	dayColumn;
	private final Column<ITransactionRow, ?>	incomesCCColumn;
	private final Column<ITransactionRow, ?>	incomesGColumn;
	private final Column<ITransactionRow, ?>	orgCCColumn;
	private final Column<ITransactionRow, ?>	orgGColumn;
	private final Column<ITransactionRow, ?>	expensesCCColumn;
	private final Column<ITransactionRow, ?>	expensesGColumn;
	private final Column<ITransactionRow, ?>	responsibleColumn;
	private final Column<ITransactionRow, ?>	originColumn;
	private final Column<ITransactionRow, ?>	destinationColumn;
	private final Column<ITransactionRow, ?>	commentsColumn;

	protected CustomHeaderBuilder(DataGrid<ITransactionRow> dataGrid,
			Column<ITransactionRow, ?> dayColumn,

			Column<ITransactionRow, ?> incomesCCColumn,
			Column<ITransactionRow, ?> incomesGColumn,
			Column<ITransactionRow, ?> orgCCColumn,
			Column<ITransactionRow, ?> orgGColumn,
			Column<ITransactionRow, ?> expensesCCColumn,
			Column<ITransactionRow, ?> expensesGColumn,
			Column<ITransactionRow, ?> responsibleColumn,
			Column<ITransactionRow, ?> originColumn,
			Column<ITransactionRow, ?> destinationColumn,
			Column<ITransactionRow, ?> comentsColumn) {
		super(dataGrid, false);

		this.dayColumn = dayColumn;
		this.incomesCCColumn = incomesCCColumn;
		this.incomesGColumn = incomesGColumn;
		this.orgCCColumn = orgCCColumn;
		this.orgGColumn = orgGColumn;
		this.expensesCCColumn = expensesCCColumn;
		this.expensesGColumn = expensesGColumn;
		this.responsibleColumn = responsibleColumn;
		this.originColumn = originColumn;
		this.destinationColumn = destinationColumn;
		this.commentsColumn = comentsColumn;

		setSortIconStartOfLine(false);
	}

	@Override
	protected boolean buildHeaderOrFooterImpl() {
		Style style = getTable().getResources().style();

		// Add a 1x2 header above the friends columns.
		TableRowBuilder tr = startRow();
		tr.startTH().colSpan(1).rowSpan(3)
				.className(style.header() + " " + style.firstColumnHeader());
		tr.endTH();

		// Get information about the sorted column.
		ColumnSortList sortList = getTable().getColumnSortList();
		ColumnSortInfo sortedInfo = (sortList.size() == 0) ? null : sortList
				.get(0);
		Column<?, ?> sortedColumn = (sortedInfo == null) ? null : sortedInfo
				.getColumn();
		// Column<?, ?> sortedColumn = null;
		boolean isSortAscending = (sortedInfo == null) ? false : sortedInfo
				.isAscending();

		// Add column headers.
		tr = startRow();
		buildHeader(tr, dayHeader, dayColumn, sortedColumn, isSortAscending,
				false, false);
		buildHeader(tr, orgCCHeader, orgCCColumn, sortedColumn,
				isSortAscending, false, false);
		buildHeader(tr, orgGHeader, orgGColumn, sortedColumn, isSortAscending,
				false, false);
		buildHeader(tr, incomesCCHeader, incomesCCColumn, sortedColumn,
				isSortAscending, false, false);
		buildHeader(tr, incomesGHeader, incomesGColumn, sortedColumn,
				isSortAscending, false, false);
		buildHeader(tr, expensesCCHeader, expensesCCColumn, sortedColumn,
				isSortAscending, false, false);
		buildHeader(tr, expensesGoldHeader, expensesGColumn, sortedColumn,
				isSortAscending, false, false);
		buildHeader(tr, originHeader, originColumn, sortedColumn,
				isSortAscending, false, false);
		buildHeader(tr, destinationHeader, destinationColumn, sortedColumn,
				isSortAscending, false, false);
		buildHeader(tr, commentsHeader, commentsColumn, sortedColumn,
				isSortAscending, false, false);
		buildHeader(tr, responsibleHeader, responsibleColumn, sortedColumn,
				isSortAscending, false, true);

		tr.endTR();

		return true;
	}

	/**
	 * Renders the header of one column, with the given options.
	 * 
	 * @param out
	 *            the table row to build into
	 * @param header
	 *            the {@link Header} to render
	 * @param column
	 *            the column to associate with the header
	 * @param sortedColumn
	 *            the column that is currently sorted
	 * @param isSortAscending
	 *            true if the sorted column is in ascending order
	 * @param isFirst
	 *            true if this the first column
	 * @param isLast
	 *            true if this the last column
	 */
	private void buildHeader(TableRowBuilder out, Header<?> header,
			Column<ITransactionRow, ?> column, Column<?, ?> sortedColumn,
			boolean isSortAscending, boolean isFirst, boolean isLast) {
		// Choose the classes to include with the element.
		Style style = getTable().getResources().style();
		boolean isSorted = (sortedColumn == column);
		StringBuilder classesBuilder = new StringBuilder(style.header());
		if (isFirst) {
			classesBuilder.append(" " + style.firstColumnHeader());
		}
		if (isLast) {
			classesBuilder.append(" " + style.lastColumnHeader());
		}
		if (column.isSortable()) {
			classesBuilder.append(" " + style.sortableHeader());
		}
		if (isSorted) {
			classesBuilder.append(" "
					+ (isSortAscending ? style.sortedHeaderAscending() : style
							.sortedHeaderDescending()));
		}

		// Create the table cell.
		TableCellBuilder th = out.startTH().rowSpan(2)
				.className(classesBuilder.toString());

		// Associate the cell with the column to enable sorting of the
		// column.
		enableColumnHandlers(th, column);

		// Render the header.
		Context context = new Context(0, 2, header.getKey());
		renderSortableHeader(th, context, header, isSorted, isSortAscending);

		// End the table cell.
		th.endTH();
	}
}
