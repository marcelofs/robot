package mfs.robot.client.erepublik.pages.accounts.transactions.adapters;

import java.util.List;

import mfs.robot.client.erepublik.pages.accounts.transactions.ITransactionRow;
import mfs.robot.client.erepublik.pages.accounts.transactions.TransactionsDataGrid;
import mfs.robot.shared.erepublik.data.CountryIncome;
import mfs.robot.shared.erepublik.data.MonetaryType;

public class IncomeRowAdapter implements ITransactionRow {

	private CountryIncome	income;
	private String			orgName;

	public IncomeRowAdapter(String orgName, CountryIncome income) {
		this.orgName = orgName;
		this.income = income;
	}

	@Override
	public Long getId() {
		return income.getId();
	}

	@Override
	public String geteRepDay() {
		if (income.getRecordedOn() != null)
			return TransactionsDataGrid.DATE_FORMAT.format(income
					.getRecordedOn());
		return "";
	}

	@Override
	public String getIncomesCC() {
		if (orgName.equalsIgnoreCase(income.getDestination()))
			if (MonetaryType.CURRENCY.equals(income.getType()))
				return TransactionsDataGrid.MONETARY_FORMAT.format(income
						.getValue());
		return "";
	}

	@Override
	public String getIncomesGold() {
		if (orgName.equalsIgnoreCase(income.getDestination()))
			if (MonetaryType.GOLD.equals(income.getType()))
				return TransactionsDataGrid.MONETARY_FORMAT.format(income
						.getValue());
		return "";
	}

	@Override
	public String getOrgCC() {
		return "";
	}

	@Override
	public String getOrgG() {
		return "";
	}

	@Override
	public String getExpensesCC() {
		return "";
	}

	@Override
	public String getExpensesG() {
		return "";
	}

	@Override
	public String getPersonResponsible() {
		return income.getPersonResponsible();
	}

	@Override
	public String getOrigin() {
		return income.getOrigin();
	}

	@Override
	public String getDestination() {
		return income.getDestination();
	}

	@Override
	public String getComments() {
		return income.getObservations();
	}

	@Override
	public List<ITransactionRow> getChildren() {
		return null;
	}
}
