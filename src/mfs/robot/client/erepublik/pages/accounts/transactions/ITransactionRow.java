package mfs.robot.client.erepublik.pages.accounts.transactions;

import java.util.Collection;

public interface ITransactionRow {

	public abstract Long getId();

	public abstract String geteRepDay();

	public abstract String getIncomesCC();

	public abstract String getIncomesGold();

	public abstract String getOrgCC();

	public abstract String getOrgG();

	public abstract String getExpensesCC();

	public abstract String getExpensesG();

	public abstract String getPersonResponsible();

	public abstract String getOrigin();

	public abstract String getDestination();

	public abstract String getComments();

	public abstract Collection<ITransactionRow> getChildren();

}
