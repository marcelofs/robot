package mfs.robot.client.erepublik.pages.accounts.transactions.adapters;

import java.util.Collection;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import mfs.robot.client.erepublik.pages.accounts.transactions.ITransactionRow;
import mfs.robot.client.erepublik.pages.accounts.transactions.TransactionsDataGrid;
import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.CountryIncome;
import mfs.robot.shared.erepublik.data.CountryMoneyTransfer;
import mfs.robot.shared.erepublik.data.MonetaryType;

public class MainRowAdapter implements ITransactionRow {

	private Integer								day;

	private Collection<CountryMoneyTransfer>	transfers;

	private Collection<CountryIncome>			incomes;

	private Double								orgCC;

	private Double								orgG;

	private Collection<CountryExpense>			expenses;

	private final String						orgName;

	public MainRowAdapter(String orgName) {
		this.orgName = orgName;
	}

	@Override
	public String geteRepDay() {
		return day + "";
	}

	@Override
	public Long getId() {
		return day.longValue();
	}

	@Override
	public String getIncomesCC() {
		if ((incomes != null && !incomes.isEmpty())
				|| (transfers != null && !transfers.isEmpty())) {
			Double value = 0d;

			if (incomes != null)
				for (CountryIncome t : incomes)
					if (MonetaryType.CURRENCY.equals(t.getType()))
						value += t.getValue();

			if (transfers != null)
				for (CountryMoneyTransfer t : transfers)
					if (orgName.equalsIgnoreCase(t.getDestination()))
						if (MonetaryType.CURRENCY.equals(t.getType()))
							value += t.getValue();

			if (value != 0)
				return TransactionsDataGrid.MONETARY_FORMAT.format(value);
		}
		return "";
	}

	@Override
	public String getIncomesGold() {
		if ((incomes != null && !incomes.isEmpty())
				|| (transfers != null && !transfers.isEmpty())) {
			Double value = 0d;
			if (incomes != null)
				for (CountryIncome t : incomes)
					if (MonetaryType.GOLD.equals(t.getType()))
						value += t.getValue();

			if (transfers != null)
				for (CountryMoneyTransfer t : transfers)
					if (orgName.equalsIgnoreCase(t.getDestination()))
						if (MonetaryType.GOLD.equals(t.getType()))
							value += t.getValue();

			if (value != 0)
				return TransactionsDataGrid.MONETARY_FORMAT.format(value);
		}
		return "";
	}

	@Override
	public String getOrgCC() {
		if (orgCC != null)
			return TransactionsDataGrid.MONETARY_FORMAT.format(orgCC);
		return "";
	}

	@Override
	public String getOrgG() {
		if (orgG != null)
			return TransactionsDataGrid.MONETARY_FORMAT.format(orgG);
		return "";
	}

	@Override
	public String getExpensesCC() {
		if ((expenses != null && !expenses.isEmpty())
				|| (transfers != null && !transfers.isEmpty())) {
			Double value = 0d;

			if (expenses != null)
				for (CountryExpense t : expenses)
					if (MonetaryType.CURRENCY.equals(t.getType()))
						value += t.getValue();

			if (transfers != null)
				for (CountryMoneyTransfer t : transfers)
					if (orgName.equalsIgnoreCase(t.getOrigin()))
						if (MonetaryType.CURRENCY.equals(t.getType()))
							value += t.getValue();

			if (value != 0)
				return TransactionsDataGrid.MONETARY_FORMAT.format(value);
		}
		return "";
	}

	@Override
	public String getExpensesG() {
		if ((expenses != null && !expenses.isEmpty())
				|| (transfers != null && !transfers.isEmpty())) {
			Double value = 0d;

			if (expenses != null)
				for (CountryExpense t : expenses)
					if (MonetaryType.GOLD.equals(t.getType()))
						value += t.getValue();

			if (transfers != null)
				for (CountryMoneyTransfer t : transfers)
					if (orgName.equalsIgnoreCase(t.getOrigin()))
						if (MonetaryType.GOLD.equals(t.getType()))
							value += t.getValue();

			if (value != 0)
				return TransactionsDataGrid.MONETARY_FORMAT.format(value);
		}
		return "";
	}

	@Override
	public String getPersonResponsible() {
		return "";
	}

	@Override
	public String getOrigin() {
		return "";
	}

	@Override
	public String getDestination() {
		return "";
	}

	@Override
	public String getComments() {
		return "";
	}

	@Override
	public Collection<ITransactionRow> getChildren() {
		Set<ITransactionRow> records = new TreeSet<ITransactionRow>(
				new Comparator<ITransactionRow>()
				{

					@Override
					public int compare(ITransactionRow o1, ITransactionRow o2) {
						return o1.geteRepDay().compareTo(o2.geteRepDay());
					}
				});

		if (transfers != null)
			for (CountryMoneyTransfer t : transfers)
				records.add(new TransferRowAdapter(orgName, t));

		if (incomes != null)
			for (CountryIncome i : incomes)
				records.add(new IncomeRowAdapter(orgName, i));

		if (expenses != null)
			for (CountryExpense e : expenses)
				records.add(new ExpenseRowAdapter(orgName, e));

		return records;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public void setTransfers(Collection<CountryMoneyTransfer> transfers) {
		this.transfers = transfers;
	}

	public void setIncomes(Collection<CountryIncome> incomes) {
		this.incomes = incomes;
	}

	public void setOrgCC(Double cn) {
		this.orgCC = cn;
	}

	public void setOrgG(Double cn) {
		this.orgG = cn;
	}

	public void setExpenses(Collection<CountryExpense> expenses) {
		this.expenses = expenses;
	}

}
