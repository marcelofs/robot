package mfs.robot.client.erepublik.pages.accounts;

import static mfs.robot.client.erepublik.Utils.addChangeHandler;
import static mfs.robot.client.erepublik.Utils.addStyle;
import static mfs.robot.client.erepublik.Utils.addStyles;
import static mfs.robot.client.erepublik.Utils.disable;
import static mfs.robot.client.erepublik.Utils.enableWidgetsCallback;
import static mfs.robot.client.erepublik.Utils.getLoadingImage;
import static mfs.robot.client.erepublik.Utils.removeLoadingImgCallback;

import java.util.Collection;

import mfs.robot.client.erepublik.DataTableFormatter;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.pages.AHistoryPage;
import mfs.robot.client.erepublik.pages.IOpenPage;
import mfs.robot.client.erepublik.rpc.RoBoTOpenRPCAsync;
import mfs.robot.shared.erepublik.CountryAccount;
import mfs.robot.shared.erepublik.data.CitizenAccount;
import mfs.robot.shared.erepublik.data.MonetaryType;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;

public class AccountsHistoryPage extends AHistoryPage implements IOpenPage {

	private DockLayoutPanel		panel		= new DockLayoutPanel(Unit.EM);

	private Anchor				orgLink		= new Anchor("Link");

	private final ListBox		account		= new ListBox();
	private final ListBox		currency	= new ListBox();

	private RoBoTOpenRPCAsync	openRpcService;

	@Override
	public Widget getWidget() {

		panel.addNorth(dashboard, 0);
		panel.addNorth(getHeader(), 3);
		panel.addSouth(numberRangeFilter, 6.25);
		panel.add(chart);

		return panel;
	}

	@Override
	public void setOpenRpc(RoBoTOpenRPCAsync openRpcService) {
		this.openRpcService = openRpcService;
	}

	private Panel getHeader() {
		Panel header = new FlowPanel();

		addStyles(header, "auto-margin", "graph-header");

		addStyle("graph-chooser", currency, account);

		for (MonetaryType t : MonetaryType.values())
			currency.addItem(t.name().toLowerCase());

		for (CountryAccount c : CountryAccount.values())
			if (c.isPublic || initialInfo != null) // loggedIn
				account.addItem(c.prefix + " " + c.name, c.id);

		header.add(account);
		header.add(currency);
		header.add(orgLink);

		currency.setSelectedIndex(0);
		account.setSelectedIndex(0);

		addChangeHandler(getUpdateChangeHandler(), currency, account);

		return header;
	}

	protected DataTable getDataTable() {
		final DataTable data = DataTable.create();
		data.addColumn(ColumnType.NUMBER, "Dia");
		data.addColumn(ColumnType.NUMBER, "Valor");
		return data;
	}

	protected void fillAccountData(MonetaryType type, DataTable data,
			Collection<CitizenAccount> result) {
		data.addRows(result.size());

		int row = 0;
		for (CitizenAccount acc : result) {
			data.setValue(row, 0, acc.geteRepDay());
			if (type == MonetaryType.CURRENCY)
				data.setValue(row, 1, acc.getCurrency());
			else
				data.setValue(row, 1, acc.getGold());
			row++;

			if (row >= result.size()) // last element
				lastDay = acc.geteRepDay();
		}

		if (type == MonetaryType.CURRENCY)
			DataTableFormatter.BRL.format(data, 1);
		else
			DataTableFormatter.GOLD.format(data, 1);
	}

	protected void draw(final MonetaryType type, final Runnable... callbacks) {

		final DataTable data = getDataTable();

		String sID = account.getValue(account.getSelectedIndex());
		Long id = Long.valueOf(sID);

		final String name = account.getItemText(account.getSelectedIndex());

		openRpcService.getCitizenAccountHistory(id,
				new SimpleCallback<Collection<CitizenAccount>>()
				{
					@Override
					public void onSuccess(Collection<CitizenAccount> result) {
						fillAccountData(type, data, result);

						setControlOptions(type.name().toLowerCase() + " do "
								+ name);
						dashboard.draw(data);

						for (Runnable r : callbacks)
							r.run();
						recordAnalyticsHit("/Accounts/History/" + name + "/"
								+ type.name());
					}
				});
	}

	@Override
	public void update() {
		super.update();

		String sID = account.getValue(account.getSelectedIndex());

		orgLink.setHref("http://www.erepublik.com/br/citizen/profile/" + sID);

		if (sID == null || sID.trim().isEmpty())
			return;

		if (!loaded) {

			disable(account, currency);

			MonetaryType pType = MonetaryType.valueOf(currency.getItemText(
					currency.getSelectedIndex()).toUpperCase());

			panel.remove(chart);
			panel.remove(numberRangeFilter);

			Image loadingImg = getLoadingImage();
			panel.add(loadingImg);
			draw(pType, removeLoadingImgCallback(loadingImg, panel),
					addChartCallback(panel),
					enableWidgetsCallback(account, currency));
			loaded = true;
		}
	}

	@Override
	protected String getPageName() {
		return "/Accounts/History";
	}
}
