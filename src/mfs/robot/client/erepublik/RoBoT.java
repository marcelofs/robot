package mfs.robot.client.erepublik;

import static mfs.robot.client.erepublik.Utils.firebugError;
import mfs.robot.client.erepublik.components.ContentsPanel;
import mfs.robot.client.erepublik.components.FooterPanel;
import mfs.robot.client.erepublik.components.HeaderPanel;
import mfs.robot.client.erepublik.components.MenuPanel;
import mfs.robot.client.erepublik.components.SimpleCallback;
import mfs.robot.client.erepublik.rpc.RoBoTOpenRPC;
import mfs.robot.client.erepublik.rpc.RoBoTOpenRPCAsync;
import mfs.robot.client.erepublik.rpc.RoBoTRPC;
import mfs.robot.client.erepublik.rpc.RoBoTRPCAsync;
import mfs.robot.shared.erepublik.InitialInfo;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.StatusCodeException;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.googlecode.gwt.charts.client.ChartLoader;
import com.googlecode.gwt.charts.client.ChartPackage;

public class RoBoT implements EntryPoint {

	private final RoBoTRPCAsync			rpcService		= GWT.create(RoBoTRPC.class);
	private final RoBoTOpenRPCAsync		openRpcService	= GWT.create(RoBoTOpenRPC.class);

	private NavigationHandler<String>	navigation;

	private InitialInfo					initialInfo;

	public void onModuleLoad() {

		final DockLayoutPanel root = new DockLayoutPanel(Unit.EM);

		root.addNorth(new HeaderPanel().getWidget(), 7);

		final Panel loading = new FlowPanel();
		final Label initialText = new Label("Autenticando...");
		loading.addStyleName("loading");
		loading.addStyleName("auto-margin");
		loading.add(initialText);
		root.add(loading);

		rpcService.getInitialInfo(new SimpleCallback<InitialInfo>()
		{

			@Override
			public void onSuccess(InitialInfo result) {
				initialInfo = result;
				initialText.setText("Inicializando...");
				onCheckLogin(true, root, loading, initialText);
			}

			@Override
			public void onFailure(Throwable caught) {
				try {
					throw caught;
				} catch (StatusCodeException e) {
					initialText.setText("Inicializando...");
					onCheckLogin(false, root, loading, initialText);
				} catch (Throwable e) {
					super.onFailure(e);
				}
			}
		});

		RootLayoutPanel.get().add(root);

	} 

	private void onCheckLogin(final boolean isLoggedIn,
			final DockLayoutPanel root, final Panel loading,
			final Label initialText) {
		GWT.runAsync(new RunAsyncCallback()
		{

			@Override
			public void onSuccess() {

				initialText.setText(isLoggedIn ? "Seja bem vind@, "
						+ initialInfo.eRepName + ". Carregando gráficos..."
						: "Carregando gráficos...");

				ChartLoader loader = new ChartLoader(ChartPackage.CONTROLS,
						ChartPackage.CORECHART);
				loader.loadApi(new Runnable()
				{
					@Override
					public void run() {
						root.remove(loading);
						onChartsLoaded(isLoggedIn, root);
					}
				});
			}

			@Override
			public void onFailure(Throwable reason) {
				initialText.setText("Falha na inicialização do aplicativo: \n"
						+ reason.getMessage());
				firebugError(reason.getMessage());
			}
		});

	}

	private void onChartsLoaded(boolean isLoggedIn, DockLayoutPanel root) {

		MenuPanel menu = new MenuPanel(isLoggedIn, initialInfo);
		root.addWest(menu.getWidget(), 15);

		if (isLoggedIn)
			root.addSouth(new FooterPanel(rpcService).getWidget(), 4);

		ContentsPanel contents = new ContentsPanel(rpcService, openRpcService,
				initialInfo);
		root.add(contents.getWidget());

		navigation = new NavigationHandler<String>(contents, menu);

		initNavigation();
	}

	private void initNavigation() {
		History.addValueChangeHandler(navigation);

		// useful if a user has bookmarked a URL other than the homepage
		if (History.getToken().isEmpty())
			History.newItem(Pages.home.name());
		else
			navigation.changePage(History.getToken());
	}
}
