package mfs.robot.client.erepublik;

import mfs.robot.client.erepublik.pages.IPage;

import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.FocusWidget;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;

public class Utils {

	public static void disable(FocusWidget... widgets) {
		for (FocusWidget widget : widgets)
			widget.setEnabled(false);
	}

	public static void setVisible(boolean visible, Widget... widgets) {
		for (Widget widget : widgets)
			widget.setVisible(visible);
	}

	public static Runnable enableWidgetsCallback(final FocusWidget... widgets) {
		return new Runnable()
		{

			@Override
			public void run() {
				for (FocusWidget widget : widgets)
					widget.setEnabled(true);
			}

		};
	}

	public static void addStyle(String style, UIObject... objects) {
		for (UIObject obj : objects)
			obj.addStyleName(style);
	}

	public static void addStyles(UIObject obj, String... styles) {
		for (String style : styles)
			obj.addStyleName(style);
	}

	public static void addChangeHandler(ChangeHandler handler,
			HasChangeHandlers... objs) {
		for (HasChangeHandlers obj : objs)
			obj.addChangeHandler(handler);
	}

	public static SelectionHandler<Integer> getLoaderSelectionHandler(
			final IPage... pages) {
		return new SelectionHandler<Integer>()
		{

			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				for (int i = 0; i < pages.length; i++)
					if (event.getSelectedItem() == i)
						pages[i].update();
			}
		};
	}

	public static Image getLoadingImage() {
		Image loadingImg = new Image("loading.cache.gif");
		loadingImg.addStyleName("auto-margin");
		return loadingImg;
	}

	public static Image getLogoImage() {
		Image loadingImg = new Image("logo-beta.cache.png");
		loadingImg.addStyleName("auto-margin");
		return loadingImg;
	}

	public static Runnable removeLoadingImgCallback(final Image loadingImg,
			final Panel panel) {
		return new Runnable()
		{

			@Override
			public void run() {
				panel.remove(loadingImg);
			}

		};
	}

	public static native void firebugLog(String message) /*-{
		if ($wnd.console.log) {
			$wnd.console.log(message);
		}
	}-*/;

	public static native void firebugError(String message) /*-{
		if ($wnd.console.error) {
			$wnd.console.error(message);
		}
	}-*/;

}
