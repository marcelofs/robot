package mfs.robot.client.erepublik;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;

public class Formatters {

	public static final NumberFormat	CURRENCY	= NumberFormat
															.getFormat("#,###.00");

	public static final NumberFormat	DAY			= NumberFormat
															.getFormat("####");

	public static final DateTimeFormat	TIME		= DateTimeFormat
															.getFormat("yyyy.MM.dd, HH:mm:ss");
}
