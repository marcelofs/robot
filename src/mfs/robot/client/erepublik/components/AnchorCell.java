package mfs.robot.client.erepublik.components;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Hyperlink;

/**
 * {@link Cell} that wraps a {@link Hyperlink} WARNING: make sure the contents
 * of your Hyperlink really are safe from XSS!
 * 
 * @author turbomanage
 *         http://code.google.com/p/listwidget/source/browse/trunk/src
 *         /main/java/
 *         com/listwidget/client/ui/HyperlinkCell.java?spec=svn12&r=12
 */
public class AnchorCell extends AbstractCell<Anchor> {

	@Override
	public void render(com.google.gwt.cell.client.Cell.Context context,
			Anchor h, SafeHtmlBuilder sb) {
		sb.append(SafeHtmlUtils.fromTrustedString(h.toString()));
	}

}
