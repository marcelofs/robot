package mfs.robot.client.erepublik.components;

import mfs.robot.shared.erepublik.CountryAccount;
import mfs.robot.shared.mdf.MdFInitialInfo;

import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox;

public class OrgSuggestBox extends SuggestBox {

	private static MultiWordSuggestOracle	oracle;

	public OrgSuggestBox(MdFInitialInfo info) {
		super(getOracle(info));
	}

	private static MultiWordSuggestOracle getOracle(MdFInitialInfo info) {
		if (oracle == null) {
			oracle = new MultiWordSuggestOracle();

			oracle.add("Tesouro Nacional");
			oracle.add("Mercado Monetário");
			oracle.add("Mercado");

			for (CountryAccount acc : CountryAccount.values())
				oracle.add(acc.name);

			for (String user : info.mdfUsers)
				oracle.add(user);

		}
		return oracle;
	}

}
