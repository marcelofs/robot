package mfs.robot.client.erepublik.components;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

public class HeaderPanel {

	public Widget getWidget() {
		Panel panel = new FlowPanel();
		panel.addStyleName("header-panel");

		Label h1 = new HTML("RoBoT<sub><i>βeta</i></sub>");
		h1.addStyleName("h1");
		panel.add(h1);

		Label h2 = new Label("Análise de mercado para o eRepublik Brasil");
		h2.addStyleName("h2");
		panel.add(h2);

		return panel;
	}

}
