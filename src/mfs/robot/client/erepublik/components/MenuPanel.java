package mfs.robot.client.erepublik.components;

import static mfs.robot.client.erepublik.Utils.addStyle;
import static mfs.robot.client.erepublik.Utils.addStyles;

import java.util.HashMap;
import java.util.Map;

import mfs.robot.client.erepublik.Pages;
import mfs.robot.shared.erepublik.InitialInfo;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

public class MenuPanel {

	private final InitialInfo			initialInfo;
	private final boolean				isLoggedIn;
	private final Map<Pages, Hyperlink>	pages;

	public MenuPanel(boolean isLoggedIn, InitialInfo initialInfo) {
		this.isLoggedIn = isLoggedIn;
		this.initialInfo = initialInfo;
		this.pages = new HashMap<Pages, Hyperlink>();
	}

	public Widget getWidget() {
		Panel panel = new FlowPanel();
		panel.addStyleName("menu");

		panel.add(getMenuLink("Início", Pages.home));
		// panel.add(getMenuLink("Banco Liberty", Pages.liberty));

		if (isLoggedIn) {
			panel.add(getMenuTitle("Economia"));
			panel.add(getMenuLink("Mercado", Pages.marketplace));
			panel.add(getMenuLink("Salários", Pages.jobs));
			panel.add(getMenuLink("Gold", Pages.gold));
			// if (initialInfo.isAdmin)
			// panel.add(getMenuLink("Retorno das Fábricas", Pages.profit));
			//panel.add(getMenuLink("Rockets", Pages.rockets));
			//panel.add(getMenuLink("Índices", Pages.indexes));
		}

		panel.add(getMenuTitle("Governo"));
		panel.add(getMenuLink("Arrecadação e Tesouro", Pages.treasury));
		panel.add(getMenuLink("Gastos do Governo", Pages.expenses));
		panel.add(getMenuLink("Organizações Nacionais", Pages.orgs));

		if (isLoggedIn) {
			panel.add(getMenuTitle("Brasil"));
			panel.add(getMenuLink("Partidos", Pages.politics));
			panel.add(getMenuLink("Cidadãos", Pages.citizens));
			panel.add(getMenuLink("Impostos", Pages.taxes));
			//panel.add(getMenuLink("Bônus", Pages.bonus));

			if (initialInfo.isMDF || initialInfo.isAdmin)
				panel.add(getMenuTitle("Outros"));

			if (initialInfo.isMDF)
				panel.add(getMenuLink("Ministério da Fazenda", Pages.mdf));

			if (initialInfo.isAdmin)
				panel.add(getMenuLink("Administração de Usuários", Pages.admin));

			panel.add(getLogoutLink());
		}

		panel.add(getFillerPanel());

		return panel;
	}

	private Panel getFillerPanel() {

		Panel filler = new FlowPanel();

		if (!isLoggedIn) {
			Label ident = new Label("Você não está identificado. Por favor...");
			Anchor login = new Anchor("Faça o login", "/login/check");
			Label or = new Label("ou");
			Anchor register = new Anchor("Registre-se", "/login/check");
			Label more = new Label(
					"...para visualizar mais informações. Dúvidas?");
			Label doubts = new Label("Canal #RoBoT, rede Rizon.");

			addStyle("auto-margin", ident, login, or, register, more, doubts);

			filler.add(ident);
			filler.add(login);
			filler.add(or);
			filler.add(register);
			filler.add(more);
			filler.add(doubts);
		}

		filler.addStyleName("filler");

		return filler;
	}

	private Widget getMenuLink(String name, Pages token) {
		Hyperlink link = new Hyperlink(name, token.name());
		addStyles(link, "menu-link", "menu-border");
		pages.put(token, link);
		return link;
	}

	private Widget getMenuTitle(String title) {
		Label lbl = new Label(title);
		addStyles(lbl, "menu-title", "menu-border");
		return lbl;
	}

	private Widget getLogoutLink() {
		Anchor link = new Anchor("Logout", "javascript:;");
		link.addClickHandler(new ClickHandler()
		{

			@Override
			public void onClick(ClickEvent event) {
				boolean isLogout = Window
						.confirm("Você fará logout da sua conta no Google");
				if (isLogout)
					Window.Location.assign("/logout");
			}
		});

		FlowPanel div = new FlowPanel();
		addStyles(div, "menu-link", "menu-border");
		div.add(link);

		return div;
	}

	public void goToPage(Pages previous, Pages destination) {

		if (previous != null) {
			Hyperlink prevLink = pages.get(previous);
			prevLink.removeStyleName("selected-menu-link");
			prevLink.addStyleName("menu-border");
		}

		Hyperlink destLink = pages.get(destination);
		if (destLink != null) {
			destLink.addStyleName("selected-menu-link");
			destLink.removeStyleName("menu-border");
		}
	}

}
