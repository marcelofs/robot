package mfs.robot.client.erepublik.components;

import mfs.robot.shared.mdf.MdFInitialInfo;

import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox;

public class MdFUsersSuggestBox extends SuggestBox {

	private static MultiWordSuggestOracle	oracle;

	public MdFUsersSuggestBox(MdFInitialInfo info) {
		super(getOracle(info));
	}

	private static MultiWordSuggestOracle getOracle(MdFInitialInfo info) {
		if (oracle == null) {
			oracle = new MultiWordSuggestOracle();

			for (String user : info.mdfUsers)
				oracle.add(user);
		}
		return oracle;
	}

}
