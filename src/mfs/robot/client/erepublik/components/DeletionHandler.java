package mfs.robot.client.erepublik.components;

public interface DeletionHandler<T> {

	public void onDelete(T object);

}
