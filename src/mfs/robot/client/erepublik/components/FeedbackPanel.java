package mfs.robot.client.erepublik.components;

import mfs.robot.client.erepublik.rpc.RoBoTRPCAsync;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Panel;

public class FeedbackPanel {

	private final RoBoTRPCAsync		rpcService;
	private final Panel				content;
	private final LabeledTextBox	text	= new LabeledTextBox(
													"Sugestões, bugs, informações que você gostaria? Envie seu comentário!");
	private final Button			send	= new Button("Enviar");

	public FeedbackPanel(RoBoTRPCAsync rpcService) {
		this.rpcService = rpcService;
		this.content = new FlowPanel();
		content.addStyleName("feedback");
		content.addStyleName("auto-margin");
		load();
	}

	protected void load() {

		send.addClickHandler(new ClickHandler()
		{

			@Override
			public void onClick(ClickEvent event) {

				if (!text.validate(5))
					return;

				text.setEnabled(false);
				send.setEnabled(false);
				String message = text.getText();
				rpcService.sendFeedback(message, History.getToken(), new SimpleCallback<Void>()
				{

					@Override
					public void onSuccess(Void result) {
						text.setText("Seu feedback foi enviado. Obrigado!");
						Timer timer = new Timer()
						{
							@Override
							public void run() {
								text.setText("");
								text.setEnabled(true);
								send.setEnabled(true);
							}

						};
						timer.schedule(15000); // 15s
					}
				});
			}
		});

		content.add(text);
		content.add(send);
	}

	public Panel getContent() {
		return content;
	}

}
