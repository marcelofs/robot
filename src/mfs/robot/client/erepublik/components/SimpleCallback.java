package mfs.robot.client.erepublik.components;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.StatusCodeException;

import static mfs.robot.client.erepublik.Utils.*;

public abstract class SimpleCallback<E> implements AsyncCallback<E> {

	@Override
	public void onFailure(Throwable caught) {
		try {
			throw caught;
		} catch (StatusCodeException e) {

			int code = e.getStatusCode();
			switch (code) {
				case 0:
				case 401:
					// must login
					Window.Location.assign("http://"
							+ Window.Location.getHost() + "/login/check");
					break;
				case 403:
					addError("Usuário não autorizado.");
					break;
				default:
					addError("Erro desconhecido, status " + code);
			}

		} catch (Throwable e) {
			addError(e);
		}
	}

	protected void addError(Throwable caught) {
		addError("ERROR " + caught.getClass() + ":  " + caught.getMessage());
	}

	protected void addError(String message) {
		firebugError(message);
		Window.alert(message);
	}

}
