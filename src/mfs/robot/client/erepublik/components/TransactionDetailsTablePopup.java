package mfs.robot.client.erepublik.components;

import static mfs.robot.client.erepublik.Utils.setVisible;

import java.util.Collection;

import mfs.robot.client.erepublik.components.tables.TransactionTable;
import mfs.robot.client.erepublik.components.tables.TransactionWithCategoryTable;
import mfs.robot.shared.erepublik.ITransaction;
import mfs.robot.shared.erepublik.data.CountryExpense;
import mfs.robot.shared.erepublik.data.CountryIncome;
import mfs.robot.shared.erepublik.data.CountryMoneyTransfer;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ScrollPanel;

public class TransactionDetailsTablePopup {

	private final TransactionWithCategoryTable	withCatTable		= new TransactionWithCategoryTable();
	private final TransactionTable				transactionTable	= new TransactionTable();

	private final PopupPanel					root				= new PopupPanel(
																			true,
																			true);

	public TransactionDetailsTablePopup() {

		root.setAnimationEnabled(true);
		root.setGlassEnabled(true);

		FlowPanel panel = new FlowPanel();

		panel.add(withCatTable.getTable());
		panel.add(transactionTable.getTable());

		setVisible(false, withCatTable.getTable(), transactionTable.getTable());

		ScrollPanel scroll = new ScrollPanel();
		scroll.setHeight(Window.getClientHeight() / 2 + "px");
		scroll.setWidth(Window.getClientWidth() * 0.8 + "px");
		scroll.setWidget(panel);

		root.setWidget(scroll);

	}

	public void loadExpenses(Collection<CountryExpense> objects) {
		if (objects != null && !objects.isEmpty()) {
			withCatTable.set(objects);
			setVisible(true, withCatTable.getTable());
			setVisible(false, transactionTable.getTable());
			root.show();
			root.center();
		}
	}

	public void loadIncomes(Collection<CountryIncome> objects) {
		loadWithoutCat(objects);
	}

	public void loadTransfers(Collection<CountryMoneyTransfer> objects) {
		loadWithoutCat(objects);
	}

	private void loadWithoutCat(Collection<? extends ITransaction> objects) {
		if (objects != null && !objects.isEmpty()) {
			transactionTable.set(objects);
			setVisible(true, transactionTable.getTable());
			setVisible(false, withCatTable.getTable());
			root.show();
			root.center();
		}
	}

}
