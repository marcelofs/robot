package mfs.robot.client.erepublik.components;

import java.util.Map;

import mfs.robot.client.erepublik.Pages;
import mfs.robot.client.erepublik.pages.AContentsPage;
import mfs.robot.client.erepublik.pages.IOpenPage;
import mfs.robot.client.erepublik.rpc.RoBoTOpenRPCAsync;
import mfs.robot.client.erepublik.rpc.RoBoTRPCAsync;
import mfs.robot.shared.erepublik.InitialInfo;

import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.ResizeLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

public class ContentsPanel {

	private final RoBoTRPCAsync		rpcService;
	private final RoBoTOpenRPCAsync	openRpcService;

	private AContentsPage			currentPage;

	private final Panel				parentPanel	= new ResizeLayoutPanel();

	private final InitialInfo		initialInfo;

	public ContentsPanel(RoBoTRPCAsync rpcService,
			RoBoTOpenRPCAsync openRpcService, InitialInfo initialInfo) {
		this.rpcService = rpcService;
		this.openRpcService = openRpcService;
		this.initialInfo = initialInfo;
		parentPanel.addStyleName("content");
	}

	public void goToPage(Pages page, Map<String, String> parameters) {

		parentPanel.clear();

		currentPage = page.getPage();

		currentPage.setRpcService(rpcService);
		currentPage.setUrlParams(parameters).update();
		currentPage.setInitialInfo(initialInfo);

		if (currentPage instanceof IOpenPage)
			((IOpenPage) currentPage).setOpenRpc(openRpcService);

		parentPanel.add(currentPage.getWidget());

	}

	public Widget getWidget() {
		return parentPanel;
	}

}
