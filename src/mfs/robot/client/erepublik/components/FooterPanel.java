package mfs.robot.client.erepublik.components;

import mfs.robot.client.erepublik.rpc.RoBoTRPCAsync;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

public class FooterPanel {

	private final RoBoTRPCAsync	rpcService;

	public FooterPanel(RoBoTRPCAsync rpcService) {
		this.rpcService = rpcService;
	}

	public Widget getWidget() {
		Panel panel = new FlowPanel();
		panel.addStyleName("footer");

		panel.add(new FeedbackPanel(rpcService).getContent());

		Panel notice = new FlowPanel();
		notice.add(new HTML(
				"<small>O uso dos gráficos é livre em artigos que contenham um link para o site. Para outros usos, entre em contato.</small>"));
		notice.addStyleName("center-text");
		panel.add(notice);

		return panel;
	}

}
