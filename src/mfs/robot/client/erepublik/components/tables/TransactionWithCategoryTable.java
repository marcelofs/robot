package mfs.robot.client.erepublik.components.tables;

import static mfs.robot.client.erepublik.Utils.addStyles;
import static mfs.robot.client.erepublik.Utils.getLoadingImage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import mfs.robot.client.erepublik.Formatters;
import mfs.robot.shared.erepublik.ITransactionWithCategory;
import mfs.robot.shared.erepublik.data.MonetaryType;

import com.google.gwt.cell.client.DateCell;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;

public class TransactionWithCategoryTable {

	protected final CellTable<ITransactionWithCategory>	grid	= new CellTable<ITransactionWithCategory>();

	public TransactionWithCategoryTable() {
		build();
		grid.setLoadingIndicator(getLoadingImage());
		addStyles(grid, "expense-grid", "auto-0margin");
	}

	protected void build() {
		Column<ITransactionWithCategory, Number> dayCol = new Column<ITransactionWithCategory, Number>(
				new NumberCell(Formatters.DAY))
		{

			@Override
			public Integer getValue(ITransactionWithCategory object) {
				return object.geteRepDay();
			}
		};
		grid.addColumn(dayCol, "Dia");
		dayCol.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);

		Column<ITransactionWithCategory, Number> goldValueCol = new Column<ITransactionWithCategory, Number>(
				new NumberCell(Formatters.CURRENCY))
		{

			@Override
			public Double getValue(ITransactionWithCategory object) {
				if (object.getType() != null
						&& object.getType() == MonetaryType.GOLD
						&& object.getValue() != 0)
					return object.getValue();
				return null;
			}
		};
		grid.addColumn(goldValueCol, "Valor (G)");
		goldValueCol.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

		Column<ITransactionWithCategory, Number> ccValueCol = new Column<ITransactionWithCategory, Number>(
				new NumberCell(Formatters.CURRENCY))
		{

			@Override
			public Double getValue(ITransactionWithCategory object) {
				if (object.getType() != null
						&& object.getType() == MonetaryType.CURRENCY
						&& object.getValue() != 0)
					return object.getValue();
				return null;
			}
		};
		grid.addColumn(ccValueCol, "Valor (BRL)");
		ccValueCol.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

		TextColumn<ITransactionWithCategory> responsibleCol = new TextColumn<ITransactionWithCategory>()
		{

			@Override
			public String getValue(ITransactionWithCategory object) {
				return object.getPersonResponsible();
			}
		};
		grid.addColumn(responsibleCol, "Responsável");

		TextColumn<ITransactionWithCategory> originCol = new TextColumn<ITransactionWithCategory>()
		{

			@Override
			public String getValue(ITransactionWithCategory object) {
				return object.getOrigin();
			}
		};
		grid.addColumn(originCol, "Origem");

		TextColumn<ITransactionWithCategory> destinationCol = new TextColumn<ITransactionWithCategory>()
		{

			@Override
			public String getValue(ITransactionWithCategory object) {
				return object.getDestination();
			}
		};
		grid.addColumn(destinationCol, "Destino");

		TextColumn<ITransactionWithCategory> catCol = new TextColumn<ITransactionWithCategory>()
		{

			@Override
			public String getValue(ITransactionWithCategory object) {
				if (object.getCategory() != null)
					return object.getCategory().getName();

				return "";
			}
		};
		grid.addColumn(catCol, "Categoria");

		TextColumn<ITransactionWithCategory> commentsCol = new TextColumn<ITransactionWithCategory>()
		{

			@Override
			public String getValue(ITransactionWithCategory object) {
				return object.getObservations();
			}
		};
		grid.addColumn(commentsCol, "Comentário");

		Column<ITransactionWithCategory, Date> recordedOnCol = new Column<ITransactionWithCategory, Date>(
				new DateCell(Formatters.TIME))
		{

			@Override
			public Date getValue(ITransactionWithCategory object) {
				return object.getRecordedOn();
			}
		};
		grid.addColumn(recordedOnCol, "Adicionado");
	}

	public void set(Collection<? extends ITransactionWithCategory> expenses) {
		grid.setRowCount(expenses.size(), true);
		grid.setRowData(new ArrayList<ITransactionWithCategory>(expenses));
	}

	public CellTable<ITransactionWithCategory> getTable() {
		return grid;
	}

}
