package mfs.robot.client.erepublik.components.tables;

import static mfs.robot.client.erepublik.Utils.addStyles;
import static mfs.robot.client.erepublik.Utils.getLoadingImage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import mfs.robot.client.erepublik.Formatters;
import mfs.robot.shared.erepublik.ITransaction;
import mfs.robot.shared.erepublik.data.MonetaryType;

import com.google.gwt.cell.client.DateCell;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;

public class TransactionTable {

	protected final CellTable<ITransaction>	grid	= new CellTable<ITransaction>();

	public TransactionTable() {
		build();
		grid.setLoadingIndicator(getLoadingImage());
		addStyles(grid, "expense-grid", "auto-0margin");
	}

	protected void build() {
		Column<ITransaction, Number> dayCol = new Column<ITransaction, Number>(
				new NumberCell(Formatters.DAY))
		{

			@Override
			public Integer getValue(ITransaction object) {
				return object.geteRepDay();
			}
		};
		grid.addColumn(dayCol, "Dia");
		dayCol.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);

		Column<ITransaction, Number> goldValueCol = new Column<ITransaction, Number>(
				new NumberCell(Formatters.CURRENCY))
		{

			@Override
			public Double getValue(ITransaction object) {
				if (object.getType() != null
						&& object.getType() == MonetaryType.GOLD
						&& object.getValue() != 0)
					return object.getValue();
				return null;
			}
		};
		grid.addColumn(goldValueCol, "Valor (G)");
		goldValueCol.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

		Column<ITransaction, Number> ccValueCol = new Column<ITransaction, Number>(
				new NumberCell(Formatters.CURRENCY))
		{

			@Override
			public Double getValue(ITransaction object) {
				if (object.getType() != null
						&& object.getType() == MonetaryType.CURRENCY
						&& object.getValue() != 0)
					return object.getValue();
				return null;
			}
		};
		grid.addColumn(ccValueCol, "Valor (BRL)");
		ccValueCol.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

		TextColumn<ITransaction> responsibleCol = new TextColumn<ITransaction>()
		{

			@Override
			public String getValue(ITransaction object) {
				return object.getPersonResponsible();
			}
		};
		grid.addColumn(responsibleCol, "Responsável");

		TextColumn<ITransaction> originCol = new TextColumn<ITransaction>()
		{

			@Override
			public String getValue(ITransaction object) {
				return object.getOrigin();
			}
		};
		grid.addColumn(originCol, "Origem");

		TextColumn<ITransaction> destinationCol = new TextColumn<ITransaction>()
		{

			@Override
			public String getValue(ITransaction object) {
				return object.getDestination();
			}
		};
		grid.addColumn(destinationCol, "Destino");

		TextColumn<ITransaction> commentsCol = new TextColumn<ITransaction>()
		{

			@Override
			public String getValue(ITransaction object) {
				return object.getObservations();
			}
		};
		grid.addColumn(commentsCol, "Comentário");

		Column<ITransaction, Date> recordedOnCol = new Column<ITransaction, Date>(
				new DateCell(Formatters.TIME))
		{

			@Override
			public Date getValue(ITransaction object) {
				return object.getRecordedOn();
			}
		};
		grid.addColumn(recordedOnCol, "Adicionado");
	}

	public void set(Collection<? extends ITransaction> expenses) {
		grid.setRowCount(expenses.size(), true);
		grid.setRowData(new ArrayList<ITransaction>(expenses));
	}

	public CellTable<ITransaction> getTable() {
		return grid;
	}

}
