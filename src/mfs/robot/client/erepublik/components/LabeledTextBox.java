package mfs.robot.client.erepublik.components;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.client.ui.TextBox;

/**
 * Simple TextBox with Label Inside - onFocus removes the Label and replaces it
 * onBlur
 **/
public class LabeledTextBox extends TextBox {

	private String	lb;
	private boolean	isClicked	= false;

	@UiConstructor
	public LabeledTextBox(String label) {
		lb = label;

		superSetText(label);
		setStyleClear();

		addFocusHandler(new FocusHandler()
		{
			@Override
			public void onFocus(FocusEvent event) {
				if (!isClicked) {
					isClicked = true;
					superSetText("");
					setStyleFilled();
				}
			}
		});

		addBlurHandler(new BlurHandler()
		{

			@Override
			public void onBlur(BlurEvent event) {
				if (isClicked)
					if (((TextBox) event.getSource()).getText().trim().length() < 1) {
						isClicked = false;
						superSetText(lb);
						setStyleClear();
					}
			}
		});
	}

	protected void setStyleClear() {
		addStyleName("light-text");
	}

	protected void setStyleFilled() {
		removeStyleName("light-text");
	}

	protected void superSetText(String text) {
		super.setText(text);
	}

	@Override
	public String getText() {
		return super.getText().equals(lb) ? "" : super.getText();
	}

	@Override
	public void setText(String text) {
		if (text == null || "".equals(text)) {
			isClicked = false;
			setStyleClear();
			superSetText(lb);
		} else {
			isClicked = true;
			setStyleFilled();
			superSetText(text);
		}
	}

	public boolean validate(int minSize) {
		boolean ok = true;
		if (getText().length() < minSize) {
			addStyleName("highlight");
			setFocus(true);
			ok = false;
		} else
			removeStyleName("highlight");

		return ok;
	}
}
